FROM node:lts-alpine AS dependencies

# Install common dependencies
RUN apk update \
    && apk add --no-cache --upgrade \
        make

FROM dependencies AS build

ARG BUILD_DIR="/opt/scenic"
ENV NODE_ENV="development"

WORKDIR ${BUILD_DIR}
COPY . ${BUILD_DIR}

RUN apk update \
    && apk add --no-cache --upgrade \
        git

# Build scenic build
RUN make build

FROM alpine:3.19

ARG USERNAME="h2o"
ARG GROUPNAME="www-data"
ARG BUILD_DIR="/opt/scenic"
ARG WWW_DIR="/var/www/scenic"

ENV NODE_ENV="${NODE_ENV:-production}" \
    LOG_DIR="/var/log/scenic" \
    CONFIG_DIR="/etc/h2o/h2o.conf"

COPY ./config/h2o.yml "${CONFIG_DIR}"
COPY --from=build "${BUILD_DIR}/dist" "${WWW_DIR}"

RUN apk update \
    && apk add --no-cache --upgrade \
        perl \
        h2o \
    && rm -rf /var/cache/apk/* \
    && mkdir -p "${LOG_DIR}" \
    && chown -R ${USERNAME}:${GROUPNAME} ${WWW_DIR} \
    && chown -R ${USERNAME}:${GROUPNAME} ${LOG_DIR} \
    && touch /run/scenic.pid \
    && chown -R ${USERNAME}:${GROUPNAME} /run/scenic.pid

CMD h2o -m master -c "${CONFIG_DIR}"
