# Contributing

Welcome! We'd love for you to contribute to Scenic's code and make it an even more incredible tool!

Scenic is currently being maintained by a full-time small team of developers working for the [Société des Arts Technologiques (SAT)](https://sat.qc.ca/), a non-profit based in Montréal, Canada for which this software was originally developed. This core team adds features and bug fixes according to the SAT's interests and needs; however, community contributions on this project are more than welcome! There are many ways to contribute, including submitting bug reports, improving documentation, adding unit tests, adding support for a new language, submitting feature requests, reviewing new submissions, or contributing code that can be incorporated into the project. We appreciate all contributions from the community!

This document describes our development process. Following these guidelines shows that you respect the time and effort of the developers managing this project. In return, you will be shown respect in addressing your issue, reviewing your changes, and incorporating your contributions.

## Code of Conduct

By participating in this project, you agree to abide by our [Code of Conduct](CODE_OF_CONDUCT.md). We expect all contributors to follow the [Code of Conduct](CODE_OF_CONDUCT.md) and to treat fellow humans with respect.

## Important Resources

* [README](https://gitlab.com/sat-mtl/tools/scenic/scenic)
* [User Documentation](https://sat-mtl.gitlab.io/tools/scenic/scenic/)
* [Issue tracker](https://gitlab.com/sat-mtl/tools/scenic/scenic/issues)

## Feature Requests

Please create a new Gitlab issue for any major changes and enhancements that you wish to make. Please provide the feature you would like to see, why you need it, and how it will work. Discuss your ideas transparently and get developer feedback before proceeding.

Major changes that you wish to contribute to the project should be discussed first in a Gitlab issue that clearly outlines the changes and benefits of the feature.

Small changes can directly be crafted and submitted to the repository as a Merge Request. See the section about the [Merge Request Process](#merge-request-process).

## Reporting Bugs

**If you find a security vulnerability, do NOT open an issue. Email scenic-dev@sat.qc.ca instead.**

Before you submit your issue, please [search the issue tracker](https://gitlab.com/sat-mtl/tools/scenic/scenic/issues) - maybe your question or issue has already been identified or addressed.

If you find a bug in the source code, you can help us by [submitting an issue to our GitLab issue tracker](https://gitlab.com/sat-mtl/tools/scenic/scenic/issues). Even better, you can submit a Merge Request with a fix.

Of course, please include as many information as possible in your issue: branch name and version, OS version, expected and observed behavior, system information (such as graphic drivers version), log files, etc. Most importantly, provide a step-by-step procedure on how to reproduce your bug. Check out our page on [issue reporting](docs/ISSUE_REPORTING.md) to get pointers on how to get logs and backtraces from Scenic.

## Adding tests

You are welcome to add unit tests if you spot some code that isn't covered by the existing tests.

We do not expect you to add unit tests when doing only minor changes to the codebase. We expect, however, the tests to pass when you're finished with your work.

When adding a new feature or doing major changes to the code, we expect you to add the relevant unit tests to the code.

In any cases, please run the tests on your computer first and ensure that they pass before pushing them to the repository. The CI pipeline will spot any broken tests, but we prefer that you make the necessary verifications beforehand rather than making the CI fail pointlessly.

### Running Tests

You can run tests with the following command:

```bash
  npm test
```
### Debugging Tests

Sometimes its hard to express the values expected for a test result without access to a debugger. Log statements are nice to pinpoint an issue but for some more complex tests it can be nice to have a global overview of all variable states. The easiest way to get a debugger working with jest is to install `ndb` and then to use it to run the tests. `ndb` will launch a chromium instance with a debugger attached to the process. You only need to add a `debugger;` statement and you're all set !

To install ndb
```bash
npm install -g ndb
```
To run tests with a debugger :
```bash
ndb npm run test
```

You can use ndb in front of any npm run test command, that way you can debug single tests.

## Improving Documentation

Should you have a suggestion for the documentation, you can open an issue and outline the problem or improvement you have - however, creating the doc fix yourself is much better!

If you want to help improve the docs, it's a good idea to let others know what you're working on to minimize duplication of effort. Create a new issue (or comment on a related existing one) to let others know what you're working on. If you're making a small change (typo, phrasing) don't worry about filing an issue first.

For large fixes, please build and test the documentation before submitting the MR to be sure you haven't accidentally introduced any layout or formatting issues.

### Building Documentation

In order to rebuild and serve the documentation, run the following command:

```bash
npm run doc:build # Build the user documentation
npm run doc:serve # Serve the user documentation
```

## Improving Localization

Scenic is currently available in both French and English, with English being the default language. Should you spot a typo or a mistake in a translated string, you can directly fix it in the code and submit a merge request without having to open an issue. If you wish to add support for another language, open a new issue and we will be more than happy to discuss it with you!

### Updating Localization Files

Localization is handled by [i18next](https://www.i18next.com/). Localization files are stored in `assets/locales/<lang>/locale.json`. Simply add your localized string at the end of the appropriate file.

Keep in mind that Scenic handles the localization of messages coming from the entire Scenic stack (including [Switcher](https://gitlab.com/sat-mtl/tools/switcher) and [Scenic Core](https://gitlab.com/sat-mtl/tools/scenic/scenic-core)).

## Contributing Code

### Finding an Issue

The list of outstanding feature requests and bugs can be found on our [GitLab issue tracker](https://gitlab.com/sat-mtl/tools/scenic/scenic/issues). Pick an unassigned issue that you think you can accomplish and add a comment that you are attempting to do it.

### Development Process

This project follows the [git flow](http://nvie.com/posts/a-successful-git-branching-model/) branching model of product development.

The **master** branch contains the latest stable production release, and is updated by the core developer team. Each commit in the **master** branch is in fact a production release and is tagged as such. The **develop** branch contains the latest development version of the project.

When contributing to the project, you should create a new *feature branch* based off the **develop** branch.

Always give a short but meaningful name to this new branch, and follow these conventions:

* A branch that aims to fix something (bug, documentation, tests, etc.) should be named *fix/\<branchname\>*
* A branch that aims to add something new (feature, tests, localization, etc.) should be named *feat/\<branchname\>*
* The branch cannot be named *master*, *develop*, *release/\** or *hotfix/\**

When your feature branch is ready, submit a Merge Request to **develop**. When the MR is approved and your changes merged, you can safely delete your feature branch.

Core developers will prepare new releases regularly by creating a *release branch* from **develop** (named *release/<new_version>*). This new branch allows developers to test the release's stability and prepare the relevant metadata. Eventually, when this new release has been tested and deemed stable enough, it will be merged in the **master** branch by the core team. This new commit on **master** will be tagged for future reference and the original release branch will then be deleted.

In the event that a severe bug is detected on **master**, a special *hotfix branch* will be created (named *hotfix/<new_version>*). This hotfix branch can only be created and merged by core developers from the **master** branch.

### Code Formatting and Style Guidelines

**Javascript** files must respect the [Javascript Standard Style](https://standardjs.com/).

**CSS** must be linted using [stylelint](https://github.com/stylelint/stylelint).

**Python** files must respect the [PEP8 Style Guide](https://www.python.org/dev/peps/pep-0008/) (with a maximum line length set to 120).

**C++** files must respect the [Google C++ Style Guide](https://google.github.io/styleguide/cppguide.html), with 2 exceptions:

* a function call’s arguments will either be all on the same line or will have one line each.
* a function declaration’s or function definition’s parameters will either all be on the same line or will have one line each.

You *may* find non-compliant code in the project, but newly introduced code must follow these guidelines.

The *.editorconfig* file present in the [dev-tools](https://gitlab.com/sat-mtl/valorisation/dev-tools) repository automatically trims trailing whitespaces. However, if you ever need to, please submit whitespace cleanups in a separate merge request.

### Git Commit Guidelines

Your commit message must follow these guidelines:

* The message must start with the appropriate [Gitmoji](https://gitmoji.carloscuesta.me/), followed by a whitespace.
* It must be written using the imperative tense.
* It must be strictly no greater than **50** characters long.
* The subject must stand on its own and not make external references, such as bug numbers.
* For example: `🌐 Update localization strings`

For the sake of uniformity, please write all of your commit messages in English.

For further notes about git commit messages, [please read this blog post](http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html).

You can use the `gitmoji-cli` tool from the `devDependencies` of the project by using `npx`:

```bash
npx gitmoji --cli
```

### CI pipeline

This repository includes a CI pipeline, used to validate that incoming commits do not break the build, and that all unit tests still pass. It will be run automatically when a new commit is pushed to the repository. If the pipeline fails, it is your responsability to checkout the [CI Jobs page](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/jobs) and figure out how to fix it. A Merge Requests that breaks the pipeline will not be merged by the core developers.

## Merge Request Process

When you are ready to submit you changes for review, either for preliminary review or for consideration of merging into the project, you can create a Merge Request to add your changes into **develop**. Only core developers are allowed to merge your branch into **develop**.

Do not forget to:

1. Update the relevant [README.md](https://gitlab.com/sat-mtl/tools/scenic/scenic) sections, if necessary.
2. Add the relevant unit tests, if needed.
3. Rebase your changes in nice, clear commits if your branch's commit history is messy.

A core developer will merge your changes into **develop** when all feedbacks have been addressed.

### Review Process

The core developer team regularly checks the repository for new merge requests. We expect the CI pipeline to succeed when you submit a MR. If it doesn't, your MR will not be approved.

If the MR concerns a minor issue, it will be merged immediately after one of the core developers approves it, if the CI succeeds. For more major issues, we will wait a day or two after all feedback has been addressed before merging the request.

### Addressing Feedback

Once a MR has been submitted, your changes will be reviewed and constructive feedback may be provided. Feedback isn't meant as an attack, but to help make sure the highest-quality code makes it into our project. Changes will be approved once required feedback has been addressed.

## License

By contributing to this repository, you agree that your contributions will be licensed in accordance to the [LICENSE](LICENSE.md) document at the root of this repository.
