| Key        	    	| Description                      |
| ----------------- | ---------------------------------|
| (1-4)             | Switch between tabs              |
| I                 | Information on selected resource |
| S                 | Selected resource settings       |
| C                 | SIP connection/contacts          |
| F                 | Make monitor fullscreen          |
| A                 | Add a resource                   |
| Ctrl+S            | Save session                     |
| Ctrl+Shift+S      | Save as...                       |
| Ctrl+O            | Open a session                   |
| Ctrl+Alt+N        | Create a new session             |
