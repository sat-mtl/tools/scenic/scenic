| Touche          	| Description                              |
| ----------------- | ---------------------------------------- |
| (1-4)             | Sélection d'onglet                       |
| I                 | Information sur la ressource sélectionnée|
| S                 | Paramètres de la ressource sélectionnée  |
| C                 | Contacts SIP                             |
| F                 | Déployer la fenêtre en plein écran       |
| A                 | Ajouter une ressource                    |
| Ctrl+S            | Enregistrer la session                   |
| Ctrl+Shift+S      | Enregistrer sous...                      |
| Ctrl+O            | Ouvir une session                        |
| Ctrl+Alt+N        | Créer une nouvelle session               |
