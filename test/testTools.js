import io from 'socket.io-client'

export function createSocket (url, port) {
  const sio = io(`${url}:${port}/switcherio`, {
    'reconnection delay': 0,
    'reopen delay': 0,
    'force new connection': true,
    autoConnect: false,
    transports: ['websocket']
  })

  sio.on(
    'connect',
    () => console.log(`Socket is connected with ${url}:${port}`)
  )

  sio.on(
    'disconnect',
    (reason) => console.log({
      msg: `Socket is disconnected from ${url}:${port} with reason ${reason}`
    })
  )

  return sio
}

export function delay (ms) {
  return new Promise((resolve, reject) => setTimeout(resolve, ms))
}
