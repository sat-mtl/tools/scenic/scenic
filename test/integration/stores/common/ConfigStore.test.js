/* global describe it expect beforeAll beforeEach afterAll */

import { createSocket } from '@test/testTools'

import defaultScenic from '@assets/json/scenic.default.json'
import defaultMenus from '@assets/json/menus.default.json'

import populateStores from '~/src/stores/populateStores'

describe('ConfigStore', () => {
  let socketStore, kindStore, configStore
  const socket = createSocket('ws://localhost', 8000)

  beforeAll((done) => {
    ({
      socketStore,
      kindStore,
      configStore
    } = populateStores())

    socket.on('connect', () => {
      socketStore.setActiveSocket(socket)
      done()
    })

    socket.open()
  })

  beforeEach(async () => {
    await socketStore.APIs.sessionAPI.clear()
    await configStore.initialize()
    await kindStore.initialize()
  })

  afterAll(() => {
    socket.close()
  })

  // this test expects default configuration files are loaded in .config folder
  // it may fail locally, please use the swio.Dockerfile image
  it('default configuration should be applied', (done) => {
    expect(configStore.userScenic).toEqual(defaultScenic)
    expect(configStore.userMenus).toEqual(defaultMenus)
    done()
  })

  it('default bundles are loaded anytime', (done) => {
    expect(Array.from(kindStore.kinds.keys())).toEqual(
      expect.arrayContaining(['videoTestInput'])
    )

    done()
  })
})
