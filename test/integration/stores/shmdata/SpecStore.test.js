/* global describe it expect beforeAll beforeEach afterAll */

import { createSocket, delay } from '@test/testTools'

import populateStores from '~/src/stores/populateStores'

describe('SpecStore', () => {
  let socketStore, quiddityStore, configStore, kindStore, specStore
  let stores
  const socket = createSocket('ws://localhost', 8000)

  beforeAll((done) => {
    stores = populateStores()

    ;({
      socketStore,
      kindStore,
      quiddityStore,
      configStore,
      specStore
    } = stores)

    socket.on('connect', async () => {
      socketStore.setActiveSocket(socket)
      await configStore.initialize()
      await delay(2000)
      await kindStore.initialize()
      done()
    })

    socket.open()
  })

  beforeEach(async () => {
    await socketStore.APIs.sessionAPI.clear()
  })

  afterAll(() => {
    socket.close()
  })

  describe('connectionSpecs', () => {
    it('should collect writer specs when a source is created', async () => {
      const quid = await quiddityStore.applyQuiddityCreation('videotestsrc', 'src')
      await delay(2000)
      expect(specStore.writerSpecs.get(quid.id).size).toEqual(1)
    })

    it('should collect follower specs when a destination is created', async () => {
      const quid = await quiddityStore.applyQuiddityCreation('oscOutput', 'src')
      await delay(2000)
      expect(specStore.followerSpecs.get(quid.id).size).toEqual(1)
    })
  })

  describe('compatibleMediaTypes', () => {
    it('should collect all compatible mediaTypes of a follower', async () => {
      const quid = await quiddityStore.applyQuiddityCreation('oscOutput', 'src')
      await delay(2000)
      expect(specStore.compatibleMediaTypes.get(quid.id).size).toEqual(1)
    })
  })
})
