/* global describe it expect beforeAll afterAll beforeEach */
import { createSocket } from '@test/testTools'

import QuiddityAPI from '@api/quiddity/QuiddityAPI'
import SessionAPI from '@api/switcher/SessionAPI'

import ConnectionSpecsAPI from '@api/tree/ConnectionSpecsAPI'
import OSCsincSpec from '@fixture/api/tree/spec.OSCsink.json'

const OSCsinkKind = 'OSCsink'

describe('ConnectionSpecsAPI', () => {
  const socket = createSocket('ws://localhost', 8000)

  let connectionSpecsAPI, sessionAPI, quiddityAPI
  let OSCsink

  beforeAll((done) => {
    socket.on('connect', () => {
      sessionAPI = new SessionAPI(socket)
      quiddityAPI = new QuiddityAPI(socket)
      connectionSpecsAPI = new ConnectionSpecsAPI(socket)
      done()
    })

    socket.open()
  })

  beforeEach(async () => {
    await sessionAPI.reset()
    OSCsink = await quiddityAPI.create(OSCsinkKind, 'OSCtest')
  })

  afterAll(() => socket.close())

  describe('get', () => {
    it('should get the connection specs of a created quiddity', async () => {
      await expect(connectionSpecsAPI.get(OSCsink.id))
        .resolves.toEqual(OSCsincSpec)
    })
  })
})
