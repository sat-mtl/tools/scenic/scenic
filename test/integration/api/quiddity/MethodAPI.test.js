/* global describe it expect beforeAll afterAll */

import { createSocket } from '@test/testTools'

import QuiddityAPI from '@api/quiddity/QuiddityAPI'
import SessionAPI from '@api/switcher/SessionAPI'
import MethodAPI from '@api/quiddity/MethodAPI'
import InfoTreeAPI from '@api/tree/InfoTreeAPI'

const {
  TEST_SIP_URI,
  TEST_SIP_USER,
  TEST_SIP_PASSWORD
} = process.env

describe('MethodAPI', () => {
  const socket = createSocket('ws://localhost', 8000)

  let quiddityAPI, sessionAPI, methodAPI, infoTreeAPI
  let sipQuid = null

  beforeAll((done) => {
    socket.on('connect', () => {
      quiddityAPI = new QuiddityAPI(socket)
      sessionAPI = new SessionAPI(socket)
      methodAPI = new MethodAPI(socket)
      infoTreeAPI = new InfoTreeAPI(socket)
      done()
    })

    socket.open()
  })

  afterAll(async () => {
    await sessionAPI.clear()
    socket.close()
  })

  describe('sip', () => {
    const buddyName = 'test'
    const newBuddyName = 'test1'

    beforeAll(async () => {
      if (!sipQuid) {
        sipQuid = await quiddityAPI.create('sip', 'sip-test')
      }
    })

    afterAll(async () => {
      if (sipQuid) {
        await quiddityAPI.delete(sipQuid.id)
        sipQuid = null
      }
    })

    describe('register', () => {
      it('should register to a sip server', async () => {
        await expect(
          methodAPI.register(
            sipQuid.id,
            TEST_SIP_URI,
            TEST_SIP_USER,
            TEST_SIP_PASSWORD
          ))
          .resolves
          .toEqual(true)
      })
    })

    describe('addBuddy', () => {
      it('should add a contact to the sip quiddity', async () => {
        await expect(infoTreeAPI.get(sipQuid.id, '.buddies'))
          .resolves
          .toEqual({})

        await expect(
          methodAPI.addBuddy(
            sipQuid.id,
            TEST_SIP_URI,
            buddyName
          ))
          .resolves
          .toEqual(true)

        await expect(infoTreeAPI.get(sipQuid.id, '.buddies'))
          .resolves
          .toHaveLength(1)
      })
    })

    describe('nameBuddy', () => {
      /** @todo I am not sure this is working, we should check a way to fetch all buddies */
      it('should rename an existing contact', async () => {
        await expect(
          methodAPI.nameBuddy(
            sipQuid.id,
            TEST_SIP_URI,
            buddyName,
            newBuddyName
          ))
          .resolves
          .toEqual(true)
      })
    })

    describe('authorize', () => {
      it('should change the authorization of a contact', async () => {
        await expect(
          methodAPI.authorize(
            sipQuid.id,
            TEST_SIP_URI,
            buddyName,
            true
          ))
          .resolves
          .toEqual(true)
      })
    })

    describe('attachShmdataToContact', () => {
      /** @todo This always returns true */
      it('should attach a shmdata to a contact when the last argument is true', async () => {
        await expect(
          methodAPI.attachShmdataToContact(
            sipQuid.id,
            TEST_SIP_URI,
            buddyName
          ))
          .resolves
          .toEqual(true)
      })
    })

    describe('detachShmdataFromContact', () => {
      /** @todo This always returns true */
      it('should detach a shmdata from a contact when the last argument is false', async () => {
        await expect(
          methodAPI.attachShmdataToContact(
            sipQuid.id,
            TEST_SIP_URI,
            buddyName
          ))
          .resolves
          .toEqual(true)
      })
    })

    describe('delBuddy', () => {
      it('should delete a contact from the sip quiddity', async () => {
        await expect(infoTreeAPI.get(sipQuid.id, '.buddies'))
          .resolves
          .toHaveLength(1)

        await expect(
          methodAPI.delBuddy(
            sipQuid.id,
            TEST_SIP_URI,
            buddyName
          ))
          .resolves
          .toEqual(true)

        await expect(infoTreeAPI.get(sipQuid.id, '.buddies'))
          .resolves
          .toHaveLength(0)
      })
    })

    describe('setStunTurn', () => {
      it('should set the stun and turn params', async () => {
        await expect(
          methodAPI.setStunTurn(
            sipQuid.id
          ))
          .resolves
          .toEqual(true)
      })
    })

    describe('unregister', () => {
      /** @todo this test fails because of the unexpected answer of the unregister method */
      it.failing('should unregister from a sip server', async () => {
        await expect(
          methodAPI.unregister(sipQuid.id))
          .resolves
          .toEqual(true)
      })
    })
  })
})
