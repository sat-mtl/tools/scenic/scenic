/* global describe it expect beforeAll beforeEach afterAll */

import { createSocket } from '@test/testTools'

import SwitcherAPI from '@api/switcher/SwitcherAPI'
import SessionAPI from '@api/switcher/SessionAPI'
import QuiddityAPI from '@api/quiddity/QuiddityAPI'

import defaultBundles from '@fixture/api/config/bundles.default.json'
import switcherConfig from '@fixture/api/config/switcher.default.json'
import scenicMenus from '@fixture/api/config/menus.default.json'

import retry from 'jest-retries'

describe('SwitcherAPI', () => {
  const socket = createSocket('ws://localhost', 8000)

  let switcherAPI, sessionAPI

  beforeAll((done) => {
    socket.on('connect', () => {
      switcherAPI = new SwitcherAPI(socket)
      sessionAPI = new SessionAPI(socket)
      done()
    })

    socket.open()
  })

  afterAll(() => socket.close())

  describe('getVersion', () => {
    it('should fetch the current version of switcher', async () => {
      await expect(switcherAPI.getVersion())
        .resolves
        .toEqual(expect.stringMatching(/^3\.\d\d?\.\d\d?$/))
    })
  })

  describe('sendBundles', () => {
    const videoTestInputKind = 'videoTestInput'

    beforeEach((done) => {
      sessionAPI.reset().then(() => done())
    })

    retry('should apply configured bundles for scenic', 3, async () => {
      const quiddityAPI = new QuiddityAPI(socket)
      await expect(switcherAPI.sendBundles(defaultBundles)).resolves.toBe(true)
      await expect(quiddityAPI.create(videoTestInputKind, 'videoTest')).resolves.toBeDefined()
    })
  })

  describe('sendLog', () => {
    it('should send and print Scenic log', async () => {
      await expect(switcherAPI.sendLog('info', 'test')).resolves.toBe(true)
    })
  })

  describe('getConfigPaths', () => {
    it('should get all extra config paths', async () => {
      const paths = await switcherAPI.getConfigPaths()
      expect(paths).toEqual(switcherConfig.extraConfig)
    })
  })

  describe('readConfig', () => {
    it('should read the extra config', async () => {
      const config = await switcherAPI.readConfig('menus.json')
      expect(config).not.toEqual(undefined)
      expect(JSON.parse(config)).toEqual(scenicMenus)
    })
  })
})
