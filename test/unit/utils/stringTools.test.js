/* global beforeEach jest describe it expect */

import { escapeToSafeCss, escapeAsArray, isStringEmpty, replaceAll, parseDate, capitalize } from '@utils/stringTools'

describe('stringTools', () => {
  describe('escapeAsArray', () => {
    it('should split a multiline string and return an array', () => {
      const MULTILINE_STRING = 'Line1  \nLine2\r  Line3\\n'
      const MULTILINE_ARRAY = ['Line1', 'Line2', 'Line3']
      expect(escapeAsArray(MULTILINE_STRING)).toEqual(MULTILINE_ARRAY)
    })
  })

  describe('isStringEmpty', () => {
    it('should return true if the string is empty', () => {
      expect(isStringEmpty(null)).toEqual(true)
      expect(isStringEmpty(undefined)).toEqual(true)
      expect(isStringEmpty('')).toEqual(true)
      expect(isStringEmpty('    ')).toEqual(true)
    })

    it('should return false if the string is not empty', () => {
      expect(isStringEmpty('test')).toEqual(false)
      expect(isStringEmpty('   test   ')).toEqual(false)
    })
  })

  describe('escapeToSafeCss', () => {
    it('should remove all special character and should ensure that strings dont start with hyphens or numbers', () => {
      expect(escapeToSafeCss(
        '---124-5--6565--432.@A@@ll@@l£¢¬³²¦¤£o@¢¤-²¢@¤¬   t¢o  u¶lm§§§ond'
      )).toEqual('Alllo-toulmond') // https://www.youtube.com/watch?v=8Z2JLOZ1q-s
    })
  })

  describe('replaceAll', () => {
    it('should replace all dots by underscores by default', () => {
      expect(replaceAll('.....')).toEqual('_____')
    })

    it('should replace all slashes by dots', () => {
      expect(replaceAll('/////', '/', '.')).toEqual('.....')
    })
  })

  describe('parseDate', () => {
    const NOW_VALUE = Date.now()
    const TEST_VALUE = '1984/10/23'

    beforeEach(() => {
      jest.spyOn(global.Date, 'now').mockReturnValue(NOW_VALUE)
    })

    it('should set the date at now when the input is NaN', () => {
      expect(parseDate('test')).toEqual(new Date(NOW_VALUE))
    })

    it('should parse the date from the served date', () => {
      expect(parseDate(TEST_VALUE)).toEqual(new Date(TEST_VALUE))
    })
  })

  describe('capitalize', () => {
    it('should capitalize the first letter of the input string', () => {
      expect(capitalize('test')).toEqual('Test')
      expect(capitalize('multiple words in a sentence')).toEqual('Multiple words in a sentence')
    })
  })
})
