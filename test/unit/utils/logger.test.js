/* global describe it expect beforeEach jest */

import populateStores from '@stores/populateStores.js'
import { Socket } from '@fixture/socket'
import loggerFunctions from '@utils/logger'

describe('logger', () => {
  let socketStore, socket

  beforeEach(() => {
    ({ socketStore } = populateStores())

    socket = new Socket()
    socketStore.setActiveSocket(socket)
  })

  describe('getLogStore', () => {
    it('should get the bound store from a log', () => {
      expect(loggerFunctions.getLogStore([{ store: 'Test' }])).toEqual('Test')
    })

    it('should return undefined if the binding is undefined', () => {
      expect(loggerFunctions.getLogStore(undefined)).toEqual(undefined)
    })

    it('should return undefined if the binding has no store entry', () => {
      expect(loggerFunctions.getLogStore([{}])).toEqual(undefined)
    })
  })

  describe('applySocketLog', () => {
    let socket
    beforeEach(() => {
      // ({ switcherAPI } = socketStore.APIs)
      socket = new Socket()
      socketStore.setActiveSocket(socket)
    })
    it('should clean the log event if a socket exists and is active', () => {
      const sendLogRequest = jest.spyOn(socketStore.APIs.switcherAPI, 'sendLog')
      loggerFunctions.cleanLogEvent = jest.fn(loggerFunctions.cleanLogEvent)
      loggerFunctions.applySocketLog({ test: 'test', messages: [{ msg: 'test' }] })
      expect(loggerFunctions.cleanLogEvent).toHaveBeenCalled()
      expect(sendLogRequest).toHaveBeenCalledWith('info', 'test')
    })
  })
})
