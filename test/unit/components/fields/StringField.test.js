/* global jest describe beforeEach it expect */

import React, { useState as useStateMock } from 'react'
import { shallow } from 'enzyme'

import Property from '@models/quiddity/Property'
import StringField from '@components/fields/StringField'

import { Inputs } from '@sat-mtl/ui-components'
import videoOutput from '@fixture/models/quiddity.videoOutput.json'

const { Field, InputText } = Inputs

jest.mock('react', () => ({
  ...jest.requireActual('react'),
  useState: jest.fn()
}))

describe('<StringField />', () => {
  const jsonProperty = videoOutput.infoTree.property.find(json => json.id === 'Window/overlay_text')
  const property = Property.fromJSON(jsonProperty)

  const undescribedProperty = Property.fromJSON({
    ...jsonProperty,
    description: undefined
  })

  let onChangeMock, onEditMock

  beforeEach(() => {
    onEditMock = jest.fn()
    onChangeMock = jest.fn()
    useStateMock.mockImplementation(init => [init, onEditMock])
  })

  function shallowField (property) {
    return shallow(
      <StringField
        {...property.toFieldProps()}
        onChange={onChangeMock}
      />
    )
  }

  describe('<InputText />', () => {
    it('should be checked with the field value', () => {
      const $input = shallowField(property).find(InputText)
      expect($input.prop('value')).toEqual(jsonProperty.value)
    })

    it('should handle each user changes on the switch in the component hook', () => {
      const $input = shallowField(property).find(InputText)
      $input.prop('onChange')({ target: { value: 'test' } })
      expect(onEditMock).toHaveBeenCalledWith('test')
    })

    it('should send the edited value on blur', () => {
      useStateMock.mockImplementation(() => ['test', onEditMock])
      const $input = shallowField(property).find(InputText)
      $input.simulate('blur')
      expect(onChangeMock).toHaveBeenCalledWith('test')
    })

    it('should send the edited value when the user press the key "Enter"', () => {
      useStateMock.mockImplementation(() => ['test', onEditMock])
      const $input = shallowField(property).find(InputText)
      $input.prop('onPressEnter')()
      expect(onChangeMock).toHaveBeenCalledWith('test')
    })

    it('shouldn\'t send the edited value on blur if the value wasn\'t changed', () => {
      useStateMock.mockImplementation(() => [jsonProperty.value, onEditMock])
      const $input = shallowField(property).find(InputText)
      $input.simulate('blur')
      expect(onChangeMock).not.toHaveBeenCalled()
    })

    it('shouldn\'t send the edited value when the user press "Enter" if the value wasn\'t changed', () => {
      useStateMock.mockImplementation(() => [jsonProperty.value, onEditMock])
      const $input = shallowField(property).find(InputText)
      $input.prop('onPressEnter')()
      expect(onChangeMock).not.toHaveBeenCalled()
    })
  })

  describe('<Field />', () => {
    it('should render the Field with the given description', () => {
      const $field = shallowField(property).find(Field)
      expect($field.prop('description')).toEqual(property.description)
    })

    it('should render a default description if it is not given', () => {
      const $field = shallowField(undescribedProperty).find(Field)
      expect($field.prop('description')).not.toBeFalsy()
    })
  })
})
