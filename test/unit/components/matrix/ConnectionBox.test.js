/* global jest describe it expect beforeEach */

import React from 'react'
import { mount } from 'enzyme'

import populateStores from '@stores/populateStores.js'
import { AppStoresContext } from '@components/App'

import ConnectionBox, { DisabledBox, ConnectableBox, ArmedBox, LockedBox, ConnectedBox } from '@components/matrix/ConnectionBox'

import SDI_INPUT_SHMDATA from '@fixture/models/shmdatas.sdiInput1.json'
import VIDEO_OUTPUT_SHMDATA from '@fixture/models/shmdatas.videoOutput.json'
import { addShmdataToInfoTree } from '@fixture/allShmdatas'

import Shmdata from '@models/shmdata/Shmdata'
import Connection from '@models/userTree/Connection'
import Scene from '@models/userTree/Scene'
import MatrixEntry from '@models/matrix/MatrixEntry'

import { sdiInput, videoOutput } from '@fixture/allQuiddities'
import { getKind } from '@fixture/allQuiddityKinds'

describe('<ConnectionBox />', () => {
  let sourceEntry, destinationEntry

  let quiddityStore, kindStore
  let shmdataStore
  let capsStore
  let sceneStore
  let connectionStore
  let lockStore
  let matrixStore
  let helpMessageStore
  let encoderStore
  let nicknameStore

  const CONNECTED_SDI_INPUT = addShmdataToInfoTree(sdiInput, SDI_INPUT_SHMDATA)
  const CONNECTED_VIDEO_OUTPUT = addShmdataToInfoTree(videoOutput, VIDEO_OUTPUT_SHMDATA)

  beforeEach(() => {
    ({
      quiddityStore,
      connectionStore,
      shmdataStore,
      capsStore,
      sceneStore,
      lockStore,
      matrixStore,
      helpMessageStore,
      kindStore,
      encoderStore,
      nicknameStore
    } = populateStores())

    sourceEntry = MatrixEntry.fromJSON({
      quiddity: CONNECTED_SDI_INPUT,
      shmdatas: [Shmdata.fromTree(SDI_INPUT_SHMDATA)[0]]
    })

    destinationEntry = MatrixEntry.fromJSON({
      quiddity: CONNECTED_VIDEO_OUTPUT
    })
  })

  function mountConnection (sourceEntry, destinationEntry) {
    return mount(
      <AppStoresContext.Provider value={{ connectionStore, shmdataStore, capsStore, lockStore, matrixStore, helpMessageStore, kindStore, encoderStore, nicknameStore }}>
        <ConnectionBox
          sourceEntry={sourceEntry}
          destinationEntry={destinationEntry}
        />
      </AppStoresContext.Provider>
    )
  }

  function addAllMockedQuiddities () {
    kindStore.addKind(getKind(sdiInput))
    quiddityStore.addQuiddity(sdiInput)

    kindStore.addKind(getKind(videoOutput))
    quiddityStore.addQuiddity(videoOutput)

    matrixStore.populateSourceCategories()
    matrixStore.populateDestinationCategories()
  }

  function addMockedScenes () {
    const connection = Connection.fromMatrixEntries(sourceEntry, destinationEntry)
    sceneStore.addUserScene(new Scene('test', 'test', true, [connection.id]))
    sceneStore.setSelectedScene('test')
  }

  function addMockedConnections () {
    const connection = Connection.fromMatrixEntries(sourceEntry, destinationEntry)
    connectionStore.addUserConnection(connection)
  }

  function addMockedShmdatas () {
    for (const shmdata of sourceEntry.shmdatas) {
      shmdataStore.addShmdata(shmdata)
    }
    shmdataStore.addWritingShmdata(sourceEntry.quiddityId, sourceEntry.shmdatas[0].path)
    shmdataStore.addFollowingShmdata(destinationEntry.quiddityId, sourceEntry.shmdatas[0].path)
  }

  describe('useEffect', () => {
    beforeEach(() => {
      shmdataStore.fallbackFollowingShmdatas = jest.fn()
      capsStore.fallbackCapsCompatibilities = jest.fn()
    })

    it('should fallback reading shmdata when it is mounted', async () => {
      await mountConnection(sourceEntry, destinationEntry)
      expect(shmdataStore.fallbackFollowingShmdatas).toHaveBeenCalledWith(destinationEntry)
    })
  })

  describe('<DisabledBox />', () => {
    it('should render a disabled box if the source and the destination are not compatible', () => {
      encoderStore.areEntriesConnectable = jest.fn().mockReturnValue(false)
      const $box = mountConnection(sourceEntry, destinationEntry)
      expect($box.find(DisabledBox).length).toEqual(1)
    })
  })

  describe('<LockedBox />', () => {
    beforeEach(() => {
      encoderStore.areEntriesConnectable = jest.fn().mockReturnValue(true)
    })

    it('should render a locked box if the destination is locked', () => {
      lockStore.addLock(destinationEntry)
      const $box = mountConnection(sourceEntry, destinationEntry)
      expect($box.find(LockedBox).length).toEqual(1)
    })
  })

  describe('<ConnectedBox />', () => {
    beforeEach(() => {
      encoderStore.areEntriesConnectable = jest.fn().mockReturnValue(true)

      addMockedScenes()
      addAllMockedQuiddities()
      addMockedShmdatas()
    })

    it('should render an active connection if the source shmdata is read', () => {
      const $box = mountConnection(sourceEntry, destinationEntry)
      expect($box.find(ConnectedBox).length).toEqual(1)
    })
  })

  describe('<ArmedBox />', () => {
    beforeEach(() => {
      encoderStore.areEntriesConnectable = jest.fn().mockReturnValue(true)
    })

    it('should render an armed box if the connection is armed', () => {
      addMockedConnections()
      addMockedScenes()

      const $box = mountConnection(sourceEntry, destinationEntry)
      expect($box.find(ArmedBox).length).toEqual(1)
    })
  })

  describe('<ConnectableBox />', () => {
    beforeEach(() => {
      encoderStore.areEntriesConnectable = jest.fn().mockReturnValue(true)
    })

    it('should render a connectable box if all other cases are invalidated', () => {
      const $box = mountConnection(sourceEntry, destinationEntry)
      expect($box.find(ConnectableBox).length).toEqual(1)
    })
  })
})
