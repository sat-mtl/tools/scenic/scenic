/* global describe it expect beforeEach jest */

import React from 'react'
import { mount } from 'enzyme'
import { act } from 'react-dom/test-utils'
import { configure } from 'mobx'

import populateStores from '@stores/populateStores.js'
import { AppStoresContext } from '@components/App'

import Matrix from '@components/matrix/Matrix'
import Source, { SourceSubtitle } from '@components/matrix/Source'

import MenuItem from '@models/menus/MenuItem'
import Shmdata from '@models/shmdata/Shmdata'
import MatrixEntry from '@models/matrix/MatrixEntry'

import { sdiInput, copyQuiddity, sipMedia } from '@fixture/allQuiddities'

configure({ safeDescriptors: false })

const sdiInput1 = copyQuiddity(sdiInput, 'sdiInput1')
const sdiShmdata = new Shmdata(`/tmp/${sdiInput.id}/0`, '', 'test')
const entry = new MatrixEntry(sdiInput, null, [sdiShmdata])
const sipEntry = new MatrixEntry(sipMedia, null, [sdiShmdata])

describe('<Source />', () => {
  let stores

  beforeEach(() => {
    stores = populateStores()
  })

  function mountMatrix () {
    return mount(
      <AppStoresContext.Provider value={stores}>
        <Matrix stores={stores} />
      </AppStoresContext.Provider>
    )
  }

  function getSources ($matrix) {
    return $matrix.find(Source)
  }

  function addQuidditySources () {
    const { quiddityStore, kindStore } = stores

    kindStore.addKind(sdiInput)
    quiddityStore.addQuiddity(sdiInput)

    kindStore.addKind(sdiInput1)
    quiddityStore.addQuiddity(sdiInput1)
  }

  it('should not display sources if no source quiddities is added', () => {
    expect(getSources(mountMatrix()).length).toEqual(0)
  })

  it('should display a source for each added source quiddity', () => {
    const $matrix = mountMatrix()

    act(() => {
      addQuidditySources()
      stores.matrixStore.populateSourceCategories()
    })

    expect(getSources($matrix.update()).length).toEqual(2)
  })

  describe('<SourceSubtitle />', () => {
    let callerLabel

    beforeEach(() => {
      callerLabel = jest.spyOn(stores.contactStore, 'makeCallerLabel')
    })

    const menuItem = new MenuItem('test', sdiInput)
    const menuItemSip = new MenuItem('SIP', sipMedia)

    function renderSubtitle (entry) {
      return mount(
        <AppStoresContext.Provider value={stores}>
          <SourceSubtitle
            entry={entry}
          />
        </AppStoresContext.Provider>
      ).html()
    }

    it('should render the kind of the quiddity by default', () => {
      expect(renderSubtitle(entry)).toEqual(`${sdiInput.kindId} (${sdiShmdata.mediaType})`)
    })

    it('should render the menu label if it exists', () => {
      jest.spyOn(stores.quiddityMenuStore, 'kindLabels', 'get').mockReturnValue(new Map().set(sdiInput.kindId, menuItem.name))
      expect(renderSubtitle(entry)).toEqual(`${menuItem.name} (${sdiShmdata.mediaType})`)
    })

    it('should render the shmdata mediaType if it is defined', () => {
      expect(renderSubtitle(entry)).toEqual(`${sdiInput.kindId} (${sdiShmdata.mediaType})`)
    })

    it('should not make a caller\'s label if there is no defined shmdata', () => {
      renderSubtitle(entry)
      expect(callerLabel).not.toHaveBeenCalled()
    })

    it('should not make a caller\'s label if the kindLabel is not SIP', () => {
      renderSubtitle(entry)
      expect(callerLabel).not.toHaveBeenCalled()
    })

    it('should make a caller\'s label if there is a defined shmdata and if the SIP quiddity has the extshmsrc kind', () => {
      jest.spyOn(stores.quiddityMenuStore, 'kindLabels', 'get').mockReturnValue(new Map().set(sipEntry.kindId, menuItemSip.name))
      renderSubtitle(sipEntry)
      expect(callerLabel).toHaveBeenCalledWith(sipMedia.id)
    })
  })
})
