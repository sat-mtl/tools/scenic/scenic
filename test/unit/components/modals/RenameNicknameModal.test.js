/* global jest describe beforeEach it expect */

import React from 'react'
import { mount } from 'enzyme'

import RenameNicknameModal, { RENAME_NICKNAME_MODAL_ID, ModalFooter } from '@components/modals/RenameNicknameModal'
import { AppStoresContext } from '@components/App'
import populateStores from '@stores/populateStores.js'

import { CancelButton } from '@components/common/Buttons'
import { Common, Feedback } from '@sat-mtl/ui-components'

const { Button } = Common
const { Modal } = Feedback

describe('<RenameNicknameModal />', () => {
  let renameMock, cancelMock
  let modalStore

  beforeEach(() => {
    ({ modalStore } = populateStores())

    modalStore.setActiveModal = jest.fn()
    modalStore.cleanActiveModal = jest.fn()

    renameMock = jest.fn()
    cancelMock = jest.fn()
  })

  function mountRenameNicknameModal () {
    return mount(
      <AppStoresContext.Provider value={{ modalStore }}>
        <RenameNicknameModal
          visible
          onClick={renameMock}
          onCancel={cancelMock}
        />
      </AppStoresContext.Provider>
    )
  }

  function mountModalFooter () {
    return mount(
      <AppStoresContext.Provider value={{ modalStore }}>
        <ModalFooter
          onRename={renameMock}
          onCancel={cancelMock}
        />
      </AppStoresContext.Provider>
    )
  }

  const getCancelButton = $modal => $modal.update().find(CancelButton).find(Button)
  const getRenameButton = $modal => $modal.update().find(Button).at(1)

  describe('<RenameNicknameModal/>', () => {
    it('should register the modal when it is mounting', () => {
      mountRenameNicknameModal()
      expect(modalStore.modals.has(RENAME_NICKNAME_MODAL_ID)).toEqual(true)
    })

    it('should be visible when it is active', () => {
      const $wrapper = mountRenameNicknameModal()

      modalStore.setActiveModal(RENAME_NICKNAME_MODAL_ID)
      expect($wrapper.exists()).toBe(true)
      expect($wrapper.prop('visible')).toEqual(true)
    })

    it('should not be visible when it is not active', () => {
      const $wrapper = mountRenameNicknameModal()
      modalStore.cleanActiveModal(RENAME_NICKNAME_MODAL_ID)
      const $modal = $wrapper.find(Modal)
      expect($modal.prop('visible')).toEqual(false)
    })
  })

  describe('<ModalFooter />', () => {
    it('should render a Cancel button and a Confirm button', () => {
      const $wrapper = mountModalFooter()
      const $cancelButton = $wrapper.find('button').at(0)
      const $renameButton = $wrapper.find(Button).at(1)

      expect($cancelButton.length).toEqual(1)
      expect($renameButton.length).toEqual(1)
    })

    it('should cancel the modal when the cancel button is clicked', () => {
      getCancelButton(mountModalFooter()).simulate('click')
      expect(cancelMock).toHaveBeenCalled()
    })

    it('should trigger the onConfirm function when clicked', () => {
      getRenameButton(mountModalFooter()).simulate('click')
      expect(renameMock).toHaveBeenCalled()
    })
  })
})
