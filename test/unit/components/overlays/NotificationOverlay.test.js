/* global jest describe beforeEach it expect */

import React from 'react'
import { mount } from 'enzyme'
import { act } from 'react-dom/test-utils'
import { CSSTransition } from 'react-transition-group'

import NotificationModel from '@models/common/Notification'
import NotificationOverlay, {
  OverlyingNotification,
  NOTIFICATION_TRANSITION_TIMEOUT
} from '@components/panels/NotificationOverlay'

import { DRAWER_WIDTH } from '@stores/common/DrawerStore'
import { Feedback } from '@sat-mtl/ui-components'
import populateStores from '@stores/populateStores.js'

import { AppStoresContext } from '@components/App'

const { Notification } = Feedback

describe('<NotificationOverlay />', () => {
  let notificationStore, drawerStore

  const NOTIFICATIONS = [
    NotificationModel.fromString('0', 'Notifiaction'),
    NotificationModel.fromString('1', 'Notifiaction'),
    NotificationModel.fromString('2', 'Notifiaction')
  ]

  beforeEach(() => {
    ({ notificationStore, drawerStore } = populateStores())
  })

  function renderNotificationOverlay () {
    return mount(
      <AppStoresContext.Provider value={{ notificationStore, drawerStore }}>
        <NotificationOverlay
          notificationStore={notificationStore}
          drawerStore={drawerStore}
        />
      </AppStoresContext.Provider>
    )
  }

  function renderOverlyingNotification (notification) {
    return mount(
      <AppStoresContext.Provider value={{ notificationStore, drawerStore }}>
        <OverlyingNotification notification={notification} />
      </AppStoresContext.Provider>
    )
  }

  describe('styles', () => {
    const DRAWER_TEST_ID = 'test'

    function getOverlayStyles ($overlay) {
      return $overlay.find('#NotificationOverlay').prop('style')
    }

    it('should be 0 by default', () => {
      const styles = getOverlayStyles(renderNotificationOverlay())
      expect(styles.right).toEqual(0)
    })

    it('should be the drawer width if a drawer is opened', () => {
      drawerStore.addDrawer(DRAWER_TEST_ID)
      drawerStore.setActiveDrawer(DRAWER_TEST_ID)
      const styles = getOverlayStyles(renderNotificationOverlay())
      expect(styles.right).toEqual(DRAWER_WIDTH)
    })
  })

  describe('<CSSTransition />', () => {
    let removeSpy

    function getTransitionProps (isIn) {
      return {
        appear: true,
        in: isIn,
        timeout: NOTIFICATION_TRANSITION_TIMEOUT,
        onExited: expect.any(Function)
      }
    }

    beforeEach(() => {
      notificationStore.addUserNotification(NOTIFICATIONS[0])
      removeSpy = jest.spyOn(notificationStore, 'removeNotification')
    })

    it('should be in if the notification is not expired', () => {
      const $appNotification = renderOverlyingNotification(NOTIFICATIONS[0])
      const $transition = $appNotification.find(CSSTransition)
      expect($transition.props()).toMatchObject(getTransitionProps(true))
    })

    it('should not be in if the notification is expired', () => {
      notificationStore.addExpiredNotification(NOTIFICATIONS[0].id)
      const $appNotification = renderOverlyingNotification(NOTIFICATIONS[0])
      const $transition = $appNotification.find(CSSTransition)
      expect($transition.props()).toMatchObject(getTransitionProps(false))
    })

    it('should remove the notification when the transition is exited', () => {
      const $appNotification = renderOverlyingNotification(NOTIFICATIONS[0])
      const $transition = $appNotification.find(CSSTransition)
      $transition.prop('onExited')()
      expect(removeSpy).toHaveBeenCalled()
    })
  })

  describe('<Notification />', () => {
    let addSpy

    function getNotificationProps () {
      return {
        id: NOTIFICATIONS[0].id,
        status: NOTIFICATIONS[0].status,
        onClick: expect.any(Function)
      }
    }

    beforeEach(() => {
      addSpy = jest.spyOn(notificationStore, 'addExpiredNotification')
    })

    it('should get the notification props', () => {
      const $appNotification = renderOverlyingNotification(NOTIFICATIONS[0])
      const $notification = $appNotification.find(Notification)
      expect($notification.props()).toMatchObject(getNotificationProps())
    })

    it('should expire the notification when the notification is clicked', () => {
      const $appNotification = renderOverlyingNotification(NOTIFICATIONS[0])
      const $notification = $appNotification.find(Notification)
      act(() => $notification.prop('onClick')())
      expect(addSpy).toHaveBeenCalled()
    })
  })

  describe('<OverlyingNotifications />', () => {
    function getNotifications () {
      return renderNotificationOverlay().find(Notification)
    }

    it('should render all notifications', () => {
      NOTIFICATIONS.forEach(n => notificationStore.addUserNotification(n))
      const $notifications = getNotifications()
      expect($notifications.length).toEqual(NOTIFICATIONS.length)
    })
  })
})
