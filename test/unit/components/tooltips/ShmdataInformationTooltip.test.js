/* global describe beforeEach it expect */

import React from 'react'
import { mount } from 'enzyme'
import populateStores from '@stores/populateStores.js'

import { sdiInput } from '@fixture/allQuiddities'

import Shmdata from '@models/shmdata/Shmdata'
import ShmdataInformationTooltip, { Capabilities, Capability, CapabilityGroup } from '@components/tooltips/ShmdataInformationTooltip'
import { AppStoresContext } from '@components/App'

const sdiShmdata = new Shmdata(`/tmp/${sdiInput.id}/0`, ['video/x-raw', 'pixel-aspect-ratio=(fraction)1/1'], 'test')

describe('<ShmdataInformationTooltip />', () => {
  let statStore, capsStore, shmdataStore, configStore

  beforeEach(() => {
    ({ statStore, capsStore, shmdataStore, configStore } = populateStores())
  })

  const WithContext = ({ children }) => (
    <AppStoresContext.Provider value={{ statStore, capsStore, shmdataStore }}>
      {children}
    </AppStoresContext.Provider>
  )

  const renderTooltip = () => {
    return mount(
      <WithContext>
        <ShmdataInformationTooltip shmdata={sdiShmdata} />
      </WithContext>
    )
  }

  const renderCaps = (shmdata) => {
    return mount(
      <WithContext>
        <Capabilities shmdata={shmdata} />
      </WithContext>
    )
  }

  it('should render tooltip', () => {
    shmdataStore.addShmdata(sdiShmdata)
    shmdataStore.addWritingShmdata(sdiInput.id, sdiShmdata.path)
    expect(renderTooltip().find(Capabilities).length > 0).toBe(true)
  })

  describe('<CapabilityGroup />', () => {
    // It also renders the mediaType and the stats
    const numOfCapabilities = sdiShmdata.capabilities.properties.size + 2

    it('should render all capabilities (+ its mediaType) of the shmdata', () => {
      expect(renderCaps(sdiShmdata).find(Capability).length).toEqual(numOfCapabilities) // It also renders the stat
    })

    it('should not render hidden capabilities', () => {
      configStore.setUserScenic({ hiddenCapabilities: ['pixel-aspect-ratio'] })
      expect(renderCaps(sdiShmdata).find(Capability).length).toEqual(numOfCapabilities - 1)
    })
  })

  describe('<Capabilities />', () => {
    it('should render at least the default capability groups', () => {
      const sdiShmdata0 = new Shmdata(`/tmp/${sdiInput.id}/0`, 'video/x-raw', 'test')
      expect(renderCaps(sdiShmdata0).find(CapabilityGroup).length).toEqual(1)
      expect(renderCaps(sdiShmdata).find(CapabilityGroup).length).toEqual(2)
    })
  })
})
