/* global describe beforeEach it expect */

import React from 'react'
import { mount } from 'enzyme'

import populateStores from '@stores/populateStores'
import { AppStoresContext } from '@components/App'
import NicknameLabel from '@components/quiddity/NicknameLabel'

describe('<NicknameLabel />', () => {
  const QUIDDITY_TEST = 'test'
  const NICKNAME_TEST = 'nickname'

  let nicknameStore

  beforeEach(() => {
    ({ nicknameStore } = populateStores())
  })

  const mountNicknameLabel = ({ quiddityId }) => {
    return mount(
      <AppStoresContext.Provider value={{ nicknameStore }}>
        <NicknameLabel
          quiddityId={quiddityId}
        />
      </AppStoresContext.Provider>
    )
  }

  describe('<NicknameLabel />', () => {
    it('should render the default name if no nickname exists', () => {
      const $label = mountNicknameLabel({ quiddityId: QUIDDITY_TEST })
      expect($label.html()).toEqual(QUIDDITY_TEST)
    })

    it('should render the stored nickname', () => {
      nicknameStore.nicknames.set(QUIDDITY_TEST, NICKNAME_TEST)
      const $label = mountNicknameLabel({ quiddityId: QUIDDITY_TEST })
      expect($label.html()).toEqual(NICKNAME_TEST)
    })
  })
})
