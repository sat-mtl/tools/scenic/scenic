/* global describe it expect beforeEach */
import React from 'react'
import { mount } from 'enzyme'

import populateStores from '@stores/populateStores.js'
import FileControlPanel from '@components/panels/FileControlPanel'

import { SendStatusEnum } from '@models/sip/SipEnums'

import { AppStoresContext } from '@components/App'
import { sip, sipMedia } from '@fixture/allQuiddities'

describe('<FileControlPanel />', () => {
  let quiddityStore, modalStore, drawerStore, sessionStore, helpMessageStore, contactStatusStore

  beforeEach(() => {
    ({ quiddityStore, modalStore, drawerStore, sessionStore, helpMessageStore, contactStatusStore } = populateStores())
    quiddityStore.addQuiddity(sip)
  })

  function mountFileControlPanel () {
    return mount(
      <AppStoresContext.Provider value={{ quiddityStore, modalStore, drawerStore, sessionStore, helpMessageStore, contactStatusStore }}>
        <FileControlPanel />
      </AppStoresContext.Provider>
    )
  }

  function getButtonsFromPanel ($panel) {
    // the first element of the Panel is the modal ResetSessionModal,
    // the second has the buttons as its props
    return $panel.children().getElements()[1].props.children
  }

  it('should have should enabled buttons in normal circumstances', () => {
    const $panel = mountFileControlPanel()
    const disabledProperties = getButtonsFromPanel($panel).map(child => child.props.disabled)
    disabledProperties.forEach(disabledProperty => expect(disabledProperty).toBe(false))
  })

  it('should have should have disabled buttons when an extshmsrc is present', () => {
    quiddityStore.addQuiddity(sipMedia)
    const $panel = mountFileControlPanel()
    const disabledProperties = getButtonsFromPanel($panel).map(child => child.props.disabled)
    disabledProperties.forEach(disabledProperty => expect(disabledProperty).toBe(true))
  })

  it('should have should have disabled buttons when a contact is being called', () => {
    /* mocks having a called contact */
    contactStatusStore.setSendingStatus('a buddy', SendStatusEnum.CALLING)
    const $panel = mountFileControlPanel()
    const disabledProperties = getButtonsFromPanel($panel).map(child => child.props.disabled)
    disabledProperties.forEach(disabledProperty => expect(disabledProperty).toBe(true))
  })
})
