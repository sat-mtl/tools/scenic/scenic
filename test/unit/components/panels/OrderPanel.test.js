/* global describe it expect jest beforeEach */
import React from 'react'
import { mount } from 'enzyme'

import populateStores from '@stores/populateStores.js'

import Quiddity from '@models/quiddity/Quiddity'

import { AppStoresContext } from '@components/App'

import {
  OrderButtonRight,
  OrderButtonLeft,
  OrderButtonUp,
  OrderButtonDown
} from '@components/panels/OrderPanel'

import OrderEnum from '@models/common/OrderEnum'

import { Common } from '@sat-mtl/ui-components'
const { Button } = Common

describe('<OrderPanel />', () => {
  let quiddityStore, orderStore
  beforeEach(() => {
    ({ quiddityStore, orderStore } = populateStores())

    orderStore.updateDestinationOrder = jest.fn()
    orderStore.updateSourceOrder = jest.fn()

    orderStore.destinationOrders.set('test0', 0)
    orderStore.destinationOrders.set('test1', 1)

    orderStore.sourceOrders.set('test0', 0)
    orderStore.sourceOrders.set('test1', 1)

    quiddityStore.quiddities.set(QUIDDITY0.id, QUIDDITY0)
    quiddityStore.quiddities.set(QUIDDITY1.id, QUIDDITY1)
  })

  const QUIDDITY0 = new Quiddity('test0', 'test0', 'test0')
  const QUIDDITY1 = new Quiddity('test1', 'test1', 'test1')

  describe('OrderButtonRight', () => {
    function mountOrderButtonRight () {
      return mount(
        <AppStoresContext.Provider value={{ quiddityStore, orderStore }}>
          <OrderButtonRight />
        </AppStoresContext.Provider>
      )
    }

    it('should increase the order of a selected destination when clicked', () => {
      quiddityStore.addSelectedQuiddity(QUIDDITY0.id)
      const $rightButton = mountOrderButtonRight()
      $rightButton.find(Button).simulate('click')
      expect(orderStore.updateDestinationOrder).toHaveBeenCalledWith(QUIDDITY0.id, 1, OrderEnum.RIGHT)
    })
  })

  describe('OrderButtonLeft', () => {
    function mountOrderButtonLeft () {
      return mount(
        <AppStoresContext.Provider value={{ quiddityStore, orderStore }}>
          <OrderButtonLeft />
        </AppStoresContext.Provider>
      )
    }

    it('should decrease the order of a selected destination when clicked', () => {
      quiddityStore.addSelectedQuiddity(QUIDDITY1.id)
      const $leftButton = mountOrderButtonLeft()
      $leftButton.find(Button).simulate('click')
      expect(orderStore.updateDestinationOrder).toHaveBeenCalledWith(QUIDDITY1.id, 0, OrderEnum.LEFT)
    })
  })

  describe('OrderButtonDown', () => {
    function mountOrderButtonDown () {
      return mount(
        <AppStoresContext.Provider value={{ quiddityStore, orderStore }}>
          <OrderButtonDown />
        </AppStoresContext.Provider>
      )
    }

    it('should increase the order of a selected source when clicked', () => {
      quiddityStore.addSelectedQuiddity(QUIDDITY0.id)
      const $downwardButton = mountOrderButtonDown()
      $downwardButton.find(Button).simulate('click')
      expect(orderStore.updateSourceOrder).toHaveBeenCalledWith(QUIDDITY0.id, 1, OrderEnum.DOWN)
    })
  })

  describe('OrderButtonUp', () => {
    function mountOrderButtonUp () {
      return mount(
        <AppStoresContext.Provider value={{ quiddityStore, orderStore }}>
          <OrderButtonUp />
        </AppStoresContext.Provider>
      )
    }

    it('should decrease the order of a selected source when clicked', () => {
      quiddityStore.addSelectedQuiddity(QUIDDITY1.id)
      const $upwardButton = mountOrderButtonUp()
      $upwardButton.find(Button).simulate('click')
      expect(orderStore.updateSourceOrder).toHaveBeenCalledWith(QUIDDITY1.id, 0, OrderEnum.UP)
    })
  })
})
