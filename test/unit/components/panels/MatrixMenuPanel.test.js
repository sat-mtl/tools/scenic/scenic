/* global jest describe it expect beforeEach */
import React from 'react'

import { mount } from 'enzyme'
import { act } from 'react-dom/test-utils'
import populateStores from '@stores/populateStores'

import MatrixMenuPanel, {
  PropertyInspectorMenu,
  SipDrawersMenu
} from '@components/panels/MatrixMenuPanel'

import { AppStoresContext } from '@components/App'
import { MenuButton } from '@components/common/Buttons'

import { PROPERTY_INSPECTOR_DRAWER_ID } from '@components/drawers/PropertyInspectorDrawer'
import { SIP_CONTACT_DRAWER_ID } from '@components/drawers/SipContactDrawer'
import { SIP_LOGIN_DRAWER_ID } from '@components/drawers/SipLoginDrawer'

describe('<MatrixMenuPanel />', () => {
  let modalStore, drawerStore, sipStore, helpMessageStore
  let toggleActiveSpy

  function registerDrawers () {
    drawerStore.addDrawer(PROPERTY_INSPECTOR_DRAWER_ID)
    drawerStore.addDrawer(SIP_CONTACT_DRAWER_ID)
    drawerStore.addDrawer(SIP_LOGIN_DRAWER_ID)
  }

  beforeEach(() => {
    ({ modalStore, drawerStore, sipStore, helpMessageStore } = populateStores())
    registerDrawers()
    toggleActiveSpy = jest.spyOn(drawerStore, 'toggleActiveDrawer')
  })

  function mountPanel () {
    return mount(
      <AppStoresContext.Provider value={{ modalStore, drawerStore, sipStore, helpMessageStore }}>
        <MatrixMenuPanel />
      </AppStoresContext.Provider>
    )
  }

  it('should display PropertyInspectorMenu and SipDrawersMenu', () => {
    const $panel = mountPanel()
    expect($panel.find(PropertyInspectorMenu).length).toEqual(1)
    expect($panel.find(SipDrawersMenu).length).toEqual(1)
  })

  describe('<PropertyInspectorMenu />', () => {
    let $menu

    beforeEach(() => {
      $menu = mountPanel().find(PropertyInspectorMenu)
    })

    function getMenuButton () {
      return mountPanel().find(PropertyInspectorMenu).find(MenuButton)
    }

    it('should mount the menu with default values', () => {
      expect($menu.find(MenuButton).length).toEqual(1)
      expect(getMenuButton().prop('isActive')).toEqual(false)
    })

    it('should toggle the property inspector drawer when it is clicked', () => {
      getMenuButton().find('button').simulate('click')
      expect(drawerStore.toggleActiveDrawer).toHaveBeenCalledWith(PROPERTY_INSPECTOR_DRAWER_ID)
    })

    it('should be active if the property inspector drawer is active', () => {
      act(() => drawerStore.toggleActiveDrawer(PROPERTY_INSPECTOR_DRAWER_ID))
      expect(getMenuButton().prop('isActive')).toEqual(true)
    })
  })

  describe('<SipDrawersMenu />', () => {
    let $menu

    beforeEach(() => {
      $menu = mountPanel().find(SipDrawersMenu)
    })

    function getMenuButton () {
      return mountPanel().find(SipDrawersMenu).find(MenuButton)
    }

    it('should mount the menu with default values', () => {
      expect($menu.find(MenuButton).length).toEqual(1)
      expect(getMenuButton().prop('isActive')).toEqual(false)
    })

    it('should toggle the sip login drawer when it is clicked', () => {
      getMenuButton().find('button').simulate('click')
      expect(toggleActiveSpy).toHaveBeenCalledWith(SIP_LOGIN_DRAWER_ID)
    })

    it('should toggle the sip contact drawer when it is clicked and the sip is connected', () => {
      sipStore.currentCredentials = 'test'
      expect(sipStore.isConnected).toEqual(true)
      getMenuButton().find('button').simulate('click')
      expect(toggleActiveSpy).toHaveBeenCalledWith(SIP_CONTACT_DRAWER_ID)
    })

    it('should be active if the property inspector drawer is active', () => {
      act(() => drawerStore.toggleActiveDrawer(SIP_LOGIN_DRAWER_ID))
      expect(getMenuButton().prop('isActive')).toEqual(true)
      act(() => drawerStore.toggleActiveDrawer(SIP_CONTACT_DRAWER_ID))
      expect(getMenuButton().prop('isActive')).toEqual(true)
    })
  })
})
