/* global jest describe beforeEach it expect */

import React from 'react'
import { mount } from 'enzyme'
import { act } from 'react-dom/test-utils'

import populateStores from '@stores/populateStores'

import { AppStoresContext } from '@components/App'

import MenuItem from '@models/menus/MenuItem'
import MenuCollection from '@models/menus/MenuCollection'

import QuiddityMenu, { DEFAULT_MENU_ADDON, QuiddityMenuFactory, QuiddityMenuGroup, QuiddityMenuItem } from '@components/menus/QuiddityMenu'

import { Common } from '@sat-mtl/ui-components'

const { Menu } = Common

function generateMenuItems (numberOfItem) {
  return [...Array(10).keys()].map(i => new MenuItem(`menu${i}`, `quid${i}`))
}

function getDataMenu ($quiddityMenu, dataMenu) {
  return $quiddityMenu.find(`[data-menu="${dataMenu}"]`)
}

describe('<QuiddityMenu />', () => {
  let quiddityMenuStore
  let simpleCollection, multipleCollection

  function mountQuiddityMenu (quiddityMenuStore, menuCollection, customAddon) {
    return mount(
      <AppStoresContext.Provider value={{ quiddityMenuStore }}>
        <QuiddityMenu
          store={quiddityMenuStore}
          collection={menuCollection}
          addonBefore={customAddon}
        />
      </AppStoresContext.Provider>
    )
  }

  function getMenu ($quiddityMenu) {
    return $quiddityMenu.update().find(Menu.Menu)
  }

  beforeEach(() => {
    ({ quiddityMenuStore } = populateStores())

    simpleCollection = new MenuCollection('test')
    multipleCollection = new MenuCollection('test', generateMenuItems(10))
  })

  it('should display a menu from the menu collection', () => {
    const $menu = mountQuiddityMenu(quiddityMenuStore, simpleCollection)
    expect(getMenu($menu).length).toEqual(1)
  })

  it('should create a QuiddityMenuFactory for every sub-menus', () => {
    const $menu = mountQuiddityMenu(quiddityMenuStore, multipleCollection)
    expect($menu.find(QuiddityMenuFactory).length).toEqual(10)
  })

  it('should be fetchable by a data attribute', () => {
    const $menu = mountQuiddityMenu(quiddityMenuStore, simpleCollection)
    expect(getDataMenu($menu, 'test').length).toEqual(1)
  })

  it('should be expanded when the menu store opens its collection', () => {
    const $menu = mountQuiddityMenu(quiddityMenuStore, simpleCollection)
    expect(getMenu($menu).prop('expanded')).toEqual(false)

    act(() => {
      quiddityMenuStore.toggleCollection(simpleCollection.collectionKey)
    })

    expect(getMenu($menu).prop('expanded')).toEqual(true)
  })

  it('should toggle its collection when it is clicked', () => {
    quiddityMenuStore.toggleCollection = jest.fn()
    const $menu = mountQuiddityMenu(quiddityMenuStore, simpleCollection)
    getMenu($menu).prop('onClick')()
    expect(quiddityMenuStore.toggleCollection).toHaveBeenCalledWith(simpleCollection.collectionKey)
  })

  it('should clear the connection when it is blured', () => {
    quiddityMenuStore.clearCollection = jest.fn()
    const $menu = mountQuiddityMenu(quiddityMenuStore, simpleCollection)
    getMenu($menu).prop('onBlur')()
    expect(quiddityMenuStore.clearCollection).toHaveBeenCalledWith(simpleCollection.collectionKey)
  })

  it('should be initialized with a default addon', () => {
    const $menu = mountQuiddityMenu(quiddityMenuStore, simpleCollection)
    expect(getMenu($menu).prop('addonBefore')).toEqual(DEFAULT_MENU_ADDON)
  })

  it('should be initialized with a custom addon', () => {
    const $menu = mountQuiddityMenu(quiddityMenuStore, simpleCollection, <div />)
    expect(getMenu($menu).prop('addonBefore')).toEqual(<div />)
  })
})

describe('<QuiddityMenuFactory />', () => {
  let quiddityMenuStore
  let simpleCollection, simpleItem

  function mountQuiddityMenuFactory (store, model) {
    return mount(
      <AppStoresContext.Provider value={{ quiddityMenuStore }}>
        <QuiddityMenuFactory quiddityMenuStore={store} menuModel={model} />
      </AppStoresContext.Provider>
    )
  }

  beforeEach(() => {
    ({ quiddityMenuStore } = populateStores())

    simpleCollection = new MenuCollection('test')
    simpleItem = new MenuItem('test', 'test')
  })

  it('should instanciate a QuiddityMenuGroup from a MenuGroup model', () => {
    const $wrapper = mountQuiddityMenuFactory(quiddityMenuStore, simpleCollection)
    expect($wrapper.find(QuiddityMenuGroup).length).toEqual(1)
    expect($wrapper.find(QuiddityMenuItem).length).toEqual(0)
  })

  it('should instanciate a QuiddityMenuItem from a MenuItem model', () => {
    const $wrapper = mountQuiddityMenuFactory(quiddityMenuStore, simpleItem)
    expect($wrapper.find(QuiddityMenuGroup).length).toEqual(0)
    expect($wrapper.find(QuiddityMenuItem).length).toEqual(1)
  })

  it('should fail when it is instanciated with anything else', () => {
    console.error = jest.fn()
    expect(() => mountQuiddityMenuFactory(quiddityMenuStore, {})).toThrow()
  })
})

describe('<QuiddityMenuGroup />', () => {
  let quiddityMenuStore
  let simpleCollection, multipleCollection

  function mountQuiddityMenuGroup (store, model) {
    return mount(
      <AppStoresContext.Provider value={{ quiddityMenuStore }}>
        <QuiddityMenuGroup quiddityMenuStore={store} menuGroup={model} />
      </AppStoresContext.Provider>
    )
  }

  function getGroup ($menu) {
    return $menu.update().find(Menu.Group)
  }

  beforeEach(() => {
    ({ quiddityMenuStore } = populateStores())

    simpleCollection = new MenuCollection('test')
    multipleCollection = new MenuCollection('test', generateMenuItems(10))
  })

  it('should be toggle when it is clicked', () => {
    quiddityMenuStore.toggleMenu = jest.fn()
    const $group = mountQuiddityMenuGroup(quiddityMenuStore, simpleCollection)
    getGroup($group).prop('onClick')()
    expect(quiddityMenuStore.toggleMenu).toHaveBeenCalledWith(simpleCollection.groupKey)
  })

  it('should be toggle when it is clicked', () => {
    const $group = mountQuiddityMenuGroup(quiddityMenuStore, simpleCollection)
    expect(getGroup($group).prop('showGroup')).toEqual(false)
    quiddityMenuStore.toggleMenu(simpleCollection.groupKey)
    expect(getGroup($group).prop('showGroup')).toEqual(true)
  })

  it('should create a QuiddityMenuFactory for every sub-menus', () => {
    const $group = mountQuiddityMenuGroup(quiddityMenuStore, multipleCollection)
    expect($group.find(QuiddityMenuFactory).length).toEqual(10)
  })
})

describe('<QuiddityMenuItem />', () => {
  let quiddityMenuStore
  let simpleItem

  function mountQuiddityMenuItem (store, model) {
    return mount(
      <AppStoresContext.Provider value={{ quiddityMenuStore }}>
        <QuiddityMenuItem quiddityMenuStore={store} menuItem={model} />
      </AppStoresContext.Provider>
    )
  }

  function getItem ($menu) {
    return $menu.update().find(Menu.Item)
  }

  beforeEach(() => {
    ({ quiddityMenuStore } = populateStores())
    simpleItem = new MenuItem('test', 'test')
  })

  it('should handle the quiddity creation and close all menus when it is clicked', () => {
    const handleSpy = jest.fn()

    quiddityMenuStore.handleQuiddityRequest = () => handleSpy
    quiddityMenuStore.clearAllMenus = jest.fn()

    const $item = mountQuiddityMenuItem(quiddityMenuStore, simpleItem)
    getItem($item).prop('onClick')()

    expect(handleSpy).toHaveBeenCalled()
    expect(quiddityMenuStore.clearAllMenus).toHaveBeenCalled()
  })
})
