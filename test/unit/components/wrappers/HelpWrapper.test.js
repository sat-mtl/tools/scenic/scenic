/* global jest describe beforeEach it expect */
/* eslint-disable no-import-assign */

import React from 'react'
import { mount } from 'enzyme'

import { AppStoresContext } from '@components/App'
import populateStores from '@stores/populateStores.js'

import HelpWrapper from '@components/wrappers/HelpWrapper'
import * as uiTools from '@utils/uiTools'

describe('<HelpWrapper />', () => {
  let helpMessageStore

  beforeEach(() => {
    ({ helpMessageStore } = populateStores())
    helpMessageStore.setHelpMessage = jest.fn()
    helpMessageStore.cleanHelpMessage = jest.fn()
  })

  function mountHelpWrapper () {
    return mount(
      <AppStoresContext.Provider value={{ helpMessageStore }}>
        <HelpWrapper message='test'>
          <div />
        </HelpWrapper>
      </AppStoresContext.Provider>
    ).find(HelpWrapper)
  }

  it('should set a message in the HelpMessageStore when it is hovered', () => {
    uiTools.useHover = jest.fn().mockReturnValue([React.createRef(), true])
    mountHelpWrapper()
    expect(helpMessageStore.setHelpMessage).toHaveBeenCalledWith('test')
  })

  it('should clean the HelpMessageStore when the cursor leave the wrapper', () => {
    uiTools.useHover = jest.fn().mockReturnValue([React.createRef(), false])
    mountHelpWrapper()
    expect(helpMessageStore.cleanHelpMessage).toHaveBeenCalled()
  })
})
