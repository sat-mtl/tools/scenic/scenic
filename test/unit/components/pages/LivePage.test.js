/* global describe it expect beforeEach */
import React from 'react'
import { mount } from 'enzyme'

import populateStores from '@stores/populateStores.js'
import LivePage, { Scene } from '@components/pages/LivePage'
import SceneModel from '@models/userTree/Scene'

import USER_TREE_REV1 from '@fixture/models/userTree.revision1.json'
const userScenes = SceneModel.fromUserTree(USER_TREE_REV1)

describe('<LivePage />', () => {
  let sceneStore

  beforeEach(() => {
    ({ sceneStore } = populateStores())
  })

  function mountLivePage () {
    return mount(
      <LivePage sceneStore={sceneStore} />
    )
  }

  describe('<Scene />', () => {
    it('should render all scenes of the session', () => {
      userScenes.forEach(sc => sceneStore.addUserScene(sc))
      expect(mountLivePage().find(Scene).length).toEqual(userScenes.length)
    })
  })
})
