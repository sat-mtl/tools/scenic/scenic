/* global jest describe beforeEach it expect */

import React from 'react'
import { configure } from 'mobx'
import { act } from 'react-dom/test-utils'
import { mount } from 'enzyme'
import populateStores from '@stores/populateStores.js'

import { AppStoresContext } from '@components/App'
import FileEntry from '@components/entries/FileEntry'

import MatrixEntry from '@models/matrix/MatrixEntry'
import sdiInput from '@fixture/models/quiddity.sdiInput1.json'
import Quiddity from '@models/quiddity/Quiddity'

import FileBridge from '@models/common/FileBridge'

import FileLoadingDrawer, {
  FILE_LOADING_DRAWER_ID,
  OpenFileButton
} from '@components/drawers/FileLoadingDrawer'

import { OpenSessionModal, OPEN_SESSION_MODAL_ID } from '@components/modals/SessionModals'
import { Common, Layout, Inputs } from '@sat-mtl/ui-components'

import '@utils/i18n'

configure({ safeDescriptors: false })

const { Backdrop } = Layout
const { Checkbox } = Inputs
const { Button } = Common

const FILE_ID = 'test.json'
const FILE_TEST = new FileBridge(FILE_ID)

describe('<FileLoadingDrawer />', () => {
  let drawerStore, sessionStore, modalStore, helpMessageStore, matrixStore

  beforeEach(() => {
    ({
      drawerStore,
      sessionStore,
      modalStore,
      helpMessageStore,
      matrixStore
    } = populateStores())

    sessionStore.updateSessionList = jest.fn()
  })

  function mountDrawer () {
    return mount(
      <AppStoresContext.Provider value={{ drawerStore, helpMessageStore, modalStore, sessionStore }}>
        <FileLoadingDrawer />
      </AppStoresContext.Provider>
    )
  }

  const getEntries = $drawer => $drawer.update().find(FileEntry)
  const getCheckboxes = $drawer => $drawer.update().find(Checkbox)
  const getOpenFileButton = $drawer => $drawer.update().find(OpenFileButton).find(Button)
  const getOpenSessionModal = $drawer => $drawer.update().find(OpenSessionModal)

  function addFiles (fileNb) {
    for (let i = 0; i < fileNb; i++) {
      sessionStore.addSession(
        new FileBridge(`${FILE_ID}${i}`, `/${FILE_ID}${i}.json`)
      )
    }
  }

  describe('useEffect', () => {
    let addDrawerSpy, removeDrawerSpy
    let addModalSpy, removeModalSpy

    beforeEach(() => {
      addDrawerSpy = jest.spyOn(drawerStore, 'addDrawer')
      removeDrawerSpy = jest.spyOn(drawerStore, 'removeDrawer')

      addModalSpy = jest.spyOn(modalStore, 'addModal')
      removeModalSpy = jest.spyOn(modalStore, 'removeModal')
    })

    it('should register the FileLoadingDrawer and the TrashCurrentSessionModal when it is mounted', () => {
      mountDrawer()
      expect(addDrawerSpy).toHaveBeenCalledWith(FILE_LOADING_DRAWER_ID)
      expect(addModalSpy).toHaveBeenCalledWith(OPEN_SESSION_MODAL_ID)
    })

    it('should unregister the FileLoadingDrawer and the TrashCurrentSessionModal when it is unmounted', () => {
      mountDrawer().unmount()
      expect(removeDrawerSpy).toHaveBeenCalledWith(FILE_LOADING_DRAWER_ID)
      expect(removeModalSpy).toHaveBeenCalledWith(OPEN_SESSION_MODAL_ID)
    })
  })

  describe('<Backdrop/>', () => {
    let clearActiveDrawerSpy

    beforeEach(() => {
      clearActiveDrawerSpy = jest.spyOn(drawerStore, 'clearActiveDrawer')
    })

    it('should clean the drawers when the backdrop is clicked', () => {
      mountDrawer().find(Backdrop).simulate('click')
      expect(clearActiveDrawerSpy).toHaveBeenCalled()
    })
  })

  describe('<FileLoadingList />', () => {
    it('should display an entry for all files', () => {
      const $drawer = mountDrawer()

      act(() => addFiles(10))
      expect(getEntries($drawer).length).toEqual(10)
    })

    it('should be checked if the file is selected', () => {
      const $drawer = mountDrawer()

      act(() => addFiles(1))

      expect(getCheckboxes($drawer).length).toEqual(1)
      expect(getCheckboxes($drawer).prop('checked')).toEqual(false)

      getCheckboxes($drawer).simulate('click')
      expect(getCheckboxes($drawer).prop('checked')).toEqual(true)
    })
  })

  describe('<OpenFileButton />', () => {
    let setActiveModalSpy

    beforeEach(() => {
      setActiveModalSpy = jest.spyOn(modalStore, 'setActiveModal')
      // applySessionLoadingSpy = jest.spyOn(sessionStore, 'applySessionLoading')
      // clearActiveDrawerSpy = jest.spyOn(drawerStore, 'clearActiveDrawer')
    })

    it('should have the class name OpenFileButton', () => {
      const $openFile = getOpenFileButton(mountDrawer())
      expect($openFile.prop('className')).toEqual('OpenFileButton')
    })

    it('should be disabled if no file is selected', () => {
      const $openFile = getOpenFileButton(mountDrawer())
      expect($openFile.find(Button).prop('disabled')).toEqual(true)
    })

    it('should not be disabled if a file is selected', () => {
      const $drawer = mountDrawer()

      act(() => addFiles(1))
      getCheckboxes($drawer).simulate('click')
      expect(getOpenFileButton($drawer).prop('disabled')).toEqual(false)
    })

    it('should open the OpenSessionModal if the opened file is the current file', () => {
      const $drawer = mountDrawer()

      act(() => {
        sessionStore.addSession(new FileBridge(FILE_ID, `/${FILE_ID}.txt`))
        sessionStore.setCurrentSession(FILE_ID)
      })

      getCheckboxes($drawer).simulate('click')
      getOpenFileButton($drawer).simulate('click')
      expect(setActiveModalSpy).toHaveBeenCalledWith(OPEN_SESSION_MODAL_ID)
    })

    it.todo('should open the selected file and close the LoadingDrawerFile if there are no entries in the matrix'/*, () => {
      const $drawer = mountDrawer()

      act(() => sessionStore.addSession(FILE_TEST))
      jest.spyOn(sessionStore, 'currentSessionId', 'get').mockReturnValue(null)
      jest.spyOn(matrixStore, 'matrixEntries', 'get').mockReturnValue(new Map())
      getCheckboxes($drawer).simulate('click')
      getOpenFileButton($drawer).simulate('click')

      expect(applySessionLoadingSpy).toHaveBeenCalledWith(FILE_TEST)
      expect(clearActiveDrawerSpy).toHaveBeenCalledWith(FILE_LOADING_DRAWER_ID)
    } */)

    it('should open the OpenSessionModal if the user has created at least one matrix entry', () => {
      const $drawer = mountDrawer()

      const testEntry = new Map([
        { quiddity: new Quiddity(sdiInput.id, 'test', 'test'), shmdatas: [] }
      ].map(MatrixEntry.fromJSON))

      jest.spyOn(matrixStore, 'matrixEntries', 'get').mockReturnValue(testEntry)
      act(() => sessionStore.addSession(FILE_TEST))
      getCheckboxes($drawer).simulate('click')
      getOpenFileButton($drawer).simulate('click')
      expect(setActiveModalSpy).toHaveBeenCalledWith(OPEN_SESSION_MODAL_ID)
    })
  })

  describe('<OpenSessionModal />', () => {
    let applySessionLoadingSpy
    let clearActiveModalSpy, clearActiveDrawerSpy

    beforeEach(() => {
      applySessionLoadingSpy = jest.spyOn(sessionStore, 'applySessionLoading')
      clearActiveModalSpy = jest.spyOn(modalStore, 'clearActiveModal')
      clearActiveDrawerSpy = jest.spyOn(drawerStore, 'clearActiveDrawer')
    })

    it('should load the selected file and close the drawer and the modal when it is confirmed', () => {
      const $drawer = mountDrawer()

      act(() => sessionStore.addSession(FILE_TEST))
      getCheckboxes($drawer).simulate('click')
      getOpenSessionModal($drawer).prop('onConfirm')()

      expect(applySessionLoadingSpy).toHaveBeenCalledWith(FILE_TEST)
      expect(clearActiveDrawerSpy).toHaveBeenCalled()
      expect(clearActiveModalSpy).toHaveBeenCalled()
    })

    it('should clear the modal when it is canceled', () => {
      const $drawer = mountDrawer()

      act(() => getOpenSessionModal($drawer).prop('onCancel')())

      expect(applySessionLoadingSpy).not.toHaveBeenCalled()
      expect(clearActiveDrawerSpy).not.toHaveBeenCalled()
      expect(clearActiveModalSpy).toHaveBeenCalled()
    })
  })
})
