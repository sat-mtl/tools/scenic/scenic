/* global jest describe beforeEach it expect */

import React from 'react'
import { act } from 'react-dom/test-utils'
import { mount } from 'enzyme'

import SipCredentialStore, { DEFAULT_SIP_CREDENTIALS } from '@stores/sip/SipCredentialStore'

import populateStores from '@stores/populateStores.js'

import PageEnum from '@models/common/PageEnum'
import SettingEnum from '@models/common/SettingEnum'

import { AppStoresContext } from '@components/App'
import { Layout } from '@sat-mtl/ui-components'

import SipLoginDrawer, {
  ResetFieldsButton,
  LoginButton,
  AdvancedSettingsLink,
  SIP_LOGIN_DRAWER_ID,
  SipCredentialForm
} from '@components/drawers/SipLoginDrawer'

import { SipServerField, SipUserField, SipPortField, SipPasswordField } from '@components/fields/SipFields'

const { Backdrop } = Layout

describe('<SipLoginDrawer />', () => {
  let drawerStore
  let sipCredentialStore, sipStore
  let pageStore, settingStore
  let helpMessageStore

  beforeEach(() => {
    ({
      sipCredentialStore,
      sipStore,
      pageStore,
      settingStore,
      drawerStore,
      helpMessageStore
    } = populateStores())
  })

  function mountDrawer () {
    return mount(
      <AppStoresContext.Provider value={{
        sipCredentialStore,
        sipStore,
        pageStore,
        settingStore,
        drawerStore,
        helpMessageStore
      }}
      >
        <SipLoginDrawer />
      </AppStoresContext.Provider>
    )
  }

  describe('<SipCredentialForm/>', () => {
    beforeEach(() => {
      sipStore.applyLogin = jest.fn()
      sipCredentialStore.applyAllValidationBindings = jest.fn().mockReturnValue(true)
    })

    it('should login on enter keyPress', () => {
      const $form = mountDrawer().find(SipCredentialForm)
      $form.simulate('keypress', { key: 'Enter' })
      expect(sipStore.applyLogin).toHaveBeenCalled()
    })

    it('should not login on keyPress other than enter', () => {
      const $form = mountDrawer().find(SipCredentialForm)
      $form.simulate('keypress', { key: undefined })
      expect(sipStore.applyLogin).not.toHaveBeenCalled()
    })

    it('should not login on enter keyPress with invalid credentials', () => {
      sipCredentialStore.applyAllValidationBindings = jest.fn().mockReturnValue(false)
      const $form = mountDrawer().find(SipCredentialForm)
      $form.simulate('keypress', { key: 'Enter' })
      expect(sipStore.applyLogin).not.toHaveBeenCalled()
    })
  })

  describe('useEffect', () => {
    let addDrawerSpy, removeDrawerSpy

    beforeEach(() => {
      addDrawerSpy = jest.spyOn(drawerStore, 'addDrawer')
      removeDrawerSpy = jest.spyOn(drawerStore, 'removeDrawer')
    })

    it('should register the SipLoginDrawer when it is mounted', () => {
      mountDrawer()
      expect(addDrawerSpy).toHaveBeenCalledWith(SIP_LOGIN_DRAWER_ID)
    })

    it('should unregister the FileDeletionDrawer and the TrashCurrentSessionModal when it is unmounted', () => {
      mountDrawer().unmount()
      expect(removeDrawerSpy).toHaveBeenCalledWith(SIP_LOGIN_DRAWER_ID)
    })
  })

  describe('<Backdrop/>', () => {
    let clearActiveDrawerSpy

    beforeEach(() => {
      clearActiveDrawerSpy = jest.spyOn(drawerStore, 'clearActiveDrawer')
    })

    it('should clean the drawers when the backdrop is clicked', () => {
      mountDrawer().find(Backdrop).simulate('click')
      expect(clearActiveDrawerSpy).toHaveBeenCalled()
    })
  })

  describe('<ResetFieldsButton />', () => {
    it('should reset the credential fields when it is clicked', () => {
      const cleanCredentialSpy = jest.spyOn(sipCredentialStore, 'cleanCredentials')
      const $drawer = mountDrawer()
      $drawer.find(ResetFieldsButton).simulate('click')
      expect(cleanCredentialSpy).toHaveBeenCalled()
    })
  })

  const getLoginButton = $drawer => $drawer.update().find(LoginButton)

  describe('<LoginButton />', () => {
    it('should be disabled if the credentials are not valid', () => {
      jest.spyOn(SipCredentialStore.prototype, 'areCredentialsValid', 'get').mockReturnValue(false)
      expect(getLoginButton(mountDrawer()).prop('disabled')).toEqual(true)
    })

    it('should be enabled if the credentials are valid', () => {
      jest.spyOn(SipCredentialStore.prototype, 'areCredentialsValid', 'get').mockReturnValue(true)
      expect(getLoginButton(mountDrawer()).prop('disabled')).toEqual(false)
    })

    it('should log in SIP when it is clicked', () => {
      jest.spyOn(SipCredentialStore.prototype, 'areCredentialsValid', 'get').mockReturnValue(true)
      const applyLoginSpy = jest.spyOn(sipStore, 'applyLogin')
      getLoginButton(mountDrawer()).simulate('click')
      expect(applyLoginSpy).toHaveBeenCalledWith(sipCredentialStore.sipCredentials)
    })
  })

  describe('fields', () => {
    const FIELD_ERROR = 'test'

    const getField = ($drawer, key) => {
      switch (key) {
        case 'sipServer': return $drawer.update().find(SipServerField)
        case 'sipPort': return $drawer.update().find(SipPortField)
        case 'sipUser': return $drawer.update().find(SipUserField)
        case 'sipPassword': return $drawer.update().find(SipPasswordField)
        default: throw new Error('This field doesn\'t exist')
      }
    }

    const suite = [
      ['SipServerField', 'sipServer', 'test'],
      ['SipUserField', 'sipUser', 'test'],
      ['SipPortField', 'sipPort', 'test'],
      ['SipPasswordField', 'sipPassword', 'test']
    ]

    describe.each(suite)('<%s />', (component, key, expected) => {
      let applyCredentialValidationSpy

      beforeEach(() => {
        applyCredentialValidationSpy = jest.spyOn(sipCredentialStore, 'applyCredentialValidation')
      })

      it('should be initialized with the default credential', () => {
        const value = getField(mountDrawer(), key).prop('value')
        expect(value).toEqual(DEFAULT_SIP_CREDENTIALS.get(key))
      })

      it('should be updated when the credential is changed', () => {
        const $drawer = mountDrawer()
        act(() => getField($drawer, key).prop('onChange')(expected))
        expect(getField($drawer, key).prop('value')).toEqual(expected)
      })

      it('should be validated when the field is checked', () => {
        const $drawer = mountDrawer()
        act(() => getField($drawer, key).prop('onCheck')())
        expect(applyCredentialValidationSpy).toHaveBeenCalled()
      })

      it('should show an error when an error is thrown', () => {
        const $drawer = mountDrawer()
        act(() => sipCredentialStore.setError(key, FIELD_ERROR))
        expect(getField($drawer, key).prop('error')).toEqual(FIELD_ERROR)
      })
    })
  })

  describe('<AdvancedSettingsLink />', () => {
    const getLink = $drawer => $drawer.update().find(AdvancedSettingsLink)

    it('should open the settings page on the sip tab', () => {
      const clearActiveDrawerSpy = jest.spyOn(drawerStore, 'clearActiveDrawer')
      const setActivePageSpy = jest.spyOn(pageStore, 'setActivePage')
      const setActiveGroup = jest.spyOn(settingStore, 'setActiveGroup')

      getLink(mountDrawer()).simulate('click')
      expect(clearActiveDrawerSpy).toHaveBeenCalled()
      expect(setActivePageSpy).toHaveBeenCalledWith(PageEnum.SETTINGS)
      expect(setActiveGroup).toHaveBeenCalledWith(SettingEnum.SIP)
    })
  })
})
