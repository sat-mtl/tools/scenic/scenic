/* global describe it expect */
import React from 'react'

import { shallow } from 'enzyme'
import { DownloadButton, MenuButton } from '@components/common/Buttons'
import { Common } from '@sat-mtl/ui-components'

const { Button, Icon } = Common

const getButton = $button => $button.find(Button)

const DefaultIcon = () => (
  <Icon type='audio' />
)

describe('Buttons', () => {
  describe('<DownloadButton/>', () => {
    function shallowDownloadButton () {
      return shallow(<DownloadButton />)
    }

    it('should have primary style', () => {
      const $download = getButton(shallowDownloadButton())
      expect($download.find(Button).prop('type')).toEqual('primary')
    })
  })

  describe('<MenuButton />', () => {
    function shallowMenuButton () {
      return shallow(
        <MenuButton>
          <DefaultIcon />
        </MenuButton>
      )
    }

    it('should display a configured icon by default', () => {
      expect(getButton(shallowMenuButton()).find(DefaultIcon).length).toEqual(1)
    })

    it('should display a close icon when it is active', () => {
      const $menu = shallowMenuButton()
      $menu.setProps({ isActive: true })
      const $icon = getButton($menu).find(Icon)
      expect($icon.prop('type')).toEqual('close')
    })
  })
})
