/* global describe it expect */

import Shmdata from '@models/shmdata/Shmdata'
// import Stat, { DEFAULT_STAT } from '@models/shmdata/Stat'

const SHM_PATH = '/tmp/switcher_default_1_video'
const SHM_CATEGORY = 'video'
const SHM_CAPS = [
  'video/x-raw',
  'width=(int)800',
  'height=(int)600',
  'framerate=(fraction)25/1',
  'pixel-aspect-ratio=(fraction)1/1',
  'format=(string)I420'
].join(', ')

describe('Shmdata', () => {
  describe('constructor', () => {
    it('should define a Shmdata model', () => {
      const shm = new Shmdata(SHM_PATH, SHM_CAPS, SHM_CATEGORY)
      expect(shm).toBeDefined()
      expect(shm.path).toEqual(SHM_PATH)
      expect(shm.caps).toEqual(SHM_CAPS)
      expect(shm.category).toEqual(SHM_CATEGORY)
    })

    /* eslint-disable no-new */
    it('should fail if required parameters are invalid', () => {
      expect(() => { new Shmdata() }).toThrow()
      expect(() => { new Shmdata(SHM_PATH) }).toThrow()
      expect(() => { new Shmdata(1) }).toThrow()
      expect(() => { new Shmdata(SHM_PATH, 1) }).toThrow()
      expect(() => { new Shmdata(SHM_PATH, SHM_CAPS, null) }).toThrow()
    })
    /* eslint-enable no-new */

    it('should accept string caps', () => {
      const shm = new Shmdata(SHM_PATH, SHM_CAPS, SHM_CATEGORY)
      expect(shm.caps).toEqual(SHM_CAPS)
    })
  })

  describe('id', () => {
    const SHM = new Shmdata(SHM_PATH, SHM_CAPS, SHM_CATEGORY)

    it('should give the shmdata\'s ID', () => {
      expect(SHM.id).toEqual(SHM_PATH.split('/').pop())
    })
  })

  describe('suffix', () => {
    const SHM = new Shmdata(SHM_PATH, SHM_CAPS, SHM_CATEGORY)

    it('should give the shmdata\'s suffix', () => {
      expect(SHM.suffix).toEqual(SHM_PATH.split('_').pop())
    })
  })

  describe('fromJSON', () => {
    it('should fail if JSON input is not well-formed', () => {
      expect(() => {
        Shmdata.fromJSON(SHM_PATH, { rate: 'failure' })
      }).toThrow()
    })

    it('should create shmdata from well-defined JSON', () => {
      expect(Shmdata.fromJSON(SHM_PATH, {
        caps: SHM_CAPS,
        category: SHM_CATEGORY,
        stat: { byteRate: 0, rate: 0 }
      })).toBeDefined()
    })
  })

  describe('fromTree', () => {
    const SHM = {
      caps: SHM_CAPS,
      category: SHM_CATEGORY,
      stat: { byteRate: 0, rate: 0 }
    }

    const SHM_READER_1_PATH = '/tmp/reader_1_video'
    const SHM_READER_2_PATH = '/tmp/reader_2_video'
    const SHM_WRITER_1_PATH = '/tmp/writer_1_video'
    const SHM_WRITER_2_PATH = '/tmp/writer_2_video'

    it('should create shmdatas from a full quiddity tree', () => {
      expect(Shmdata.fromTree({
        writer: {
          [SHM_WRITER_1_PATH]: SHM
        },
        reader: {
          [SHM_READER_1_PATH]: SHM
        }
      })).toEqual([
        Shmdata.fromJSON(SHM_WRITER_1_PATH, SHM),
        Shmdata.fromJSON(SHM_READER_1_PATH, SHM)
      ])
    })

    it('should create shmdatas from a quiddity tree with writers', () => {
      expect(Shmdata.fromTree({
        writer: {
          [SHM_WRITER_1_PATH]: SHM,
          [SHM_WRITER_2_PATH]: SHM
        }
      })).toEqual([
        Shmdata.fromJSON(SHM_WRITER_1_PATH, SHM),
        Shmdata.fromJSON(SHM_WRITER_2_PATH, SHM)
      ])
    })

    it('should create shmdatas from a quiddity tree with readers', () => {
      expect(Shmdata.fromTree({
        reader: {
          [SHM_READER_1_PATH]: SHM,
          [SHM_READER_2_PATH]: SHM
        }
      })).toEqual([
        Shmdata.fromJSON(SHM_READER_1_PATH, SHM),
        Shmdata.fromJSON(SHM_READER_2_PATH, SHM)
      ])
    })
  })
})
