/* global describe it expect beforeEach */

import Kind from '@models/quiddity/Kind'
import descriptions from '@fixture/description.classes.json'

describe('Kind', () => {
  const QUID_ID = 'test'
  const QUID_NAME = 'test'
  const QUID_CATEGORY = 'test'

  let defaultKind

  beforeEach(() => {
    defaultKind = new Kind(QUID_ID, QUID_NAME, QUID_CATEGORY)
  })

  describe('constructor', () => {
    /* eslint-disable no-new */
    it('should fail if required parameters are not well-typed', () => {
      expect(() => { new Kind() }).toThrow()
      expect(() => { new Kind(0) }).toThrow()

      expect(() => { new Kind(QUID_ID) }).toThrow()
      expect(() => { new Kind(QUID_ID, 0) }).toThrow()

      expect(() => { new Kind(QUID_ID, QUID_NAME, QUID_CATEGORY, undefined, 0) }).toThrow()
      expect(() => { new Kind(QUID_ID, QUID_NAME, QUID_CATEGORY, undefined, undefined, 0) }).toThrow()

      expect(() => { new Kind(QUID_ID, QUID_NAME) }).not.toThrow()
      expect(() => { new Kind(QUID_ID, QUID_NAME, QUID_CATEGORY, undefined, 'description', false) }).not.toThrow()
    })
    /* eslint-enable no-new */

    it('should define a quiddity kind with default arguments', () => {
      const quidKind = new Kind(QUID_ID, QUID_NAME, QUID_CATEGORY)
      expect(quidKind.tags).toEqual([])
      expect(quidKind.description).toEqual('')
      expect(quidKind.readOnly).toEqual(false)
    })
  })

  describe('isMatchingQuiddity', () => {
    it('should\'t match anything', () => {
      expect(defaultKind.isMatchingQuiddity('I am a failure')).toEqual(false)
      expect(defaultKind.isMatchingQuiddity('tset0')).toEqual(false)
    })

    it('should match the same ID', () => {
      expect(defaultKind.isMatchingQuiddity(QUID_ID)).toEqual(true)
    })

    it('should match all names built from its ID and a number', () => {
      for (const num of [...Array(20).keys()]) {
        expect(defaultKind.isMatchingQuiddity(`${QUID_ID}${num}`)).toEqual(true)
      }
    })
  })

  describe('isReader & isWriter', () => {
    it('should match writer tag', () => {
      expect(defaultKind.isWriter).toEqual(false)
      defaultKind.tags.push('writer')
      expect(defaultKind.isWriter).toEqual(true)
    })

    it('should match reader tag', () => {
      expect(defaultKind.isFollower).toEqual(false)
      defaultKind.tags.push('follower')
      expect(defaultKind.isFollower).toEqual(true)
    })
  })

  describe('fromJSON', () => {
    it('should fail if JSON input is not well-formed', () => {
      expect(() => { Kind.fromJSON() }).toThrow()
      expect(() => { Kind.fromJSON({}) }).toThrow()
      expect(() => { Kind.fromJSON({ 1: 1 }) }).toThrow()
    })

    it('should create a quiddity kind from a valid JSON', () => {
      const quidKind = Kind.fromJSON({
        kind: 'videotest',
        name: 'Video Test Signal',
        category: 'video',
        tags: ['writer']
      })

      expect(quidKind).toBeDefined()
    })

    it('should create quiddity kinds from definitions', () => {
      const kinds = descriptions.kinds

      for (const json of kinds) {
        expect(Kind.fromJSON(json)).toBeDefined()
      }
    })
  })
})
