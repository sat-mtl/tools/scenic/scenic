/* global describe it expect */

import MatrixEntry from '@models/matrix/MatrixEntry'

import Quiddity from '@models/quiddity/Quiddity'
import Shmdata from '@models/shmdata/Shmdata'
import Contact from '@models/sip/Contact'

import { SIP_ID, SIP_KIND_ID } from '@models/quiddity/specialQuiddities'

import SDI_INPUT_QUIDDITY from '@fixture/models/quiddity.sdiInput1.json'
import VIDEO_OUTPUT_QUIDDITY from '@fixture/models/quiddity.videoOutput.json'

const SHM_CAPS = [
  'video/x-raw',
  'width=(int)800',
  'height=(int)600',
  'framerate=(fraction)25/1',
  'pixel-aspect-ratio=(fraction)1/1',
  'format=(string)I420'
].join(', ')

const videoOutput = Quiddity.fromJSON({ ...VIDEO_OUTPUT_QUIDDITY, id: 'videoOutput' })
const sdiInput = Quiddity.fromJSON({ ...SDI_INPUT_QUIDDITY, id: 'sdiInput' })
const sipQuiddity = new Quiddity(SIP_ID, 'sip0', SIP_KIND_ID)

const sipContact = new Contact('contact', 'contact@dev.sip.test')
const sdiShmdata = new Shmdata(`/tmp/${sdiInput.id}/0`, SHM_CAPS, 'test')

describe('MatrixEntry', () => {
  const sdiEntry = new MatrixEntry(sdiInput, null, [sdiShmdata])
  const videoEntry = new MatrixEntry(videoOutput)
  const contactEntry = new MatrixEntry(sipQuiddity, sipContact)

  describe('constructor', () => {
    it('should throw an error if it is intantiated with only a shmdata', () => {
      expect(() => new MatrixEntry(null, null, sdiShmdata)).toThrow(TypeError)
    })

    it('should build an entry with a quiddity and a shmdata', () => {
      expect(() => new MatrixEntry(sdiInput, null, sdiShmdata)).not.toThrow()
    })

    it('should build an entry with a contact', () => {
      expect(() => new MatrixEntry(null, sipContact)).toThrow()
      expect(() => new MatrixEntry(sipQuiddity, sipContact)).not.toThrow()
    })
  })

  describe('id', () => {
    it('should return an unique id from the quiddity id', () => {
      expect(videoEntry.id).toEqual(videoOutput.id)
    })

    it('should return an unique id from the quiddity id and the shmdata path', () => {
      expect(sdiEntry.id).toEqual(`${sdiInput.id}-${sdiShmdata.path}`)
    })

    it('should return an unique id from the contact id and the SIP quiddity id', () => {
      expect(contactEntry.id).toEqual(`${SIP_ID}-${sipContact.id}`)
    })
  })

  describe('shmdataPath', () => {
    it('should not return a shmdata path if it has no shmdata', () => {
      expect(videoEntry.shmdataPaths).toEqual([])
    })

    it('should return a shmdata path if it has a shmdata', () => {
      expect(sdiEntry.shmdataPaths).toEqual([sdiShmdata.path])
    })
  })

  describe('mediaType', () => {
    it('should not return a media type if it has no shmdata', () => {
      expect(videoEntry.mediaTypes).toEqual([])
    })

    it('should return a media type if it has a shmdata', () => {
      expect(sdiEntry.mediaTypes).toEqual([sdiShmdata.mediaType])
    })
  })

  describe('quiddityId', () => {
    it('should return the SIP quiddity id if it is a contact', () => {
      expect(contactEntry.quiddityId).toEqual(SIP_ID)
    })

    it('should return the attached quiddity ID either', () => {
      expect(sdiEntry.quiddityId).toEqual(sdiInput.id)
    })
  })

  describe('contactUri', () => {
    it('should return the contact uri if it is a contact', () => {
      expect(contactEntry.contactUri).toEqual(sipContact.uri)
    })

    it('should return null if it is not a contact', () => {
      expect(sdiEntry.contactUri).toEqual(undefined)
    })
  })

  describe('kindId', () => {
    it('should return the SIP quiddity kind ID if it is a contact', () => {
      expect(contactEntry.kindId).toEqual(SIP_KIND_ID)
    })

    it('should return the attached quiddity kind ID either', () => {
      expect(sdiEntry.kindId).toEqual(sdiInput.kindId)
    })
  })

  describe('isContact', () => {
    it('should return true if it is a contact', () => {
      expect(contactEntry.isContact).toEqual(true)
    })

    it('should return false if it is a quiddity', () => {
      expect(sdiEntry.isContact).toEqual(false)
    })
  })

  describe('hasShmdata', () => {
    it('should return true if it has a shmdata', () => {
      expect(videoEntry.hasShmdata).toEqual(false)
      expect(contactEntry.hasShmdata).toEqual(false)
    })

    it('should return false if it has no shmdata', () => {
      expect(sdiEntry.hasShmdata).toEqual(true)
    })
  })

  describe('isEncodableVideoSource', () => {
    it('should return true if the source can be encoded', () => {
      expect(sdiEntry.isEncodableVideoSource).toEqual(true)
    })

    it('should return false if the source cannot be encoded', () => {
      expect(videoEntry.isEncodableVideoSource).toEqual(false)
    })
  })

  describe('fromJSON', () => {
    it('should return an entry from the given JSON object', () => {
      expect(MatrixEntry.fromJSON({ quiddity: sipQuiddity, contact: sipContact })).toEqual(contactEntry)
      expect(MatrixEntry.fromJSON({ quiddity: sdiInput, shmdatas: [sdiShmdata] })).toEqual(sdiEntry)
      expect(() => MatrixEntry.fromJSON({ shmdatas: sdiShmdata })).toThrow(TypeError)
    })
  })
})
