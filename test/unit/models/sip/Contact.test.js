/* global describe it expect */

import Contact from '@models/sip/Contact'
import { SendStatusEnum, RecvStatusEnum, BuddyStatusEnum, SubStateEnum } from '@models/sip/SipEnums'

import contact0 from '@fixture/models/contact0.sample.json'

describe('Contact', () => {
  const CONTACT_ID = '0'
  const CONTACT_URI = 'test@sip.dev'
  const CONTACT = new Contact(CONTACT_ID, CONTACT_URI)

  describe('constructor', () => {
    it('should throw an error if it is constructed without id', () => {
      expect(() => { new Contact() }).toThrow() // eslint-disable-line no-new
    })

    it('should throw an error if it is constructed without uri', () => {
      expect(() => { new Contact(CONTACT_ID) }).toThrow() // eslint-disable-line no-new
    })

    /* eslint-disable no-new */
    it('should fail if required parameters are not well-typed', () => {
      expect(() => { new Contact(0) }).toThrow()
      expect(() => { new Contact(CONTACT_ID, 0) }).toThrow()
      expect(() => { new Contact(CONTACT_ID, CONTACT_URI, 0) }).toThrow()
      expect(() => { new Contact(CONTACT_ID, CONTACT_URI, undefined, 0) }).toThrow()
      expect(() => { new Contact(CONTACT_ID, CONTACT_URI, undefined, undefined, 0) }).toThrow()
      expect(() => { new Contact(CONTACT_ID, CONTACT_URI, undefined, undefined, undefined, 0) }).toThrow()
      expect(() => { new Contact(CONTACT_ID, CONTACT_URI, undefined, undefined, undefined, undefined, 0) }).toThrow()
      expect(() => { new Contact(CONTACT_ID, CONTACT_URI, undefined, undefined, undefined, undefined, undefined, 0) }).toThrow()
      expect(() => { new Contact(CONTACT_ID, CONTACT_URI, undefined, undefined, undefined, undefined, undefined, undefined, '0') }).toThrow()
      expect(() => { new Contact(CONTACT_ID, CONTACT_URI, undefined, undefined, undefined, undefined, undefined, undefined, undefined, 0) }).toThrow()
    })
    /* eslint-enable no-new */

    it('should ensure incorrect status values correspond to default status enums', () => {
      const contact = new Contact(CONTACT_ID, CONTACT_URI, 'name', 'status', 'statusText', 'subState', 'sendStatus', 'recvStatus', true, [])

      expect(contact.status).toEqual(BuddyStatusEnum.UNKNOWN)
      expect(contact.subState).toEqual(SubStateEnum.UNKNOWN)
      expect(contact.sendStatus).toEqual(SendStatusEnum.UNKNOWN)
      expect(contact.recvStatus).toEqual(RecvStatusEnum.UNKNOWN)
    })

    it('should ensure status values correspond to status enums values', () => {
      const contact = new Contact(CONTACT_ID, CONTACT_URI, 'name', BuddyStatusEnum.ONLINE, 'statusText', SubStateEnum.TERMINATED, SendStatusEnum.CALLING, RecvStatusEnum.RECEIVING, true, [])

      expect(contact.status).toEqual(BuddyStatusEnum.ONLINE)
      expect(contact.subState).toEqual(SubStateEnum.TERMINATED)
      expect(contact.sendStatus).toEqual(SendStatusEnum.CALLING)
      expect(contact.recvStatus).toEqual(RecvStatusEnum.RECEIVING)
    })
  })

  describe('isInSession', () => {
    it('should return true if we are sending to contact', () => {
      const contact = Contact.fromJSON({
        id: '0',
        uri: 'test@dev.sip',
        sendStatus: SendStatusEnum.CALLING
      })

      expect(contact.isInSession).toEqual(true)
    })

    it('should return true if contact is sending to us', () => {
      const contact = Contact.fromJSON({
        id: '0',
        uri: 'test@dev.sip',
        recvStatus: RecvStatusEnum.RECEIVING
      })

      expect(contact.isInSession).toEqual(true)
    })

    it('should return true if contact has shmdata connections', () => {
      const contact = Contact.fromJSON({
        id: '0',
        uri: 'test@dev.sip',
        connections: ['/tmp_test', '/tmp_test2']
      })

      expect(contact.isInSession).toEqual(true)
    })

    it('should return false if we are not sending to contact, contact is not sending to us and no shmdata connection are present', () => {
      const contact = Contact.fromJSON({ id: '0', uri: 'test@dev.sip' })
      expect(contact.isInSession).toEqual(false)
    })
  })

  describe('userName', () => {
    it('should parse a well formated uri', () => {
      expect(Contact.fromJSON({ id: '0', uri: 'test@dev.sip' }).userName).toEqual('test')
    })

    it('should fallback a bad formated uri', () => {
      expect(Contact.fromJSON({ id: '0', uri: 'test.dev.sip' }).userName).toEqual('test_dev_sip')
    })
  })

  describe('sipSever', () => {
    it('should parse a well formated uri', () => {
      expect(Contact.fromJSON({ id: '0', uri: 'test@dev.sip' }).sipServer).toEqual('dev.sip')
    })

    it('should not fallback a bad formated uri', () => {
      expect(Contact.fromJSON({ id: '0', uri: 'test.dev.sip' }).sipServer).toEqual(null)
    })
  })

  describe('fromJSON', () => {
    it('should create a Contact from a valid JSON', () => {
      expect(Contact.fromJSON(contact0)).toEqual(CONTACT)
    })

    it('should throw an error from an invalid JSON', () => {
      expect(() => { Contact.fromJSON({}) }).toThrow()
    })
  })

  describe('toJSON', () => {
    it('should return an identical JSON', () => {
      expect(Contact.fromJSON(contact0).toJSON()).toEqual(contact0)
    })
  })
})
