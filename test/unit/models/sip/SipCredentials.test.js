/* global describe it expect */

import SipCredentials from '@models/sip/SipCredentials'
import sipCredentials0 from '@fixture/models/sipCredentials0.sample.json'

describe('SipCredentials', () => {
  const SIP_USER = 'test'
  const SIP_SERVER = 'sip.dev'
  const SIP_CREDENTIALS = new SipCredentials(SIP_USER, SIP_SERVER)

  const DEFAULT_CREDENTIALS = {
    sipUser: '',
    sipServer: '',
    sipPassword: '',
    turnUser: '',
    turnServer: '',
    stunServer: '',
    turnPassword: '',
    port: 5060,
    destinationPort: 5060
  }

  describe('constructor', () => {
    it('should instantiate the model with defaults', () => {
      const credentials = new SipCredentials()
      expect(credentials.toJSON()).toEqual(DEFAULT_CREDENTIALS)
    })
  })

  describe('uri', () => {
    const CREDENTIALS = new SipCredentials(SIP_USER, SIP_SERVER)

    it('should give the credentials\' URI', () => {
      expect(CREDENTIALS.uri).toEqual(SIP_USER + '@' + SIP_SERVER)
    })
  })

  describe('fromJSON', () => {
    it('should create SipCredentials from a valid JSON', () => {
      expect(SipCredentials.fromJSON(sipCredentials0)).toEqual(SIP_CREDENTIALS)
    })

    it('should resolve an invalid JSON with defaults', () => {
      expect(SipCredentials.fromJSON({}).toJSON()).toEqual(DEFAULT_CREDENTIALS)
    })
  })

  describe('toJSON', () => {
    it('should return an identical JSON', () => {
      expect(SipCredentials.fromJSON(sipCredentials0).toJSON()).toEqual(sipCredentials0)
    })
  })
})
