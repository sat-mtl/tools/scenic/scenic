/* global describe it expect */

import Scene, { DEFAULT_SCENE_ID } from '@models/userTree/Scene'
import scene0 from '@fixture/scene0.sample.json'

describe('Scene', () => {
  const SCENE_INDEX = 10
  const SCENE_ID = 'sceneTest'
  const SCENE_NAME = 'Scene Test'

  const scene = new Scene(SCENE_ID, SCENE_NAME)

  describe('constructor', () => {
    it('should throw an error if it is constructed without id', () => {
      expect(() => { new Scene() }).toThrow() // eslint-disable-line no-new
    })
  })

  describe('index', () => {
    it('should return -1 from an unindexed scene', () => {
      const unindexed = new Scene(SCENE_ID)
      expect(unindexed.index).toEqual(-1)
    })

    it('should return 0 if the scene is the default scene', () => {
      const unindexed = new Scene(DEFAULT_SCENE_ID)
      expect(unindexed.index).toEqual(0)
    })

    it('should return the index of the scene', () => {
      const ID = 1
      const indexed = new Scene(`Scene${ID}`)
      expect(indexed.index).toEqual(ID)
    })
  })

  describe('fromJSON', () => {
    it('should create a Scene from a valid JSON', () => {
      expect(Scene.fromJSON(scene0)).toEqual(scene)
    })

    it('should throw an error from an invalid JSON', () => {
      expect(() => { Scene.fromJSON({}) }).toThrow()
    })
  })

  describe('toJSON', () => {
    it('should return an identical JSON', () => {
      expect(Scene.fromJSON(scene0).toJSON()).toEqual(scene0)
    })
  })

  describe('fromIndex', () => {
    it('should create a new Scene from a specific index', () => {
      expect(Scene.fromIndex(SCENE_INDEX).index).toEqual(SCENE_INDEX)
    })
  })
})
