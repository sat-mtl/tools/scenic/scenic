/* global jest describe it expect beforeEach */
import populateStores from '@stores/populateStores.js'

describe('DrawerStore', () => {
  const TEST_KEY = 'test'
  const ALT_KEY = 'alt'

  let drawerStore

  const DRAWER_TEST_ID = 'test'
  const DRAWER_TEST_ID2 = 'test2'
  const DRAWER_ALT_ID = 'alt'

  beforeEach(() => {
    ({ drawerStore } = populateStores())
  })

  describe('constructor', () => {
    it('should create a drawer store with default values', () => {
      expect(drawerStore.drawers.toJSON()).toEqual(Array.from(new Set()))
      expect(drawerStore.activeDrawer).toEqual(null)
      expect(drawerStore.drawerProps.toJSON()).toEqual(Array.from(new Map()))
    })
  })

  describe('cleanActiveDrawer', () => {
    beforeEach(() => {
      drawerStore.addDrawer(TEST_KEY)
      drawerStore.setActiveDrawer(TEST_KEY)
      drawerStore.clearActiveDrawer = jest.fn()
    })

    it('should clean the active drawer', () => {
      drawerStore.cleanActiveDrawer(TEST_KEY)
      expect(drawerStore.clearActiveDrawer).toHaveBeenCalled()
    })

    it('shouldn\'t clean drawer that is not active', () => {
      drawerStore.cleanActiveDrawer(ALT_KEY)
      expect(drawerStore.clearActiveDrawer).not.toHaveBeenCalled()
    })
  })

  describe('setActiveModal', () => {
    beforeEach(() => {
      drawerStore.addDrawer(DRAWER_TEST_ID)
    })

    it('should set a modal as active', () => {
      drawerStore.setActiveDrawer(DRAWER_TEST_ID)
      expect(drawerStore.activeDrawer).toEqual(DRAWER_TEST_ID)
    })

    it('should not set a modal as active if it wasn\'t registered', () => {
      drawerStore.setActiveDrawer(DRAWER_ALT_ID)
      expect(drawerStore.activeDrawer).not.toEqual(DRAWER_ALT_ID)
    })
  })

  describe('toggleActiveDrawer', () => {
    beforeEach(() => {
      drawerStore.clearActiveDrawer()
    })

    it('should set the active drawer to the passed drawer Id if this one is not already active', () => {
      drawerStore.addDrawer(DRAWER_TEST_ID2)
      drawerStore.setActiveDrawer(DRAWER_TEST_ID2)
      drawerStore.setActiveDrawer = jest.fn()
      drawerStore.toggleActiveDrawer(DRAWER_TEST_ID)
      expect(drawerStore.setActiveDrawer).toHaveBeenCalledWith(DRAWER_TEST_ID)
    })

    it('should clear the active drawer if this one is already active', () => {
      drawerStore.addDrawer(DRAWER_TEST_ID)
      drawerStore.setActiveDrawer(DRAWER_TEST_ID)
      drawerStore.cleanActiveDrawer = jest.fn()
      drawerStore.toggleActiveDrawer(DRAWER_TEST_ID)
      expect(drawerStore.cleanActiveDrawer).toHaveBeenCalledWith(DRAWER_TEST_ID)
    })
  })

  describe('setDrawerProps', () => {
    beforeEach(() => {
      drawerStore.clearActiveDrawer()
    })

    it('should set the drawer props of a registered drawer', () => {
      drawerStore.addDrawer(DRAWER_TEST_ID)
      const testFunction = jest.fn(() => {})
      drawerStore.setDrawerProps(DRAWER_TEST_ID, testFunction)
      expect(drawerStore.drawerProps.keys()).toContain(DRAWER_TEST_ID)
    })

    it('should not set the drawer props of a unregistered drawer', () => {
      const testFunction = jest.fn(() => {})
      drawerStore.setDrawerProps(DRAWER_TEST_ID, testFunction)
      expect(drawerStore.drawerProps.keys()).not.toContain(DRAWER_TEST_ID)
    })
  })
})
