/* global jest describe it expect beforeEach */
import NotificationStore from '@stores/common/NotificationStore'
import Notification from '@models/common/Notification'

import populateStores from '@stores/populateStores.js'
import { bindStores, applyNotificationLog } from '@utils/logger'

const NOTIFICATION = new Notification('message')

describe('NotificationStore', () => {
  let notificationStore, setTimeoutMock

  beforeEach(() => {
    ({ notificationStore } = populateStores())

    jest.useFakeTimers()
    setTimeoutMock = jest.spyOn(global, 'setTimeout')
  })

  describe('constructor', () => {
    it('should require a socket store', () => {
      expect(() => new NotificationStore()).toThrow()
    })

    it('should be instantiated with a socket store', () => {
      expect(notificationStore).toBeDefined()
    })
  })

  describe('applyNotificationLog', () => {
    beforeEach(() => {
      notificationStore.applyNotificationPush = jest.fn()
      bindStores({ notificationStore })
    })

    it('should not push the log when it is not a notification', () => {
      applyNotificationLog('info', { messages: [{ msg: 'test' }] })
      expect(notificationStore.applyNotificationPush).not.toHaveBeenCalled()
    })

    it('should push a log that has a notification flag', () => {
      applyNotificationLog('info', { messages: [{ msg: 'test', notification: true }] })
      expect(notificationStore.applyNotificationPush).toHaveBeenCalled()
    })
  })

  describe('applyNotificationPush', () => {
    const NOTIFICATION_WITH_CLASS = new Notification('message', 'description', null, null, 'Test')

    beforeEach(() => {
      notificationStore.addUserNotification = jest.fn()
      notificationStore.applyDurationTimeout = jest.fn()
    })

    it('should add a notification in the queue in the queue and set a timeout on it', () => {
      notificationStore.applyNotificationPush(NOTIFICATION)
      expect(notificationStore.addUserNotification).toHaveBeenCalled()
      expect(notificationStore.applyDurationTimeout).toHaveBeenCalled()
    })

    it('should not push a muted notification', () => {
      notificationStore.addMuteClass('Test')
      notificationStore.applyNotificationPush(NOTIFICATION_WITH_CLASS)
      expect(notificationStore.addUserNotification).not.toHaveBeenCalled()
      notificationStore.removeMuteClass('Test')
      notificationStore.applyNotificationPush(NOTIFICATION_WITH_CLASS)
      expect(notificationStore.addUserNotification).toHaveBeenCalled()
    })

    it('should push a notification without class', () => {
      notificationStore.addMuteClass('Test')
      notificationStore.applyNotificationPush(NOTIFICATION)
      expect(notificationStore.addUserNotification).toHaveBeenCalled()
    })
  })

  describe('applyDurationTimeout', () => {
    jest.useFakeTimers()

    it('should not apply a timeout if the notification is not in the queue', () => {
      notificationStore.applyDurationTimeout(NOTIFICATION)
      expect(setTimeoutMock).not.toHaveBeenCalled()
    })

    it('should apply a timeout on notification with duration', () => {
      notificationStore.addUserNotification(NOTIFICATION)
      notificationStore.applyDurationTimeout(NOTIFICATION)
      expect(setTimeoutMock).toHaveBeenCalledWith(expect.any(Function), NOTIFICATION.duration)
    })

    it('should not apply a timeout on a permanent notification', () => {
      const notification = new Notification('message', null, undefined, NaN)
      notificationStore.addUserNotification(notification)
      notificationStore.applyDurationTimeout(notification)
      expect(setTimeoutMock).not.toHaveBeenCalled()
    })

    it('should tag the notification as expired when the timeout is expired', () => {
      const notification = new Notification('id', 'message')
      notificationStore.addUserNotification(notification)
      notificationStore.addExpiredNotification = jest.fn()
      notificationStore.applyDurationTimeout(notification)
      expect(notificationStore.addExpiredNotification).not.toHaveBeenCalled()
      jest.runAllTimers()
      expect(notificationStore.addExpiredNotification).toHaveBeenCalledWith(notification.id)
    })
  })

  describe('isExpired', () => {
    it('should flag true if the notification is expired', () => {
      notificationStore.addExpiredNotification(NOTIFICATION.id)
      expect(notificationStore.isExpired(NOTIFICATION)).toEqual(true)
    })

    it('should flag false if the notification isn\'t expired', () => {
      expect(notificationStore.isExpired(NOTIFICATION)).toEqual(false)
    })
  })

  describe('removeNotification', () => {
    beforeEach(() => {
      notificationStore.addUserNotification(NOTIFICATION)
      notificationStore.addExpiredNotification(NOTIFICATION.id)
    })

    it('should remove the notification from the queue and the expired set', () => {
      expect(notificationStore.userNotifications.size).toEqual(1)
      expect(notificationStore.expiredNotifications.size).toEqual(1)
      notificationStore.removeNotification(NOTIFICATION)
      expect(notificationStore.userNotifications.size).toEqual(0)
      expect(notificationStore.expiredNotifications.size).toEqual(0)
    })
  })

  describe('clear', () => {
    beforeEach(() => {
      notificationStore.addUserNotification(NOTIFICATION)
      notificationStore.addExpiredNotification(NOTIFICATION.id)
    })

    it('should clear all the notification structures', () => {
      expect(notificationStore.userNotifications.size).toEqual(1)
      expect(notificationStore.expiredNotifications.size).toEqual(1)
      notificationStore.clear()
      expect(notificationStore.userNotifications.size).toEqual(0)
      expect(notificationStore.expiredNotifications.size).toEqual(0)
    })
  })
})
