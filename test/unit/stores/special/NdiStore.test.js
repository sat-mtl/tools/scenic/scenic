/* global describe it expect jest beforeEach */
import { configure } from 'mobx'

import { Socket } from '@fixture/socket'
import { InitializationError, RequirementError } from '@utils/errors'

import { NDI_STREAM_MODAL_ID } from '@components/modals/NdiStreamModal'

import NdiStore, {
  LOG
} from '@stores/special/NdiStore'

import {
  NDI_INPUT_KIND_ID,
  NDI_OUTPUT_KIND_ID,
  NDI_SNIFFER_KIND_ID,
  NDI_SNIFFER_NICKNAME
} from '@models/quiddity/specialQuiddities'

import populateStores from '@stores/populateStores.js'
import InitStateEnum from '@models/common/InitStateEnum'

import NdiStream from '@models/quiddity/NdiStream'
import Property from '@models/quiddity/Property'
import MatrixEntry from '@models/matrix/MatrixEntry'

import { getKind } from '@fixture/allQuiddityKinds'
import { ndiSniffer, ndiOutput, ndiInput } from '@fixture/allQuiddities'

configure({ safeDescriptors: false })

const { INITIALIZED, NOT_INITIALIZED } = InitStateEnum

const NDI_SNIFFER_INIT = {
  id: 'ndiSniffer',
  kindId: 'scenicNdiSniffer',
  isHidden: true,
  isProtected: true,
  properties: {
    started: true
  },
  userTree: {}
}

jest.mock('hash-it')

describe('NdiStore', () => {
  let socket, socketStore, configStore, quiddityStore, maxLimitStore, modalStore, ndiStore, lockStore, propertyStore, kindStore
  let infoTreeAPI

  const NDI_ENTRY = 'TEST (test)'
  const NDI_STREAM = new NdiStream('test', 'test', 'test', 'test')

  beforeEach(() => {
    ({ socketStore, configStore, quiddityStore, maxLimitStore, modalStore, ndiStore, lockStore, propertyStore, kindStore } = populateStores())
    LOG.error = jest.fn()

    socket = new Socket()
    socketStore.setActiveSocket(socket)

    jest.spyOn(ndiStore, 'ndiSnifferId', 'get').mockReturnValue(NDI_SNIFFER_NICKNAME)
  })

  describe('constructor', () => {
    beforeEach(() => {
      ndiStore.handleNdiInputRequest = jest.fn()
      ndiStore.handleQuiddityStoreInitialization = jest.fn()
      ndiStore.handleSocketChange = jest.fn()
    })

    it('should NdiStreamsStore to be defined', () => {
      expect(ndiStore).toBeDefined()
      expect(ndiStore.isNetworkScanned).toEqual(false)
      expect(ndiStore.isNdiInputRequested).toEqual(false)
      expect(ndiStore.ndiStreams).toEqual([])
      expect(ndiStore.selectedNdiStream).toEqual(null)
    })

    it('should fail if the configStore is not given', () => {
      expect(() => new NdiStore(socketStore)).toThrow(RequirementError)
    })

    it('should fail if the quiddityStore is not given', () => {
      expect(() => new NdiStore(socketStore, configStore)).toThrow(RequirementError)
    })

    it('should fail if the maxLimitStore is not given', () => {
      expect(() => new NdiStore(socketStore, configStore, quiddityStore)).toThrow(RequirementError)
    })

    it('should fail if the modalStore is not given', () => {
      expect(() => new NdiStore(socketStore, configStore, quiddityStore, maxLimitStore)).toThrow(RequirementError)
    })

    it('should react to the SocketStore\'s active socket change', () => {
      socketStore.activeSocket = new Socket()
      expect(ndiStore.handleSocketChange).toHaveBeenCalledWith(socketStore.activeSocket)
    })

    it('should react and handle NDI request the stream are requested', () => {
      ndiStore.setNdiInputRequestFlag(true)
      expect(ndiStore.handleNdiInputRequest).toHaveBeenCalledWith(true)
    })

    it('should react to the QuiddityStore\'s initialization', () => {
      quiddityStore.setInitState(INITIALIZED)
      expect(ndiStore.handleQuiddityStoreInitialization).toHaveBeenCalled()
    })

    it('should flag the NdiOutput as lockable', () => {
      expect(lockStore.lockableKindIds.has(NDI_OUTPUT_KIND_ID)).toEqual(true)
    })
  })

  describe('initialize', () => {
    function addNdiSniffer () {
      kindStore.addKind(ndiSniffer)
      quiddityStore.addQuiddity(ndiSniffer)
    }

    beforeEach(() => {
      ndiStore.createNdiSnifferQuiddity = jest.fn()
    })

    it('should not fallback if the quiddity store is not initialized', async () => {
      expect(ndiStore.initialize()).rejects.toThrow(InitializationError)
      expect(ndiStore.createNdiSnifferQuiddity).not.toHaveBeenCalled()
    })

    it('should not fallback if the NDI sniffer was already created', async () => {
      addNdiSniffer()
      quiddityStore.setInitState(InitStateEnum.INITIALIZED)
      await ndiStore.initialize()
      expect(ndiStore.createNdiSnifferQuiddity).not.toHaveBeenCalled()
    })

    it('should fallback the NDI sniffer if the quiddity is initialized and the NDI sniffer was not created', async () => {
      quiddityStore.setInitState(InitStateEnum.INITIALIZED)
      await ndiStore.initialize()
      expect(ndiStore.createNdiSnifferQuiddity).toHaveBeenCalled()
    })
  })

  describe('createNdiSnifferQuiddity', () => {
    beforeEach(() => {
      quiddityStore.setInitState(InitStateEnum.INITIALIZED)
      configStore.findInitialConfiguration = jest.fn().mockReturnValue(NDI_SNIFFER_INIT)
      quiddityStore.applyQuiddityCreation = jest.fn().mockResolvedValue(true)
    })

    it('should request NDISniffer creation if it is not created', async () => {
      await ndiStore.createNdiSnifferQuiddity()
      configStore.findInitialConfiguration = jest.fn().mockReturnValue(true)
      expect(quiddityStore.applyQuiddityCreation).toHaveBeenCalledWith(NDI_SNIFFER_KIND_ID, NDI_SNIFFER_NICKNAME, NDI_SNIFFER_INIT.properties, NDI_SNIFFER_INIT.userTree)
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should request the NDISniffer quiddity creation with no default properties and userTree if they don\'t exist', async () => {
      configStore.findInitialConfiguration = jest.fn().mockReturnValue(null)
      await ndiStore.createNdiSnifferQuiddity()
      expect(configStore.findInitialConfiguration).toHaveBeenCalledWith(NDI_SNIFFER_KIND_ID, NDI_SNIFFER_NICKNAME)
      expect(quiddityStore.applyQuiddityCreation).toHaveBeenCalledWith(NDI_SNIFFER_KIND_ID, NDI_SNIFFER_NICKNAME)
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should log an error if the request has failed', async () => {
      quiddityStore.applyQuiddityCreation = jest.fn().mockRejectedValue(false)
      await ndiStore.createNdiSnifferQuiddity()
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('findNdiStream', () => {
    beforeEach(() => {
      ndiStore.setNdiStreams([NDI_STREAM])
    })

    it('should find a NDI stream from its label', () => {
      expect(ndiStore.findNdiStream('test')).toEqual(NDI_STREAM)
    })

    it('should return null if no stream is found', () => {
      expect(ndiStore.findNdiStream('fail')).toEqual(null)
    })
  })

  describe('handleSocketChange', () => {
    beforeEach(() => {
      ({ infoTreeAPI } = socketStore.APIs)
      ndiStore.handleSnifferOutputUpdate = jest.fn()
      ndiStore.clear = jest.fn()
    })

    it('should define the APIs if there is a new socket', () => {
      const onGraftSpy = jest.spyOn(infoTreeAPI, 'onGrafted')

      const newSocket = new Socket()

      ndiStore.handleSocketChange(newSocket)
      expect(ndiStore.clear).toHaveBeenCalled()

      expect(onGraftSpy).toHaveBeenCalledWith(
        expect.any(Function),
        expect.any(Function),
        expect.any(Function)
      )
    })

    it('should clear the store if the new socket is null', () => {
      ndiStore.handleSocketChange(null)
      expect(ndiStore.clear).toHaveBeenCalled()
    })
  })

  describe('handleStartedPropertyChange', () => {
    const ndiEntry = new MatrixEntry(ndiOutput)

    function addNdiOutput () {
      kindStore.addKind(getKind(ndiOutput))
      quiddityStore.addQuiddity(ndiOutput)
    }

    function startNdiOutput (isStarted = false) {
      propertyStore.addProperty(ndiOutput.id, new Property('started', 'boolean'))
      propertyStore.setPropertyValue(ndiOutput.id, 'started', isStarted)
    }

    it('should be triggered when a started property is changed', () => {
      ndiStore.handleStartedPropertyChange = jest.fn()
      startNdiOutput()
      expect(ndiStore.handleStartedPropertyChange).toHaveBeenCalled()
    })

    it('should apply a lock on the ndiOutput', () => {
      lockStore.setLock = jest.fn()
      addNdiOutput()
      ndiStore.handleStartedPropertyChange()
      expect(lockStore.setLock).toHaveBeenCalledWith(ndiEntry, false)
      startNdiOutput(true)
      expect(lockStore.setLock).toHaveBeenCalledWith(ndiEntry, true)
    })
  })

  describe('handleQuiddityStoreInitialization', () => {
    beforeEach(() => {
      ndiStore.clear = jest.fn()
      ndiStore.setInitState = jest.fn()
    })

    it('should clear the Store if the QuiddityStore gets uninitialized', async () => {
      quiddityStore.setInitState(InitStateEnum.INITIALIZED)
      expect(ndiStore.clear).not.toHaveBeenCalled()
      quiddityStore.setInitState(InitStateEnum.NOT_INITIALIZED)
      expect(ndiStore.clear).toHaveBeenCalled()
    })
  })

  describe('handleNdiInputRequest', () => {
    beforeEach(() => {
      modalStore.setActiveModal = jest.fn()
      modalStore.cleanActiveModal = jest.fn()
    })

    it('should fallback NdiStreams if the NDI request status is set to true and if the network is not scanned', () => {
      ndiStore.populateNdiStreams = jest.fn()
      ndiStore.setIsNetworkScanned(false)
      ndiStore.setNdiInputRequestFlag(true)
      expect(ndiStore.populateNdiStreams).toHaveBeenCalled()
    })

    it('should not fallback NdiStreams if the NDI request status is set to true and if the network is scanned', () => {
      ndiStore.populateNdiStreams = jest.fn()
      ndiStore.setIsNetworkScanned(true)
      ndiStore.setNdiInputRequestFlag(true)
      expect(ndiStore.populateNdiStreams).not.toHaveBeenCalled()
    })

    it('should not fallback NdiStreams if NDI request status is set to false', () => {
      ndiStore.populateNdiStreams = jest.fn()
      ndiStore.setNdiInputRequestFlag(false)
      expect(ndiStore.populateNdiStreams).not.toHaveBeenCalled()
    })

    it('should make NdiStreamModal active or inactive depending on NDI request status', async () => {
      ndiStore.setNdiInputRequestFlag(true)
      await ndiStore.populateNdiStreams()
      expect(modalStore.setActiveModal).toHaveBeenCalledWith(NDI_STREAM_MODAL_ID)
      ndiStore.setNdiInputRequestFlag(false)
      expect(modalStore.cleanActiveModal).toHaveBeenCalledWith(NDI_STREAM_MODAL_ID)
    })
  })

  describe('populateNdiStreams', () => {
    beforeEach(() => {
      ({ infoTreeAPI } = socketStore.APIs)
      ndiStore.handleSnifferOutputUpdate = jest.fn()
    })

    it('should query the NDISniffer tree and handle the output', async () => {
      infoTreeAPI.get = jest.fn().mockResolvedValue('output')
      await ndiStore.populateNdiStreams()
      expect(ndiStore.handleSnifferOutputUpdate).toHaveBeenCalledWith('output')
      infoTreeAPI.get = jest.fn().mockResolvedValue('')
      await ndiStore.populateNdiStreams()
      expect(ndiStore.handleSnifferOutputUpdate).toHaveBeenCalledWith('')
    })

    it('should log an error if the NDI Sniffer query failed', async () => {
      infoTreeAPI.get = jest.fn().mockRejectedValue(false)
      await ndiStore.populateNdiStreams()
      expect(ndiStore.handleSnifferOutputUpdate).not.toHaveBeenCalled()
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('updateNdiStreamSelection', () => {
    beforeEach(() => {
      ndiStore.setNdiStreams([NDI_STREAM])
      ndiStore.setNdiStreamSelection = jest.fn()
    })

    it('should update the NDI stream selection from a label', () => {
      ndiStore.updateNdiStreamSelection(NDI_STREAM.label)
      expect(ndiStore.setNdiStreamSelection).toHaveBeenCalledWith(NDI_STREAM.toOption())
    })

    it('should reset the selection if no stream is found', () => {
      ndiStore.updateNdiStreamSelection('fail')
      expect(ndiStore.setNdiStreamSelection).toHaveBeenCalledWith()
    })
  })

  describe('handleSnifferOutputUpdate', () => {
    beforeEach(() => {
      ndiStore.setNdiStreams = jest.fn()
    })

    it('should set the NDI streams if output contains something', () => {
      ndiStore.handleSnifferOutputUpdate(NDI_ENTRY)
      expect(ndiStore.setNdiStreams).toHaveBeenCalledWith([
        NdiStream.fromNdiEntry(NDI_ENTRY)
      ])
      expect(ndiStore.isNetworkScanned).toEqual(true)
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should clear the NDI entries if output contains nothing', () => {
      ndiStore.handleSnifferOutputUpdate('')
      expect(ndiStore.setNdiStreams).toHaveBeenCalledWith([])
      expect(ndiStore.isNetworkScanned).toEqual(true)
      ndiStore.handleSnifferOutputUpdate('null')
      expect(ndiStore.setNdiStreams).toHaveBeenCalledWith([])
      expect(ndiStore.isNetworkScanned).toEqual(true)
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should log an error and clear the NDI entries if the query failed', async () => {
      ndiStore.handleSnifferOutputUpdate('fail')
      expect(ndiStore.setNdiStreams).toHaveBeenCalledWith([])
      expect(ndiStore.isNetworkScanned).toEqual(true)
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('applyNdiInputQuiddityCreation', () => {
    beforeEach(() => {
      quiddityStore.setInitState(INITIALIZED)
      quiddityStore.applyQuiddityCreation = jest.fn().mockResolvedValue(true)
      ndiStore.setNdiStreams([NDI_STREAM])
      kindStore.addKind(getKind(ndiInput))
    })

    it('should create an NDIInput quiddity from the selected stream', async () => {
      ndiStore.setNdiStreamSelection(NDI_STREAM.toOption())
      await ndiStore.applyNdiInputQuiddityCreation()
      expect(quiddityStore.applyQuiddityCreation).toHaveBeenCalledWith(NDI_INPUT_KIND_ID, null, NDI_STREAM.properties)
    })

    it('should create an NDIInput quiddity from the label given as argument', async () => {
      await ndiStore.applyNdiInputQuiddityCreation(NDI_STREAM.label)
      expect(quiddityStore.applyQuiddityCreation).toHaveBeenCalledWith(NDI_INPUT_KIND_ID, null, NDI_STREAM.properties)
    })

    it('should not create an NDIInput if quiddityStore is not initialized', async () => {
      quiddityStore.setInitState(NOT_INITIALIZED)

      await ndiStore.applyNdiInputQuiddityCreation(NDI_STREAM.label)
      expect(quiddityStore.applyQuiddityCreation).not.toHaveBeenCalled()
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should log an error if no NDI stream is selected and given label is invalid', async () => {
      await ndiStore.applyNdiInputQuiddityCreation('invalid')
      expect(quiddityStore.applyQuiddityCreation).not.toHaveBeenCalled()
      expect(LOG.error).toHaveBeenCalled()
    })

    it('should log an error if no NDI stream is selected and label is given as argument', async () => {
      await ndiStore.applyNdiInputQuiddityCreation()
      expect(quiddityStore.applyQuiddityCreation).not.toHaveBeenCalled()
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('setNdiStreamSelection', () => {
    it('should set selected NDI stream with provided object', () => {
      ndiStore.setNdiStreamSelection(NDI_STREAM)
      expect(ndiStore.selectedNdiStream).toEqual(NDI_STREAM)
      ndiStore.setNdiStreamSelection()
      expect(ndiStore.selectedNdiStream).toEqual(null)
    })
  })

  describe('setNdiInputRequestFlag', () => {
    it('should set the isNdiInputRequested flag with given value', () => {
      ndiStore.handleNdiInputRequest = jest.fn()

      ndiStore.setNdiInputRequestFlag(true)
      expect(ndiStore.isNdiInputRequested).toEqual(true)
      ndiStore.setNdiInputRequestFlag(false)
      expect(ndiStore.isNdiInputRequested).toEqual(false)
    })
  })

  describe('setNdiStreams', () => {
    it('should set NDI streams with provided streams', () => {
      ndiStore.setNdiStreams([NDI_STREAM])
      expect(ndiStore.ndiStreams).toEqual([NDI_STREAM])
      ndiStore.setNdiStreams([])
      expect(ndiStore.ndiStreams).toEqual([])
    })
  })

  describe('clear', () => {
    beforeEach(() => {
      ndiStore.setInitState = jest.fn()
    })
    it('should clear store', () => {
      ndiStore.setNdiStreams([NDI_STREAM])
      expect(ndiStore.ndiStreams).toEqual([NDI_STREAM])
      ndiStore.clear()
      expect(ndiStore.ndiStreams).toEqual([])
      expect(ndiStore.setInitState).toHaveBeenCalled()
    })
  })
})
