/* global describe it expect beforeEach */

import populateStores from '@stores/populateStores.js'

import MatrixCategoryEnum from '@models/matrix/MatrixCategoryEnum'
import Contact from '@models/sip/Contact'

import LayoutHelper from '@stores/matrix/LayoutHelper'

import { sip, sdiInput, videoOutput, ndiOutput, rtmpOutput } from '@fixture/allQuiddities'
import { getKind } from '@fixture/allQuiddityKinds'

describe('LayoutHelper', () => {
  const { COMMON, NDI, RTMP, SIP } = MatrixCategoryEnum

  const contact = new Contact('contact', 'contact@dev.sip.test')

  let matrixStore, quiddityStore, contactStore, kindStore
  let layoutHelper

  beforeEach(() => {
    ({ matrixStore, quiddityStore, contactStore, kindStore } = populateStores())
    layoutHelper = new LayoutHelper(matrixStore)
  })

  function addAllMockedDestinations () {
    kindStore.addKind(getKind(videoOutput))
    quiddityStore.addQuiddity(videoOutput)

    kindStore.addKind(getKind(ndiOutput))
    quiddityStore.addQuiddity(ndiOutput)

    kindStore.addKind(getKind(rtmpOutput))
    quiddityStore.addQuiddity(rtmpOutput)

    kindStore.addKind(getKind(sip))
    quiddityStore.addQuiddity(sip)

    contactStore.addContact(contact)
    contactStore.addContactToSession(contact)
  }

  function addAllMockedSources () {
    kindStore.addKind(getKind(sdiInput))
    quiddityStore.addQuiddity(sdiInput)
  }

  describe('columnHeadingsAreasTemplate', () => {
    it('should return only the `matrix-header` area if no destination is instantiated', () => {
      expect(layoutHelper.columnHeadingsAreasTemplate).toEqual([
        layoutHelper.getMatrixHeaderAreaKey()
      ])
    })

    it('should return areas and separators for all destination categories', () => {
      addAllMockedDestinations()

      expect(layoutHelper.columnHeadingsAreasTemplate).toEqual([
        layoutHelper.getMatrixHeaderAreaKey(),
        layoutHelper.getDestinationAreaKey(COMMON),
        layoutHelper.getDestinationSeparatorAreaKey(NDI),
        layoutHelper.getDestinationAreaKey(NDI),
        layoutHelper.getDestinationSeparatorAreaKey(RTMP),
        layoutHelper.getDestinationAreaKey(RTMP),
        layoutHelper.getDestinationSeparatorAreaKey(SIP),
        layoutHelper.getDestinationAreaKey(SIP)
      ])
    })
  })

  describe('getRowAreasTemplate', () => {
    it('should return only the source header area if no destination is instantiated', () => {
      expect(layoutHelper.getRowAreasTemplate(COMMON)).toEqual([
        layoutHelper.getSourceAreaKey(COMMON)
      ])
    })

    it('should return areas and separators for all destination\'s categories corresponding to the source category', () => {
      addAllMockedDestinations()

      expect(layoutHelper.getRowAreasTemplate(COMMON)).toEqual([
        layoutHelper.getSourceAreaKey(COMMON),
        layoutHelper.getConnectionAreaKey(COMMON, COMMON),
        layoutHelper.getDestinationSeparatorAreaKey(NDI),
        layoutHelper.getConnectionAreaKey(COMMON, NDI),
        layoutHelper.getDestinationSeparatorAreaKey(RTMP),
        layoutHelper.getConnectionAreaKey(COMMON, RTMP),
        layoutHelper.getDestinationSeparatorAreaKey(SIP),
        layoutHelper.getConnectionAreaKey(COMMON, SIP)
      ])
    })
  })

  describe('gridTemplateAreasStyleProperty', () => {
    it('should get row areas for every sources categories', () => {
      addAllMockedSources()

      matrixStore.setSourceCategories(matrixStore.makeSourceCategories())

      expect(layoutHelper.gridTemplateAreasStyleProperty).toEqual([
        layoutHelper.getMatrixHeaderAreaKey(),
        layoutHelper.getSourceAreaKey(COMMON)
      ].map(p => `'${p}'`).join(' '))
    })
  })

  describe('gridTemplateColumnsStyleProperty', () => {
    it('should return `min-content` when no destination is instantiated', () => {
      expect(layoutHelper.gridTemplateColumnsStyleProperty).toEqual('min-content')
    })

    it('should repeat 2 times `max-content` for every destination categories', () => {
      addAllMockedDestinations()

      expect(layoutHelper.gridTemplateColumnsStyleProperty).toEqual(
        `repeat(${[COMMON, NDI, RTMP, SIP].length * 2}, max-content)`
      )
    })
  })
})
