/* global describe it expect beforeEach jest */

import { configure } from 'mobx'
import MatrixStore from '@stores/matrix/MatrixStore'
import { SIP_ID, SIP_KIND_ID, NDI_OUTPUT_KIND_ID, RTMP_KIND_ID, NDI_INPUT_KIND_ID } from '@models/quiddity/specialQuiddities'
import populateStores from '@stores/populateStores.js'

import Quiddity from '@models/quiddity/Quiddity'
import Kind from '@models/quiddity/Kind'
import MatrixEntry from '@models/matrix/MatrixEntry'
import MatrixCategoryEnum from '@models/matrix/MatrixCategoryEnum'
import Shmdata from '@models/shmdata/Shmdata'
import Contact from '@models/sip/Contact'

import SDI_INPUT_QUIDDITY from '@fixture/models/quiddity.sdiInput1.json'
import SIPMEDIA0_INPUT_QUIDDITY from '@fixture/models/quiddity.sipMedia0.json'
import SIPMEDIA1_INPUT_QUIDDITY from '@fixture/models/quiddity.sipMedia1.json'
import SIPMEDIA2_INPUT_QUIDDITY from '@fixture/models/quiddity.sipMedia2.json'
import SIPMEDIA3_INPUT_QUIDDITY from '@fixture/models/quiddity.sipMedia3.json'
import SIP_QUIDDITY_WITH_SHMDATA from '@fixture/models/quiddity.sip2.json'
import VIDEO_OUTPUT_QUIDDITY from '@fixture/models/quiddity.videoOutput.json'
import NDI_INPUT_QUIDDITY from '@fixture/models/quiddity.ndiInput0.json'
import NDI_OUTPUT_QUIDDITY from '@fixture/models/quiddity.ndiOutput0.json'
import Connection from '@models/userTree/Connection'
import { video, ndiVideoInputShmdata, ndiAudioInputShmdata, ndiVideoInputShmdataPath, ndiAudioInputShmdataPath } from '@fixture/allShmdatas'
import QUIDDITY_KINDS from '@fixture/description.classes.json'
import { DISPLAY_ENCODERS_ID } from '@stores/common/SettingStore'

configure({ safeDescriptors: false })
describe('MatrixStore', () => {
  let matrixStore
  let quiddityStore, kindStore
  let shmdataStore
  let contactStore
  let propertyStore
  let orderStore
  let sceneStore
  let lockStore
  let connectionStore
  let sipStore
  let settingStore
  let encoderStore
  let capsStore

  const quiddityKinds = Object.values(QUIDDITY_KINDS.kinds).map(Kind.fromJSON)
  const contact0 = new Contact('contact0', 'contact0@dev.sip.test')
  const contact1 = new Contact('contact1', 'contact1@dev.sip.test')

  const videoOutput0 = Quiddity.fromJSON({ ...VIDEO_OUTPUT_QUIDDITY, id: 'videoOutput0' })
  const videoOutput1 = Quiddity.fromJSON({ ...VIDEO_OUTPUT_QUIDDITY, id: 'videoOutput1' })

  const ndiInput0 = Quiddity.fromJSON({ ...NDI_INPUT_QUIDDITY, id: 'ndiInput0', kindId: NDI_INPUT_KIND_ID })
  const ndiOutput0 = Quiddity.fromJSON({ ...NDI_OUTPUT_QUIDDITY, id: 'ndiOutput0' })

  const ndi0 = Quiddity.fromJSON({ ...VIDEO_OUTPUT_QUIDDITY, id: 'ndiOutput0', kindId: NDI_OUTPUT_KIND_ID })
  const ndi1 = Quiddity.fromJSON({ ...VIDEO_OUTPUT_QUIDDITY, id: 'ndiOutput1', kindId: NDI_OUTPUT_KIND_ID })

  const rtmp0 = Quiddity.fromJSON({ ...VIDEO_OUTPUT_QUIDDITY, id: 'rtmpOutput0', kindId: RTMP_KIND_ID })
  const rtmp1 = Quiddity.fromJSON({ ...VIDEO_OUTPUT_QUIDDITY, id: 'rtmpOutput1', kindId: RTMP_KIND_ID })

  const sdiInput0 = Quiddity.fromJSON({ ...SDI_INPUT_QUIDDITY, id: 'sdiInput0', nickname: 'sdiInput0', kindId: 'sdiInput0' })
  const sdiInput1 = Quiddity.fromJSON({ ...SDI_INPUT_QUIDDITY, id: 'sdiInput1', nickname: 'sdiInput1', kindId: 'sdiInput1' })
  const sdiInput2 = Quiddity.fromJSON({ ...SDI_INPUT_QUIDDITY, id: 'sdiInput2', nickname: 'sdiInput2', kindId: 'sdiInput2' })

  const sdiShmdata0 = new Shmdata(`/tmp/${sdiInput0.id}/0`, 'video/x-raw', 'test')
  const sdiShmdata1 = new Shmdata(`/tmp/${sdiInput1.id}/0`, 'video/x-raw', 'test')
  const sdiShmdata2 = new Shmdata('/tmp/switcher_sdiInput20_1_video', 'video/x-raw', 'video')
  const sdiShmdataEnc = new Shmdata('/tmp/switcher_sdiInput20_2_video-encoded', 'video/x-h264', 'compressed video')

  const connection = new Connection(sdiInput0.id, videoOutput0.id, video.mediaType)
  const sipMedia0 = Quiddity.fromJSON({ ...SIPMEDIA0_INPUT_QUIDDITY, id: 'sipMedia0' })
  const sipMedia1 = Quiddity.fromJSON({ ...SIPMEDIA1_INPUT_QUIDDITY, id: 'sipMedia1' })
  const sipMedia2 = Quiddity.fromJSON({ ...SIPMEDIA2_INPUT_QUIDDITY, id: 'sipMedia2' })
  const sipMedia3 = Quiddity.fromJSON({ ...SIPMEDIA3_INPUT_QUIDDITY, id: 'sipMedia3' })

  const sipMediaShmdata0 = new Shmdata('/tmp/switcher_scenic8000_3_video-0', 'video/x-raw', 'video')
  const sipMediaShmdata1 = new Shmdata('/tmp/switcher_scenic8000_3_video-2', 'video/x-raw', 'video')
  const sipMediaShmdata2 = new Shmdata('/tmp/switcher_scenic8000_3_audio-1', 'audio/x-raw', 'audio')

  const sipQuiddity = new Quiddity(SIP_ID, 'sip', SIP_KIND_ID)
  // This sip quiddity has shmdata writer compatibles with the sipMedia0, sipMedia1, sipMedia2 and sipMedia3
  const sipQuiddityWithShmdatas = Quiddity.fromJSON({ ...SIP_QUIDDITY_WITH_SHMDATA })

  beforeEach(() => {
    ({
      matrixStore,
      quiddityStore,
      propertyStore,
      orderStore,
      shmdataStore,
      contactStore,
      sceneStore,
      lockStore,
      connectionStore,
      kindStore,
      sipStore,
      settingStore,
      encoderStore,
      capsStore
    } = populateStores())
  })

  function addAllMockedKinds () {
    quiddityKinds.forEach(c => kindStore.addKind(c))
  }

  function addAllMockedContacts () {
    [contact0, contact1].forEach(contact => {
      contactStore.addContact(contact)
      contactStore.addContactToSession(contact)
    })
  }

  function addAllMockedDestinations () {
    quiddityStore.addQuiddity(videoOutput0)
    quiddityStore.addQuiddity(videoOutput1)
  }

  function addAllMockedStartableSources () {
    quiddityStore.addQuiddity(sdiInput0)
    quiddityStore.addQuiddity(sdiInput1)
  }

  function addAllLocalMockedSources () {
    quiddityStore.addQuiddity(sdiInput0)
    quiddityStore.addQuiddity(sdiInput1)
  }

  function addAllMockedSipSources () {
    quiddityStore.addQuiddity(sipQuiddityWithShmdatas)

    quiddityStore.addQuiddity(sipMedia0)
    quiddityStore.addQuiddity(sipMedia1)
    quiddityStore.addQuiddity(sipMedia2)
    quiddityStore.addQuiddity(sipMedia3)
  }

  function addAllMockedShmdatas () {
    shmdataStore.addShmdata(sdiShmdata0)
    shmdataStore.addShmdata(sdiShmdata1)

    shmdataStore.addShmdata(sipMediaShmdata0)
    shmdataStore.addShmdata(sipMediaShmdata1)
    shmdataStore.addShmdata(sipMediaShmdata2)

    shmdataStore.addWritingShmdata(sdiInput0.id, sdiShmdata0.path)
    shmdataStore.addWritingShmdata(sdiInput1.id, sdiShmdata1.path)

    shmdataStore.addWritingShmdata(sipMedia0.id, sipMediaShmdata0.path)
    shmdataStore.addWritingShmdata(sipMedia1.id, sipMediaShmdata1.path)
    shmdataStore.addWritingShmdata(sipMedia2.id, sipMediaShmdata2.path)
  }

  describe('constructor', () => {
    it('should throw an error if the quiddity store is omitted', () => {
      expect(() => new MatrixStore()).toThrow(TypeError)
    })

    it('should throw an error if the property store is omitted', () => {
      expect(() => new MatrixStore(quiddityStore)).toThrow(TypeError)
    })

    it('should throw an error if the order store is omitted', () => {
      expect(() => new MatrixStore(quiddityStore, propertyStore)).toThrow(TypeError)
    })

    it('should throw an error if the shmdata store is omitted', () => {
      expect(() => new MatrixStore(quiddityStore, propertyStore, orderStore)).toThrow(TypeError)
    })

    it('should throw an error if the contact store is omitted', () => {
      expect(() => new MatrixStore(quiddityStore, propertyStore, orderStore, shmdataStore)).toThrow(TypeError)
    })

    it('should throw an error if the scene store is omitted', () => {
      expect(() => new MatrixStore(quiddityStore, propertyStore, orderStore, shmdataStore, contactStore)).toThrow(TypeError)
    })

    it('should throw an error if the connection store is omitted', () => {
      expect(() => new MatrixStore(quiddityStore, propertyStore, orderStore, shmdataStore, contactStore, sceneStore)).toThrow(TypeError)
    })

    it('should throw an error if the lock store is omitted', () => {
      expect(() => new MatrixStore(quiddityStore, propertyStore, orderStore, shmdataStore, contactStore, sceneStore, connectionStore)).toThrow(TypeError)
    })

    it('should throw an error if the sip store is omitted', () => {
      expect(() => new MatrixStore(quiddityStore, propertyStore, orderStore, shmdataStore, contactStore, sceneStore, connectionStore, lockStore)).toThrow(TypeError)
    })

    it('should throw an error if the setting store is omitted', () => {
      expect(() => new MatrixStore(quiddityStore, propertyStore, orderStore, shmdataStore, contactStore, sceneStore, connectionStore, lockStore, sipStore)).toThrow(TypeError)
    })

    it('should throw an error if the encoder store is omitted', () => {
      expect(() => new MatrixStore(quiddityStore, propertyStore, orderStore, shmdataStore, contactStore, sceneStore, connectionStore, lockStore, sipStore, settingStore)).toThrow(TypeError)
    })

    it('should throw an error if the caps store is omitted', () => {
      expect(() => new MatrixStore(quiddityStore, propertyStore, orderStore, shmdataStore, contactStore, sceneStore, connectionStore, lockStore, sipStore, settingStore, encoderStore)).toThrow(TypeError)
    })

    it('should not throw an error if all the stores are given', () => {
      expect(() => new MatrixStore(
        quiddityStore,
        propertyStore,
        orderStore,
        shmdataStore,
        contactStore,
        sceneStore,
        connectionStore,
        lockStore,
        sipStore,
        settingStore,
        encoderStore,
        capsStore
      )).not.toThrow()
    })

    it('should react to displayEncoders change', () => {
      matrixStore.populateSourceCategories = jest.fn()
      settingStore.setSetting(DISPLAY_ENCODERS_ID, false)
      expect(matrixStore.populateSourceCategories).toHaveBeenCalled()
    })
  })

  describe('getAllDestinationQuiddityEntries', () => {
    beforeEach(() => {
      addAllMockedKinds()
      addAllMockedDestinations()
    })

    it('should return all ordered destinations as matrix entries', () => {
      expect(matrixStore.getAllDestinationQuiddityEntries()).toEqual([
        { quiddity: videoOutput0 },
        { quiddity: videoOutput1 }
      ].map(MatrixEntry.fromJSON))
    })
  })

  describe('sourceEntries', () => {
    beforeEach(() => {
      addAllMockedStartableSources()
    })

    it('should return all source entries', () => {
      matrixStore.populateSourceCategories()
      expect(matrixStore.sourceEntries).toEqual([
        { quiddity: sdiInput0 },
        { quiddity: sdiInput1 }
      ].map(MatrixEntry.fromJSON))
    })
  })

  describe('destinationEntries', () => {
    beforeEach(() => {
      addAllMockedDestinations()
    })

    it('should return all destination entries', () => {
      matrixStore.populateDestinationCategories()
      expect(matrixStore.destinationEntries).toEqual([
        { quiddity: videoOutput0 },
        { quiddity: videoOutput1 }
      ].map(MatrixEntry.fromJSON))
    })
  })

  describe('matrixEntries', () => {
    beforeEach(() => {
      addAllMockedDestinations()
    })

    it('should return all matrix entries', () => {
      matrixStore.populateDestinationCategories()
      expect(Array.from(matrixStore.matrixEntries.values())).toEqual([
        { quiddity: videoOutput0 },
        { quiddity: videoOutput1 }
      ].map(MatrixEntry.fromJSON))
    })
  })

  describe('startableSources', () => {
    it('should return an empty set when there are no startable sources', () => {
      addAllMockedSipSources()
      expect(matrixStore.startableSources).toEqual(new Set())
    })
    it('should return all startable sources only', () => {
      addAllMockedSipSources()
      // Adds sdi sources which are startable
      addAllMockedStartableSources()
      // TODO : This should work without a setTimeout but right now it doesnt. The event chain that
      // populates the propertyStore is way too long and on the first iteration, most of the
      // properties are not in the propertyStore.
      // See https://gitlab.com/sat-mtl/tools/scenic/scenic/-/issues/400
      setTimeout(
        () => expect(matrixStore.startableSources).toEqual(new Set().add('sdiInput0').add('sdiInput1')),
        1000
      )
    })
  })

  describe('lockableCategories', () => {
    beforeEach(() => {
      quiddityStore.addQuiddity(ndiOutput0)
    })

    it('should return an empty set of lockable categories when they don\'t exist', () => {
      expect(matrixStore.lockableCategories).toEqual(new Set())
    })

    it('should return all lockable categories when they exist', () => {
      matrixStore.populateDestinationCategories()
      expect(matrixStore.lockableCategories).toEqual(new Set().add(MatrixCategoryEnum.NDI))
    })
  })

  describe('lockedConnections', () => {
    beforeEach(() => {
      lockStore.isLockedConnection = jest.fn().mockReturnValue(true)
    })

    it('should return all locked connections', () => {
      connectionStore.addUserConnection(connection)
      expect(matrixStore.lockedConnections).toEqual(new Set().add(connection))
    })
  })

  describe('destinationQuiddities', () => {
    beforeEach(() => {
      quiddityStore.addQuiddity(ndiOutput0)
      matrixStore.populateDestinationCategories()
    })

    it('should return all destination quiddities', () => {
      expect(matrixStore.destinationQuiddities).toEqual(new Map().set(ndiOutput0.id, new Set().add(ndiOutput0.id)))
    })
  })

  describe('matrixQuiddities', () => {
    beforeEach(() => {
      addAllMockedStartableSources()
      matrixStore.populateSourceCategories()
      addAllMockedDestinations()
      matrixStore.populateDestinationCategories()
    })

    it('should return all matrix quiddities, source and destination ones', () => {
      expect(matrixStore.matrixQuiddities).toEqual(
        new Map().set(sdiInput0.id, new Set().add(sdiInput0.id))
          .set(sdiInput1.id, new Set().add(sdiInput1.id))
          .set(videoOutput0.id, new Set().add(videoOutput0.id))
          .set(videoOutput1.id, new Set().add(videoOutput1.id))
      )
    })
  })

  describe('connectedQuiddities', () => {
    const srcEntry = new MatrixEntry(sdiInput0)
    const destEntry = new MatrixEntry(videoOutput0)

    beforeEach(() => {
      addAllMockedStartableSources()
      matrixStore.populateSourceCategories()
      addAllMockedDestinations()
      matrixStore.populateDestinationCategories()
      matrixStore.findCompatibleShmdata = jest.fn().mockReturnValue({ capabilities: { mediaType: 'video/x-raw' } })
    })

    it('should return a map of all connections hashed by quiddity ids when they are available', () => {
      jest.spyOn(matrixStore, 'matrixConnectionsFromEntries', 'get').mockReturnValue(new Map().set(connection.id, [srcEntry, destEntry]))

      expect(matrixStore.connectedQuiddities).toEqual(
        new Map().set(sdiInput0.id, new Set().add(connection))
          .set(sdiInput1.id, new Set())
          .set(videoOutput0.id, new Set().add(connection))
          .set(videoOutput1.id, new Set())
      )
    })
  })

  describe('makeDestinationCategories', () => {
    beforeEach(() => {
      addAllMockedKinds()
    })

    it('should return all videoOutput destinations in the common category', () => {
      addAllMockedDestinations()

      expect(matrixStore.makeDestinationCategories()).toEqual(new Map([
        [MatrixCategoryEnum.COMMON, [
          { quiddity: videoOutput0 },
          { quiddity: videoOutput1 }
        ].map(MatrixEntry.fromJSON)]
      ]))
    })

    it('should return all NDI quiddities in the NDI category', () => {
      quiddityStore.addQuiddity(ndi0)
      quiddityStore.addQuiddity(ndi1)

      expect(matrixStore.makeDestinationCategories()).toEqual(new Map([
        [MatrixCategoryEnum.NDI, [
          { quiddity: ndi0 },
          { quiddity: ndi1 }
        ].map(MatrixEntry.fromJSON)]
      ]))
    })

    it('should return all RTMP quiddities in the NDI category', () => {
      quiddityStore.addQuiddity(rtmp0)
      quiddityStore.addQuiddity(rtmp1)

      expect(matrixStore.makeDestinationCategories()).toEqual(new Map([
        [MatrixCategoryEnum.RTMP, [
          { quiddity: rtmp0 },
          { quiddity: rtmp1 }
        ].map(MatrixEntry.fromJSON)]
      ]))
    })

    it('should return all contacts in the SIP category', () => {
      quiddityStore.addQuiddity(sipQuiddity)
      addAllMockedContacts()

      expect(matrixStore.makeDestinationCategories()).toEqual(new Map([
        [MatrixCategoryEnum.SIP, [
          { quiddity: sipQuiddity, contact: contact0 },
          { quiddity: sipQuiddity, contact: contact1 }
        ].map(MatrixEntry.fromJSON)]
      ]))
    })
  })

  describe('getSourceQuiddityEntries', () => {
    beforeEach(() => {
      addAllMockedKinds()

      quiddityStore.addQuiddity(ndiInput0)
      quiddityStore.addQuiddity(sdiInput0)
      quiddityStore.addQuiddity(ndiInput0)
      shmdataStore.addShmdata(sdiShmdataEnc)
      shmdataStore.addShmdata(sdiShmdata2)
      shmdataStore.addShmdata(ndiVideoInputShmdata)
      shmdataStore.addShmdata(ndiAudioInputShmdata)
      shmdataStore.addWritingShmdata(ndiInput0.id, ndiVideoInputShmdataPath)
      shmdataStore.addWritingShmdata(ndiInput0.id, ndiAudioInputShmdataPath)
      shmdataStore.addWritingShmdata(sdiInput2.id, sdiShmdataEnc.path)
      shmdataStore.addWritingShmdata(sdiInput2.id, sdiShmdata2.path)
    })

    it('should only return the source if no shmdata is written', () => {
      expect(matrixStore.getSourceQuiddityEntries(sdiInput0)).toEqual([
        { quiddity: sdiInput0 }
      ].map(MatrixEntry.fromJSON))
    })

    it('should return only one source with all its written shmdatas if the display encoder config is false and if the source is not of type ndi', () => {
      settingStore.setSetting(DISPLAY_ENCODERS_ID, false)
      expect(matrixStore.getSourceQuiddityEntries(sdiInput2)).toEqual([
        { quiddity: sdiInput2, shmdatas: [sdiShmdata2, sdiShmdataEnc] }
      ].map(MatrixEntry.fromJSON))
    })

    it('should return two sources each with one shmdata if the display encoder config is false and the source is ndi with two shmdatas', () => {
      expect(matrixStore.getSourceQuiddityEntries(ndiInput0)).toEqual([
        { quiddity: ndiInput0, shmdatas: [ndiVideoInputShmdata] },
        { quiddity: ndiInput0, shmdatas: [ndiAudioInputShmdata] }
      ].map(MatrixEntry.fromJSON))
    })

    it('should return two sources each with one shmdata (raw and encoded) if the display encoder config is true', () => {
      settingStore.setSetting(DISPLAY_ENCODERS_ID, true)

      expect(matrixStore.getSourceQuiddityEntries(sdiInput2)).toEqual([
        { quiddity: sdiInput2, shmdatas: [sdiShmdata2] },
        { quiddity: sdiInput2, shmdatas: [sdiShmdataEnc] }
      ].map(MatrixEntry.fromJSON))
    })
  })

  describe('getAllSourceQuiddityEntries', () => {
    beforeEach(() => {
      addAllMockedKinds()
      addAllLocalMockedSources()
      addAllMockedShmdatas()
    })

    it('should return all sources as matrix entries', () => {
      expect(matrixStore.getAllSourceQuiddityEntries()).toEqual([
        { quiddity: sdiInput0, shmdatas: [sdiShmdata0] },
        { quiddity: sdiInput1, shmdatas: [sdiShmdata1] }
      ].map(MatrixEntry.fromJSON))
    })
  })

  describe('makeSourceCategories', () => {
    beforeEach(() => {
      addAllMockedKinds()
      addAllMockedContacts()
    })

    it('should return an empty map if no source is instantiated', () => {
      expect(matrixStore.makeSourceCategories()).toEqual(new Map())
    })
    describe('with sources this time', () => {
      beforeEach(() => {
        addAllLocalMockedSources()
        addAllMockedShmdatas()
        addAllMockedSipSources()
      })
      it('should have 3 categories in this order : "common", "Waiting for data stream", "contact0@dev.sip.test"', () => {
        expect(Array.from(matrixStore.makeSourceCategories().keys())).toEqual(['common', 'Waiting for data stream', 'contact0@dev.sip.test'])
      })
      it('should return all local sources in the common categories', () => {
        expect(matrixStore.makeSourceCategories().get(MatrixCategoryEnum.COMMON)).toEqual(
          [
            { quiddity: sdiInput0, shmdatas: [sdiShmdata0] },
            { quiddity: sdiInput1, shmdatas: [sdiShmdata1] }
          ].map(MatrixEntry.fromJSON)
        )
      })
      it('should return external source associated with a shmdata in the contact0@dev.sip.test categories grouped by media category', () => {
        expect(matrixStore.makeSourceCategories().get('contact0@dev.sip.test')).toEqual(
          [ // Audio first (sipMedia2) followed by 2 video sources (sipMedia0 sipMedia1)
            { quiddity: sipMedia2, shmdatas: [sipMediaShmdata2] },
            { quiddity: sipMedia0, shmdatas: [sipMediaShmdata0] },
            { quiddity: sipMedia1, shmdatas: [sipMediaShmdata1] }
          ].map(MatrixEntry.fromJSON)
        )
      })
      it('should return external sources not associated with a shmdata in the "Waiting for data stream" category', () => {
        expect(matrixStore.makeSourceCategories().get('Waiting for data stream')).toEqual(
          [
            { quiddity: sipMedia3, shmdatas: [] }
          ].map(MatrixEntry.fromJSON)
        )
      })
    })
  })

  describe('findCompatibleShmdata', () => {
    beforeEach(() => {
      settingStore.setSetting(DISPLAY_ENCODERS_ID, false)
    })

    it('should return the default shmdata if the source is non-sip encodable and the destination is non-sip', () => {
      const srcEntry = new MatrixEntry(sdiInput0, null, [sdiShmdata0, sdiShmdataEnc])
      const destEntry = new MatrixEntry(videoOutput0, null, [])
      expect(matrixStore.findCompatibleShmdata(srcEntry, destEntry)).toEqual(sdiShmdata0)
    })

    it('should return the encoded shmdata if the source is non-sip encodable and the destination is sip', () => {
      settingStore.setSetting(DISPLAY_ENCODERS_ID, false)
      const srcEntry = new MatrixEntry(sdiInput0, null, [sdiShmdata0, sdiShmdataEnc])
      const destEntry = new MatrixEntry(sipQuiddity, null, [])
      expect(matrixStore.findCompatibleShmdata(srcEntry, destEntry)).toEqual(sdiShmdataEnc)
    })

    it('should return the default shmdata if the source is non-encodable and the destination is sip or non-sip', () => {
      settingStore.setSetting(DISPLAY_ENCODERS_ID, false)
      const srcEntry = new MatrixEntry(ndiInput0, null, [ndiAudioInputShmdata])
      const destEntry1 = new MatrixEntry(videoOutput0, null, [])
      const destEntry2 = new MatrixEntry(sipQuiddity, null, [])
      expect(matrixStore.findCompatibleShmdata(srcEntry, destEntry1)).toEqual(ndiAudioInputShmdata)
      expect(matrixStore.findCompatibleShmdata(srcEntry, destEntry2)).toEqual(ndiAudioInputShmdata)
    })
  })
})
