/* global describe it expect jest beforeEach */

import FilterStore, { LOG } from '@stores/matrix/FilterStore'
import { SIP_ID, SIP_KIND_ID } from '@models/quiddity/specialQuiddities'
import { configure } from 'mobx'

import populateStores from '@stores/populateStores.js'

import Quiddity from '@models/quiddity/Quiddity'
import Contact from '@models/sip/Contact'
import MatrixEntry from '@models/matrix/MatrixEntry'

import quiddityKinds, { getKind, sipKind } from '@fixture/allQuiddityKinds'
import { sdiInput, videoOutput, systemUsage, sip, sipMedia } from '@fixture/allQuiddities'

import BUDDY_TREE from '@fixture/buddy.tree.json'

configure({ safeDescriptors: false })

describe('FilterStore', () => {
  let filterStore
  let quiddityStore, kindStore
  let contactStore
  let socketStore
  let sipStore

  const sipQuiddity = new Quiddity(SIP_ID, 'sip0', SIP_KIND_ID)
  const CATEGORIES = ['test0', 'test1']

  beforeEach(() => {
    ({ socketStore, filterStore, quiddityStore, contactStore, kindStore, sipStore } = populateStores())

    LOG.error = jest.fn()
    LOG.warn = jest.fn()

    kindStore.addKind(sipKind)
    quiddityStore.addQuiddity(sipQuiddity)

    jest.spyOn(sipStore, 'sipId', 'get').mockReturnValue(SIP_ID)
  })

  function getQuiddity (json) {
    const isQuid = json instanceof Quiddity
    return isQuid ? json : Quiddity.fromJSON(json)
  }

  function getQuiddityKind (json) {
    const quiddity = getQuiddity(json)

    return quiddityKinds.find(cl => cl.id === quiddity.kindId)
  }

  function getCategory (model, contactId) {
    let category

    if (model instanceof Quiddity || !contactId) {
      category = getQuiddityKind(model).category
    } else if (model instanceof Contact || contactId) {
      category = getQuiddityKind(sipQuiddity).category
    }

    return category
  }

  function setupPartner (id, jsonContact) {
    const contact = Contact.fromJSON({ id, ...jsonContact })

    contactStore.addContact(contact)
    contactStore.addContactToSession(contact)
  }

  function getMatrixEntry (jsonQuid, contactId, jsonContact) {
    let quiddity, contact

    if (jsonQuid) {
      quiddity = getQuiddity(jsonQuid)
    }

    if (contactId && jsonContact) {
      contact = Contact.fromJSON({ id: contactId, ...jsonContact })
    }

    return new MatrixEntry(quiddity, contact)
  }

  describe('constructor', () => {
    it('should be correctly instantiated', () => {
      expect(filterStore).toBeDefined()
    })

    it('should throw an error if not quiddity store is provided', () => {
      expect(() => new FilterStore(socketStore)).toThrow()
    })
  })

  describe('handleCategoryChange', () => {
    beforeEach(() => {
      filterStore.handleCategoryChange = jest.fn()
    })

    it('should apply a reaction when the used kinds change', () => {
      kindStore.addKind(getKind(sdiInput))
      quiddityStore.addQuiddity(sdiInput)

      expect(filterStore.handleCategoryChange).toHaveBeenCalled()
    })

    it('should apply a reaction when the partners change', () => {
      setupPartner('test', BUDDY_TREE['0'])
      expect(filterStore.handleCategoryChange).toHaveBeenCalled()
    })
  })

  describe('orderedCategories', () => {
    beforeEach(() => {
      [sip, sdiInput, videoOutput, systemUsage].forEach(q => {
        kindStore.addKind(getKind(q))
        quiddityStore.addQuiddity(q)
      })
    })

    it('should order all categories from the added quiddities', () => {
      expect(JSON.stringify(filterStore.orderedCategories)).toEqual(
        JSON.stringify(Array.from(new Map([
          [sip.id, getKind(sip).category],
          [sdiInput.id, getKind(sdiInput).category],
          [videoOutput.id, getKind(videoOutput).category],
          [systemUsage.id, getKind(systemUsage).category],
          [sip.id, 'SIP']
        ]))
        )
      )
    })
  })

  describe('userCategories', () => {
    beforeEach(() => {
      [sdiInput, videoOutput, systemUsage].forEach(q => {
        kindStore.addKind(getKind(q))
        quiddityStore.addQuiddity(q)
      })
    })

    it('should return all the categories used in the matrix', () => {
      expect(filterStore.userCategories).toEqual(
        new Set([getKind(sdiInput).category])
      )
    })
  })

  describe('applyQuiddityFilter', () => {
    beforeEach(() => {
      [sip, sdiInput].forEach(q => {
        kindStore.addKind(getKind(q))
        quiddityStore.addQuiddity(q)
      })
    })

    it('should not be filtered when the quiddity isn\'t in the matrix', () => {
      expect(filterStore.applyQuiddityFilter(videoOutput.id)).toEqual(false)
    })

    it('should be filtered when the quiddity is in the matrix', () => {
      expect(filterStore.applyQuiddityFilter(sdiInput.id)).toEqual(true)
    })

    it('should not be filtered when the category is the SIP one and there is no partner', () => {
      expect(filterStore.applyQuiddityFilter(sipQuiddity.id)).toEqual(false)
    })

    it('should be filtered when the category is the SIP one and there is partner', () => {
      setupPartner('test', BUDDY_TREE['0'])
      expect(filterStore.applyQuiddityFilter(sipQuiddity.id)).toEqual(true)
    })
  })

  describe('applyEntryFilter', () => {
    const SDI_ENTRY = getMatrixEntry(sdiInput)
    const VIDEO_ENTRY = getMatrixEntry(videoOutput)
    const SIP_MEDIA_ENTRY = getMatrixEntry(sipMedia)
    const CONTACT_ENTRY = getMatrixEntry(sip, 'test', BUDDY_TREE['0'])

    beforeEach(() => {
      [sdiInput, sip, sipMedia].forEach(q => {
        kindStore.addKind(getKind(q))
        quiddityStore.addQuiddity(q)
      })

      filterStore.addCategoryFilter(getKind(sdiInput).category)
    })

    it('should be filtered when no category is selected', () => {
      expect(filterStore.applyEntryFilter(SDI_ENTRY)).toEqual(true)
    })

    it('should be filtered when the category of the entry\'s quiddity matches the category filters', () => {
      expect(filterStore.applyEntryFilter(SDI_ENTRY)).toEqual(true)
    })

    it('should not be filtered when the category of the entry doesn\'t match the category filters', () => {
      expect(filterStore.applyEntryFilter(VIDEO_ENTRY)).toEqual(false)
    })

    it('should be filtered when it is a contact and the category filters includes the SIP category', () => {
      filterStore.addCategoryFilter(getCategory(sipQuiddity))
      expect(filterStore.applyEntryFilter(CONTACT_ENTRY)).toEqual(true)
    })

    it('should not be filtered when it is a contact and the category filters doesn\'t includes the SIP category', () => {
      expect(filterStore.applyEntryFilter(CONTACT_ENTRY)).toEqual(false)
    })

    it('should be filtered when it is a sip source quiddity and the category filters is SIP', () => {
      filterStore.addCategoryFilter('SIP')
      // the applyEntryFilter function looks for the existence of the shmpath of a matrix entry in
      // the sipShmdatas map. We need to set this manually for this unit test.
      sipStore.sipShmdatas.set(SIP_MEDIA_ENTRY.shmdata?.path, 'test')
      expect(filterStore.applyEntryFilter(SIP_MEDIA_ENTRY)).toEqual(true)
    })

    it('should not be filtered when it is a sip source quiddity and the category filters is not SIP', () => {
      expect(filterStore.applyEntryFilter(SIP_MEDIA_ENTRY)).toEqual(false)
    })

    it('should not be filtered when it is not a sip source quiddity with extshmsrc kindId and the category filters is SIP', () => {
      filterStore.addCategoryFilter('SIP')
      expect(filterStore.applyEntryFilter(SIP_MEDIA_ENTRY)).toEqual(false)
    })
  })

  describe('removeCategoryFilter', () => {
    beforeEach(() => {
      CATEGORIES.map((c) => filterStore.addCategoryFilter(c))
    })

    it('should delete a category filter', () => {
      expect(filterStore.categoryFilters.size).toEqual(CATEGORIES.length)
      filterStore.removeCategoryFilter(CATEGORIES[0])
      expect(filterStore.categoryFilters.size).toEqual(1)
    })
  })

  describe('clearCategoryFilters', () => {
    beforeEach(() => {
      CATEGORIES.map((c) => filterStore.addCategoryFilter(c))
    })

    it('should clear the category filters', () => {
      expect(filterStore.categoryFilters.size).toEqual(CATEGORIES.length)
      filterStore.clearCategoryFilters()
      expect(filterStore.categoryFilters.size).toEqual(0)
    })
  })
})
