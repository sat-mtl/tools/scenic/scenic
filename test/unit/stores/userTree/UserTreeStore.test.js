/* global describe it expect jest beforeEach */

import { Socket } from '@fixture/socket'

import populateStores from '@stores/populateStores.js'
import UserTreeStore, { LOG, UserTreeSyncError } from '@stores/userTree/UserTreeStore'
import InitStateEnum from '@models/common/InitStateEnum'
import {
  USER_TREE_NICKNAME,
  USER_TREE_KIND_ID
} from '@models/quiddity/specialQuiddities'

import USER_TREE_REV1 from '@fixture/models/userTree.revision1.json'

describe('UserTreeStore', () => {
  const USER_TREE_SCENE_PATH = 'scenes'
  const USER_DATA_VALUE = 'TEST'

  let socketStore, quiddityStore, userTreeStore, configStore
  let userTreeAPI

  beforeEach(() => {
    ({ socketStore, quiddityStore, userTreeStore, configStore } = populateStores())

    LOG.error = jest.fn()
    socketStore.setActiveSocket(new Socket())
    userTreeAPI = userTreeStore.socketStore.APIs.userTreeAPI
  })

  describe('UserTreeSyncError', () => {
    it('should be a UserTreeSyncError error', () => {
      const error = new UserTreeSyncError()
      expect(error).toBeInstanceOf(Error)
      expect(error.name).toEqual('UserTreeSyncError')
    })
  })

  describe('constructor', () => {
    it('should fail if the quiddityStore is not provided', () => {
      /* eslint-disable no-new */
      expect(() => { new UserTreeStore(socketStore) }).toThrow()
      /* eslint-enable no-new */
    })

    it('should be correctly instantiated', () => {
      expect(userTreeStore).toBeDefined()
      expect(userTreeStore.initState).toEqual(InitStateEnum.NOT_INITIALIZED)
    })
  })

  describe('fallbackUserTreeQuiddity', () => {
    beforeEach(() => {
      quiddityStore.setInitState(InitStateEnum.INITIALIZED)
      quiddityStore.applyQuiddityCreation = jest.fn().mockResolvedValue(true)
    })

    it('should request the userTree quiddity creation if it was not created', async () => {
      configStore.findInitialConfiguration = jest.fn().mockReturnValue(false)
      await userTreeStore.fallbackUserTreeQuiddity()
      expect(quiddityStore.applyQuiddityCreation).toHaveBeenCalledWith(USER_TREE_KIND_ID, USER_TREE_NICKNAME)
    })

    it('should log an error if the request has failed', async () => {
      quiddityStore.applyQuiddityCreation = jest.fn().mockRejectedValue(false)
      await userTreeStore.fallbackUserTreeQuiddity()
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('fetchUserTree', () => {
    beforeEach(() => {
      userTreeAPI.get = jest.fn().mockResolvedValue(USER_DATA_VALUE)
    })

    it('should fetch the current user data of the userTree', async () => {
      jest.spyOn(userTreeStore, 'userTreeQuiddity', 'get').mockReturnValue(5)
      const userTree = await userTreeStore.fetchUserTree(USER_TREE_SCENE_PATH)
      expect(userTree).toEqual(USER_DATA_VALUE)
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should log an error if the request has failed', async () => {
      userTreeAPI.get = jest.fn().mockRejectedValue(false)
      const userTree = await userTreeStore.fetchUserTree(USER_TREE_SCENE_PATH)
      expect(userTree).toEqual({})
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('applyUserTreeCreation', () => {
    beforeEach(() => {
      userTreeAPI.graft = jest.fn().mockResolvedValue(true)
    })

    it('should set the new user data to the requested branch', async () => {
      jest.spyOn(userTreeStore, 'userTreeQuiddity', 'get').mockReturnValue({ id: USER_TREE_NICKNAME })
      await userTreeStore.applyUserTreeCreation(USER_TREE_SCENE_PATH, USER_TREE_REV1)
      expect(userTreeAPI.graft).toHaveBeenCalledWith(USER_TREE_NICKNAME, USER_TREE_SCENE_PATH, USER_TREE_REV1)
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should log an error if the request has failed', async () => {
      userTreeAPI.graft = jest.fn().mockRejectedValue(false)
      await userTreeStore.applyUserTreeCreation(USER_TREE_SCENE_PATH, USER_TREE_REV1)
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('applyUserTreeRemoval', () => {
    beforeEach(() => {
      userTreeAPI.prune = jest.fn().mockResolvedValue(true)
    })

    it('should remove the user data', async () => {
      jest.spyOn(userTreeStore, 'userTreeQuiddity', 'get').mockReturnValue({ id: USER_TREE_NICKNAME })
      await userTreeStore.applyUserTreeRemoval(USER_TREE_SCENE_PATH)
      expect(userTreeAPI.prune).toHaveBeenCalledWith(USER_TREE_NICKNAME, USER_TREE_SCENE_PATH)
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should log an error if the request has failed', async () => {
      userTreeAPI.prune = jest.fn().mockRejectedValue(false)
      await userTreeStore.applyUserTreeRemoval(USER_TREE_SCENE_PATH)
      expect(LOG.error).toHaveBeenCalled()
    })
  })
})
