/* global describe it expect jest beforeEach */

import { Socket } from '@fixture/socket'
import { configure } from 'mobx'

import ShmdataStore, { LOG } from '@stores/shmdata/ShmdataStore'

import populateStores from '@stores/populateStores.js'
import QuidditiesDescription from '@fixture/description.quiddities.json'
import Shmdata from '@models/shmdata/Shmdata'
import Kind from '@models/quiddity/Kind'
import ShmdataRoleEnum from '@models/shmdata/ShmdataRoleEnum'

import { videoOutput } from '@fixture/allQuiddities'

configure({ safeDescriptors: false })

const { quiddities } = QuidditiesDescription

const OSC_QUIDDITY = quiddities.find(q => q.kind === 'OSCsrc')
const OSC_SINK_QUIDDITY = quiddities.find(q => q.kind === 'OSCsink')
const RTMP_OUTPUT_QUIDDITY = quiddities.find(q => q.kind === 'rtmp')

const SHMDATA_NAME = 'SHMDATA'
const SHMDATA_PATH = `/tmp/${SHMDATA_NAME}`
const TREE_PATH = `.shmdata.writer.${SHMDATA_PATH}`

const SHMDATA = new Shmdata(SHMDATA_PATH, 'caps', 'category')
const SHMDATA_JSON = SHMDATA.toJSON()
const SHMDATA2 = new Shmdata(`another_${SHMDATA_PATH}`, 'caps', 'category')

describe('ShmdataStore', () => {
  let socket, socketStore, shmdataStore, quiddityStore, kindStore
  let infoTreeAPI

  beforeEach(() => {
    ({ socketStore, shmdataStore, quiddityStore, kindStore } = populateStores())

    LOG.debug = jest.fn()
    LOG.error = jest.fn()

    socket = new Socket()
    socketStore.setActiveSocket(socket)
  })

  function getShmdataTreePath (shmdata, role) {
    return `.shmdata.${role}.${shmdata.path}.stat`
  }

  describe('constructor', () => {
    beforeEach(() => {
      shmdataStore.updateFollowingShmdatas = jest.fn()
    })

    it('should fail if the quiddityStore is not given', () => {
      expect(() => new ShmdataStore(socketStore)).toThrow()
    })

    it('should react to the SocketStore\'s active socket change', () => {
      const { socketStore, shmdataStore } = populateStores()

      shmdataStore.handleSocketChange = jest.fn()
      expect(shmdataStore.handleSocketChange).not.toHaveBeenCalled()

      socketStore.setActiveSocket(socket)
      expect(shmdataStore.handleSocketChange).toHaveBeenCalledTimes(1)
      expect(shmdataStore.handleSocketChange).toHaveBeenCalledWith(socketStore.activeSocket)
    })

    it('should react to a new follower quiddity', () => {
      const quidKind = new Kind(videoOutput.id, videoOutput.kindId, videoOutput.description, [ShmdataRoleEnum.READER])

      expect(shmdataStore.updateFollowingShmdatas).not.toHaveBeenCalled()

      kindStore.addKind(quidKind)
      quiddityStore.addQuiddity(videoOutput)

      expect(shmdataStore.updateFollowingShmdatas).toHaveBeenCalledTimes(1)
    })
  })

  describe('allFollowerPaths', () => {
    it('should return an array of all follower shmdata paths', () => {
      expect(shmdataStore.allFollowerPaths).toEqual([])
      shmdataStore.addFollowingShmdata(OSC_QUIDDITY.id, SHMDATA.path)
      expect(shmdataStore.allFollowerPaths).not.toEqual([])
      expect(shmdataStore.allFollowerPaths).toEqual([SHMDATA.path])
    })
  })

  describe('writingQuiddities', () => {
    beforeEach(() => {
      LOG.error = jest.fn()
    })

    it('should return a map of all writing quiddities hashed by shmdata paths', () => {
      expect(shmdataStore.writingQuiddities).toEqual(new Map())

      shmdataStore.addShmdata(SHMDATA)
      shmdataStore.addWritingShmdata(OSC_QUIDDITY.id, SHMDATA.path)
      expect(LOG.error).not.toHaveBeenCalled()
      expect(shmdataStore.writingQuiddities).toEqual(new Map().set(SHMDATA.path, OSC_QUIDDITY.id))
    })
  })

  describe('followingShmdatas', () => {
    it('should return a map of all follower shmdata models hashed by quiddity ID and update it accordingly', () => {
      expect(shmdataStore.followingShmdatas).toEqual(new Map())

      shmdataStore.addShmdata(SHMDATA)
      shmdataStore.addShmdata(SHMDATA2)
      shmdataStore.populateFollowingShmdatas(OSC_SINK_QUIDDITY.id, [SHMDATA.path, SHMDATA2.path])
      expect(shmdataStore.followingShmdatas).toEqual(new Map().set(OSC_SINK_QUIDDITY.id, new Set().add(SHMDATA).add(SHMDATA2)))
      shmdataStore.removeShmdata(SHMDATA2)
      shmdataStore.removeFollowingShmdata(OSC_SINK_QUIDDITY.id, SHMDATA2.path)
      expect(shmdataStore.followingShmdatas).toEqual(new Map().set(OSC_SINK_QUIDDITY.id, new Set().add(SHMDATA)))
    })
  })

  describe('followingQuiddities', () => {
    it('should return a map of all following quiddity IDs hashed by shmdata path and update it accordingly', () => {
      expect(shmdataStore.followingQuiddities).toEqual(new Map())

      shmdataStore.addShmdata(SHMDATA)
      shmdataStore.populateFollowingShmdatas(OSC_SINK_QUIDDITY.id, [SHMDATA.path])
      expect(shmdataStore.followingQuiddities).toEqual(new Map()
        .set(SHMDATA.path, new Set().add(OSC_SINK_QUIDDITY.id))
      )
      shmdataStore.populateFollowingShmdatas(RTMP_OUTPUT_QUIDDITY.id, [SHMDATA.path])
      expect(shmdataStore.followingQuiddities).toEqual(new Map()
        .set(SHMDATA.path, new Set().add(OSC_SINK_QUIDDITY.id).add(RTMP_OUTPUT_QUIDDITY.id))
      )
    })
  })

  describe('shmdataSuffixes', () => {
    it('should return all shmdata paths with the same suffix', () => {
      expect(shmdataStore.shmdataSuffixes).toEqual(new Map())

      shmdataStore.addShmdata(SHMDATA)
      expect(shmdataStore.shmdataSuffixes).toEqual(new Map()
        .set(SHMDATA.suffix, new Set().add(SHMDATA.path))
      )

      shmdataStore.addShmdata(SHMDATA2)
      expect(shmdataStore.shmdataSuffixes).toEqual(new Map()
        .set(SHMDATA.suffix, new Set().add(SHMDATA.path).add(SHMDATA2.path))
      )
    })
  })

  describe('fetchShmdata', () => {
    beforeEach(() => {
      ({ infoTreeAPI } = socketStore.APIs)
      LOG.error = jest.fn()
    })

    it('should get the shmdata of a quiddity given its shmdata path', () => {
      infoTreeAPI.get = jest.fn().mockResolvedValue(SHMDATA)
      shmdataStore.fetchShmdata(OSC_QUIDDITY.id, 'writer', SHMDATA.path)
      expect(infoTreeAPI.get).toHaveBeenCalledWith(OSC_QUIDDITY.id, `.shmdata.writer.${SHMDATA.path}`)
      expect(LOG.error).not.toHaveBeenCalled()
    })
  })

  describe('handleSocketChange', () => {
    beforeEach(() => {
      ({ infoTreeAPI } = socketStore.APIs)

      shmdataStore.handleGraftedShmdata = jest.fn()
      shmdataStore.handlePrunedShmdata = jest.fn()
      shmdataStore.clear = jest.fn()
    })

    it('should define the APIs if there is a new socket', () => {
      const onGraftSpy = jest.spyOn(infoTreeAPI, 'onGrafted')
      const onPruneSpy = jest.spyOn(infoTreeAPI, 'onPruned')

      const newSocket = new Socket()

      shmdataStore.handleSocketChange(newSocket)
      expect(shmdataStore.clear).toHaveBeenCalled()

      expect(onGraftSpy).toHaveBeenCalledWith(
        expect.any(Function),
        expect.any(Function),
        expect.any(Function)
      )
      expect(onPruneSpy).toHaveBeenCalledWith(
        expect.any(Function),
        expect.any(Function),
        expect.any(Function)
      )
    })

    it('should clear the store if the new socket is null', () => {
      shmdataStore.handleSocketChange(null)
      expect(shmdataStore.clear).toHaveBeenCalled()
    })

    it('should call update when `info_tree.grafted` is emitted and (quiddity ID and path) match', () => {
      socket.onEmit('info_tree_grafted', 'quiddityId', '.shmdata.writer.test', '"test"')
      expect(shmdataStore.handleGraftedShmdata).toHaveBeenCalledWith('quiddityId', '.shmdata.writer.test', JSON.parse('"test"'))
    })

    it('should not call update on tree graft when the path doesn\'t match', () => {
      socket.onEmit('info_tree_grafted', 'quiddityId', 'i am a failure', '"test"')
      expect(shmdataStore.handleGraftedShmdata).not.toHaveBeenCalled()
    })

    it('should call update when `info_tree.pruned` is emitted and (quiddity ID and path) match', () => {
      socket.onEmit('info_tree_pruned', 'quiddityId', '.shmdata.writer.test')
      expect(shmdataStore.handlePrunedShmdata).toHaveBeenCalledWith('quiddityId', '.shmdata.writer.test')
    })

    it('should not call update on tree prune when the path doesn\'t match', () => {
      socket.onEmit('info_tree_pruned', 'quiddityId', 'i am a failure', '"test"')
      expect(shmdataStore.handlePrunedShmdata).not.toHaveBeenCalled()
    })
  })

  describe('handleGraftedShmdata', () => {
    beforeEach(() => {
      shmdataStore.makeShmdataModel = jest.fn()
      shmdataStore.addFollowingShmdata = jest.fn()
      shmdataStore.addWritingShmdata = jest.fn()
    })

    it('should not make the shmdata model if the shmdatas map has the shmdata path', async () => {
      shmdataStore.addShmdata(SHMDATA)
      shmdataStore.hasShmdataPath = jest.fn().mockResolvedValue(null)
      await shmdataStore.handleGraftedShmdata(OSC_QUIDDITY.id, '.shmdata.writer./tmp/SHMDATA', SHMDATA_JSON)
      expect(shmdataStore.makeShmdataModel).not.toHaveBeenCalled()
    })

    it('should not add the shmdata model if it doesn\'t exist', async () => {
      shmdataStore.addShmdata = jest.fn()
      shmdataStore.makeShmdataModel = jest.fn().mockResolvedValue(null)
      await shmdataStore.handleGraftedShmdata(OSC_QUIDDITY.id, '.shmdata.writer./tmp/SHMDATA', SHMDATA_JSON)
      expect(shmdataStore.makeShmdataModel).toHaveBeenCalled()
      expect(shmdataStore.addShmdata).not.toHaveBeenCalled()
    })

    it('should add the shmdata model if it exists', async () => {
      shmdataStore.addShmdata = jest.fn()
      shmdataStore.makeShmdataModel = jest.fn().mockResolvedValue(SHMDATA)
      await shmdataStore.handleGraftedShmdata(OSC_QUIDDITY.id, '.shmdata.writer./tmp/SHMDATA', SHMDATA_JSON)
      expect(shmdataStore.makeShmdataModel).toHaveBeenCalled()
      expect(shmdataStore.addShmdata).toHaveBeenCalled()
    })

    it('should add the writing shmdata if the shmdatas map has the writing shmdata path', async () => {
      shmdataStore.addShmdata(SHMDATA)
      shmdataStore.hasShmdataPath = jest.fn().mockResolvedValue(true)
      await shmdataStore.handleGraftedShmdata(OSC_QUIDDITY.id, '.shmdata.writer./tmp/SHMDATA', SHMDATA_JSON)
      expect(shmdataStore.addWritingShmdata).toHaveBeenCalled()
      expect(shmdataStore.addFollowingShmdata).not.toHaveBeenCalled()
    })

    it('should add the following shmdata if the shmdatas map has the following shmdata path', async () => {
      shmdataStore.addShmdata(SHMDATA)
      shmdataStore.hasShmdataPath = jest.fn().mockResolvedValue(true)
      await shmdataStore.handleGraftedShmdata(RTMP_OUTPUT_QUIDDITY.id, '.shmdata.reader./tmp/SHMDATA', SHMDATA_JSON)
      expect(shmdataStore.addFollowingShmdata).toHaveBeenCalled()
      expect(shmdataStore.addWritingShmdata).not.toHaveBeenCalled()
    })

    it('should update the shmdata\'s caps if the shmdata map has the shmdata path but the caps changed.', async () => {
      // we make a new shmdata here because otherwise the mutating side effect of this call would change the SHMDATA object and
      // poullute the rest of the tests.
      shmdataStore.addShmdata(new Shmdata(SHMDATA_PATH, 'caps', 'category'))
      await shmdataStore.handleGraftedShmdata(RTMP_OUTPUT_QUIDDITY.id, '.shmdata.reader./tmp/SHMDATA', { caps: 'audio' })
      expect(shmdataStore.shmdatas.get('/tmp/SHMDATA').capabilities.toString()).toEqual('audio')
    })

    it('should return false if the shmdata path is not added to the map of shmdatas', async () => {
      expect(await shmdataStore.handleGraftedShmdata(OSC_SINK_QUIDDITY.id, '.shmdata.reader./tmp/SHMDATA', SHMDATA_JSON)).toEqual(false)
    })
  })

  describe('handlePrunedShmdata', () => {
    const writerTreePath = getShmdataTreePath(SHMDATA, 'writer')
    const followerTreePath = getShmdataTreePath(SHMDATA, 'reader')

    beforeEach(() => {
      shmdataStore.removeFollowingShmdata = jest.fn()
      shmdataStore.removeWritingShmdata = jest.fn()
    })

    it('should not remove the shmdata if it is not found in the shmdatas map', () => {
      shmdataStore.hasShmdataPath = jest.fn().mockReturnValue(false)
      shmdataStore.handlePrunedShmdata(OSC_QUIDDITY.id, followerTreePath)
      expect(shmdataStore.removeFollowingShmdata).not.toHaveBeenCalled()
      shmdataStore.handlePrunedShmdata(OSC_QUIDDITY.id, writerTreePath)
      expect(shmdataStore.removeWritingShmdata).not.toHaveBeenCalled()
    })

    it('should remove the shmdata if it is found in the shmdatas map', () => {
      shmdataStore.hasShmdataPath = jest.fn().mockReturnValue(true)
      shmdataStore.handlePrunedShmdata(OSC_QUIDDITY.id, followerTreePath)
      expect(shmdataStore.removeFollowingShmdata).toHaveBeenCalled()
      shmdataStore.handlePrunedShmdata(OSC_QUIDDITY.id, writerTreePath)
      expect(shmdataStore.removeWritingShmdata).toHaveBeenCalled()
    })
  })

  describe('makeShmdataModel', () => {
    beforeEach(() => {
      ({ infoTreeAPI } = socketStore.APIs)
    })

    it('should not create a shmdata if it exists', async () => {
      const fromJSONSpy = jest.spyOn(Shmdata, 'fromJSON')
      const treeQuerySpy = jest.spyOn(infoTreeAPI, 'get')

      shmdataStore.shmdatas.set(SHMDATA_PATH, SHMDATA)

      expect(
        await shmdataStore.makeShmdataModel(OSC_QUIDDITY.id, 'writer', SHMDATA_PATH, SHMDATA_JSON)
      ).toEqual(SHMDATA)

      expect(fromJSONSpy).not.toHaveBeenCalled()
      expect(treeQuerySpy).not.toHaveBeenCalled()
    })

    it('should create a shmdata by parsing its JSON if it is valid', async () => {
      const fromJSONSpy = jest.spyOn(Shmdata, 'fromJSON')
      const treeQuerySpy = jest.spyOn(infoTreeAPI, 'get')

      expect(await shmdataStore.makeShmdataModel(OSC_QUIDDITY.id, 'writer', SHMDATA_PATH, SHMDATA_JSON)).toEqual(SHMDATA)
      expect(fromJSONSpy).toHaveBeenCalledWith(SHMDATA_PATH, SHMDATA_JSON)
      expect(treeQuerySpy).not.toHaveBeenCalled()
    })

    it('should create a shmdata by fetching the quiddity tree if it has no JSON', async () => {
      const fromJSONSpy = jest.spyOn(Shmdata, 'fromJSON')
      infoTreeAPI.get = jest.fn().mockResolvedValue(SHMDATA_JSON)

      expect(await shmdataStore.makeShmdataModel(OSC_QUIDDITY.id, 'writer', SHMDATA_PATH)).toEqual(SHMDATA)
      expect(fromJSONSpy).toHaveBeenCalledWith(SHMDATA_PATH, SHMDATA_JSON)
      expect(infoTreeAPI.get).toHaveBeenCalledWith(OSC_QUIDDITY.id, TREE_PATH)
    })

    it('should log an error when it loads an invalid shmdata JSON', async () => {
      shmdataStore.fetchShmdata = jest.fn().mockResolvedValue(null)
      expect(await shmdataStore.makeShmdataModel(OSC_QUIDDITY.id, 'writer', TREE_PATH, { fail: true })).toEqual(null)
    })
  })

  describe('isShmdataWriting', () => {
    it('should return false if the path is not found in the writing shmdatas', () => {
      expect(shmdataStore.isShmdataWriting(SHMDATA.path)).toEqual(false)
    })

    it('should return true if the path is found in the writing shmdatas', () => {
      jest.spyOn(shmdataStore, 'writingShmdatas', 'get').mockReturnValue(new Map().set(OSC_QUIDDITY.id, new Set().add(SHMDATA)))
      expect(shmdataStore.isShmdataWriting(SHMDATA.path)).toEqual(true)
    })
  })

  describe('hasShmdataPath', () => {
    it('should return false if shmdata does\'t exist in the store', () => {
      expect(shmdataStore.hasShmdataPath(SHMDATA_PATH)).toEqual(false)
    })

    it('should return false if shmdata path is null', () => {
      expect(shmdataStore.hasShmdataPath(null)).toEqual(false)
    })

    it('should return true if shmdata path exists in the store', () => {
      shmdataStore.shmdatas.set(SHMDATA_PATH, SHMDATA)
      expect(shmdataStore.hasShmdataPath(SHMDATA_PATH)).toEqual(true)
    })
  })

  describe('isShmdataPath', () => {
    it('should return true with a common shmdata path', () => {
      expect(shmdataStore.isShmdataPath('/tmp/switcher_testSignal0_1_audio')).toEqual(true)
    })

    it('should return true with a shmdata path that contains a dash', () => {
      expect(shmdataStore.isShmdataPath('/tmp/switcher_sdi30_2_video-encoded')).toEqual(true)
    })

    it('should return true with a shmdata path that is encapsulated by brackets', () => {
      expect(shmdataStore.isShmdataPath('[/tmp/switcher_sdi30_2_video-encoded]')).toEqual(true)
    })

    it('should return false if the shmdata path is null', () => {
      expect(shmdataStore.isShmdataPath('null')).toEqual(false)
    })
  })

  describe('addShmdata', () => {
    it('should add a shmdata with its shmdata path', () => {
      expect(shmdataStore.addShmdata(SHMDATA)).toEqual(true)
      expect(shmdataStore.shmdatas.get(SHMDATA_PATH)).toEqual(SHMDATA)
    })
  })

  describe('removeShmdata', () => {
    beforeEach(() => {
      shmdataStore.shmdatas.set(SHMDATA.path, SHMDATA)
    })

    it('should remove shmdata from its model', () => {
      expect(shmdataStore.removeShmdata(SHMDATA)).toEqual(true)
      expect(shmdataStore.shmdatas.has(SHMDATA_PATH)).toEqual(false)
    })
  })

  describe('addWritingShmdata', () => {
    it('should return true if the shmdata path has been added', () => {
      expect(shmdataStore.addWritingShmdata(OSC_QUIDDITY.id, SHMDATA.path)).toEqual(true)
    })

    it('should return false if the shmdata path has not been added', () => {
      shmdataStore.writerPaths.set(OSC_QUIDDITY.id, new Set([SHMDATA.path]))
      expect(shmdataStore.addWritingShmdata(OSC_QUIDDITY.id, SHMDATA.path)).toEqual(false)
    })
  })

  describe('removeWritingShmdata', () => {
    it('should return false if the quiddity is not a writing shmdata', () => {
      expect(shmdataStore.removeWritingShmdata(OSC_QUIDDITY.id, SHMDATA.path)).toEqual(false)
    })

    it('should return true if the shmdata path has been deleted', () => {
      shmdataStore.writerPaths.set(OSC_QUIDDITY.id, new Set([SHMDATA.path]))
      expect(shmdataStore.removeWritingShmdata(OSC_QUIDDITY.id, SHMDATA.path)).toEqual(true)
    })
  })
})
