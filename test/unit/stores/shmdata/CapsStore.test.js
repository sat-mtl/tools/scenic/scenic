/* global describe it expect jest beforeEach */

import { Socket } from '@fixture/socket'
import { configure } from 'mobx'

import populateStores from '@stores/populateStores.js'
import MatrixEntry from '@models/matrix/MatrixEntry'

import CapsStore, { LOG } from '@stores/shmdata/CapsStore'
import Capabilities from '@models/shmdata/Capabilities'

import { sdiInput, videoOutput } from '@fixture/allQuiddities'
import sdiShmdata from '@fixture/models/shmdatas.sdiInput1.json'

configure({ safeDescriptors: false })

describe('CapsStore', () => {
  const videoFollowerSpec = videoOutput.connectionSpecs.follower
  const caps = Capabilities.fromArray([sdiShmdata.writer['/tmp/switcher_sdi10_1_1'].caps])

  let socket, socketStore, configStore, shmdataStore, capsStore, specStore

  beforeEach(() => {
    ({ shmdataStore, socketStore, configStore, capsStore, specStore } = populateStores())

    LOG.error = jest.fn()
    LOG.warn = jest.fn()

    socket = new Socket()
    socketStore.setActiveSocket(socket)
  })

  describe('constructor', () => {
    beforeEach(() => {
      capsStore.handleSocketChange = jest.fn()
      capsStore.handleWritingShmdatas = jest.fn()
    })

    it('should fail if the shmdataStore is not given', () => {
      expect(() => new CapsStore()).toThrow()
    })

    it('should fail if the specStore is not given', () => {
      expect(() => new CapsStore(shmdataStore)).toThrow()
    })

    it('should fail if the configStore is not given', () => {
      expect(() => new CapsStore(shmdataStore, specStore)).toThrow()
    })

    it('should not fail if all required store are given', () => {
      expect(() => new CapsStore(shmdataStore, specStore, configStore)).not.toThrow()
    })

    it('should be well instantiated', () => {
      expect(capsStore).toBeDefined()
    })
  })

  describe('areConnectable', () => {
    it('should return true when follower specs canDo has the writer caps mediaType', () => {
      jest.spyOn(capsStore, 'writerCaps', 'get').mockReturnValue(new Map().set(sdiInput.id, [{ ...caps, mediaType: 'test' }]))
      jest.spyOn(specStore, 'followerSpecs', 'get').mockReturnValue(new Map().set(videoOutput.id, [{ ...videoFollowerSpec, canDo: new Set().add('test') }]))
      expect(capsStore.areConnectable(sdiInput.id, videoOutput.id, 'test')).toEqual(true)
    })

    it('should return false when follower specs canDo does not have the writer caps mediaType', () => {
      jest.spyOn(capsStore, 'writerCaps', 'get').mockReturnValue(new Map().set(sdiInput.id, [{ ...caps, mediaType: 'test' }]))
      jest.spyOn(specStore, 'followerSpecs', 'get').mockReturnValue(new Map().set(videoOutput.id, [{ ...videoFollowerSpec, canDo: new Set().add('test2') }]))
      expect(capsStore.areConnectable(sdiInput.id, videoOutput.id)).toEqual(false)
    })

    it('should return false when the writerCaps do not have the source ID', () => {
      jest.spyOn(capsStore, 'writerCaps', 'get').mockReturnValue(new Map().set(sdiInput.id, [{ ...caps, mediaType: 'test' }]))
      jest.spyOn(specStore, 'followerSpecs', 'get').mockReturnValue(new Map().set(videoOutput.id, [{ ...videoFollowerSpec, canDo: new Set().add('test') }]))
      expect(capsStore.areConnectable('other', videoOutput.id)).toEqual(false)
    })

    it('should return false when the followerSpecs do not have the destination ID', () => {
      jest.spyOn(capsStore, 'writerCaps', 'get').mockReturnValue(new Map().set(sdiInput.id, [{ ...caps, mediaType: 'test' }]))
      jest.spyOn(specStore, 'followerSpecs', 'get').mockReturnValue(new Map().set(videoOutput.id, [{ ...videoFollowerSpec, canDo: new Set().add('test') }]))
      expect(capsStore.areConnectable(sdiInput.id, 'other')).toEqual(false)
    })
  })

  describe('areEntriesConnectable', () => {
    const sdiEntry = new MatrixEntry(sdiInput, null, [sdiShmdata])
    const videoEntry = new MatrixEntry(videoOutput)

    beforeEach(() => {
      capsStore.areConnectable = jest.fn()
    })

    it('should call areConnectable to check if passed quiddities can be connected', () => {
      capsStore.areEntriesConnectable(sdiEntry, videoEntry)
      expect(capsStore.areConnectable).toHaveBeenCalledWith(sdiEntry.quiddityId, videoEntry.quiddityId)
    })
  })

  describe('findCompatibleCaps', () => {
    beforeEach(() => {
      capsStore.areConnectable = jest.fn()
    })

    it('should return null if the writer caps do not include the source ID', () => {
      expect(capsStore.findCompatibleCaps(sdiInput.id, videoOutput.id)).toEqual(null)
    })

    it('should return null if the compatible media types do not include the destination ID', () => {
      jest.spyOn(capsStore, 'writerCaps', 'get').mockReturnValue(new Map().set(sdiInput.id, [{ ...caps, mediaType: 'test' }]))
      expect(capsStore.findCompatibleCaps(sdiInput.id, videoOutput.id)).toEqual(null)
    })

    it('should return the compatible caps when the writer caps include the source ID and when the compatible media types include the destination ID', () => {
      jest.spyOn(capsStore, 'writerCaps', 'get').mockReturnValue(new Map().set(sdiInput.id, [{ ...caps, mediaType: 'test' }]))
      jest.spyOn(specStore, 'compatibleMediaTypes', 'get').mockReturnValue(new Map().set(videoOutput.id, new Set().add('test')))
      expect(capsStore.findCompatibleCaps(sdiInput.id, videoOutput.id)).toEqual({ mediaType: 'test', properties: new Map(), types: new Map() })
    })
  })

  describe('isHidden', () => {
    it('should return when the capabilities is hidden', () => {
      configStore.setUserScenic({ hiddenCapabilities: ['test'] })
      expect(capsStore.isHidden('test')).toEqual(true)
    })

    it('should return false when the capability is not hidden', () => {
      configStore.setUserScenic({ hiddenCapabilities: [] })
      expect(capsStore.isHidden('test')).toEqual(false)
    })
  })
})
