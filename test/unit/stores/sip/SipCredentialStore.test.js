/* global jest describe it expect beforeEach */

import SocketStore from '@stores/SocketStore'

import SipCredentialStore, {
  DEFAULT_SIP_CREDENTIALS,
  SIP_DEFAULT_PORT
} from '@stores/sip/SipCredentialStore'

describe('SipCredentialStore', () => {
  let sipCredentialStore

  beforeEach(() => {
    sipCredentialStore = new SipCredentialStore(new SocketStore())
  })

  describe('constructor', () => {
    it('should be initialized with default credentials', () => {
      expect(sipCredentialStore.credentials.toJSON()).toEqual(Array.from(DEFAULT_SIP_CREDENTIALS))
    })
  })

  describe('sipCredentials', () => {
    beforeEach(() => {
      sipCredentialStore.setCredential('sipServer', 'SIP')
      sipCredentialStore.setCredential('turnServer', 'TURN')
      sipCredentialStore.setCredential('stunServer', 'STUN')
    })

    it('should return the credential model with the same credentials for SIP, STUN and TURN', () => {
      expect(sipCredentialStore.sipCredentials.turnServer).toEqual('SIP')
      expect(sipCredentialStore.sipCredentials.stunServer).toEqual('SIP')
    })

    it('should return the credential model with custom STUN and TURN credentials if the SameStunTurnLogin toggle is false', () => {
      sipCredentialStore.applySameStunTurnLoginToggle(false)
      expect(sipCredentialStore.sipCredentials.turnServer).toEqual('TURN')
      expect(sipCredentialStore.sipCredentials.stunServer).toEqual('STUN')
    })
  })

  describe('areCredentialsValid', () => {
    beforeEach(() => {
      sipCredentialStore.applyValidationBinding = jest.fn()
    })

    it('should apply the bindings for SIP credentials only', () => {
      expect(sipCredentialStore.areCredentialsValid).toEqual(true)
      expect(sipCredentialStore.applyValidationBinding).toHaveBeenCalledTimes(4)
      expect(sipCredentialStore.applyValidationBinding).toHaveBeenNthCalledWith(1, 'sipServer')
      expect(sipCredentialStore.applyValidationBinding).toHaveBeenNthCalledWith(2, 'sipUser')
      expect(sipCredentialStore.applyValidationBinding).toHaveBeenNthCalledWith(3, 'sipPassword')
      expect(sipCredentialStore.applyValidationBinding).toHaveBeenNthCalledWith(4, 'sipPort')
    })

    it('should apply the bindings for SIP, STUN and TURN credentials if the SameStunTurnLogin toggle is false', () => {
      sipCredentialStore.applySameStunTurnLoginToggle(false)
      expect(sipCredentialStore.areCredentialsValid).toEqual(true)
      expect(sipCredentialStore.applyValidationBinding).toHaveBeenCalledTimes(8)
      expect(sipCredentialStore.applyValidationBinding).toHaveBeenNthCalledWith(1, 'sipServer')
      expect(sipCredentialStore.applyValidationBinding).toHaveBeenNthCalledWith(2, 'sipUser')
      expect(sipCredentialStore.applyValidationBinding).toHaveBeenNthCalledWith(3, 'sipPassword')
      expect(sipCredentialStore.applyValidationBinding).toHaveBeenNthCalledWith(4, 'sipPort')
      expect(sipCredentialStore.applyValidationBinding).toHaveBeenNthCalledWith(5, 'turnUser')
      expect(sipCredentialStore.applyValidationBinding).toHaveBeenNthCalledWith(6, 'turnServer')
      expect(sipCredentialStore.applyValidationBinding).toHaveBeenNthCalledWith(7, 'turnPassword')
      expect(sipCredentialStore.applyValidationBinding).toHaveBeenNthCalledWith(8, 'stunServer')
    })
  })

  describe('applyValidationBinding', () => {
    const suite = ['sipServer', 'sipUser', 'sipPassword', 'turnUser', 'turnServer', 'turnPassword', 'stunServer']

    describe.each(suite)('%s', credentialId => {
      it(`should fail if the credential ${credentialId} is empty`, () => {
        sipCredentialStore.setCredential(credentialId, '')
        expect(() => sipCredentialStore.applyValidationBinding(credentialId)).toThrow()
      })

      it(`should not fail if the credential ${credentialId} is filled`, () => {
        sipCredentialStore.setCredential(credentialId, 'test')
        expect(() => sipCredentialStore.applyValidationBinding(credentialId)).not.toThrow()
      })
    })

    describe('sipPort', () => {
      it('should pass with the default port', () => {
        sipCredentialStore.setCredential('sipPort', SIP_DEFAULT_PORT)
        expect(() => sipCredentialStore.applyValidationBinding('sipPort')).not.toThrow()
      })

      it('should fail if the SIP port is not a number', () => {
        sipCredentialStore.setCredential('sipPort', 'test')
        expect(() => sipCredentialStore.applyValidationBinding('sipPort')).toThrow()
      })

      it('should fail if the SIP port is empty', () => {
        sipCredentialStore.setCredential('sipPort', '')
        expect(() => sipCredentialStore.applyValidationBinding('sipPort')).toThrow()
      })
    })
  })

  describe('applyCredentialValidation', () => {
    it('should trim the credential when it is checked', () => {
      sipCredentialStore.applyValidationBinding = jest.fn()
      sipCredentialStore.setCredential('sipServer', 'test            ')
      sipCredentialStore.applyCredentialValidation('sipServer')
      expect(sipCredentialStore.applyValidationBinding).toHaveBeenCalled()
      expect(sipCredentialStore.credentials.get('sipServer')).toEqual('test')
    })

    it('should set the credential error if it fails', () => {
      sipCredentialStore.setCredential('sipServer', '')
      sipCredentialStore.applyCredentialValidation('sipServer')
      expect(sipCredentialStore.errors.get('sipServer')).toEqual('The SIP server address is required')
    })
  })
})
