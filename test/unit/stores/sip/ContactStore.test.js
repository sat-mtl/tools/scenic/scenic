/* global describe it expect jest beforeEach */

import { Socket } from '@fixture/socket'
import { configure } from 'mobx'

import populateStores from '@stores/populateStores.js'
import { SIP_ID } from '@models/quiddity/specialQuiddities'
import ContactStore, { LOG } from '@stores/sip/ContactStore'

import InitStateEnum from '@models/common/InitStateEnum'
import { SendStatusEnum } from '@models/sip/SipEnums'
import Contact from '@models/sip/Contact'
import SipCredentials from '@models/sip/SipCredentials'
import Shmdata from '@models/shmdata/Shmdata'
import Quiddity from '@models/quiddity/Quiddity'

import { sipKind, getKind } from '@fixture/allQuiddityKinds'
import { sdiInput } from '@fixture/allQuiddities'
import { video } from '@fixture/allShmdatas'
import SDI_INPUT_QUIDDITY from '@fixture/models/quiddity.sdiInput1.json'
import SIP_QUIDDITY_WITH_SHMDATA from '@fixture/models/quiddity.sip2.json'
import SIPMEDIA0_INPUT_QUIDDITY from '@fixture/models/quiddity.sipMedia0.json'

import CONTACT_LIST from '@fixture/contactList0.sample.json'
import BuddiesTree from '@fixture/buddy.tree.json'

configure({ safeDescriptors: false })

const DEFAULT_AUTHORIZATION = false

const BUDDY_ID = '0'
const BUDDY_PATH = '.buddies.' + BUDDY_ID
const BUDDY_USER = 'buddy'
const BUDDY_SERVER = 'dev.sip'
const BUDDY_URI_JSON = `"${BUDDY_USER}@${BUDDY_SERVER}"`
const BUDDY_URI = `${BUDDY_USER}@${BUDDY_SERVER}`
const BUDDY_URI_PATH = BUDDY_PATH + '.uri'
const BUDDY_NAME = 'test'

const CONTACT_JSON = { id: BUDDY_ID, uri: BUDDY_URI }
const CONTACT = Contact.fromJSON(CONTACT_JSON)

const SIP_CREDENTIALS = SipCredentials.fromJSON({ sipUser: 'test', sipServer: 'dev.sip' })

const sdiInput0 = Quiddity.fromJSON({ ...SDI_INPUT_QUIDDITY, id: 'sdiInput0', nickname: 'sdiInput0', kindId: 'sdiInput0' })

const sipQuiddity = Quiddity.fromJSON({ ...SIP_QUIDDITY_WITH_SHMDATA })

const sipSourceContact = new Contact(100000, 'contact0@dev.sip.test', 'sip contact')
const sipSource = Quiddity.fromJSON({ ...SIPMEDIA0_INPUT_QUIDDITY, id: 'sipMedia0' })
const sipSourceShmdata = new Shmdata('/tmp/switcher_scenic8000_3_video-0', 'video/x-raw', 'video')

describe('ContactStore', () => {
  let socket
  let socketStore, configStore, quiddityStore, sipStore, contactStore, lockStore, shmdataStore, kindStore
  let infoTreeAPI, methodAPI

  beforeEach(() => {
    ({ socketStore, configStore, quiddityStore, shmdataStore, sipStore, contactStore, lockStore, kindStore } = populateStores())

    LOG.error = jest.fn()
    LOG.warn = jest.fn()
    LOG.info = jest.fn()

    socket = new Socket()
    socketStore.setActiveSocket(socket)

    kindStore.addKind(sipKind)
    quiddityStore.addQuiddity(sipQuiddity)
    quiddityStore.addQuiddity(sipSource)

    jest.spyOn(sipStore, 'sipId', 'get').mockReturnValue(SIP_ID)
  })

  describe('constructor', () => {
    beforeEach(() => {
      contactStore.handleSocketChange = jest.fn()
      contactStore.handleSipRegistration = jest.fn()
    })

    it('should fail if the configStore is not provided', () => {
      /* eslint-disable no-new */
      expect(() => { new ContactStore(socketStore) }).toThrow()
      /* eslint-enable no-new */
    })

    it('should fail if the sipStore is not provided', () => {
      /* eslint-disable no-new */
      expect(() => { new ContactStore(socketStore, configStore) }).toThrow()
      /* eslint-enable no-new */
    })

    it('should be correctly instantiated', () => {
      contactStore = new ContactStore(socketStore, configStore, quiddityStore, shmdataStore, sipStore, lockStore, DEFAULT_AUTHORIZATION)
      expect(contactStore).toBeDefined()
      expect(contactStore.initState).toEqual(InitStateEnum.NOT_INITIALIZED)
    })

    it('should react to SIP connection', () => {
      sipStore.setCredentials(SIP_CREDENTIALS)
      expect(contactStore.handleSipRegistration).toHaveBeenCalled()
    })

    it('should react to contacts being added to session and apply the correct authorization', () => {
      contactStore.applyContactAuthorization = jest.fn()
      contactStore.addContact(CONTACT)
      contactStore.addContactToSession(CONTACT)
      expect(contactStore.applyContactAuthorization).toHaveBeenCalledWith(CONTACT, true)
    })

    it('should react to a contact being removed from the session and apply default authorization', () => {
      contactStore.addContact(CONTACT)
      contactStore.addContactToSession(CONTACT)
      contactStore.applyContactAuthorization = jest.fn()
      contactStore.removeContactFromSession(BUDDY_ID)
      expect(contactStore.applyContactAuthorization).toHaveBeenCalledWith(CONTACT, contactStore.defaultAuthorization)
    })

    it('should not react and apply default authorization to contact when removed from session if contact is deleted', () => {
      contactStore.addContact(CONTACT)
      contactStore.addContactToSession(CONTACT)
      contactStore.applyContactAuthorization = jest.fn()
      contactStore.removeContact(BUDDY_ID)
      expect(contactStore.applyContactAuthorization).not.toHaveBeenCalled()
    })

    it('should react to default authorization change', () => {
      contactStore.applyBlocklist = jest.fn()
      contactStore.setDefaultAuthorization(true)
      expect(contactStore.applyBlocklist).toHaveBeenCalled()
    })

    it('should react and update sessionContacts when contacts are updated and session contact exists ', () => {
      contactStore.cleanSessionContacts = jest.fn()
      const CONTACT2 = Contact.fromJSON({ id: '1', uri: 'test2@dev.sip' })
      contactStore.addContact(CONTACT)
      contactStore.addContact(CONTACT2)
      contactStore.addContactToSession(CONTACT)
      contactStore.addContactToSession(CONTACT2)
      contactStore.removeContact(CONTACT.id)
      expect(contactStore.cleanSessionContacts).toHaveBeenCalled()
    })

    it('should not react and update sessionContacts when contacts are updated but no session contact exists ', () => {
      contactStore.cleanSessionContacts = jest.fn()
      contactStore.addContact(CONTACT)
      contactStore.removeContact(CONTACT.id)
      expect(contactStore.cleanSessionContacts).not.toHaveBeenCalled()
    })
  })

  describe('initialize', () => {
    beforeEach(() => {
      contactStore.fetchJSONContacts = jest.fn().mockResolvedValue({})
      contactStore.handleContactCreation = jest.fn()
      contactStore.isContactInSession = jest.fn().mockReturnValue(false)
      contactStore.populateContactListFromConfig = jest.fn()
      contactStore.applyBlocklist = jest.fn()
    })

    it('should initialize contact store when the SIP is registered', () => {
      const mockInitialize = jest.spyOn(contactStore, 'initialize')
      sipStore.setCredentials(SIP_CREDENTIALS)
      expect(mockInitialize).toHaveBeenCalled()
    })

    it('should not initialize if store is already initialized', async () => {
      contactStore.setInitState(InitStateEnum.INITIALIZED)
      expect(await contactStore.initialize()).toEqual(false)
    })

    it('should initialize store but not initialize contacts if not logged to SIP server', async () => {
      expect(await contactStore.initialize()).toEqual(true)
      expect(contactStore.fetchJSONContacts).not.toHaveBeenCalled()
      expect(contactStore.handleContactCreation).not.toHaveBeenCalled()
      expect(contactStore.populateContactListFromConfig).not.toHaveBeenCalled()
      expect(contactStore.applyBlocklist).not.toHaveBeenCalled()
      expect(contactStore.initState).toEqual(InitStateEnum.INITIALIZED)
    })

    it('should initialize store and populate contacts if logged to SIP server', async () => {
      contactStore.handleSipRegistration = jest.fn()
      sipStore.setCredentials(SIP_CREDENTIALS)

      expect(await contactStore.initialize()).toEqual(true)
      expect(contactStore.fetchJSONContacts).toHaveBeenCalled()
      expect(contactStore.handleContactCreation).not.toHaveBeenCalled()
      expect(contactStore.populateContactListFromConfig).toHaveBeenCalledWith(sipStore.currentUri)
      expect(contactStore.applyBlocklist).toHaveBeenCalled()
      expect(contactStore.initState).toEqual(InitStateEnum.INITIALIZED)
    })

    it('should initialize store and handle existing contacts if logged to SIP server', async () => {
      contactStore.handleSipRegistration = jest.fn()
      contactStore.isContactInSession = jest.fn().mockReturnValue(true)
      contactStore.addContactToSession = jest.fn()
      sipStore.setCredentials(SIP_CREDENTIALS)

      expect(await contactStore.initialize()).toEqual(true)
      expect(contactStore.populateContactListFromConfig).toHaveBeenCalledWith(sipStore.currentUri)
      expect(contactStore.applyBlocklist).toHaveBeenCalled()
      expect(contactStore.initState).toEqual(InitStateEnum.INITIALIZED)
    })

    it('should not handle existing contacts if none exist', async () => {
      contactStore.handleSipRegistration = jest.fn()
      sipStore.setCredentials(SIP_CREDENTIALS)
      contactStore.fetchJSONContacts = jest.fn().mockResolvedValue({})
      contactStore.isContactInSession = jest.fn()
      contactStore.addContactToSession = jest.fn()

      expect(await contactStore.initialize()).toEqual(true)
      expect(contactStore.fetchJSONContacts).toHaveBeenCalled()
      expect(contactStore.handleContactCreation).not.toHaveBeenCalled()
      expect(contactStore.initState).toEqual(InitStateEnum.INITIALIZED)
    })
  })

  describe('fetchJSONContacts', () => {
    beforeEach(() => {
      ({ infoTreeAPI } = socketStore.APIs)
    })

    it('should fetch the current buddies', async () => {
      infoTreeAPI.get = jest.fn().mockResolvedValue(BuddiesTree)
      expect(await contactStore.fetchJSONContacts()).toEqual(BuddiesTree)
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should return an empty object if no buddies exist', async () => {
      infoTreeAPI.get = jest.fn().mockResolvedValue('null')
      expect(await contactStore.fetchJSONContacts()).toEqual({})
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should log an error if the request has failed', async () => {
      infoTreeAPI.get = jest.fn().mockRejectedValue(false)
      expect(await contactStore.fetchJSONContacts()).toEqual({})
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('hasSessionContact', () => {
    beforeEach(() => {
      contactStore.addContact(CONTACT)
    })

    it('should return true if contact is in sessionContacts Map', () => {
      contactStore.addContactToSession(CONTACT)
      expect(contactStore.hasSessionContact(CONTACT)).toEqual(true)
    })

    it('should return false if contact not in sessionContacts Map', () => {
      contactStore.addContactToSession(CONTACT)
      expect(contactStore.hasSessionContact(Contact.fromJSON({ id: '1', uri: 'test@sip.dev' }))).toEqual(false)
    })

    it('should return false if no contacts are in sessionContacts Map', () => {
      expect(contactStore.hasSessionContact(CONTACT)).toEqual(false)
    })
  })

  describe('contactAddresses', () => {
    it('should return contact model if it exists', () => {
      contactStore.addContact(CONTACT)
      expect(contactStore.contactAddresses.get(BUDDY_URI)).toEqual(CONTACT)
    })

    it('should return undefined if contact doesn\'t exists', () => {
      expect(contactStore.contactAddresses.get('test')).toEqual(undefined)
    })
  })

  describe('handleSocketChange', () => {
    const buddyProperty = '"unknown"'
    const buddyPropertyPath = BUDDY_PATH + '.status'
    const buddyConnection = '["test1", "test2"]'
    const buddyConnectionPath = BUDDY_PATH + '.connections'

    beforeEach(() => {
      ({ infoTreeAPI } = socketStore.APIs)

      contactStore.handleContactUriUpdate = jest.fn()
      contactStore.handleContactRemoval = jest.fn()
      contactStore.handleContactPropertyUpdate = jest.fn()
      contactStore.handleConnectionUpdate = jest.fn()
      contactStore.clear = jest.fn()
    })

    it('should define the APIs if there is a new socket', () => {
      const onGraftSpy = jest.spyOn(infoTreeAPI, 'onGrafted')
      const onPruneSpy = jest.spyOn(infoTreeAPI, 'onPruned')

      const newSocket = new Socket()

      contactStore.handleSocketChange(newSocket)
      expect(contactStore.clear).toHaveBeenCalled()

      expect(onPruneSpy).toHaveBeenCalledWith(
        expect.any(Function),
        expect.any(Function),
        expect.any(Function)
      )
      expect(onGraftSpy).toHaveBeenNthCalledWith(
        1,
        expect.any(Function),
        expect.any(Function),
        expect.any(Function)
      )
      expect(onGraftSpy).toHaveBeenNthCalledWith(
        2,
        expect.any(Function),
        expect.any(Function),
        expect.any(Function)
      )
      expect(onGraftSpy).toHaveBeenNthCalledWith(
        3,
        expect.any(Function),
        expect.any(Function),
        expect.any(Function)
      )
    })

    it('should clear the store if the new socket is null', () => {
      contactStore.handleSocketChange(null)
      expect(contactStore.clear).toHaveBeenCalled()
    })

    it('should handle grafted contact', () => {
      socket.onEmit('info_tree_grafted', SIP_ID, BUDDY_URI_PATH, BUDDY_URI_JSON)
      expect(contactStore.handleContactUriUpdate).toHaveBeenCalledWith(BUDDY_URI_PATH, JSON.parse(BUDDY_URI_JSON))
    })

    it('should handle grafted contact property', () => {
      socket.onEmit('info_tree_grafted', SIP_ID, buddyPropertyPath, buddyProperty)
      expect(contactStore.handleContactPropertyUpdate).toHaveBeenCalledWith(buddyPropertyPath, JSON.parse(buddyProperty))
    })

    it('should handle grafted connection', () => {
      socket.onEmit('info_tree_grafted', SIP_ID, buddyConnectionPath, buddyConnection)
      expect(contactStore.handleConnectionUpdate).toHaveBeenCalledWith(buddyConnectionPath, JSON.parse(buddyConnection))
    })

    it('should handle pruned contact', () => {
      socket.onEmit('info_tree_pruned', SIP_ID, BUDDY_PATH)
      expect(contactStore.handleContactRemoval).toHaveBeenCalledWith(BUDDY_PATH)
    })

    it('should not handle tree updates when the path doesn\'t match', () => {
      socket.onEmit('info_tree_grafted', SIP_ID, 'I am a failure', '"fail"')
      socket.onEmit('info_tree_pruned', SIP_ID, 'I am a failure', '"fail"')

      expect(contactStore.handleContactUriUpdate).not.toHaveBeenCalled()
      expect(contactStore.handleContactRemoval).not.toHaveBeenCalled()
      expect(contactStore.handleContactPropertyUpdate).not.toHaveBeenCalled()
      expect(contactStore.handleConnectionUpdate).not.toHaveBeenCalled()
    })
  })

  describe('handleSipRegistration', () => {
    beforeEach(() => {
      contactStore.initialize = jest.fn()
      contactStore.clear = jest.fn()
    })

    it('should initialize store when logging in if its not initialized', async () => {
      sipStore.setCredentials(SIP_CREDENTIALS)
      expect(contactStore.initialize).toHaveBeenCalled()
      expect(contactStore.clear).not.toHaveBeenCalled()
    })

    it('should not initialize store when logging in if its already initialized', async () => {
      contactStore.setInitState(InitStateEnum.INITIALIZED)
      sipStore.setCredentials(SIP_CREDENTIALS)
      expect(contactStore.initialize).not.toHaveBeenCalled()
      expect(contactStore.clear).not.toHaveBeenCalled()
    })

    it('should clear store when logging out', async () => {
      await contactStore.handleSipRegistration()
      expect(contactStore.initialize).not.toHaveBeenCalled()
      expect(contactStore.clear).toHaveBeenCalled()
    })
  })

  describe('handleQuiddityChanges', () => {
    const contact = Contact.fromJSON(CONTACT.toJSON())

    beforeEach(() => {
      contact.connections = [video.path]
      contactStore.addContact(contact)
      shmdataStore.addShmdata(video)
      shmdataStore.addWritingShmdata(sdiInput.id, video.path)
      kindStore.addKind(getKind(sdiInput))
      quiddityStore.addQuiddity(sdiInput)
      contactStore.applyDetachShmdata = jest.fn()
    })

    it('should be called when a quiddity is changed', () => {
      contactStore.handleQuiddityChanges = jest.fn()
      quiddityStore.removeQuiddity(sdiInput)
      expect(contactStore.handleQuiddityChanges).toHaveBeenCalledTimes(1)
      quiddityStore.addQuiddity(sdiInput)
      expect(contactStore.handleQuiddityChanges).toHaveBeenCalledTimes(2)
    })

    it('should remove all shmdatas that are written by removed quiddities', () => {
      quiddityStore.removeQuiddity(sdiInput)
      expect(contactStore.applyDetachShmdata).toHaveBeenCalled()
    })

    it('shouldn\'t remove the shmdatas that are written by current quiddities', () => {
      contactStore.handleQuiddityChanges()
      expect(contactStore.applyDetachShmdata).not.toHaveBeenCalled()
    })
  })

  describe('handleContactCreation', () => {
    const USER_CONTACT_JSON = {
      id: BUDDY_ID,
      uri: BUDDY_URI,
      name: BUDDY_NAME,
      sendStatus: SendStatusEnum.CALLING
    }

    const USER_CONTACT = Contact.fromJSON(USER_CONTACT_JSON)

    beforeEach(() => {
      contactStore.addContact = jest.fn()
      contactStore.applyContactAuthorization = jest.fn()
      contactStore.applyContactName = jest.fn()
      contactStore.addContactToSession = jest.fn()
    })

    it('should handle a contact model', () => {
      contactStore.handleContactCreation(CONTACT)
      expect(contactStore.addContact).toHaveBeenCalledWith(CONTACT)
    })

    it('should handle a contact json', () => {
      contactStore.handleContactCreation(CONTACT_JSON)
      expect(contactStore.addContact).toHaveBeenCalledWith(CONTACT)
    })

    it('should handle contact by merging it with its user configuration', () => {
      contactStore.findUserContactFromUri = jest.fn().mockReturnValue(USER_CONTACT_JSON)
      contactStore.handleContactCreation(CONTACT)
      expect(contactStore.addContact).toHaveBeenCalledWith(USER_CONTACT)
    })

    it('should find and clear a contact that is present in the pending contact map by uri', () => {
      contactStore.pendingContacts.set(BUDDY_URI, Contact.fromJSON({ uri: BUDDY_URI }))
      contactStore.handleContactCreation(CONTACT_JSON)
      expect(contactStore.pendingContacts.size).toEqual(0)
    })

    it('should apply default authorization everytime', () => {
      contactStore.handleContactCreation(CONTACT)
      expect(contactStore.applyContactAuthorization)
        .toHaveBeenCalledWith(CONTACT, contactStore.defaultAuthorization)
    })

    it('should apply contact name everytime', () => {
      contactStore.handleContactCreation(CONTACT)
      expect(contactStore.applyContactName)
        .toHaveBeenCalledWith(CONTACT.uri, CONTACT.name)
    })

    it('should not add contact to session if it is not called', () => {
      contactStore.handleContactCreation(CONTACT)
      expect(contactStore.addContactToSession).not.toHaveBeenCalled()
    })

    it('should add contact to session if it is called', () => {
      contactStore.findUserContactFromUri = jest.fn().mockReturnValue(USER_CONTACT_JSON)
      contactStore.handleContactCreation(CONTACT)
      expect(contactStore.addContactToSession).toHaveBeenCalledWith(USER_CONTACT)
    })
  })

  describe('cleanSessionContacts', () => {
    beforeEach(() => {
      contactStore.removeContactFromSession = jest.fn()
    })

    it('should not remove contact from session if contact still exists', () => {
      contactStore.addContact(CONTACT)
      contactStore.addContactToSession(CONTACT)
      contactStore.cleanSessionContacts()
      expect(contactStore.removeContactFromSession).not.toHaveBeenCalled()
    })

    it('should remove contact from session if contact does not exist', () => {
      contactStore.partners.add(CONTACT.id)
      contactStore.cleanSessionContacts()
      expect(contactStore.removeContactFromSession).toHaveBeenCalledWith(CONTACT.id)
    })
  })

  describe('handleContactRemoval', () => {
    it('should request contact removal from contacts Map', () => {
      contactStore.removeContact = jest.fn()

      contactStore.handleContactRemoval(BUDDY_PATH)
      expect(contactStore.removeContact).toHaveBeenCalledWith(BUDDY_ID)
    })
  })

  describe('handleContactPropertyUpdate', () => {
    beforeEach(() => {
      contactStore.addContact(CONTACT)
      contactStore.updateContact = jest.fn()
    })

    it('should update contact\'s name property', () => {
      contactStore.handleContactPropertyUpdate('.buddies.' + BUDDY_ID + '.name', 'test')
      expect(contactStore.updateContact).toHaveBeenCalledWith(BUDDY_ID, 'name', 'test')
    })

    it('should update contact\'s name authorized', () => {
      contactStore.handleContactPropertyUpdate('.buddies.' + BUDDY_ID + '.whitelisted', true)
      expect(contactStore.updateContact).toHaveBeenCalledWith(BUDDY_ID, 'authorized', true)
    })

    it('should update contact\'s name send_status', () => {
      contactStore.handleContactPropertyUpdate('.buddies.' + BUDDY_ID + '.send_status', 'test')
      expect(contactStore.updateContact).toHaveBeenCalledWith(BUDDY_ID, 'sendStatus', 'test')
    })

    it('should update contact\'s name recv_status', () => {
      contactStore.handleContactPropertyUpdate('.buddies.' + BUDDY_ID + '.recv_status', 'test')
      expect(contactStore.updateContact).toHaveBeenCalledWith(BUDDY_ID, 'recvStatus', 'test')
    })

    it('should update contact\'s name status_text', () => {
      contactStore.handleContactPropertyUpdate('.buddies.' + BUDDY_ID + '.status_text', 'test')
      expect(contactStore.updateContact).toHaveBeenCalledWith(BUDDY_ID, 'statusText', 'test')
    })

    it('should update contact\'s name subscription_state', () => {
      contactStore.handleContactPropertyUpdate('.buddies.' + BUDDY_ID + '.subscription_state', 'test')
      expect(contactStore.updateContact).toHaveBeenCalledWith(BUDDY_ID, 'subState', 'test')
    })
  })

  describe('handleConnectionUpdate', () => {
    beforeEach(() => {
      contactStore.addContact(CONTACT)
      contactStore.addContactToSession = jest.fn()
      contactStore.updateContact = jest.fn()
    })

    it('should request contact update with new connections and add contact to session', () => {
      contactStore.handleConnectionUpdate('.buddies.' + BUDDY_ID + '.connections', ['test'])
      expect(contactStore.addContactToSession).toHaveBeenCalledWith(CONTACT)
      expect(contactStore.updateContact).toHaveBeenCalledWith(BUDDY_ID, 'connections', ['test'])
    })

    it('should request contact update but not add contact to session if there are no connections', () => {
      contactStore.handleConnectionUpdate('.buddies.' + BUDDY_ID + '.connections', [])
      expect(contactStore.addContactToSession).not.toHaveBeenCalled()
      expect(contactStore.updateContact).toHaveBeenCalledWith(BUDDY_ID, 'connections', [])

      contactStore.handleConnectionUpdate('.buddies.' + BUDDY_ID + '.connections', '[]')
      expect(contactStore.addContactToSession).not.toHaveBeenCalled()
      expect(contactStore.updateContact).toHaveBeenCalledWith(BUDDY_ID, 'connections', [])
    })

    it('should not update contact and log a warning if grafted value is not an array', () => {
      contactStore.handleConnectionUpdate('.buddies.' + BUDDY_ID + '.connections', 'I am a failure')
      expect(contactStore.updateContact).not.toHaveBeenCalled()
      expect(LOG.warn).toHaveBeenCalled()
    })

    it('should not update contact and log a warning if contact does not exist in contacts Map', () => {
      contactStore.handleConnectionUpdate('.buddies.99.connections', ['test'])
      expect(contactStore.updateContact).not.toHaveBeenCalled()
      expect(LOG.warn).toHaveBeenCalled()
    })
  })

  describe('populateContactListFromConfig', () => {
    const uris = Object.keys(CONTACT_LIST['qasip1@dev.sip.test'])

    const name0 = CONTACT_LIST['qasip1@dev.sip.test'][uris[0]].name
    const name1 = CONTACT_LIST['qasip1@dev.sip.test'][uris[1]].name

    const contact0 = Contact.fromJSON({ id: uris[0], uri: uris[0], name: name0 })
    const contact1 = Contact.fromJSON({ id: uris[1], uri: uris[1], name: name1 })

    beforeEach(() => {
      configStore.setUserContacts(CONTACT_LIST)
      contactStore.applyContactCreation = jest.fn().mockResolvedValue(true)
    })

    it('should create contacts associated with specified URI', async () => {
      await contactStore.populateContactListFromConfig('qasip1@dev.sip.test')

      expect(contactStore.applyContactCreation)
        .toHaveBeenNthCalledWith(1, contact0, contactStore.defaultAuthorization)

      expect(contactStore.applyContactCreation)
        .toHaveBeenNthCalledWith(2, contact1, contactStore.defaultAuthorization)

      expect(LOG.warn).not.toHaveBeenCalled()
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should do nothing if no contacts exist for specified URI', async () => {
      await contactStore.populateContactListFromConfig('test')
      expect(contactStore.applyContactCreation).not.toHaveBeenCalled()
    })

    it('should skip existing contacts', async () => {
      contactStore.addContact(Contact.fromJSON({ id: '0', uri: uris[0], ...contact0 }))
      await contactStore.populateContactListFromConfig('qasip1@dev.sip.test')

      expect(contactStore.applyContactCreation)
        .toHaveBeenNthCalledWith(1, contact1, contactStore.defaultAuthorization)
    })
  })

  describe('applyContactCreation', () => {
    beforeEach(() => {
      ({ methodAPI } = socketStore.APIs)
      methodAPI.addBuddy = jest.fn().mockResolvedValue(true)
    })

    it('should apply contact creation', async () => {
      expect(await contactStore.applyContactCreation(CONTACT)).toEqual(true)
      expect(methodAPI.addBuddy).toHaveBeenCalledWith(SIP_ID, CONTACT.sipUri, CONTACT.sipUser)
      expect(LOG.warn).not.toHaveBeenCalled()
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should stop creation and log a warning if buddy URI is the current logged in user', async () => {
      sipStore.setCredentials(SipCredentials.fromJSON({ sipUser: BUDDY_USER, sipServer: BUDDY_SERVER }))
      expect(await contactStore.applyContactCreation(CONTACT)).toEqual(false)
      expect(methodAPI.addBuddy).not.toHaveBeenCalled()
      expect(LOG.warn).toHaveBeenCalled()
    })

    it('should stop creation and log an error if buddy cannot be created', async () => {
      methodAPI.addBuddy = jest.fn().mockResolvedValue(false)
      expect(await contactStore.applyContactCreation(CONTACT)).toEqual(false)
      expect(methodAPI.addBuddy).toHaveBeenCalledWith(SIP_ID, CONTACT.sipUri, CONTACT.sipUser)
      expect(LOG.error).toHaveBeenCalled()
    })

    it('should log an error if a request fails', async () => {
      methodAPI.addBuddy = jest.fn().mockRejectedValue(false)
      expect(await contactStore.applyContactCreation(CONTACT)).toEqual(false)
      expect(methodAPI.addBuddy).toHaveBeenCalledWith(SIP_ID, CONTACT.sipUri, CONTACT.sipUser)
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('applyContactName', () => {
    beforeEach(() => {
      ({ methodAPI } = socketStore.APIs)
      contactStore.addContact(CONTACT)
      methodAPI.invoke = jest.fn().mockResolvedValue(true)
    })

    it('should apply name to contact', async () => {
      expect(await contactStore.applyContactName(BUDDY_URI, 'name')).toEqual(true)
      expect(methodAPI.invoke).toHaveBeenCalledWith(SIP_ID, 'name_buddy', ['name', BUDDY_URI])
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should stop applying name and log an error if contact doesn\'t exist', async () => {
      expect(await contactStore.applyContactName('test', 'name')).toEqual(false)
      expect(methodAPI.invoke).not.toHaveBeenCalled()
      expect(LOG.error).toHaveBeenCalled()
    })

    it('should log an error if name could not be set', async () => {
      methodAPI.invoke = jest.fn().mockResolvedValue(false)

      expect(await contactStore.applyContactName(BUDDY_URI, 'name')).toEqual(false)
      expect(methodAPI.invoke).toHaveBeenCalledWith(SIP_ID, 'name_buddy', ['name', BUDDY_URI])
      expect(LOG.error).toHaveBeenCalled()
    })

    it('should log an error if request fails', async () => {
      methodAPI.invoke = jest.fn().mockRejectedValue(false)

      expect(await contactStore.applyContactName(BUDDY_URI, 'name')).toEqual(false)
      expect(methodAPI.invoke).toHaveBeenCalledWith(SIP_ID, 'name_buddy', ['name', BUDDY_URI])
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('applyContactAuthorization', () => {
    beforeEach(() => {
      ({ methodAPI } = socketStore.APIs)
      contactStore.addContact(CONTACT)
      methodAPI.authorize = jest.fn().mockResolvedValue(true)
    })

    it('should apply authorization to contact', async () => {
      expect(await contactStore.applyContactAuthorization(CONTACT, true)).toEqual(true)
      expect(methodAPI.authorize).toHaveBeenCalledWith(SIP_ID, CONTACT.sipUri, CONTACT.sipUser, true)
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should stop applying authorization and log an error if contact doesn\'t exist', async () => {
      expect(await contactStore.applyContactAuthorization('test', true)).toEqual(false)
      expect(methodAPI.authorize).not.toHaveBeenCalled()
      expect(LOG.error).toHaveBeenCalled()
    })

    it('should log an error if authorization could not be set', async () => {
      methodAPI.authorize = jest.fn().mockResolvedValue(false)
      expect(await contactStore.applyContactAuthorization(CONTACT, true)).toEqual(false)
      expect(methodAPI.authorize).toHaveBeenCalledWith(SIP_ID, CONTACT.sipUri, CONTACT.sipUser, true)
      expect(LOG.error).toHaveBeenCalled()
    })

    it('should log an error if request fails', async () => {
      methodAPI.authorize = jest.fn().mockRejectedValue(false)
      expect(await contactStore.applyContactAuthorization(CONTACT, true)).toEqual(false)
      expect(methodAPI.authorize).toHaveBeenCalledWith(SIP_ID, CONTACT.sipUri, CONTACT.sipUser, true)
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('applyCompleteContactCreation', () => {
    beforeEach(() => {
      ({ methodAPI } = socketStore.APIs)
      contactStore.applyContactCreation = jest.fn().mockResolvedValue(true)
    })
    it('should call applyContactCreation once', async () => {
      expect(await contactStore.applyCompleteContactCreation(BUDDY_URI, BUDDY_NAME, true)).toEqual(true)
      expect(contactStore.applyContactCreation).toHaveBeenCalled()
    })

    it('should add an entry to pendingContacts', async () => {
      expect(await contactStore.applyCompleteContactCreation(BUDDY_URI, BUDDY_NAME, true)).toEqual(true)
      expect(contactStore.pendingContacts.size).toEqual(1)
    })
  })

  describe('applyAttachShmdata', () => {
    const shmdata = Shmdata.fromJSON('test', { caps: 'caps', category: 'test' })

    beforeEach(() => {
      ({ methodAPI } = socketStore.APIs)
      contactStore.addContact(CONTACT)
      methodAPI.attachShmdataToContact = jest.fn().mockResolvedValue(true)
      contactStore.addContactToSession = jest.fn()
    })

    it('should stop attaching and log an error if contact doesn\'t exist', async () => {
      contactStore.removeContact(CONTACT.id)
      expect(await contactStore.applyAttachShmdata(CONTACT, shmdata.path)).toEqual(false)
      expect(methodAPI.attachShmdataToContact).not.toHaveBeenCalled()
      expect(LOG.error).toHaveBeenCalled()
    })

    it('should apply shmdata attachment', async () => {
      expect(await contactStore.applyAttachShmdata(CONTACT, shmdata.path)).toEqual(true)
      expect(methodAPI.attachShmdataToContact).toHaveBeenCalledWith(SIP_ID, CONTACT.sipUri, CONTACT.sipUser, shmdata.path)
      expect(contactStore.addContactToSession).toHaveBeenCalledWith(CONTACT)
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should stop attaching and log an error when shmdata cannot be attached', async () => {
      methodAPI.attachShmdataToContact = jest.fn().mockResolvedValue(false)
      expect(await contactStore.applyAttachShmdata(CONTACT, shmdata.path)).toEqual(false)
      expect(methodAPI.attachShmdataToContact).toHaveBeenCalledWith(SIP_ID, CONTACT.sipUri, CONTACT.sipUser, shmdata.path)
      expect(contactStore.addContactToSession).not.toHaveBeenCalled()
      expect(LOG.error).toHaveBeenCalled()
    })

    it('should log an error if request fails', async () => {
      methodAPI.attachShmdataToContact = jest.fn().mockRejectedValue(false)
      expect(await contactStore.applyAttachShmdata(CONTACT, shmdata.path)).toEqual(false)
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('applyDetachShmdata', () => {
    const shmdata = Shmdata.fromJSON('test', { caps: 'caps', category: 'test' })

    beforeEach(() => {
      ({ methodAPI } = socketStore.APIs)
      contactStore.addContact(CONTACT)
      methodAPI.detachShmdataFromContact = jest.fn().mockResolvedValue(true)
    })

    it('should stop detaching and log an error if contact doesn\'t exist', async () => {
      contactStore.removeContact(CONTACT.id)
      expect(await contactStore.applyDetachShmdata(BUDDY_URI, shmdata.path)).toEqual(false)
      expect(methodAPI.detachShmdataFromContact).not.toHaveBeenCalled()
      expect(LOG.error).toHaveBeenCalled()
    })

    it('should apply shmdata detachment', async () => {
      expect(await contactStore.applyDetachShmdata(BUDDY_URI, shmdata.path)).toEqual(true)
      expect(methodAPI.detachShmdataFromContact).toHaveBeenCalledWith(SIP_ID, CONTACT.sipUri, CONTACT.sipUser, shmdata.path)
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should log an error when shmdata cannot be detached', async () => {
      methodAPI.detachShmdataFromContact = jest.fn().mockResolvedValue(false)
      expect(await contactStore.applyDetachShmdata(BUDDY_URI, shmdata.path)).toEqual(false)
      expect(methodAPI.detachShmdataFromContact).toHaveBeenCalledWith(SIP_ID, CONTACT.sipUri, CONTACT.sipUser, shmdata.path)
      expect(LOG.error).toHaveBeenCalled()
    })

    it('should log an error if request fails', async () => {
      methodAPI.detachShmdataFromContact = jest.fn().mockRejectedValue(false)
      expect(await contactStore.applyDetachShmdata(BUDDY_URI, shmdata.path)).toEqual(false)
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('applyCall', () => {
    beforeEach(() => {
      ({ methodAPI } = socketStore.APIs)
      contactStore.addContact(CONTACT)
      contactStore.addContactToSession(CONTACT)
      contactStore.updateContact(CONTACT.id, 'connections', ['test'])
      methodAPI.invoke = jest.fn().mockResolvedValue(true)
    })

    it('should stop calling and log an error if contact doesn\'t exist', async () => {
      contactStore.removeContact(CONTACT.id)
      expect(await contactStore.applyCall(BUDDY_URI)).toEqual(false)
      expect(methodAPI.invoke).not.toHaveBeenCalled()
      expect(LOG.error).toHaveBeenCalled()
    })

    it('should stop calling and log an error if no shmdata is attached to contact', async () => {
      contactStore.updateContact(CONTACT.id, 'connections', [])
      expect(await contactStore.applyCall(BUDDY_URI)).toEqual(false)
      expect(methodAPI.invoke).not.toHaveBeenCalled()
      expect(LOG.error).toHaveBeenCalled()
    })

    it('should apply call', async () => {
      expect(await contactStore.applyCall(BUDDY_URI)).toEqual(true)
      expect(methodAPI.invoke).toHaveBeenCalledWith(SIP_ID, 'send', [BUDDY_URI])
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should log an error when call cannot be made', async () => {
      methodAPI.invoke = jest.fn().mockResolvedValue(false)
      expect(await contactStore.applyCall(BUDDY_URI)).toEqual(false)
      expect(methodAPI.invoke).toHaveBeenCalledWith(SIP_ID, 'send', [BUDDY_URI])
      expect(LOG.error).toHaveBeenCalled()
    })

    it('should log an error if request fails', async () => {
      methodAPI.invoke = jest.fn().mockRejectedValue(false)
      expect(await contactStore.applyCall(BUDDY_URI)).toEqual(false)
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('applyHangup', () => {
    beforeEach(() => {
      ({ methodAPI } = socketStore.APIs)
      contactStore.addContact(CONTACT)
      methodAPI.invoke = jest.fn().mockResolvedValue(true)
    })

    it('should stop hanging up and log an error if contact doesn\'t exist', async () => {
      contactStore.removeContact(CONTACT.id)

      expect(await contactStore.applyHangup(BUDDY_URI)).toEqual(false)
      expect(methodAPI.invoke).not.toHaveBeenCalled()
      expect(LOG.error).toHaveBeenCalled()
    })

    it('should apply hang up', async () => {
      expect(await contactStore.applyHangup(BUDDY_URI)).toEqual(true)
      expect(methodAPI.invoke).toHaveBeenCalledWith(SIP_ID, 'hang-up', [BUDDY_URI])
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should log an error when hanging up cannot be executed', async () => {
      methodAPI.invoke = jest.fn().mockResolvedValue(false)

      expect(await contactStore.applyHangup(BUDDY_URI)).toEqual(false)
      expect(methodAPI.invoke).toHaveBeenCalledWith(SIP_ID, 'hang-up', [BUDDY_URI])
      expect(LOG.error).toHaveBeenCalled()
    })

    it('should log an error if request fails', async () => {
      methodAPI.invoke = jest.fn().mockRejectedValue(false)

      expect(await contactStore.applyHangup(BUDDY_URI)).toEqual(false)
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('applyHangupAllCalls', () => {
    beforeEach(() => {
      ({ methodAPI } = socketStore.APIs)
      methodAPI.invoke = jest.fn().mockResolvedValue(true)
    })

    it('should apply hang up for all calls', async () => {
      expect(await contactStore.applyHangupAllCalls()).toEqual(true)
      expect(methodAPI.invoke).toHaveBeenCalledWith(SIP_ID, 'hang_up_all', [])
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should log an error when hanging up cannot be executed', async () => {
      methodAPI.invoke = jest.fn().mockResolvedValue(false)

      expect(await contactStore.applyHangupAllCalls()).toEqual(false)
      expect(methodAPI.invoke).toHaveBeenCalledWith(SIP_ID, 'hang_up_all', [])
      expect(LOG.error).toHaveBeenCalled()
    })

    it('should log an error if request fails', async () => {
      methodAPI.invoke = jest.fn().mockRejectedValue(false)

      expect(await contactStore.applyHangupAllCalls()).toEqual(false)
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('applyBlocklist', () => {
    beforeEach(() => {
      contactStore.addContact(CONTACT)
      contactStore.applyContactAuthorization = jest.fn().mockResolvedValue(true)
    })

    it('should apply blacklist to all contacts not in current session', async () => {
      contactStore.hasSessionContact = jest.fn().mockReturnValue(false)
      await contactStore.applyBlocklist()
      expect(contactStore.applyContactAuthorization).toHaveBeenCalledWith(CONTACT, contactStore.defaultAuthorization)
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should not apply blacklist to contacts in current session', async () => {
      contactStore.hasSessionContact = jest.fn().mockReturnValue(true)

      await contactStore.applyBlocklist()
      expect(contactStore.applyContactAuthorization).not.toHaveBeenCalled()
      expect(LOG.error).not.toHaveBeenCalled()
    })
  })

  describe('getExportableContacts', () => {
    beforeEach(() => {
      configStore.setUserContacts(CONTACT_LIST)
    })

    it('should pack user contacts when not logged in', () => {
      expect(contactStore.getExportableContacts()).toEqual(CONTACT_LIST)
    })

    it('should pack user contacts when logged in and include existing contacts', () => {
      contactStore.addContact(CONTACT)
      sipStore.setCredentials(SIP_CREDENTIALS)

      const localContacts = { [sipStore.currentUri]: { [BUDDY_URI]: { name: BUDDY_URI } } }
      const exportedContacts = { ...CONTACT_LIST, ...localContacts }

      expect(contactStore.getExportableContacts()).toEqual(exportedContacts)
    })
  })

  describe('updateContact', () => {
    beforeEach(() => {
      contactStore.addContact(CONTACT)
      LOG.info = jest.fn()
    })

    it('should update contact', () => {
      expect(contactStore.contacts.get(BUDDY_ID).name).toEqual(BUDDY_URI)
      contactStore.updateContact(BUDDY_ID, 'name', 'test')
      expect(contactStore.contacts.get(BUDDY_ID).name).toEqual('test')
      expect(LOG.info).toHaveBeenCalled()
    })

    it('should not update contact when value is the same', async () => {
      contactStore.updateContact(BUDDY_ID, 'name', BUDDY_URI)
      expect(contactStore.contacts.get(BUDDY_ID).name).toEqual(BUDDY_URI)
      expect(LOG.info).not.toHaveBeenCalled()
    })
  })

  describe('setDefaultAuthorization', () => {
    it('should set default authorization', () => {
      expect(contactStore.defaultAuthorization).toEqual(DEFAULT_AUTHORIZATION)
      contactStore.setDefaultAuthorization(true)
      expect(contactStore.defaultAuthorization).toEqual(true)
    })
  })

  describe('addContact', () => {
    it('should add contact to Map', () => {
      contactStore.addContact(CONTACT)
      expect(contactStore.contacts.get(BUDDY_ID)).toEqual(CONTACT)
    })
  })

  describe('removeContact', () => {
    it('should remove contact from Map', () => {
      contactStore.addContact(CONTACT)
      contactStore.removeContact(BUDDY_ID)
      expect(contactStore.contacts.get(BUDDY_ID)).toEqual(undefined)
    })
  })

  describe('addContactToSession', () => {
    beforeEach(() => {
      contactStore.addContact(CONTACT)
    })

    it('should not add contact to session and log an error if it doesn\'t exist', () => {
      contactStore.removeContact(BUDDY_ID)
      contactStore.addContactToSession(CONTACT)
      expect(contactStore.sessionContacts.get(BUDDY_ID)).toEqual(undefined)
      expect(LOG.error).toHaveBeenCalled()
    })

    it('should add contact to session', () => {
      contactStore.addContactToSession(CONTACT)
      expect(contactStore.sessionContacts.get(BUDDY_ID)).toEqual(CONTACT)
      expect(LOG.error).not.toHaveBeenCalled()
    })
  })

  describe('removeContactFromSession', () => {
    it('should remove contact from session', () => {
      contactStore.addContact(CONTACT)
      contactStore.addContactToSession(CONTACT)
      contactStore.removeContactFromSession(BUDDY_ID)
      expect(contactStore.sessionContacts.get(BUDDY_ID)).toEqual(undefined)
    })
  })

  describe('clear', () => {
    it('should clear ContactsStore', () => {
      contactStore.setInitState(InitStateEnum.INITIALIZED)
      contactStore.addContact(CONTACT)
      contactStore.addContactToSession(CONTACT)

      contactStore.clear()
      expect(contactStore.sessionContacts.size).toEqual(0)
      expect(contactStore.contacts.size).toEqual(0)
      expect(contactStore.initState).toEqual(InitStateEnum.NOT_INITIALIZED)
    })
  })
  describe('makeCallerLabel', () => {
    beforeEach(() => {
      quiddityStore.addQuiddity(sipSource)
      shmdataStore.addShmdata(sipSourceShmdata)
      shmdataStore.addWritingShmdata(sipSource.id, sipSourceShmdata.path)
      // in order
      contactStore.addContact(sipSourceContact)
      contactStore.addContactToSession(sipSourceContact)
    })

    it('should return a placeholder if the quiddity is not a sip quiddity', () => {
      expect(contactStore.makeCallerLabel(sdiInput0.id)).toEqual('Waiting for data stream')
    })

    it('should return a placeholder if the quiddity is extshmsrc with a non sip shmdata path', () => {
      contactStore.makeCallerLabel(sipSource.id)
      expect(contactStore.makeCallerLabel(sdiInput0.id)).toEqual('Waiting for data stream')
    })

    it('should return the caller name when the quiddity is a extshmsrc quiddity that is receiving data', () => {
      sipStore.setSipShmdatas(sipSourceShmdata.path, sipSourceShmdata)
      contactStore.makeCallerLabel(sipSource.id)
      expect(contactStore.makeCallerLabel(sipSource.id)).toEqual('sip contact')
    })
  })
})
