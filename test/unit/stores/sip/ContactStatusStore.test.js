/* global describe it expect jest beforeEach */

import { Socket } from '@fixture/socket'
import { configure, toJS } from 'mobx'

import populateStores from '@stores/populateStores.js'

import { SIP_ID } from '@models/quiddity/specialQuiddities'
import { LOG } from '@stores/sip/ContactStatusStore'

import Contact from '@models/sip/Contact'
import SipCredentials from '@models/sip/SipCredentials'
import { BuddyStatusEnum } from '@models/sip/SipEnums'
import StatusEnum from '@models/common/StatusEnum'

configure({ safeDescriptors: false })

const SIP_CREDENTIALS = SipCredentials.fromJSON({
  sipUser: 'test',
  sipServer: 'dev.sip'
})

const BUDDY_ID = '0'
const BUDDY_PATH = '.buddies.' + BUDDY_ID
const BUDDY_USER = 'buddy'
const BUDDY_SERVER = 'dev.sip'
const BUDDY_URI = BUDDY_USER + '@' + BUDDY_SERVER
const CONTACT_JSON = { id: BUDDY_ID, uri: BUDDY_URI }

describe('ContactStatusStore', () => {
  const contact0 = Contact.fromJSON(CONTACT_JSON)
  const contact1 = Contact.fromJSON({
    id: '1',
    uri: 'buddy1@dev.sip',
    status: BuddyStatusEnum.ONLINE
  })
  const contact2 = Contact.fromJSON({
    id: '2',
    uri: 'buddy2@dev.sip',
    status: BuddyStatusEnum.OFFLINE
  })

  let socket
  let socketStore
  let sipStore
  let contactStore
  let contactStatusStore

  beforeEach(() => {
    ({ contactStatusStore, contactStore, socketStore, sipStore } = populateStores())

    socket = new Socket()
    socketStore.setActiveSocket(socket)

    jest.spyOn(sipStore, 'sipId', 'get').mockReturnValue(SIP_ID)
  })

  describe('handleSipRegistration', () => {
    beforeEach(() => {
      contactStatusStore.setSelfStatus = jest.fn()
    })

    it('should set the self status to active when the sip is connected', () => {
      sipStore.setCredentials(SIP_CREDENTIALS)
      expect(contactStatusStore.setSelfStatus).toHaveBeenCalledWith(StatusEnum.ACTIVE)
    })

    it('should set the sip status to error when the sip is not connected', () => {
      contactStatusStore.handleSipRegistration()
      expect(contactStatusStore.setSelfStatus).toHaveBeenCalledWith(StatusEnum.DANGER)
    })
  })

  describe('handleBuddyUpdate', () => {
    beforeEach(() => {
      contactStatusStore.handleContactStatusesUpdate = jest.fn()
      contactStatusStore.setBuddyStatus = jest.fn()
      contactStatusStore.setReceivingStatus = jest.fn()
      contactStatusStore.setSendingStatus = jest.fn()
      contactStore.handleContactUpdate = jest.fn()
    })

    it('should be called when the infoTreeAPI send a grafted buddy', () => {
      contactStatusStore.handleBuddyUpdate = jest.fn()
      socket.onEmit('info_tree_grafted', SIP_ID, BUDDY_PATH, true)
      expect(contactStatusStore.handleBuddyUpdate).toHaveBeenCalled()
    })

    it('should log an error if the buddy isn\'t well-formatted', () => {
      LOG.error = jest.fn()
      contactStatusStore.handleBuddyUpdate(BUDDY_PATH)
      expect(LOG.error).toHaveBeenCalled()
    })

    it('should log a warning if the buddy isn\'t in the contactStore list', () => {
      LOG.warn = jest.fn()
      contactStatusStore.handleBuddyUpdate(BUDDY_PATH, CONTACT_JSON)
      expect(LOG.warn).toHaveBeenCalled()
    })

    it('should update all the statuses from the grafted buddy', () => {
      contactStore.addContact(contact0)
      contactStatusStore.handleBuddyUpdate(BUDDY_PATH, CONTACT_JSON)
      expect(contactStatusStore.setBuddyStatus).toHaveBeenCalledTimes(1)
      expect(contactStatusStore.setReceivingStatus).toHaveBeenCalledTimes(1)
      expect(contactStatusStore.setSendingStatus).toHaveBeenCalledTimes(1)
    })
  })

  describe('contact maps by status', () => {
    beforeEach(() => {
      contactStatusStore.contactStore.addContact(contact0)
      contactStatusStore.contactStore.addContact(contact1)
      contactStatusStore.contactStore.addContact(contact2)
    })
    describe('onlineContacts', () => {
      it('should have a list of ONLINE contacts containing only as many contact as are online', () => {
        expect(Array.from(toJS(contactStatusStore.onlineContacts).values()).length).toEqual(1)
        contactStore.updateContact(contact0.id, 'status', BuddyStatusEnum.ONLINE)
        expect(Array.from(toJS(contactStatusStore.onlineContacts).values()).length).toEqual(2)
      })
    })

    describe('unavailableContacts', () => {
      it('should have a list of contacts that are not ONLINE containing only as many contact as are not online', () => {
        expect(Array.from(toJS(contactStatusStore.unavailableContacts).values()).length).toEqual(2)
        contactStore.updateContact(contact0.id, 'status', BuddyStatusEnum.ONLINE)
        expect(Array.from(toJS(contactStatusStore.unavailableContacts).values()).length).toEqual(1)
      })
    })

    describe('sortedContacts', () => {
      it('should have a list of contacts sorted by their ONLINE status, with the online contacts at the beginning and the other at the end', () => {
        expect(Array.from(toJS(contactStatusStore.sortedContacts).values()).map(contacts => contacts.id)).toEqual(['1', '0', '2'])
        contactStore.updateContact(contact2.id, 'status', BuddyStatusEnum.ONLINE)
        expect(Array.from(toJS(contactStatusStore.sortedContacts).values()).map(contacts => contacts.id)).toEqual(['1', '2', '0'])
      })
    })
  })

  describe('handleContactStatusesUpdate', () => {
    beforeEach(() => {
      contactStatusStore.populateContactStatuses = jest.fn()
      contactStatusStore.cleanContactStatuses = jest.fn()
    })

    it('should populate and clean the statuses for each contact updates', () => {
      contactStore.addContact(contact0)
      expect(contactStatusStore.populateContactStatuses).toHaveBeenCalledTimes(1)
      expect(contactStatusStore.cleanContactStatuses).toHaveBeenCalledTimes(1)
    })
  })

  describe('populateContactStatuses', () => {
    beforeEach(() => {
      contactStatusStore.handleContactStatusesUpdate = jest.fn()
      contactStatusStore.setBuddyStatus = jest.fn()
      contactStatusStore.setReceivingStatus = jest.fn()
      contactStatusStore.setSendingStatus = jest.fn()
    })

    it('should populate the statuses for each contact', () => {
      contactStore.addContact(contact0)
      contactStatusStore.populateContactStatuses()
      expect(contactStatusStore.setBuddyStatus).toHaveBeenCalledTimes(1)
      expect(contactStatusStore.setReceivingStatus).toHaveBeenCalledTimes(1)
      expect(contactStatusStore.setSendingStatus).toHaveBeenCalledTimes(1)
    })
  })

  describe('cleanContactStatuses', () => {
    beforeEach(() => {
      contactStatusStore.removeStatuses = jest.fn()
    })

    it('should populate the statuses for each contact', () => {
      contactStatusStore.setBuddyStatus(contact0.id, BuddyStatusEnum.OFFLINE)
      contactStatusStore.cleanContactStatuses()
      expect(contactStatusStore.removeStatuses).toHaveBeenCalledTimes(1)
    })
  })
})
