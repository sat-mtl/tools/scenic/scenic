/* global describe it expect jest beforeEach */

import { Socket } from '@fixture/socket'
import { configure } from 'mobx'

import populateStores from '@stores/populateStores.js'
import { InitializationError, RequirementError } from '@utils/errors'

import SipStore, { LOG } from '@stores/sip/SipStore'
import { SIP_ID, SIP_KIND_ID } from '@models/quiddity/specialQuiddities'
import InitStateEnum from '@models/common/InitStateEnum'

import Quiddity from '@models/quiddity/Quiddity'
import SipCredentials from '@models/sip/SipCredentials'

configure({ safeDescriptors: false })

const SIP_PORT = 5060
const SELF_PATH = '.self'
const SELF_USER = 'test'
const SELF_SERVER = 'dev.sip'
const SELF_URI = `${SELF_USER}@${SELF_SERVER}`
const SELF_URI_JSON = `"${SELF_USER}@${SELF_SERVER}"`

const SIP_CREDENTIALS = SipCredentials.fromJSON({
  sipUser: SELF_USER,
  sipServer: SELF_SERVER,
  port: SIP_PORT
})

const SIP_INIT = {
  id: 'SIP',
  kindId: 'sip',
  isHidden: true,
  isProtected: true,
  properties: {
    port: 5060
  },
  userTree: { hello: true }
}

describe('SipStore', () => {
  let socket, socketStore, configStore, quiddityStore, sipStore, shmdataStore
  let infoTreeAPI, methodAPI, propertyAPI

  beforeEach(() => {
    ({ socketStore, configStore, quiddityStore, sipStore, shmdataStore } = populateStores())

    LOG.error = jest.fn()
    LOG.warn = jest.fn()

    socket = new Socket()
    socketStore.setActiveSocket(socket)

    jest.spyOn(sipStore, 'sipId', 'get').mockReturnValue(SIP_ID)
  })

  describe('constructor', () => {
    beforeEach(() => {
      sipStore.handleSocketChange = jest.fn()
      sipStore.handleQuiddityStoreInitialization = jest.fn()
    })

    it('should fail if the configStore is not provided', () => {
      /* eslint-disable no-new */
      expect(() => { new SipStore(socketStore) }).toThrow(RequirementError)
      /* eslint-enable no-new */
    })

    it('should fail if the quiddityStore is not provided', () => {
      /* eslint-disable no-new */
      expect(() => { new SipStore(socketStore, configStore) }).toThrow(RequirementError)
      /* eslint-enable no-new */
    })

    it('should be correctly instantiated', () => {
      sipStore = new SipStore(socketStore, configStore, quiddityStore, shmdataStore) // eslint-disable-line no-new
      expect(sipStore).toBeDefined()
      expect(sipStore.initState).toEqual(InitStateEnum.NOT_INITIALIZED)
    })

    it('should react to the SocketStore\'s active socket change', () => {
      socketStore.activeSocket = new Socket()
      expect(sipStore.handleSocketChange).toHaveBeenCalledWith(socketStore.activeSocket)
    })

    it('should react to the QuiddityStore\'s initialization state changes', () => {
      quiddityStore.setInitState(InitStateEnum.INITIALIZED)
      expect(sipStore.handleQuiddityStoreInitialization).toHaveBeenCalledWith(InitStateEnum.INITIALIZED)
      quiddityStore.setInitState(InitStateEnum.NOT_INITIALIZED)
      expect(sipStore.handleQuiddityStoreInitialization).toHaveBeenCalledWith(InitStateEnum.NOT_INITIALIZED)
    })
  })

  describe('initialize', () => {
    const QUIDDITY_TEST = new Quiddity(SIP_ID, 'sip0', SIP_KIND_ID)

    beforeEach(() => {
      sipStore.fallbackSipQuiddity = jest.fn()
      sipStore.fetchRegisteredUri = jest.fn().mockResolvedValue(null)
      sipStore.handleSipRegistration = jest.fn()
    })

    it('should not initialize if the quiddity store isn\'t initialized', async () => {
      sipStore.setInitState = jest.fn()

      expect(sipStore.initialize()).rejects.toThrow(InitializationError)
      expect(sipStore.setInitState).not.toHaveBeenCalled()
    })

    it('should not initialize if store was already initialized', async () => {
      sipStore.setInitState(InitStateEnum.INITIALIZED)
      sipStore.setInitState = jest.fn()

      expect(await sipStore.initialize()).toEqual(true)
      expect(sipStore.setInitState).not.toHaveBeenCalled()
    })

    it('should not fallback if the SIP quiddity was created', async () => {
      quiddityStore.setInitState(InitStateEnum.INITIALIZED)
      quiddityStore.quiddities.set(SIP_ID, QUIDDITY_TEST)
      sipStore.fallbackSIPQuiddity = jest.fn()

      expect(await sipStore.initialize()).toEqual(true)
      expect(sipStore.fallbackSIPQuiddity).not.toHaveBeenCalled()
    })

    it('should fallback if the SIP quiddity wasn\'t created', async () => {
      quiddityStore.setInitState(InitStateEnum.INITIALIZED)
      sipStore.fallbackSIPQuiddity = jest.fn()

      expect(await sipStore.initialize()).toEqual(true)
      expect(sipStore.fallbackSIPQuiddity).toHaveBeenCalled()
    })

    it('should initialize store but not SIP registration if not logged in to SIP server', async () => {
      quiddityStore.setInitState(InitStateEnum.INITIALIZED)
      expect(await sipStore.initialize()).toEqual(true)
      expect(sipStore.handleSipRegistration).not.toHaveBeenCalled()
      expect(sipStore.initState).toEqual(InitStateEnum.INITIALIZED)
    })

    it('should initialize store and handle SIP registration if logged in to SIP server', async () => {
      quiddityStore.setInitState(InitStateEnum.INITIALIZED)
      sipStore.fetchRegisteredUri = jest.fn().mockResolvedValue(SELF_URI)

      expect(await sipStore.initialize()).toEqual(true)
      expect(sipStore.handleSipRegistration).toHaveBeenCalledWith(SELF_URI)
      expect(sipStore.initState).toEqual(InitStateEnum.INITIALIZED)
    })
  })

  describe('fallbackSIPQuiddity', () => {
    beforeEach(() => {
      quiddityStore.setInitState(InitStateEnum.INITIALIZED)
      configStore.findInitialConfiguration = jest.fn().mockReturnValue(SIP_INIT)
      quiddityStore.applyQuiddityCreation = jest.fn().mockResolvedValue(true)
    })

    it('should request the SIP quiddity creation if it was not created', async () => {
      await sipStore.fallbackSIPQuiddity()
      expect(configStore.findInitialConfiguration).toHaveBeenCalledWith(SIP_KIND_ID, SIP_ID)
      expect(quiddityStore.applyQuiddityCreation).toHaveBeenCalledWith(SIP_KIND_ID, SIP_ID, SIP_INIT.properties, SIP_INIT.userTree)
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should request the SIP quiddity creation with no default properties and userTree if they don\'t exist', async () => {
      configStore.findInitialConfiguration = jest.fn().mockReturnValue(null)
      await sipStore.fallbackSIPQuiddity()
      expect(configStore.findInitialConfiguration).toHaveBeenCalledWith(SIP_KIND_ID, SIP_ID)
      expect(quiddityStore.applyQuiddityCreation).toHaveBeenCalledWith(SIP_KIND_ID, SIP_ID)
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should log an error if the request has failed', async () => {
      quiddityStore.applyQuiddityCreation = jest.fn().mockRejectedValue(false)
      await sipStore.fallbackSIPQuiddity()
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('fetchRegisteredUri', () => {
    beforeEach(() => {
      ({ infoTreeAPI } = socketStore.APIs)
    })

    it('should fetch the current registered URI if logged in to SIP server', async () => {
      infoTreeAPI.get = jest.fn().mockResolvedValue(SELF_URI)
      expect(await sipStore.fetchRegisteredUri()).toEqual(SELF_URI)
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should return null if not logged in to SIP server', async () => {
      infoTreeAPI.get = jest.fn().mockResolvedValue('null')
      expect(await sipStore.fetchRegisteredUri()).toEqual(null)
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should return null and log an error if fetch value is invalid', async () => {
      infoTreeAPI.get = jest.fn().mockResolvedValue(undefined)
      expect(await sipStore.fetchRegisteredUri()).toEqual(null)
      expect(LOG.error).toHaveBeenCalled()
      infoTreeAPI.get = jest.fn().mockResolvedValue('')
      expect(await sipStore.fetchRegisteredUri()).toEqual(null)
      expect(LOG.error).toHaveBeenCalled()
    })

    it('should return null and log an error if the request has failed', async () => {
      infoTreeAPI.get = jest.fn().mockRejectedValue(false)
      expect(await sipStore.fetchRegisteredUri()).toEqual(null)
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('handleSocketChange', () => {
    beforeEach(() => {
      ({ infoTreeAPI } = socketStore.APIs)

      sipStore.handleSipRegistration = jest.fn()
      sipStore.clear = jest.fn()
    })

    it('should define the APIs if there is a new socket', () => {
      const onGraftSpy = jest.spyOn(infoTreeAPI, 'onGrafted')
      const newSocket = new Socket()

      sipStore.handleSocketChange(newSocket)
      expect(sipStore.clear).toHaveBeenCalled()

      expect(onGraftSpy).toHaveBeenCalledWith(
        expect.any(Function),
        expect.any(Function),
        expect.any(Function)
      )
    })

    it('should clear the store if the new socket is null', () => {
      sipStore.handleSocketChange(null)
      expect(sipStore.clear).toHaveBeenCalled()
    })

    it('should call update when `info_tree_grafted` is emitted and (quiddity ID and path) match', () => {
      socket.onEmit('info_tree_grafted', SIP_ID, SELF_PATH, SELF_URI_JSON)

      expect(sipStore.handleSipRegistration).toHaveBeenCalledWith(JSON.parse(SELF_URI_JSON))
    })

    it('should not call update when the path or ID doesn\'t match', () => {
      socket.onEmit('info_tree_grafted', SIP_ID, 'i am a failure', '"test"')
      expect(sipStore.handleSipRegistration).not.toHaveBeenCalled()
      socket.onEmit('info_tree_grafted', 'failure', SELF_PATH, '"test"')
      expect(sipStore.handleSipRegistration).not.toHaveBeenCalled()
    })
  })

  describe('handleQuiddityStoreInitialization', () => {
    beforeEach(() => {
      sipStore.clear = jest.fn()
    })

    it('should clear the Store if the QuiddityStore gets uninitialized', () => {
      quiddityStore.setInitState(InitStateEnum.INITIALIZED)
      expect(sipStore.clear).not.toHaveBeenCalled()
      quiddityStore.setInitState(InitStateEnum.NOT_INITIALIZED)
      expect(sipStore.clear).toHaveBeenCalled()
    })
  })

  describe('handleSipRegistration', () => {
    beforeEach(() => {
      ({ propertyAPI } = socketStore.APIs)
      sipStore.setCredentials = jest.fn()
    })

    it('should update credentials with current URI if logging in to SIP server', async () => {
      sipStore.currentCredentials = null
      propertyAPI.get = jest.fn().mockResolvedValue(SIP_PORT)

      await sipStore.handleSipRegistration(SELF_URI)
      expect(sipStore.setCredentials).toHaveBeenCalledWith(SIP_CREDENTIALS)
    })

    it('should clear current credentials if logging out of SIP server', async () => {
      await sipStore.handleSipRegistration('null')
      expect(sipStore.setCredentials).toHaveBeenCalledWith(null)
    })

    it('should not update current credentials if new credentials are the same', async () => {
      sipStore.currentCredentials = SIP_CREDENTIALS

      await sipStore.handleSipRegistration(SELF_URI)
      expect(sipStore.setCredentials).not.toHaveBeenCalled()
    })
  })

  describe('applySipPort', () => {
    beforeEach(() => {
      ({ propertyAPI } = socketStore.APIs)
      propertyAPI.set = jest.fn().mockResolvedValue(true)
    })

    it('should set new port', async () => {
      expect(await sipStore.applySipPort(5060)).toEqual(true)
      expect(propertyAPI.set).toHaveBeenCalledWith(SIP_ID, 'port', 5060)
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should log an error if port could not be set', async () => {
      propertyAPI.set = jest.fn().mockResolvedValue(false)

      expect(await sipStore.applySipPort(5060)).toEqual(false)
      expect(propertyAPI.set).toHaveBeenCalledWith(SIP_ID, 'port', 5060)
      expect(LOG.error).toHaveBeenCalled()
    })

    it('should log an error if request fails', async () => {
      propertyAPI.set = jest.fn().mockRejectedValue(false)

      expect(await sipStore.applySipPort(5060)).toEqual(false)
      expect(propertyAPI.set).toHaveBeenCalledWith(SIP_ID, 'port', 5060)
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('applyStunTurnConfiguration', () => {
    beforeEach(() => {
      ({ methodAPI } = socketStore.APIs)
      methodAPI.setStunTurn = jest.fn().mockResolvedValue(true)
    })

    it('should set STUN/TURN configuration', async () => {
      expect(
        await sipStore.applyStunTurnConfiguration(
          SIP_CREDENTIALS.stunServer,
          SIP_CREDENTIALS.turnServer,
          SIP_CREDENTIALS.turnUser,
          SIP_CREDENTIALS.turnPassword)
      ).toEqual(true)

      expect(methodAPI.setStunTurn).toHaveBeenCalledWith(SIP_ID,
        SIP_CREDENTIALS.stunServer,
        SIP_CREDENTIALS.turnServer,
        SIP_CREDENTIALS.turnUser,
        SIP_CREDENTIALS.turnPassword
      )

      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should log an error if port could not be set', async () => {
      methodAPI.setStunTurn = jest.fn().mockResolvedValue(false)

      expect(
        await sipStore.applyStunTurnConfiguration(
          SIP_CREDENTIALS.stunServer,
          SIP_CREDENTIALS.turnServer,
          SIP_CREDENTIALS.turnUser,
          SIP_CREDENTIALS.turnPassword
        )
      ).toEqual(false)

      expect(methodAPI.setStunTurn).toHaveBeenCalledWith(SIP_ID,
        SIP_CREDENTIALS.stunServer,
        SIP_CREDENTIALS.turnServer,
        SIP_CREDENTIALS.turnUser,
        SIP_CREDENTIALS.turnPassword
      )

      expect(LOG.error).toHaveBeenCalled()
    })

    it('should log an error if request fails', async () => {
      methodAPI.setStunTurn = jest.fn().mockRejectedValue(false)

      expect(
        await sipStore.applyStunTurnConfiguration(
          SIP_CREDENTIALS.stunServer,
          SIP_CREDENTIALS.turnServer,
          SIP_CREDENTIALS.turnUser,
          SIP_CREDENTIALS.turnPassword
        )
      ).toEqual(false)

      expect(methodAPI.setStunTurn).toHaveBeenCalledWith(SIP_ID,
        SIP_CREDENTIALS.stunServer,
        SIP_CREDENTIALS.turnServer,
        SIP_CREDENTIALS.turnUser,
        SIP_CREDENTIALS.turnPassword
      )

      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('applySipRegistration', () => {
    beforeEach(() => {
      ({ methodAPI } = socketStore.APIs)
      methodAPI.register = jest.fn().mockResolvedValue(true)
    })

    it('should apply SIP registration', async () => {
      expect(
        await sipStore.applySipRegistration(SIP_CREDENTIALS)).toEqual(true)

      expect(methodAPI.register).toHaveBeenCalledWith(SIP_ID,
        SIP_CREDENTIALS.sipServer,
        SIP_CREDENTIALS.sipUser,
        SIP_CREDENTIALS.sipPassword,
        SIP_CREDENTIALS.destinationPort
      )

      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should log an error if registration could not be applied', async () => {
      methodAPI.register = jest.fn().mockResolvedValue(false)

      expect(
        await sipStore.applySipRegistration(SIP_CREDENTIALS)).toEqual(false)

      expect(methodAPI.register).toHaveBeenCalledWith(SIP_ID,
        SIP_CREDENTIALS.sipServer,
        SIP_CREDENTIALS.sipUser,
        SIP_CREDENTIALS.sipPassword,
        SIP_CREDENTIALS.destinationPort
      )

      expect(LOG.error).toHaveBeenCalled()
    })

    it('should log an error if request fails', async () => {
      methodAPI.register = jest.fn().mockRejectedValue(false)

      expect(
        await sipStore.applySipRegistration(SIP_CREDENTIALS)).toEqual(false)

      expect(methodAPI.register).toHaveBeenCalledWith(SIP_ID,
        SIP_CREDENTIALS.sipServer,
        SIP_CREDENTIALS.sipUser,
        SIP_CREDENTIALS.sipPassword,
        SIP_CREDENTIALS.destinationPort
      )

      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('applySipUnregistration', () => {
    beforeEach(() => {
      ({ methodAPI, propertyAPI } = socketStore.APIs)
      methodAPI.unregister = jest.fn().mockResolvedValue(true)
    })

    it('should apply SIP unregistration', async () => {
      // applySipUnregistration() fetches 'sip-registration' property to
      // validate if unregistration was sucessful, let's mock it to false
      propertyAPI.get = jest.fn().mockResolvedValue(false)
      expect(await sipStore.applySipUnregistration()).toEqual(true)
      expect(methodAPI.unregister).toHaveBeenCalledWith(SIP_ID)
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should log an error if registration could not be applied', async () => {
      methodAPI.unregister = jest.fn().mockResolvedValue(false)

      expect(await sipStore.applySipUnregistration()).toEqual(false)
      expect(methodAPI.unregister).toHaveBeenCalledWith(SIP_ID)
      expect(LOG.error).toHaveBeenCalled()
    })

    it('should log an error if request fails', async () => {
      methodAPI.unregister = jest.fn().mockRejectedValue(false)

      expect(await sipStore.applySipUnregistration()).toEqual(false)
      expect(methodAPI.unregister).toHaveBeenCalledWith(SIP_ID)
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('applyTerminateCalls', () => {
    beforeEach(() => {
      ({ methodAPI } = socketStore.APIs)
      methodAPI.invoke = jest.fn().mockResolvedValue(true)
    })

    it('should terminate all SIP calls', async () => {
      expect(await sipStore.applyTerminateCalls()).toEqual(true)
      expect(methodAPI.invoke).toHaveBeenCalledWith(SIP_ID, 'hang_up_all', [])
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should log an error if registration could not be applied', async () => {
      methodAPI.invoke = jest.fn().mockResolvedValue(false)

      expect(await sipStore.applyTerminateCalls()).toEqual(false)
      expect(methodAPI.invoke).toHaveBeenCalledWith(SIP_ID, 'hang_up_all', [])
      expect(LOG.error).toHaveBeenCalled()
    })

    it('should log an error if request fails', async () => {
      methodAPI.invoke = jest.fn().mockRejectedValue(false)

      expect(await sipStore.applyTerminateCalls()).toEqual(false)
      expect(methodAPI.invoke).toHaveBeenCalledWith(SIP_ID, 'hang_up_all', [])
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('applyLogin', () => {
    beforeEach(() => {
      sipStore.currentCredentials = null
      sipStore.applySipPort = jest.fn().mockResolvedValue(true)
      sipStore.applyStunTurnConfiguration = jest.fn().mockResolvedValue(true)
      sipStore.applySipRegistration = jest.fn().mockResolvedValue(true)
      sipStore.setCredentials = jest.fn()
    })

    it('should not apply login if already logged in to SIP server', async () => {
      sipStore.currentCredentials = SIP_CREDENTIALS

      expect(await sipStore.applyLogin(SIP_CREDENTIALS)).toEqual(true)
      expect(sipStore.applySipPort).not.toHaveBeenCalled()
      expect(sipStore.applyStunTurnConfiguration).not.toHaveBeenCalled()
      expect(sipStore.applySipRegistration).not.toHaveBeenCalled()
      expect(LOG.warn).toHaveBeenCalled()
    })

    it('should apply login if not logged in to SIP server', async () => {
      expect(await sipStore.applyLogin(SIP_CREDENTIALS)).toEqual(true)
      expect(sipStore.applySipPort).toHaveBeenCalledWith(SIP_PORT)

      expect(sipStore.applyStunTurnConfiguration).toHaveBeenCalledWith(
        SIP_CREDENTIALS.stunServer,
        SIP_CREDENTIALS.turnServer,
        SIP_CREDENTIALS.turnUser,
        SIP_CREDENTIALS.turnPassword
      )

      expect(sipStore.applySipRegistration).toHaveBeenCalledWith(SIP_CREDENTIALS)
      expect(sipStore.setCredentials).toHaveBeenCalledWith(SIP_CREDENTIALS)
    })

    it('should stop login if port cannot be set', async () => {
      sipStore.applySipPort = jest.fn().mockResolvedValue(false)

      expect(await sipStore.applyLogin(SIP_CREDENTIALS)).toEqual(false)
      expect(sipStore.applySipPort).toHaveBeenCalledWith(SIP_PORT)
      expect(sipStore.applyStunTurnConfiguration).not.toHaveBeenCalled()
      expect(sipStore.applySipRegistration).not.toHaveBeenCalled()
      expect(sipStore.setCredentials).not.toHaveBeenCalled()
    })

    it('should stop login if STUN/TURN configuration cannot be set', async () => {
      sipStore.applyStunTurnConfiguration = jest.fn().mockResolvedValue(false)

      expect(await sipStore.applyLogin(SIP_CREDENTIALS)).toEqual(false)
      expect(sipStore.applySipPort).toHaveBeenCalledWith(SIP_PORT)

      expect(sipStore.applyStunTurnConfiguration).toHaveBeenCalledWith(
        SIP_CREDENTIALS.stunServer,
        SIP_CREDENTIALS.turnServer,
        SIP_CREDENTIALS.turnUser,
        SIP_CREDENTIALS.turnPassword
      )

      expect(sipStore.applySipRegistration).not.toHaveBeenCalled()
      expect(sipStore.setCredentials).not.toHaveBeenCalled()
    })

    it('should stop login if registration failed', async () => {
      sipStore.applySipRegistration = jest.fn().mockResolvedValue(false)

      expect(await sipStore.applyLogin(SIP_CREDENTIALS)).toEqual(false)
      expect(sipStore.applySipPort).toHaveBeenCalledWith(SIP_PORT)

      expect(sipStore.applyStunTurnConfiguration).toHaveBeenCalledWith(
        SIP_CREDENTIALS.stunServer,
        SIP_CREDENTIALS.turnServer,
        SIP_CREDENTIALS.turnUser,
        SIP_CREDENTIALS.turnPassword
      )

      expect(sipStore.applySipRegistration).toHaveBeenCalledWith(SIP_CREDENTIALS)
      expect(sipStore.setCredentials).not.toHaveBeenCalled()
    })
  })

  describe('applyLogout', () => {
    beforeEach(() => {
      sipStore.currentCredentials = SIP_CREDENTIALS
      sipStore.applyTerminateCalls = jest.fn().mockResolvedValue(true)
      sipStore.applySipUnregistration = jest.fn().mockResolvedValue(true)
    })

    it('should not apply logout if already logged out of SIP server', async () => {
      sipStore.currentCredentials = null

      expect(await sipStore.applyLogout()).toEqual(true)
      expect(sipStore.applyTerminateCalls).not.toHaveBeenCalled()
      expect(sipStore.applySipUnregistration).not.toHaveBeenCalled()
      expect(LOG.warn).toHaveBeenCalled()
    })

    it('should apply logout if not logged out of SIP server', async () => {
      expect(await sipStore.applyLogout()).toEqual(true)
      expect(sipStore.applyTerminateCalls).toHaveBeenCalled()
      expect(sipStore.applySipUnregistration).toHaveBeenCalled()
    })

    it('should return false if unregistration failed', async () => {
      sipStore.applySipUnregistration = jest.fn().mockResolvedValue(false)

      expect(await sipStore.applyLogout()).toEqual(false)
      expect(sipStore.applyTerminateCalls).toHaveBeenCalled()
      expect(sipStore.applySipUnregistration).toHaveBeenCalled()
    })
  })

  describe('setCredentials', () => {
    it('should set current credentials with provided SipCredentials', () => {
      sipStore.setCredentials(SIP_CREDENTIALS)
      expect(sipStore.currentUri).toEqual(SELF_URI)
    })
  })

  describe('clear', () => {
    it('should clear SipStore', () => {
      sipStore.setInitState(InitStateEnum.INITIALIZED)
      sipStore.setCredentials(SIP_CREDENTIALS)
      expect(sipStore.currentUri).toEqual(SELF_URI)
      expect(sipStore.initState).toEqual(InitStateEnum.INITIALIZED)

      sipStore.clear()
      expect(sipStore.currentUri).toEqual(null)
      expect(sipStore.initState).toEqual(InitStateEnum.NOT_INITIALIZED)
    })
  })
})
