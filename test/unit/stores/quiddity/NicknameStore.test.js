/* global describe it expect jest beforeEach */

import { Socket } from '@fixture/socket'

import populateStores from '@stores/populateStores.js'
import Quiddity from '@models/quiddity/Quiddity'
import { LOG } from '@stores/quiddity/NicknameStore'

describe('NicknameStore', () => {
  const QUIDDITY = new Quiddity('test', 'test0', 'test')
  const NICKNAME_TEST = 'nickname'
  const QUIDDITY_IDS = [QUIDDITY.id]

  let socket, socketStore, quiddityStore, nicknameStore
  let nicknameAPI

  beforeEach(() => {
    ({ socketStore, quiddityStore, nicknameStore } = populateStores())

    LOG.warn = jest.fn()

    socket = new Socket()
    socketStore.setActiveSocket(socket)
  })

  describe('constructor', () => {
    beforeEach(() => {
      nicknameStore.handleQuiddityChange = jest.fn()
      nicknameStore.handleSocketChange = jest.fn()
    })

    it('should be correctly instantiated', () => {
      expect(nicknameStore).toBeDefined()
    })

    it('should react to the SocketStore\'s active socket change', () => {
      socketStore.activeSocket = new Socket()
      expect(nicknameStore.handleSocketChange).toHaveBeenCalledWith(socketStore.activeSocket)
    })

    it('should initialize nickname when quiddity store is updated', () => {
      quiddityStore.addQuiddity(QUIDDITY)
      expect(nicknameStore.handleQuiddityChange).toHaveBeenCalledWith([QUIDDITY.id])
    })
  })

  describe('handleSocketChange', () => {
    beforeEach(() => {
      ({ nicknameAPI } = socketStore.APIs)
      nicknameStore.addNickname = jest.fn()
    })

    it('should define the APIs if there is a new socket', () => {
      const onUpdateSpy = jest.spyOn(nicknameAPI, 'onUpdated')
      nicknameStore.handleSocketChange(socket)

      expect(onUpdateSpy).toHaveBeenCalledWith(
        expect.any(Function)
      )
    })

    it('should update nicknames when the API triggers an update', () => {
      socket.onEmit('nickname_updated', QUIDDITY.id, NICKNAME_TEST)
      expect(nicknameStore.addNickname).toHaveBeenCalledWith(QUIDDITY.id, NICKNAME_TEST)
    })
  })

  describe('handleQuiddityChange', () => {
    beforeEach(() => {
      nicknameStore.updateNickname = jest.fn().mockResolvedValue(QUIDDITY.id)
    })

    it('should feed nicknames with the provided quiddity', async () => {
      await nicknameStore.handleQuiddityChange(QUIDDITY_IDS)
      expect(nicknameStore.updateNickname).toHaveBeenCalledWith(QUIDDITY.id)
    })
  })

  describe('updateNickname', () => {
    let addNicknameSpy

    beforeEach(() => {
      nicknameStore.fetchNickname = jest.fn().mockResolvedValue(NICKNAME_TEST)
      addNicknameSpy = jest.spyOn(nicknameStore, 'addNickname')
    })

    it('should feed nickname twice: one with the quiddity id as default and then with the fetched nickname', async () => {
      await nicknameStore.updateNickname(QUIDDITY.id)
      expect(addNicknameSpy).toHaveBeenCalledTimes(2)
      expect(addNicknameSpy).toHaveBeenNthCalledWith(1, QUIDDITY.id, QUIDDITY.id)
      expect(addNicknameSpy).toHaveBeenNthCalledWith(2, QUIDDITY.id, NICKNAME_TEST)
    })

    it('should\'t feed nickname if it is stored and it fetches the same value', async () => {
      nicknameStore.addNickname(QUIDDITY.id, NICKNAME_TEST)
      addNicknameSpy.mockReset()
      await nicknameStore.updateNickname(QUIDDITY.id)
      expect(addNicknameSpy).not.toHaveBeenCalled()
    })

    it('should feed nickname if it is stored and it fetches a different value', async () => {
      nicknameStore.addNickname(QUIDDITY.id, 'another nickname')
      addNicknameSpy.mockReset()
      await nicknameStore.updateNickname(QUIDDITY.id)
      expect(addNicknameSpy).toHaveBeenCalledTimes(1)
      expect(addNicknameSpy).toHaveBeenCalledWith(QUIDDITY.id, NICKNAME_TEST)
    })
  })

  describe('fetchNickname', () => {
    beforeEach(() => {
      ({ nicknameAPI } = socketStore.APIs)
      nicknameAPI.get = jest.fn().mockResolvedValue(NICKNAME_TEST)
    })

    it('should fetch the nickname of the requested quiddity', async () => {
      expect(await nicknameStore.fetchNickname(QUIDDITY.id)).toEqual(NICKNAME_TEST)
    })

    it('should fail and log an error if no nickname is fetched', async () => {
      nicknameAPI.get = jest.fn().mockRejectedValue(false)
      expect(await nicknameStore.fetchNickname(QUIDDITY.id)).toEqual(QUIDDITY.id)
      expect(LOG.warn).toHaveBeenCalled()
    })
  })

  describe('initializeNickname', () => {
    beforeEach(() => {
      LOG.error = jest.fn()
      nicknameStore.updateNickname = jest.fn()
    })

    it('should initialize nickname by updating it', async () => {
      await nicknameStore.initializeNickname(QUIDDITY.id)
      expect(nicknameStore.updateNickname).toHaveBeenCalled()
    })
  })
})
