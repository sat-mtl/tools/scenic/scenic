/* global describe it expect jest beforeEach */
import populateStores from '@stores/populateStores.js'
import QuiddityMenuStore, { LOG } from '@stores/quiddity/QuiddityMenuStore'
import InitStateEnum from '@models/common/InitStateEnum'

import Quiddity from '@models/quiddity/Quiddity'
import Kind from '@models/quiddity/Kind'
import QuiddityTagEnum from '@models/quiddity/QuiddityTagEnum'

import MenuCollection from '@models/menus/MenuCollection'
import MenuItem from '@models/menus/MenuItem'

const MENU = [
  new MenuCollection('Collection1', [
    new MenuItem('Quiddity1', 'test')
  ])
]

const HIDDEN_MENU = [
  new MenuCollection('Collection1', [
    new MenuItem('Quiddity1', 'test', [], false, false, true)
  ])
]

const { INITIALIZED, NOT_INITIALIZED } = InitStateEnum

const QUIDDITY_KIND = new Kind('test', 'test', 'test', [QuiddityTagEnum.DESTINATION])
const QUIDDITY = new Quiddity('test', 'test0', 'test')

describe('QuiddityMenuStore', () => {
  let socketStore, configStore, quiddityStore, quiddityMenuStore, kindStore

  beforeEach(() => {
    ({ socketStore, configStore, quiddityStore, quiddityMenuStore, kindStore } = populateStores())
    LOG.error = jest.fn()
  })

  describe('constructor', () => {
    beforeEach(() => {
      quiddityMenuStore.populateUserMenus = jest.fn()
      quiddityMenuStore.handleQuiddityStoreInitialization = jest.fn()
    })

    it('should be correctly instantiated', () => {
      expect(quiddityMenuStore).toBeDefined()
    })

    it('should fail without configStore', () => {
      expect(() => new QuiddityMenuStore(socketStore)).toThrow()
    })

    it('should fail without quiddityStore', () => {
      expect(() => new QuiddityMenuStore(socketStore, configStore)).toThrow()
    })

    it('should react to the QuiddityStore\'s initialization', () => {
      quiddityStore.setInitState(INITIALIZED)
      expect(quiddityMenuStore.handleQuiddityStoreInitialization).toHaveBeenCalledWith(INITIALIZED)
    })

    it('should react to a quiddity update', () => {
      kindStore.addKind(QUIDDITY_KIND)
      quiddityStore.addQuiddity(QUIDDITY)
      expect(quiddityMenuStore.populateUserMenus).toHaveBeenCalled()
    })
  })

  describe('showableMenus', () => {
    it('should show all simple menus', () => {
      quiddityMenuStore.setMenus(MENU)
      expect(quiddityMenuStore.showableMenus).toEqual(MENU)
    })

    it('shouldn\'t show the hidden menus', () => {
      quiddityMenuStore.setMenus(HIDDEN_MENU)
      expect(quiddityMenuStore.showableMenus).toEqual([])
    })
  })

  describe('populateUserMenu', () => {
    let updateSpy, addSpy

    beforeEach(() => {
      quiddityMenuStore.setMenus(MENU)
      updateSpy = jest.spyOn(quiddityMenuStore, 'updateMenus')
      addSpy = jest.spyOn(quiddityMenuStore, 'addUserMenu')
    })

    it('should update the menus with the quiddities\' state', () => {
      quiddityMenuStore.populateUserMenus()
      expect(updateSpy).toHaveBeenCalled()
      expect(addSpy).toHaveBeenCalled()
    })
  })

  describe('updateMenus', () => {
    beforeEach(() => {
      kindStore.addKind(QUIDDITY_KIND)
      quiddityStore.addQuiddity(QUIDDITY)
      quiddityMenuStore.setMenus(MENU)
    })

    it('should update all menus with the created kinds\' ID', () => {
      expect(quiddityMenuStore.menus[0].items[0].created).toEqual(false)
      quiddityMenuStore.updateMenus()
      expect(quiddityMenuStore.menus[0].items[0].created).toEqual(true)
    })
  })

  describe('handleQuiddityStoreInitialization', () => {
    let initializeSpy, clearSpy

    beforeEach(() => {
      quiddityMenuStore.setMenus(MENU)
      initializeSpy = jest.spyOn(quiddityMenuStore, 'initialize')
      clearSpy = jest.spyOn(quiddityMenuStore, 'clear')
    })

    it('should initialize the menu if the QuiddityStore is initialized', () => {
      quiddityMenuStore.handleQuiddityStoreInitialization(INITIALIZED)
      expect(initializeSpy).toHaveBeenCalled()
      expect(clearSpy).not.toHaveBeenCalled()
    })

    it('should clear the menu if the QuiddityStore is not initialized', () => {
      quiddityMenuStore.handleQuiddityStoreInitialization(NOT_INITIALIZED)
      expect(initializeSpy).not.toHaveBeenCalled()
      expect(clearSpy).toHaveBeenCalled()
    })
  })

  describe('initialize', () => {
    beforeEach(() => {
      quiddityMenuStore.setMenus = jest.fn()
      quiddityMenuStore.populateUserMenus = jest.fn()
    })

    it('should initialize menus only when the QuiddityStore is initialized', () => {
      quiddityMenuStore.initialize()
      expect(quiddityMenuStore.setMenus).not.toHaveBeenCalled()
      expect(quiddityMenuStore.populateUserMenus).not.toHaveBeenCalled()
    })

    it('should populate the menu from the user configuration', () => {
      quiddityMenuStore.menus = ['menu']
      quiddityStore.setInitState(INITIALIZED)

      quiddityMenuStore.initialize()
      expect(quiddityMenuStore.setMenus).toHaveBeenCalledWith(MenuCollection.fromConfig(configStore.menuConfiguration))
      expect(quiddityMenuStore.populateUserMenus).toHaveBeenCalled()
    })

    it('should populate the menu from the quiddity kinds only if it is not configured', () => {
      MenuCollection.fromConfig = jest.fn().mockReturnValue([])
      quiddityStore.setInitState(INITIALIZED)

      quiddityMenuStore.initialize()
      expect(quiddityMenuStore.setMenus).toHaveBeenCalledWith([])
      expect(quiddityMenuStore.populateUserMenus).toHaveBeenCalled()
    })
  })

  describe('clear', () => {
    beforeEach(() => {
      quiddityMenuStore.setMenus(MENU)
      quiddityMenuStore.populateUserMenus()
    })

    it('should clear the user menu', () => {
      expect(quiddityMenuStore.userMenus.size).toEqual(1)
      quiddityMenuStore.clear()
      expect(quiddityMenuStore.userMenus.size).toEqual(0)
    })
  })

  describe('handleQuiddityRequest', () => {
    let store, bindMock

    beforeEach(() => {
      bindMock = jest.fn()
      store = new QuiddityMenuStore(socketStore, configStore, kindStore, quiddityStore)
      store.addMenuBinding('quid1', bindMock)
      quiddityStore.applyQuiddityCreation = jest.fn()
    })

    it('should call mapped function when the bound function is triggered', async () => {
      await store.handleQuiddityRequest('quid1')()
      expect(quiddityStore.applyQuiddityCreation).not.toHaveBeenCalled()
      expect(bindMock).toHaveBeenCalled()
    })

    it('should call API for quiddity creation when the bound function is triggered', async () => {
      await store.handleQuiddityRequest('quid2')()
      expect(quiddityStore.applyQuiddityCreation).toHaveBeenCalledWith('quid2')
      expect(bindMock).not.toHaveBeenCalled()
    })
  })
})
