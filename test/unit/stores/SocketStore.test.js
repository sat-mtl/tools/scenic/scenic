/* global describe it expect jest beforeEach afterEach */

import SocketStore from '@stores/SocketStore'
import populateAPIs from '@api'
import { Socket } from '@fixture/socket'

jest.mock('@api', () => jest.fn().mockReturnValue({ testAPI: Function.prototype }))

const HOST = 'localhost'
const PORT = '8080'

describe('SocketStore', () => {
  let socketStore

  beforeEach(() => {
    socketStore = new SocketStore()
    socketStore.setRemote(HOST, PORT)
  })

  afterEach(() => {
    if (socketStore.candidateSocket) {
      socketStore.candidateSocket.disconnect()
    }
  })

  describe('applyConnection', () => {
    // let onSpy

    // beforeEach(() => {
    //   onSpy = jest.spyOn(Socket.prototype, 'on')
    // })

    it('should create a candidate socket', () => {
      expect(socketStore.candidateSocket).toEqual(null)
      expect(socketStore.activeSocket).toEqual(null)
      socketStore.applyConnection()
      expect(socketStore.candidateSocket).not.toBeNull()
      expect(socketStore.activeSocket).toEqual(null)
    })

    describe('makeSessionId', () => {
      it('should return a string in the format dd-mm-yyy_hh:mm/hostId/uuid', () => {
        const value = socketStore.makeSessionId()
        expect(value).toBeDefined()
        expect(typeof (value)).toBe('string')
      })
    })

    /*
      Moving to Socket.IO 3.0 has broken this test, and fixing it has proven to be hard.
      It has something to do with the new ES6 imports; spying on the Socket class no longer seems to work.
      TODO: Fix this test

      it('should assign callbacks to candidate socket', () => {
      socketStore.applyConnection()
      // On socket creation, Socket.on is always called two times: default callbacks
      // are assigned to 'connecting' and 'connect' events.
      // See node_modules/socket.io-client/lib/manager.js
      expect(onSpy).toHaveBeenCalledTimes(4)
      expect(onSpy).toHaveBeenNthCalledWith(
      3,
      'reconnect_failed',
      expect.any(Function)
      )
      expect(onSpy).toHaveBeenNthCalledWith(
      4,
      'connect',
      expect.any(Function)
      )
      })
    */
  })

  describe('setRemote', () => {
    it('should set current host and port with provided values', () => {
      socketStore.setRemote('test', '1234')
      expect(socketStore.host).toEqual('test')
      expect(socketStore.port).toEqual('1234')
    })
  })

  describe('onConnection', () => {
    it('should set onConnectionCb with provided function', () => {
      const onConnectCb = jest.fn()

      expect(socketStore.onConnectionCb).toEqual(null)
      socketStore.onConnection(onConnectCb)
      expect(socketStore.onConnectionCb).toEqual(onConnectCb)
    })
  })

  describe('onDisconnection', () => {
    it('should set onDisconnectionCb with provided function', () => {
      const onDisconnectCb = jest.fn()

      expect(socketStore.onDisconnectionCb).toEqual(null)
      socketStore.onDisconnection(onDisconnectCb)
      expect(socketStore.onDisconnectionCb).toEqual(onDisconnectCb)
    })
  })

  describe('onReconnectionFailed', () => {
    it('should set onReconnectionFailedCb with provided function', () => {
      const onReconnectFailedCb = jest.fn()

      expect(socketStore.onReconnectionFailedCb).toEqual(null)
      socketStore.onReconnectionFailed(onReconnectFailedCb)
      expect(socketStore.onReconnectionFailedCb).toEqual(onReconnectFailedCb)
    })
  })

  describe('handleSocketConnection', () => {
    const socket = new Socket()
    const connectionSpy = jest.fn()

    beforeEach(() => {
      jest.spyOn(socketStore, 'setActiveSocket')
      socketStore.candidateSocket = socket
      socketStore.onConnection(connectionSpy)
    })

    it('should set the session id', () => {
      socketStore.setSessionId = jest.fn()
      socketStore.handleSocketConnection()
      expect(socketStore.setSessionId).toHaveBeenCalled()
    })

    it('should set the active socket', () => {
      socketStore.handleSocketConnection()
      expect(socketStore.setActiveSocket).toHaveBeenCalledWith(socket)
    })

    it('should configure the socket disconnection', () => {
      socket.on = jest.fn()
      socketStore.handleSocketConnection()
      expect(socket.on).toHaveBeenCalledWith('disconnect', expect.any(Function))
    })

    it('should call the connection callbacks', () => {
      socketStore.handleSocketConnection()
      expect(socketStore.onConnectionCb).toHaveBeenCalled()
    })
  })

  describe('handleSocketDisconnection', () => {
    const socket = new Socket()
    const connectionSpy = jest.fn()

    beforeEach(() => {
      jest.spyOn(socketStore, 'setActiveSocket')
      socketStore.candidateSocket = socket
      socketStore.onDisconnection(connectionSpy)
    })

    it('should reset the active socket', () => {
      socketStore.handleSocketDisconnection()
      expect(socketStore.setActiveSocket).toHaveBeenCalledWith(null)
    })

    it('should call the disconnection callbacks', () => {
      socketStore.handleSocketDisconnection()
      expect(socketStore.onDisconnectionCb).toHaveBeenCalled()
    })
  })

  describe('setActiveSocket', () => {
    it('should populate the APIs when a active socket is set', () => {
      const socket = new Socket()
      socketStore.setActiveSocket(socket)
      expect(populateAPIs).toHaveBeenCalledWith(socket)
      expect(socketStore.APIs).toEqual(populateAPIs())
      expect(socketStore.hasActiveAPIs).toEqual(true)
    })

    it('should not populate the APIs when a active socket is null', () => {
      socketStore.setActiveSocket(null)
      expect(populateAPIs).not.toHaveBeenCalled()
      expect(socketStore.APIs).toEqual({})
      expect(socketStore.hasActiveAPIs).toEqual(false)
    })
  })
})
