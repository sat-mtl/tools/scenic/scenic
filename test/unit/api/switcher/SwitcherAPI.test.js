/* global describe it expect beforeEach */

import { Socket, mockSocketCallback } from '@fixture/api/socket.mock'

import SwitcherAPI from '@api/switcher/SwitcherAPI'

describe('SwitcherAPI', () => {
  const descriptionTest = '{test}'
  const logObjectTest = '{test}'
  const logLevel = 'info'
  const CONFIG_NAME_TEST = 'test'
  let api, mockEmitSocketCallback

  beforeEach(() => {
    api = new SwitcherAPI(new Socket())
    mockEmitSocketCallback = mockSocketCallback.bind(null, api.socket, 'emit')
  })

  describe('constructor', () => {
    it('should SwitcherAPI to be defined', () => {
      expect(api).toBeDefined()
      expect(api.socket).toBeDefined()
    })
  })

  describe('sendBundles', () => {
    const execSendBundles = async (description) => {
      return api.sendBundles(description)
    }

    it('should emit switcher_bundles', async () => {
      execSendBundles(descriptionTest)

      expect(api.socket.emit).toHaveBeenCalledWith(
        'switcher_bundles',
        JSON.stringify(descriptionTest),
        expect.any(Function)
      )
    })

    it('should resolve bundle loading when emit callback contains data', async () => {
      const apiResult = true
      mockEmitSocketCallback(null, apiResult)

      expect(await execSendBundles()).toEqual(apiResult)
    })

    it('should reject subscription when emit callback contains an error', async () => {
      const apiError = new Error('error')
      mockEmitSocketCallback(apiError)

      await expect(execSendBundles()).rejects.toEqual(apiError)
    })
  })

  describe('sendLog', () => {
    const execSendLog = async (logLevel, logObject) => {
      return api.sendLog(logLevel, logObject)
    }

    it('should emit switcher_log', async () => {
      execSendLog(logLevel, logObjectTest)

      expect(api.socket.emit).toHaveBeenCalledWith(
        'switcher_log',
        logLevel,
        logObjectTest,
        expect.any(Function)
      )
    })

    it('should resolve sending logs when emit callback contains data', async () => {
      const apiResult = true
      mockEmitSocketCallback(null, apiResult)

      expect(await execSendLog()).toEqual(apiResult)
    })

    it('should reject when emit callback contains an error', async () => {
      const apiError = new Error('error')
      mockEmitSocketCallback(apiError)

      await expect(execSendLog()).rejects.toEqual(apiError)
    })
  })

  describe('getVersion', () => {
    const execGetVersion = async () => {
      return api.getVersion()
    }

    it('should emit switcher_version', async () => {
      execGetVersion()

      expect(api.socket.emit).toHaveBeenCalledWith(
        'switcher_version',
        expect.any(Function)
      )
    })

    it('should resolve getting the current version when emit callback contains data', async () => {
      const apiResult = true
      mockEmitSocketCallback(null, apiResult)

      expect(await execGetVersion()).toEqual(apiResult)
    })

    it('should reject when emit callback contains an error', async () => {
      const apiError = new Error('error')
      mockEmitSocketCallback(apiError)

      await expect(execGetVersion()).rejects.toEqual(apiError)
    })
  })

  describe('getConfigPaths', () => {
    const execGetConfigPath = async () => {
      return api.getConfigPaths()
    }

    it('should emit switcher_extra_config_paths', async () => {
      execGetConfigPath()

      expect(api.socket.emit).toHaveBeenCalledWith(
        'switcher_extra_config_paths',
        expect.any(Function)
      )
    })

    it('should resolve getting the config paths when emit callback contains data', async () => {
      const apiResult = ['path1', 'path2']
      mockEmitSocketCallback(null, apiResult)

      expect(await execGetConfigPath()).toEqual(apiResult)
    })

    it('should reject when emit callback contains an error', async () => {
      const apiError = new Error('error')
      mockEmitSocketCallback(apiError)

      await expect(execGetConfigPath()).rejects.toEqual(apiError)
    })
  })

  describe('readConfig', () => {
    const execReadConfig = async (configName) => {
      return api.readConfig(configName)
    }

    it('should emit switcher_extra_config_read', async () => {
      execReadConfig(CONFIG_NAME_TEST)

      expect(api.socket.emit).toHaveBeenCalledWith(
        'switcher_extra_config_read',
        CONFIG_NAME_TEST,
        expect.any(Function)
      )
    })

    it('should resolve read config when emit callback contains data', async () => {
      const apiResult = true
      mockEmitSocketCallback(null, apiResult)

      expect(await execReadConfig()).toEqual(apiResult)
    })

    it('should reject subscription when emit callback contains an error', async () => {
      const apiError = new Error('error')
      mockEmitSocketCallback(apiError)

      await expect(execReadConfig()).rejects.toEqual(apiError)
    })
  })
})
