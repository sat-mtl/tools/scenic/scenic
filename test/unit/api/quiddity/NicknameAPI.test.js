/* global jest describe it expect beforeEach */

import { Socket, mockSocketCallback } from '@fixture/api/socket.mock'

import QuiddityAPI from '@api/quiddity/NicknameAPI'

describe('NicknameAPI', () => {
  const QUIDDITY_TEST = 'test'
  const NICKNAME_TEST = 'test'
  const NICKNAME_ERROR = new Error()

  let api, socket, mockEmitSocketCallback

  beforeEach(() => {
    socket = new Socket()
    api = new QuiddityAPI(socket)
    mockEmitSocketCallback = mockSocketCallback.bind(null, api.socket, 'emit')
  })

  describe('constructor', () => {
    it('should expect NicknameAPI to be defined', () => {
      expect(api).toBeDefined()
      expect(api.socket).toStrictEqual(socket)
    })
  })

  describe('get', () => {
    const execGetNickname = async (quiddityID) => {
      return api.get(quiddityID)
    }

    it('should emit `nickname_get`', async () => {
      mockEmitSocketCallback(null, NICKNAME_TEST)
      await execGetNickname(NICKNAME_TEST)

      expect(api.socket.emit).toHaveBeenCalledWith(
        'nickname_get',
        NICKNAME_TEST,
        expect.any(Function)
      )
    })

    it('should resolve nickname getter when callback contains data', async () => {
      mockEmitSocketCallback(null, NICKNAME_TEST)
      expect(await execGetNickname()).toEqual(NICKNAME_TEST)
    })

    it('should reject nickname getter when callback contains error', async () => {
      mockEmitSocketCallback(NICKNAME_ERROR)
      await expect(execGetNickname()).rejects.toEqual(NICKNAME_ERROR)
    })

    it('should reject nickname getter when callback contains nothing', async () => {
      mockEmitSocketCallback(null, null)
      await expect(execGetNickname()).rejects.toThrow()
    })
  })

  describe('set', () => {
    const execSetNickname = async (quiddityId, newNickname) => {
      return api.set(quiddityId, newNickname)
    }

    it('should emit `nickname_set`', async () => {
      mockEmitSocketCallback(null, true)
      await execSetNickname(QUIDDITY_TEST, NICKNAME_TEST)

      expect(api.socket.emit).toHaveBeenCalledWith(
        'nickname_set',
        QUIDDITY_TEST,
        NICKNAME_TEST,
        expect.any(Function)
      )
    })

    it('should resolve nickname setter when callback contains data', async () => {
      mockEmitSocketCallback(null, true)
      expect(await execSetNickname()).toEqual(true)
    })

    it('should reject nickname setter when callback contains error', async () => {
      mockEmitSocketCallback(NICKNAME_ERROR)
      await expect(execSetNickname()).rejects.toEqual(NICKNAME_ERROR)
    })

    it('should resolve nickname setter when callback contains nothing', async () => {
      mockEmitSocketCallback(null, null)
      expect(await execSetNickname()).toEqual(true)
    })
  })

  describe('onUpdated', () => {
    let onUpdateAction

    beforeEach(() => {
      onUpdateAction = jest.fn()
    })

    const execOnUpdate = (quiddityId) => {
      api.onUpdated(onUpdateAction, quiddityId)
      socket.onEmit('nickname_updated', QUIDDITY_TEST, NICKNAME_TEST)
    }

    it('should handle `nickname_updated` by calling a defined onUpdateAction function', () => {
      execOnUpdate(QUIDDITY_TEST)

      expect(api.socket.on).toHaveBeenCalledWith(
        'nickname_updated',
        expect.any(Function)
      )
    })

    it('should call onUpdateAction function when `quiddityId` is matching', () => {
      execOnUpdate(QUIDDITY_TEST)
      expect(onUpdateAction).toHaveBeenCalledWith(QUIDDITY_TEST, NICKNAME_TEST)
    })

    it('shouldn\'t call onUpdateAction function when `quiddityId` isn\'t matching', () => {
      execOnUpdate('I am a failure')
      expect(onUpdateAction).not.toHaveBeenCalled()
    })
  })
})
