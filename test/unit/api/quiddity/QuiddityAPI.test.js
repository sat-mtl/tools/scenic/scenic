/* global jest describe it expect beforeEach */

import { Socket, mockSocketCallback } from '@fixture/api/socket.mock'

import QuiddityAPI from '@api/quiddity/QuiddityAPI'

describe('QuiddityAPI', () => {
  let api, socket, mockEmitSocketCallback

  beforeEach(() => {
    socket = new Socket()
    api = new QuiddityAPI(socket)
    mockEmitSocketCallback = mockSocketCallback.bind(null, api.socket, 'emit')
  })

  describe('constructor', () => {
    it('should expect QuiddityAPI to be defined', () => {
      expect(api).toBeDefined()
      expect(api.socket).toStrictEqual(socket)
    })
  })

  describe('listKinds', () => {
    const execListAvailableQuiddities = async () => {
      return api.listKinds()
    }

    it('should emit switcher_kinds', async () => {
      execListAvailableQuiddities()

      expect(api.socket.emit).toHaveBeenCalledWith(
        'switcher_kinds',
        expect.any(Function)
      )
    })

    it('should resolve list when emit callback contains data', async () => {
      const apiResult = [{ kind: 'test', category: 'test' }]
      mockEmitSocketCallback(null, apiResult)

      expect(await execListAvailableQuiddities()).toEqual(apiResult)
    })

    it('should reject list when emit callback contains an error', async () => {
      const apiError = new Error('error')
      mockEmitSocketCallback(apiError)

      await expect(execListAvailableQuiddities()).rejects.toEqual(apiError)
    })
  })

  describe('listCreated', () => {
    const execListQuiddities = async () => {
      return api.listCreated()
    }

    it('should emit switcher_quiddities', async () => {
      execListQuiddities()

      expect(api.socket.emit).toHaveBeenCalledWith(
        'switcher_quiddities',
        expect.any(Function)
      )
    })

    it('should resolve list when emit callback contains data', async () => {
      const apiResult = ['test']
      mockEmitSocketCallback(null, apiResult)

      expect(await execListQuiddities()).toEqual(apiResult)
    })

    it('should reject list when emit callback contains an error', async () => {
      const apiError = new Error('error')
      mockEmitSocketCallback(apiError)

      await expect(execListQuiddities()).rejects.toEqual(apiError)
    })
  })

  describe('create', () => {
    const quidKind = 'kind'
    const quidNickname = 'nickname'
    const quidProps = { props: 'test' }
    const quidUserData = { scene: 'test' }

    const execCreateQuiddity = async (quidKind, quidNickname, quidProps, quidUserData) => {
      return api.create(quidKind, quidNickname, quidProps, quidUserData)
    }

    it('should emit quiddity_create', async () => {
      const apiResult = ['test']
      mockEmitSocketCallback(null, apiResult)
      await execCreateQuiddity(quidKind, quidNickname, quidProps, quidUserData)

      expect(api.socket.emit).toHaveBeenCalledWith(
        'quiddity_create',
        quidKind,
        quidNickname,
        quidProps,
        quidUserData,
        expect.any(Function)
      )
    })

    it('should resolve quiddity_create when emit callback contains data of type string', async () => {
      const apiResult = { id: 1 }
      mockEmitSocketCallback(null, apiResult)

      expect(await execCreateQuiddity(quidKind, quidNickname, quidProps, quidUserData)).toEqual(apiResult)
    })

    it('should resolve quiddity_create when emit callback contains data of type object', async () => {
      const apiResult = { test: 'test' }
      mockEmitSocketCallback(null, apiResult)

      expect(await execCreateQuiddity(quidKind, quidNickname, quidProps, quidUserData)).toEqual(apiResult)
    })

    it('should reject quiddity_create when emit callback contains an error', async () => {
      const apiError = new Error('error')
      mockEmitSocketCallback(apiError)

      await expect(execCreateQuiddity(quidKind, quidNickname, quidProps, quidUserData)).rejects.toEqual(apiError)
    })
  })

  describe('onCreated', () => {
    const quidId = 'id'

    const execOnQuiddityCreated = (id = socket.id) => {
      socket.onEmit('quiddity_created', { id: quidId }, id)
    }

    it('should call `onCreateAction` when `quiddity_created` is emitted', () => {
      const onCreateAction = jest.fn()
      api.onCreated(onCreateAction)
      execOnQuiddityCreated()
      expect(onCreateAction).toHaveBeenCalledWith({ id: quidId })
    })

    it('should not be called when the quiddityId doesn\'t match', () => {
      const onCreateAction = jest.fn()
      api.onCreated(onCreateAction, 'id1')
      execOnQuiddityCreated()
      expect(onCreateAction).not.toHaveBeenCalled()
    })

    it('should be called when the quiddityId matches', () => {
      const onCreateAction = jest.fn()
      api.onCreated(onCreateAction, quidId)
      execOnQuiddityCreated()
      expect(onCreateAction).toHaveBeenCalledWith({ id: quidId })
    })
  })

  describe('onDeleted', () => {
    const quidId = 'id'

    const execOnQuiddityRemoved = (id = socket.id) => {
      socket.onEmit('quiddity_deleted', quidId, id)
    }

    it('should call `onRemoveAction` when `quiddity_removed` is emitted', () => {
      const onRemoveAction = jest.fn()
      api.onDeleted(onRemoveAction)
      execOnQuiddityRemoved()
      expect(onRemoveAction).toHaveBeenCalledWith(quidId)
    })

    it('should not be called when the quiddityId doesn\'t match', () => {
      const onRemoveAction = jest.fn()
      api.onDeleted(onRemoveAction, 'id1')
      execOnQuiddityRemoved()
      expect(onRemoveAction).not.toHaveBeenCalled()
    })

    it('should be called when the quiddityId matches', () => {
      const onRemoveAction = jest.fn()
      api.onDeleted(onRemoveAction, quidId)
      execOnQuiddityRemoved()
      expect(onRemoveAction).toHaveBeenCalled()
    })
  })

  describe('delete', () => {
    const DELETE_ERROR = new Error()
    const QUIDDITY_TEST = 'test'

    const execOnDeleteQuiddity = async (quidId) => {
      return api.delete(quidId)
    }

    it('should emit `quiddity_delete`', async () => {
      const quidId = 'test'

      mockEmitSocketCallback(null, quidId)
      await execOnDeleteQuiddity(quidId)

      expect(api.socket.emit).toHaveBeenCalledWith(
        'quiddity_delete',
        quidId,
        expect.any(Function)
      )
    })

    it('should resolve the request when a success callback is emitted', async () => {
      mockEmitSocketCallback(null, 'test')
      expect(await execOnDeleteQuiddity(QUIDDITY_TEST)).toEqual('test')
    })

    it('should reject the request when an error callback is emitted', async () => {
      mockEmitSocketCallback(DELETE_ERROR)
      await expect(execOnDeleteQuiddity(QUIDDITY_TEST)).rejects.toEqual(DELETE_ERROR)
    })
  })
})
