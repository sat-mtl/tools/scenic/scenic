/* global describe it expect beforeEach jest */

import { Socket, mockSocketCallback } from '@fixture/api/socket.mock'
import connectMethod from '@fixture/api/connect.method.json'

import MethodAPI from '@api/quiddity/MethodAPI'

describe('MethodAPI', () => {
  const QUIDDITY_TEST = 'test'
  const METHOD_TEST = 'test'
  const METHOD_ARGS = connectMethod.args.map(arg => arg.name)
  const METHOD_ERROR = new Error()

  let api, socket, mockEmitSocketCallback

  beforeEach(() => {
    socket = new Socket()
    api = new MethodAPI(socket)
    mockEmitSocketCallback = mockSocketCallback.bind(null, api.socket, 'emit')
  })

  describe('constructor', () => {
    it('should expect MethodAPI to be defined', () => {
      expect(api).toBeDefined()
      expect(api.socket).toStrictEqual(socket)
    })
  })

  describe('invoke', () => {
    const execInvoke = async (quiddityId, methodName, methodArgs) => {
      return api.invoke(quiddityId, methodName, methodArgs)
    }

    it('should emit `quiddity_invoke`', async () => {
      mockEmitSocketCallback(null, true)
      await execInvoke(QUIDDITY_TEST, METHOD_TEST, METHOD_ARGS)

      expect(api.socket.emit).toHaveBeenCalledWith(
        'quiddity_invoke',
        QUIDDITY_TEST,
        METHOD_TEST,
        METHOD_ARGS,
        expect.any(Function)
      )
    })

    it('should resolve method invoke when callback return true', async () => {
      mockEmitSocketCallback(null, true)
      expect(await execInvoke()).toEqual(true)
    })

    it('should resolve method invoke when callback return false', async () => {
      mockEmitSocketCallback(null, false)
      expect(await execInvoke()).toEqual(false)
    })

    it('should reject method invoke when callback return undefined', async () => {
      mockEmitSocketCallback(null, undefined)
      await expect(execInvoke()).rejects.toEqual(expect.any(Error))
    })

    it('should reject method invoke when callback return an error', async () => {
      mockEmitSocketCallback(METHOD_ERROR)
      await expect(execInvoke()).rejects.toEqual(METHOD_ERROR)
    })
  })

  describe('helpers', () => {
    const invokeSpy = jest.spyOn(MethodAPI.prototype, 'invoke')
    const QUID_ID = 2

    describe('canSinkCaps', () => {
      const CAPS = 'caps'

      it('should invoke the method `can-sink-caps`', async () => {
        await api.canSinkCaps(QUID_ID, 'caps')

        expect(invokeSpy).toHaveBeenCalledWith(
          QUID_ID, 'can-sink-caps', [CAPS]
        )
      })
    })

    describe('register', () => {
      it('should invoke the method `register` with the default destination port', async () => {
        await api.register(QUID_ID, 'test', 'test', 'test')

        expect(invokeSpy).toHaveBeenCalledWith(
          QUID_ID, 'register', ['test@test:9060', 'test']
        )
      })
      it('should invoke the method `register` with the passed destination port', async () => {
        await api.register(QUID_ID, 'test', 'test', 'test', '5060')

        expect(invokeSpy).toHaveBeenCalledWith(
          QUID_ID, 'register', ['test@test:5060', 'test']
        )
      })
    })

    describe('unregister', () => {
      it('should invoke the method `unregister`', async () => {
        await api.unregister(QUID_ID)

        expect(invokeSpy).toHaveBeenCalledWith(
          QUID_ID, 'unregister', []
        )
      })
    })

    describe('setStunTurn', () => {
      it('should invoke the method `set_stun_turn`', async () => {
        await api.setStunTurn(QUID_ID, 'test', 'test', 'test', 'test')

        expect(invokeSpy).toHaveBeenCalledWith(
          QUID_ID, 'set_stun_turn', ['test', 'test', 'test', 'test']
        )
      })
    })

    describe('addBuddy', () => {
      it('should invoke the method `add_buddy`', async () => {
        await api.addBuddy(QUID_ID, 'uri', 'test')

        expect(invokeSpy).toHaveBeenCalledWith(
          QUID_ID, 'add_buddy', ['test@uri']
        )
      })
    })

    describe('delBuddy', () => {
      it('should invoke the method `del_buddy`', async () => {
        await api.delBuddy(QUID_ID, 'uri', 'test')

        expect(invokeSpy).toHaveBeenCalledWith(
          QUID_ID, 'del_buddy', ['test@uri']
        )
      })
    })

    describe('nameBuddy', () => {
      it('should invoke the method `name_buddy`', async () => {
        await api.nameBuddy(QUID_ID, 'uri', 'oldName', 'newName')

        expect(invokeSpy).toHaveBeenCalledWith(
          QUID_ID, 'name_buddy', ['newName', 'oldName@uri']
        )
      })
    })

    describe('authorize', () => {
      it('should invoke the method `authorize`', async () => {
        await api.authorize(QUID_ID, 'uri', 'test', true)

        expect(invokeSpy).toHaveBeenCalledWith(
          QUID_ID, 'authorize', ['test@uri', true]
        )
      })
    })

    describe('attachShmdataToContact', () => {
      it('should invoke the method `attach_shmdata_to_contact`', async () => {
        await api.attachShmdataToContact(QUID_ID, 'uri', 'test', 'path')

        expect(invokeSpy).toHaveBeenCalledWith(
          QUID_ID, 'attach_shmdata_to_contact', ['path', 'test@uri', true]
        )
      })
    })

    describe('send', () => {
      it('should invoke the method `send`', async () => {
        await api.send(QUID_ID, 'uri', 'test')

        expect(invokeSpy).toHaveBeenCalledWith(
          QUID_ID, 'send', ['test@uri']
        )
      })
    })
  })
})
