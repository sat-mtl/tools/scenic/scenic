/* global Blob describe it expect beforeEach */

import { Socket, mockSocketCallback } from '@fixture/api/socket.mock'

import ImageAPI from '@api/image/ImageAPI'

describe('ImageAPI', () => {
  let api, socket, mockEmitSocketCallback

  beforeEach(() => {
    socket = new Socket()
    api = new ImageAPI(socket)
    mockEmitSocketCallback = mockSocketCallback.bind(null, api.socket, 'emit')
  })

  describe('constructor', () => {
    it('should expect ImageAPI to be defined', () => {
      expect(api).toBeDefined()
      expect(api.socket).toStrictEqual(socket)
    })
  })

  describe('readImage', () => {
    const READ_ERROR = new Error()
    const execReadImage = async (fileName) => {
      return api.readImage(fileName)
    }

    it('should emit `image_read`', async () => {
      mockEmitSocketCallback(null, new Blob([1, 2, 3]))
      await execReadImage('/tmp/test')
      expect(api.socket.emit).toHaveBeenCalledWith(
        'image_read',
        '/tmp/test',
        expect.any(Function)
      )
    })

    it('should resolve the request when a success callback is emitted', async () => {
      mockEmitSocketCallback(null, new Blob([1, 2, 3]))
      expect(await execReadImage('/tmp/test')).toEqual(new Blob([1, 2, 3]))
    })

    it('should reject the request when an error callback is emitted', async () => {
      mockEmitSocketCallback(READ_ERROR)
      await expect(execReadImage('/tmp/test')).rejects.toEqual(READ_ERROR)
    })
  })
})
