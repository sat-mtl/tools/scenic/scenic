# Webdriver_Prototypes

Contained herein are test suites created using Selenium Webdriver(Python), leveraging the Pytest testing framework.

The Webdriver tests are capable of a much more powerful and complex set of actions and are more easily maintainable.
These tests are being written with Scenic 4.0.0 in mind so may be incompatible with earlier versions of Scenic.

## Getting Started

### Prerequisites

| Prerequisite           | Command
| -----------------------|----------
| Python (>= 3.8 )       | `sudo apt install python3`
| Pipenv (latest)        | `sudo apt install pipenv`
| Google Chrome (latest) | [Install Chrome](https://www.google.com/chrome/)
| Chromedriver (latest)  | [Download Chromedriver](http://chromedriver.chromium.org/downloads) and add it to the system's path (in `/usr/local/bin`)

#### Install prerequisites on Ubuntu 20.04

```bash
# Install Chrome
sudo curl -sS -o - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add
sudo echo "deb https://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list
sudo apt -y update
# sudo apt -y install google-chrome-stable chromium-chromedriver

# Install chromedriver from binary
wget https://chromedriver.storage.googleapis.com/101.0.4951.41/chromedriver_linux64.zip
unzip chromedriver_linux64.zip
sudo mv chromedriver /usr/bin/chromedriver
sudo chown root:root /usr/bin/chromedriver
sudo chmod +x /usr/bin/chromedriver
```

#### Notes

* It is recommended that a tester maintains a 'clean' version of Chrome such that it contains no browser extensions or personalizations in order to avoid false positives or negatives caused by the behaviour of said extensions.
* [Pycharm](https://www.jetbrains.com/pycharm/) can be used to run these tests. Make sure to install the latest version and ensure that the Selenium plugin is active; if it's not, try reinstalling it.

### Installation

```bash
git clone git@gitlab.com:sat-mtl/telepresence/scenic-selenium.git
cd scenic-selenium
git checkout develop  # Optional
cp .env.defaults .env
pipenv install --dev
```

That's it! To make sure your Selenium installation is working, you can run the [Simple Web Page Test](./scenic_webdriver/research/standard_web_page_test.py)

## Running the tests

### Prepare Scenic for testing

1. Launch any Scenic instance from the terminal using the `scenic` command
2. Open the [scenic_driver](./scenic_webdriver/utilities/scenic_driver) file in a text editor and adjust the MAX_ATTEMPTS constant if necessary. This controls how many times `scenic_driver` will attempt to login to SIP before reporting a fail.
3. Configure the environment variables.

### Configure environment variables

`scenic-selenium` is designed to work with environmental variables using `pipenv`; it will look for and find a `.env` file placed anywhere in the directory.  To configure your test environment, you will have to configure these variables in your `.env` file:

* `SERVER_ADDRESS` = This is the address of the sip server.
* `SCENIC1_URL` = This is the URL of the main Scenic instance under test. (i.e `http://localhost:8080` or `http://scenic-app.ca`)
* `SWITCHER1_HOST` = This is the hostname or IP address of main switcher instance. (i.e. `0.0.0.0` for local test)
* `SWITCHER1_PORT` =  This is the websocket port of main Switcher instace. (i.e `8000`)
* `USER1_NAME` = This is the SIP username being used on the main Scenic instance under test.
* `USER1_PASSWORD` = This is the password for the above user.
* `SCENIC2_URL` = This is the URL of the second Scenic instance under test. (i.e `http://localhost:8080` or `http://scenic-app.ca`)
* `SWITCHER2_HOST` = This is the hostname or IP address of second switcher instance. (i.e. `0.0.0.0` for local test)
* `SWITCHER2_PORT` =  This is the websocket port of second Switcher instace. (i.e `8000`)
* `USER2_NAME` = Used in SIP call tests, SIP username being used on the second Scenic instance under test.
* `USER2_PASSWORD` = Used in SIP call tests, this is the password for the above user.
* `SCENIC3_URL` = This is the URL of the third Scenic instance under test. (i.e `http://localhost:8080` or `http://scenic-app.ca`)
* `SWITCHER3_HOST` = This is the hostname or IP address of third switcher instance. (i.e. `0.0.0.0` for local test)
* `SWITCHER3_PORT` =  This is the websocket port of third Switcher instace. (i.e `8000`)
* `USER3_PASSWORD` = Used in SIP call tests, this is the password for the above user.
* `PATH_TO_DOWNLOADS_FOLDER` = Used in Load Session tests, this is the path to your local downloads folder which will contain the .json file that represents a saved session file. make sure you include the full path (ie 'home/user/Downloads/' including the concluding /)

### Testing 1, 2, 3

First, activate the `virtualenv` for this project using `pipenv shell`. Once inside the virtual environment, launch the whole test suite by running the standard `pytest` command.

```bash
pipenv shell  # Activate the virtualenv
pytest  # Runs all tests; alternatively you can run a single test by typing 'pytest path/to/test.py'
```

## Known Issues

* Tests have been designed to test the desired behaviour of Scenic; this means that some tests currently fail by design because the current behaviour of a component does not match the desired behaviour.  This is normal.
* Some tests that test NDI features may fail if there are other NDI streams available on the local network.  It is a good idea to make sure there are no other NDI streams available before running these tests.

## Documentation by Test Suite

### Essential Test Suite
The Essential Test Suite is a selection of tests that are intended to be run using the gitlab CI pipeline in order to offer a quick, automated, smoke test on all merge requests.  They are not intended to validate functionality but rather to ensure that elements that are tested for in the rest of the automated test suites are still present and still bear the same identifiers (ie class, ID, xpath address, etc).

Eventually it could be expanded to have more functional tests, but for now it is simply enough to filter out merge requests that might unknowingly break the automated tests by changing relatively unimportant things - for instance, changing the CSS selector for the Load Session Drawer does not change its functionality; it simply breaks the test for Loading Sessions and makes it confusing to know if the test is broken because the app functionality is broken or if a selector has changed, or if the test simply doesn't work.

The Essential Test Suite hopes to remove this doubt.

## Build Docker image

To rebuild and push Docker image used by CI pipeline:

```bash
docker build \
    -t registry.gitlab.com/sat-mtl/tools/scenic/scenic/selenium:develop \
    -f test/selenium/Dockerfile \
    .
docker login registry.gitlab.com
docker push registry.gitlab.com/sat-mtl/tools/scenic/scenic/selenium:develop
```
