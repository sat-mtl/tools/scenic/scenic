from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
import time


def test_preview(scenic_instance_1) -> None:

    try:
        scenic_instance_1.create_quiddity_from_menu('Sources', 'Video', 'SDI 1')
        time.sleep(2)
        scenic_instance_1.activate_source('sdiInput11')
        # Find the Preview and attempt to open it
        scenic_instance_1.open_video_preview_window('sdiInput11', 'video')

        # Check to see if the Preview window is visible
        preview_window = scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '#PreviewWrapper')
        time.sleep(3)
        assert preview_window.is_displayed(), ' The preview window is not displayed.'
        print("The preview window is visible!")

        # Find the preview window at t1 and get its img src value TODO --- this selector is not particularly semantic
        preview_state1 = scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '#PreviewWrapper > div > div > img').get_attribute('src')
        time.sleep(2)

        # Find the preview window at t2 and get its img src value TODO --- this selector is not particularly semantic
        preview_state2 = scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '#PreviewWrapper > div > div > img').get_attribute('src')

        # Since the src value is dynamically updated as frames change, if preview_state1 is not equal to preview_state2, we can infer the preview is working
        assert preview_state1 != preview_state2, 'The preview is not updating correctly'
        print("The preview is updating correctly")

    except NoSuchElementException as e:
        assert False, f'UI element could not be found. Error: {e.msg}'
