import scenic_webdriver.utilities.scenic_driver
import pytest
import os


@pytest.fixture
def scenic_instance_1():
    SCENIC1_URL = os.environ['SCENIC1_URL']
    SWITCHER1_HOST = os.environ['SWITCHER1_HOST']
    SWITCHER1_PORT = os.environ['SWITCHER1_PORT']
    scenic1 = scenic_webdriver.utilities.scenic_driver.ScenicDriver(SCENIC1_URL)
    scenic1.set_switcher_instance(SWITCHER1_HOST, SWITCHER1_PORT)
    scenic1.reload_scenic()
    scenic1.initialize_scenic()
    yield scenic1
    scenic1.tear_down_scenic()


@pytest.fixture
def scenic_instance_2():
    SCENIC2_URL = os.environ['SCENIC2_URL']
    SWITCHER2_HOST = os.environ['SWITCHER2_HOST']
    SWITCHER2_PORT = os.environ['SWITCHER2_PORT']
    scenic2 = scenic_webdriver.utilities.scenic_driver.ScenicDriver(SCENIC2_URL)
    scenic2.set_switcher_instance(SWITCHER2_HOST, SWITCHER2_PORT)
    scenic2.reload_scenic()
    scenic2.initialize_scenic()
    yield scenic2
    scenic2.tear_down_scenic()


@pytest.fixture
def scenic_instance_3():
    SCENIC3_URL = os.environ['SCENIC3_URL']
    SWITCHER3_HOST = os.environ['SWITCHER3_HOST']
    SWITCHER3_PORT = os.environ['SWITCHER3_PORT']
    scenic3 = scenic_webdriver.utilities.scenic_driver.ScenicDriver(SCENIC3_URL)
    scenic3.set_switcher_instance(SWITCHER3_HOST, SWITCHER3_PORT)
    scenic3.reload_scenic()
    scenic3.initialize_scenic()
    yield scenic3
    scenic3.tear_down_scenic()
