from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
import time
import uuid

def test_OpenSessionModal(scenic_instance_1) -> None:
    TEST_FILE_NAME_1 = 'SeleniumTestFile' + str(uuid.uuid4())

    try:
        # Case 1: user enters load session file workflow after saving first (also ensures there is a session file to load)
        # Create a saved session
        scenic_instance_1.create_quiddity_from_menu('Sources', 'Video', 'SDI 1')
        scenic_instance_1.save_session_file(TEST_FILE_NAME_1)

        # Attempt to open the saved file
        scenic_instance_1.load_session_file(TEST_FILE_NAME_1)

        # Assert that the OpenSessionModal has appeared
        assert scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '#OpenSessionModal'), 'The OpenSessionModal did not appear after a user had saved a session'

        # Assert that the modal contains the proper buttons
        assert scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.CancelButton'), 'The OpenSessionModal does not contain a cancel button'
        assert scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.ConfirmButton'), 'The OpenSessionModal does not contain a confirm button'

        # Assert that cancelling the Modal causes it to close
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.CancelButton').click()
        number_of_modals_visible = scenic_instance_1.driver.find_elements(By.CSS_SELECTOR, '#OpenSessionModal')
        assert len(number_of_modals_visible) == 0, 'The OpenSessionModal is still visible after clicking cancel'

        # Case 2: user enters load session workflow without first creating a session file.
        scenic_instance_1.initialize_scenic()
        time.sleep(2)
        scenic_instance_1.close_active_drawer()
        scenic_instance_1.create_quiddity_from_menu('Sources', 'Video', 'SDI 1')
        scenic_instance_1.load_session_file(TEST_FILE_NAME_1)

        # Assert that the OpenSessionModal has appeared #TODO this currently fails because the OpenSessionModal does not appear in this case
        assert scenic_instance_1.driver.find_element(By.CSS_SELECTOR,
                                                     '#OpenSessionModal'), 'The OpenSessionModal did not appear when a user attempted to open a file without saving changes'

        # Click the confirm button just to assert that it works.
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.ConfirmButton')

        # Delete the test session file to avoid test session file pollution
        scenic_instance_1.delete_session_file(TEST_FILE_NAME_1)



    except NoSuchElementException as e:
        assert False, f'UI element could not be found. Error: {e.msg}'
