from selenium import webdriver
from scenic_webdriver.utilities.scenic_driver import ScenicDriver
import os
import pytest


def test_sip_login(scenic_instance_1) -> None:
    SERVER_ADDRESS = os.environ["SERVER_ADDRESS"]
    USER1_SIP_SOURCE_PORT = os.environ["USER1_SIP_SOURCE_PORT"]
    USER1_NAME = os.environ["USER1_NAME"]
    USER1_PASSWORD = os.environ["USER1_PASSWORD"]

    # Error catching for the login attempt is handled in the login function in scenic_driver
    scenic_instance_1.login_to_sip(SERVER_ADDRESS, USER1_SIP_SOURCE_PORT, USER1_NAME, USER1_PASSWORD)
