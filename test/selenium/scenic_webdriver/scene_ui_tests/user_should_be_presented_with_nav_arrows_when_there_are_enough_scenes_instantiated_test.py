from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException


def test_scene_tab_nav_arrows(scenic_instance_1) -> None:
    CLICK_ATTEMPTS = 1
    try:
        left_nav_arrow = scenic_instance_1.driver.find_elements(By.CSS_SELECTOR, '.arrow-button .icon-left')
        while len(left_nav_arrow) == 0 and CLICK_ATTEMPTS <= 10:
            # Find the scene adding button --- this statement also asserts it's presence in Selenium's functionality
            scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.AddSceneButton').click()
            left_nav_arrow = scenic_instance_1.driver.find_elements(By.CSS_SELECTOR, '.arrow-button .icon-left')
            if len(left_nav_arrow) > 0:
                break
            else:
                CLICK_ATTEMPTS += 1
                assert CLICK_ATTEMPTS != 10, 'The Nav arrows are not appearing'

    except NoSuchElementException as e:
        assert False, f'UI element could not be found. Error: {e.msg}'
