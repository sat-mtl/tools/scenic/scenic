from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
import time


def test_delete_default_scene(scenic_instance_1) -> None:
    try:
        time.sleep(5)  # As with other short tests, this time.sleep ensures the initialize_scenic method has finished.
        # Assert the loading of the scenic driver --- just to make sure the app has actually loaded
        assert scenic_instance_1.driver
        # Assert that the default scene exists
        assert scenic_instance_1.driver.find_element(
            By.XPATH, '//div[contains(text(), "Default scene")]'), 'There is no default scene, or something else is wrong'

        # Attempt to find the close button for scenes and assert that there are none
        scene_close_button = scenic_instance_1.driver.find_elements(By.CSS_SELECTOR, '.DeleteSceneButton')
        assert not scene_close_button, 'It appears to be possible to delete the default scene'

    except NoSuchElementException as e:
        assert False, f'UI element could not be found. Error: {e.msg}'
