from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.color import Color
import time


def test_scene_creation(scenic_instance_1) -> None:
    try:
        # This time.sleep ensures that the driver doesn't attempt to click the scene adder before the initialize method from scenic_driver is completely finished.
        time.sleep(5)
        # Click the scene adder; this line will also assert the presence of the scene-adder due to the nature of Selenium
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.AddSceneButton').click()

        # Assert that Scenic is now displaying the newly instantiated Scene 1 and that it is inactive
        selected_scene_text = scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '#SceneTabBar .navtab-tab-selected').text
        assert selected_scene_text == 'Scene 1', 'The newly added scene is not displayed, or, it was not added.'

        colour_of_displayed_scene_active_state_indicator = Color.from_string(scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '#SceneControlPanel .Checkbox').value_of_css_property('background-color')).hex
        assert colour_of_displayed_scene_active_state_indicator == '#000000', 'It appears that the displayed scene is active, already.'  # Note that if a background-color is not specified, it appears to default to #000000

        # Find the scene label input text field and attempt to rename the scene to TEST SCENE
        scene_name_field = scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.InputTextField[value="Scene 1"]')
        scene_name_field.clear()
        scene_name_field.send_keys('TEST SCENE')

        # Click the matrix page to enter the change
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '#MatrixPage').click()
        # Ensure the confirmation has appeared, afterwards, click it Confirm
        assert scenic_instance_1.driver.find_elements(By.CSS_SELECTOR,
                                                      '#RenameSceneModal') is not False, 'Scenic is not asking for confirmation that the scene should be renamed!'
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '#RenameSceneModal .RenameButton').click()

        # Ensure that there is now a scene tab that contains the text 'TEST SCENE'
        assert scenic_instance_1.driver.find_elements(
            By.XPATH, '//div[contains(text(), "TEST SCENE")]') is not False, 'The name change did not work.'

    except NoSuchElementException as e:
        assert False, f'UI element could not be found. Error: {e.msg}'
