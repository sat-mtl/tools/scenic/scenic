from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.color import Color
import time


def test_scene_switching(scenic_instance_1) -> None:
    try:

        # This time.sleep ensures that the driver doesn't attempt to click the scene adder before the initialize method from scenic_driver is completely finished.
        time.sleep(5)

        # Click the scene adder; this line will also assert the presence of the scene-adder due to the nature of Selenium
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.AddSceneButton').click()

        # Assert that Scenic is now displaying the newly instantiated Scene 1 and that it is inactive
        selected_scene_text = scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '#SceneTabBar .navtab-tab-selected').text
        assert selected_scene_text == 'Scene 1', 'The newly added scene is not displayed, or, it was not added.'

        colour_of_displayed_scene_active_state_indicator = Color.from_string(scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '#SceneControlPanel .Checkbox').value_of_css_property('background-color')).hex
        assert colour_of_displayed_scene_active_state_indicator == '#000000', 'It appears that the displayed scene is active, already.'  # Note that if a background-color is not specified, it appears to default to #000000

        # Activate the scene - serves as a marker for changing back to default scene but also a sanity check to make sure the scene can be activated
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '#SceneControlPanel .Checkbox-active').click()
        colour_of_displayed_scene_active_state_indicator = Color.from_string(scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '#SceneControlPanel .Checkbox .styledCheckbox-0-2-221').value_of_css_property('background-color')).hex
        assert colour_of_displayed_scene_active_state_indicator == '#38c566', 'It appears that the displayed scene was not activated'

        # Switch back to default scene
        scenic_instance_1.driver.find_element(By.XPATH,
                                              '//div[contains(text(), "Default scene")]').click()
        breakpoint()
        # Assert the default scene is inactive
        colour_of_displayed_scene_active_state_indicator = Color.from_string(scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '#SceneControlPanel .Checkbox .styledCheckbox-0-2-221').value_of_css_property('background-color')).hex
        assert colour_of_displayed_scene_active_state_indicator == '#000000', 'It appears that the default scene is active, or switching scenes did not work.'

    except NoSuchElementException as e:
        assert False, f'UI element could not be found. Error: {e.msg}'
