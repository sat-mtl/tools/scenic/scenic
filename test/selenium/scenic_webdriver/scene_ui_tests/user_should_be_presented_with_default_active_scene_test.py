from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.color import Color


def test_default_active_scene(scenic_instance_1) -> None:
    try:
        # Assert the loading of the scenic driver
        assert scenic_instance_1.driver

        # Discover how many scene tabs exist; Scene tabs and the Scene adder are both considered tabs right now, so this test looks for 2 tabs
        number_of_tabs_present = len(scenic_instance_1.driver.find_elements(
            By.CSS_SELECTOR, '#SceneTabBar .navtab-tab-title'))

        assert number_of_tabs_present == 2, 'There are more tabs than just the default scene tab present'

        # Ensure that the selected scene is the default scene by finding the selected scene and interrogating its text value.
        selected_scene_text = scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '#SceneTabBar .navtab-tab-selected').text

        assert selected_scene_text == 'Scène par défaut', 'The default scene is not the selected tab'

        # Ensure that the default scene is displayed by checking the Input Text field for the 'Default scene' placeholder text.
        scene_input_text_field = scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.InputTextField[value="Default scene"]')

        assert scene_input_text_field is not False, 'There is no scene input text field for a default scene'

        # Find and assert the colour of the active state indicator - this gives an rgba value so must be changed to hex for comparison
        colour_of_active_indicator = Color.from_string(scenic_instance_1.driver.find_element(
            By.CSS_SELECTOR, '#SceneControlPanel .Checkbox .styledCheckbox-0-2-221').value_of_css_property('background-color')).hex

        assert colour_of_active_indicator == '#38c566'

    except NoSuchElementException as e:
        assert False, f'UI element could not be found. Error: {e.msg}'
