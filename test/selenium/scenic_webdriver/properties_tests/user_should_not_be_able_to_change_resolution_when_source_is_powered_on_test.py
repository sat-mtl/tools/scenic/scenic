from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
import time


def test_video_resolution_dropdown(scenic_instance_1) -> None:
    try:

        scenic_instance_1.create_quiddity_from_menu('Sources', 'Video', 'SDI 1')
        scenic_instance_1.activate_source('sdiInput11')
        time.sleep(5)

        # Find the source label and click it to open the properties label
        scenic_instance_1.open_source_properties('sdiInput11')
        time.sleep(2)

        # Find the Capture sub-menu
        scenic_instance_1.open_property_sub_menu('Capture')
        time.sleep(2)

        # Find the select field for the resolution
        resolution_select_button = scenic_instance_1.select_property_dropdown_button("sdiInput11", "Capture/resolution")

        # Assert that the aria-disabled role is set to true, which causes the select field to be disabled
        assert resolution_select_button.get_attribute('aria-disabled'), 'The resolution dropdown is not disabled.'

        # Dummy check to make sure that the field is enabled when powered off

        scenic_instance_1.activate_source('sdiInput11')  # Should work to power off the source too ;)

        # Find the Capture sub-menu, again
        time.sleep(2)

        # Find the select field for the resolution
        resolution_select_button = scenic_instance_1.select_property_dropdown_button("sdiInput11", "Capture/resolution")

        # Assert that the aria-disabled role is NOT set to true, which causes the select field to be disabled
        assert resolution_select_button.get_attribute(
            'aria-disabled') == 'false', 'The resolution dropdown is disabled.'

    except NoSuchElementException as e:
        assert False, f'UI element could not be found. Error: {e.msg}'
