from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException


def test_live_page(scenic_instance_1) -> None:
    try:
        # Find and click the Live Page Tab
        # TODO Left Sidebar menu items do not contain custom selectors; when they do replace XPath Selectors with CSS Selectors
        scenic_instance_1.driver.find_element(By.XPATH, '//div[contains(text(), "live")]').click()

        # Search for the Live Page ID and assert that it is present
        assert scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '#LivePage'), 'The Live Page ID did not appear'

        # Search for the expected page header and assert it's presence
        # TODO Uses Xpath for now - the page header does not have a specific class/ID
        assert scenic_instance_1.driver.find_element(
            By.XPATH, '//header[contains(text(), "Click on a scene to activate it")]'), 'The Live Page Header did not appear'

        # Search for the expected Active Scene element
        assert scenic_instance_1.driver.find_element(
            By.CSS_SELECTOR, '#LivePage .ActiveScene'), 'There is no active scene element on the page'
        scenic_instance_1.return_to_main_page()

    except NoSuchElementException as e:
        scenic_instance_1.return_to_main_page()
        assert False, f'UI element could not be found. Error: {e.msg}'
