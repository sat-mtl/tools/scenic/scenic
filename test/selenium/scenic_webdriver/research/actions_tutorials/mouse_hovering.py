from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver import ActionChains
import time


def test1():

    baseUrl = "https://letskodeit.teachable.com/pages/practice"
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get(baseUrl)
    driver.implicitly_wait(3)

    driver.execute_script("window.scrollBy(0,600);")
    time.sleep(3)
    element = driver.find_element(By.CLASS_NAME, "mouse-hover")
    itemToClickLocator = '//*[@id="block-1069048"]/div/div/div/div[4]/div/div/div/a[1]'

    try:
        actions = ActionChains(driver)
        actions.move_to_element(element).perform()
        print("Mouse Hovered On Element")
        time.sleep(3)
        topLink = driver.find_element(By.XPATH, itemToClickLocator)
        actions.move_to_element(topLink).perform()
        print("Item Clicked")
    except:
        print("Mouse Hover Failed")


test1()
