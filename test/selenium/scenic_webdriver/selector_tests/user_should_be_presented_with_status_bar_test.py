from selenium.common.exceptions import NoSuchElementException

def test_status_bar_elements(scenic_instance_1):
    # This tests for the presence of the status bar elements and sections
    try:

        scenic_instance_1.assert_element_presence('#StatusBar')
        scenic_instance_1.assert_element_presence('#CpuSection')
        scenic_instance_1.assert_element_presence('#CpuBars')
        scenic_instance_1.assert_element_presence('#MemorySection')
        scenic_instance_1.assert_element_presence('#NetworkInterfaceSection')
        scenic_instance_1.assert_element_presence('#NetworkInterface')
        scenic_instance_1.assert_element_presence('#NetworkIp')
        scenic_instance_1.assert_element_presence('#NetworkRx')
        scenic_instance_1.assert_element_presence('#NetworkTx')
        scenic_instance_1.assert_element_presence('#HelpSection')
        scenic_instance_1.assert_element_presence('#NetworkTx')

    except NoSuchElementException as e:
        assert False, f'UI element could not be found. Error: {e.msg}'
