from selenium.common.exceptions import NoSuchElementException

def test_compression_menu_elements(scenic_instance_1):
    # This tests for the selectors for the Compression Menu elements
    try:
        # Assert presence of Compression menu
        scenic_instance_1.assert_element_presence('.Menu .SelectButton[data-menu="Compression"]')
        scenic_instance_1.assert_element_presence('.Item[data-menu="Video Encoder"]')

    except NoSuchElementException as e:
        assert False, f'UI element could not be found. Error: {e.msg}'
