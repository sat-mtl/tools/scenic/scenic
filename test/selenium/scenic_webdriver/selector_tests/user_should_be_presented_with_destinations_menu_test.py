from selenium.common.exceptions import NoSuchElementException

def test_destination_menu_elements(scenic_instance_1):
    # This tests for the selectors for the Destination Menu elements
    try:
        # Assert presence of Destination menu
        scenic_instance_1.assert_element_presence('.Menu .SelectButton[data-menu="Destinations"]')

        # Assert presence of Video menu and submenu items
        scenic_instance_1.assert_element_presence('.Menu .GroupTitle[data-menu="Video"]') # TODO This selector is the same as the selector for the Sources menu item
        scenic_instance_1.assert_element_presence('.Item[data-menu="Video Monitor"]')

        # Assert presence of Audio menu and submenu items
        scenic_instance_1.assert_element_presence('.Menu .GroupTitle[data-menu="Audio"]') # TODO Again, this is the same selector as the Sources menu Audio submenu sooooooo
        scenic_instance_1.assert_element_presence('.Item[data-menu="JACK Output"]')
        scenic_instance_1.assert_element_presence('.Item[data-menu="Pulse Output"]')

        # Assert presence of Control menu and submenu items
        scenic_instance_1.assert_element_presence('.Menu .GroupTitle[data-menu="Control"]') # TODO see above
        scenic_instance_1.assert_element_presence('.Item[data-menu="MIDI Output"]')
        scenic_instance_1.assert_element_presence('.Item[data-menu="OSC Output"]')

        # Assert presence of Network menu and submenu items
        scenic_instance_1.assert_element_presence('.Menu .GroupTitle[data-menu="Network"]') # TODO Ibid.
        scenic_instance_1.assert_element_presence('.Item[data-menu="RTMP Streaming"]')
        scenic_instance_1.assert_element_presence('.Item[data-menu="NDI® Output"]')

    except NoSuchElementException as e:
        assert False, f'UI element could not be found. Error: {e.msg}'
