# Class names glossary for Scenic UI testing

All classes are named with their respective category and components. It should be very logical, so please, raise an issue if a name seems weird.

Moreover, if you have suggestions for new components or composed components, raise an issue :)

## Common components

The common components should have a **generic** name and shouldn't contain references to Scenic. All these classes will be updated soon.

| Component    | Description                                 | Class
|--------------|---------------------------------------------|-----
| Input.Number | Used to get a range of numeric values       | `input-number`
| + field      | Let the user input an arbitrary value       | `input-number-field`
| + decrement  | Button which decrements the input value     | `input-number-decrement`
| + increment  | Button which increments the input value     | `input-number-increment`
| Input.Select | Displays a select input                     | `input-select`
| + button     | Clicking on button displays the select menu | `input-select-button`
| + menu       | Displays all selectable options             | `input-select-menu`
| + label      | Shows the selected option                   | `input-select-label`
| + option     | Shows the selected option in the menu         | `input-select-option`
| Menu.Group   | Menu group used to set item into categories | `menu-group`
| + collapsed  | When Menu.Group is **collapsed**            | `menu-group-children-collapsed`
| Menu.Item    | Menu item displays an element from a list   | `menu-item`
| Menu.Wrapper | Wrap collection of Menu components (Group or Item) | `menu-wrapper`
| Menu.Select  | Displays a Menu when it is clicked          | `menu-select`
| Modal        | Displays a dialog                           | `senic-dialog` (**deprecated**)
| Preview      | Displays an absolute preview of a media     | `scenic-preview` (**deprecated**)
| Spinner      | Displays a spinner on a backdrop            |
| + backdrop   |                                             | `spinner-backdrop`
| + container  | Displays the spinner                        | `spinner-container`
| Select       | Displays a select input                     | `scenic-select` (**deprecated**)
| NavTab.Bar   | Displays an horizontal bar with Tabs        | `navbar-bar`
| NavTab.ExtraButton | Displays a button inside a Tab           | `navtab-extra-button`
| NavTab.Panel | Displays a panel when a tab is selected     | `navbar-panel` `navbar-panel-{Name of the selected Tab}`
| NavTab.Tab   | A tab                                       | `navbar-tab`
| + selected   | A selected tab                              | `navbar-tab-selected`
| + disabled   | A disabled tab                              | `navbar-tab-disabled`

## Matrix components

| Component    | Description                                  | Class
|--------------|----------------------------------------------|-----
| ConnectionCell | Displays a connection between two quiddities | `matrix-connection-cell`

## Composed components

QuiddityMenu is a composition of multiple Menu components (Select, Group, Item) in order to display  quiddities in a Menu. It doesn't add some extra classes. It could if requested.
