# Javascript References

## Modules

<dl>
<dt><a href="#module_utils/logger">utils/logger</a></dt>
<dd><p>Define all logging capabilities of the webapp</p>
</dd>
<dt><a href="#module_models/common">models/common</a></dt>
<dd><p>Define all common structures</p>
</dd>
<dt><a href="#module_models/shmdata">models/shmdata</a></dt>
<dd><p>Define all structures that modelizes shmdatas</p>
</dd>
<dt><a href="#module_models/quiddity">models/quiddity</a></dt>
<dd><p>Define all structures that modelize quiddities</p>
</dd>
<dt><a href="#module_stores/common">stores/common</a></dt>
<dd><p>Define all stores for common UI behaviors</p>
</dd>
<dt><a href="#module_stores/quiddity">stores/quiddity</a></dt>
<dd><p>Define all stores for UI behaviors about quiddities</p>
</dd>
<dt><a href="#module_stores/matrix">stores/matrix</a></dt>
<dd><p>Define all stores for UI behaviors about the matrix</p>
</dd>
<dt><a href="#module_components/Common">components/Common</a></dt>
<dd><p>Define all common components</p>
</dd>
<dt><a href="#module_components/fields">components/fields</a></dt>
<dd><p>Define all field components</p>
</dd>
<dt><a href="#module_components/pages">components/pages</a></dt>
<dd><p>Define all page components</p>
</dd>
<dt><a href="#module_components/bars">components/bars</a></dt>
<dd><p>Define all bar components</p>
</dd>
<dt><a href="#module_components/wrappers">components/wrappers</a></dt>
<dd><p>Define all wrapper components</p>
</dd>
<dt><a href="#module_components/modals">components/modals</a></dt>
<dd><p>Define all modal components</p>
</dd>
</dl>

## Constants

<dl>
<dt><a href="#loggerFunctions">loggerFunctions</a> : <code>Object.&lt;string, function()&gt;</code></dt>
<dd><p>Allows mocking of a function called by another function in the same module</p>
</dd>
<dt><a href="#SIP_DEFAULT_PORT">SIP_DEFAULT_PORT</a></dt>
<dd></dd>
<dt><a href="#PROPERTY_ID_REGEX">PROPERTY_ID_REGEX</a> : <code>RegExp</code></dt>
<dd><p>Matches prefix and suffix pattern of property IDs</p>
</dd>
<dt><a href="#List">List</a></dt>
<dd></dd>
<dt><a href="#PageWrapper">PageWrapper</a> ⇒ <code><a href="#external_mobx-react/ObserverComponent">mobx-react/ObserverComponent</a></code></dt>
<dd><p>Renders a wrapper that selects and renders the active page</p>
</dd>
<dt><a href="#StartedCheckbox">StartedCheckbox</a> ⇒ <code><a href="#external_mobx-react/ObserverComponent">mobx-react/ObserverComponent</a></code></dt>
<dd><p>Displays a checkbox in order to change the <code>started</code> property</p>
</dd>
<dt><a href="#NicknameLabel">NicknameLabel</a> ⇒ <code><a href="#external_mobx-react/ObserverComponent">mobx-react/ObserverComponent</a></code></dt>
<dd><p>Displays a nickname label for a source entry</p>
</dd>
<dt><a href="#SendingStatus">SendingStatus</a> ⇒ <code><a href="#external_react/Component">react/Component</a></code></dt>
<dd><p>Renders the sending status of a SIP contact</p>
</dd>
<dt><a href="#ReceivingStatus">ReceivingStatus</a> ⇒ <code><a href="#external_react/Component">react/Component</a></code></dt>
<dd><p>Renders the receiving status of a SIP contact</p>
</dd>
<dt><a href="#SipContactInformationTooltip">SipContactInformationTooltip</a></dt>
<dd><p>Tooltip that displays all contact informations</p>
</dd>
<dt><a href="#Capabilities">Capabilities</a> ⇒ <code><a href="#external_react/Component">react/Component</a></code></dt>
<dd><p>Renders all capabilities in a HTML definition list</p>
</dd>
<dt><a href="#ShmdataInformationTooltip">ShmdataInformationTooltip</a> ⇒ <code><a href="#external_react/Component">react/Component</a></code></dt>
<dd><p>Renders the tooltip</p>
</dd>
<dt><a href="#SceneTabBar">SceneTabBar</a> ⇒ <code><a href="#external_mobx-react/ObserverComponent">mobx-react/ObserverComponent</a></code></dt>
<dd><p>Renders the scene tab bar that contains all tabs of user created scenes and the default scene tab</p>
</dd>
<dt><a href="#PageButton">PageButton</a> ⇒ <code><a href="#external_mobx-react/ObserverComponent">mobx-react/ObserverComponent</a></code></dt>
<dd><p>Renders the menu button that triggers the page rendering</p>
</dd>
<dt><a href="#QuiddityMenu">QuiddityMenu</a> ⇒ <code><a href="#external_ui-components/Menu">ui-components/Menu</a></code></dt>
<dd><p>A whole quiddity menu built from a menu collection</p>
</dd>
<dt><a href="#PropertyField">PropertyField</a> ⇒ <code><a href="#external_mobx-react/ObserverComponent">mobx-react/ObserverComponent</a></code></dt>
<dd><p>Renders the Field used to change a property according to its type</p>
</dd>
<dt><a href="#PropertyInspectorDrawer">PropertyInspectorDrawer</a> ⇒ <code><a href="#external_mobx-react/ObserverComponent">mobx-react/ObserverComponent</a></code></dt>
<dd><p>Drawer that displays all property of a selected quiddity</p>
</dd>
<dt><a href="#ConfigurationModal">ConfigurationModal</a></dt>
<dd><p>Scenic modal used to warn user of an invalid configuration</p>
</dd>
<dt><a href="#PanelBar">PanelBar</a> ⇒ <code><a href="#external_react/Component">react/Component</a></code></dt>
<dd><p>Renders all matrix panels</p>
</dd>
<dt><a href="#MenuBar">MenuBar</a> ⇒ <code><a href="#external_react/Component">react/Component</a></code></dt>
<dd><p>Renders the menu bar</p>
</dd>
<dt><a href="#Scenes">Scenes</a> ⇒ <code><a href="#external_mobx-react/ObserverComponent">mobx-react/ObserverComponent</a></code></dt>
<dd><p>Displays all scenes created by the user</p>
</dd>
<dt><a href="#SeparatorLockIcon">SeparatorLockIcon</a> ⇒ <code><a href="#external_mobx-react/ObserverComponent">mobx-react/ObserverComponent</a></code></dt>
<dd><p>Renders a lock icon at the top of the separator</p>
</dd>
<dt><a href="#SeparatorHeader">SeparatorHeader</a> ⇒ <code><a href="#external_mobx-react/ObserverComponent">mobx-react/ObserverComponent</a></code></dt>
<dd><p>Renders the header at the top of the separator</p>
</dd>
<dt><a href="#PreviewWrapper">PreviewWrapper</a></dt>
<dd><p>A preview windows for all video shmdatas</p>
</dd>
<dt><a href="#DestinationHead">DestinationHead</a> ⇒ <code><a href="#external_mobx-react/ObserverComponent">mobx-react/ObserverComponent</a></code></dt>
<dd><p>Displays a destination head for a destination quiddity</p>
</dd>
<dt><a href="#SourceStarter">SourceStarter</a> ⇒ <code><a href="#external_mobx-react/ObserverComponent">mobx-react/ObserverComponent</a></code></dt>
<dd><p>Renders a checkbox that starts or stops a source</p>
</dd>
<dt><a href="#SourceHead">SourceHead</a> ⇒ <code><a href="#external_mobx-react/ObserverComponent">mobx-react/ObserverComponent</a></code></dt>
<dd><p>Renders the head of the source</p>
</dd>
<dt><a href="#Source">Source</a> ⇒ <code><a href="#external_mobx-react/ObserverComponent">mobx-react/ObserverComponent</a></code></dt>
<dd><p>Renders a source row in the matrix</p>
</dd>
<dt><a href="#QuiddityMenuPanel">QuiddityMenuPanel</a> ⇒ <code><a href="#external_mobx-react/ObserverComponent">mobx-react/ObserverComponent</a></code></dt>
<dd><p>Displays a menu panel for quiddities (sources, destinations and compressions)</p>
</dd>
<dt><a href="#SaveSessionAsMenu">SaveSessionAsMenu</a> ⇒ <code>module.components/common.MenuButton</code></dt>
<dd><p>Renders the button that opens the FileSavingDrawer</p>
</dd>
<dt><a href="#OpenSessionMenu">OpenSessionMenu</a> ⇒ <code>module.components/common.MenuButton</code></dt>
<dd><p>Renders the button that opens the FileLoadingDrawer</p>
</dd>
<dt><a href="#DeleteSessionMenu">DeleteSessionMenu</a> ⇒ <code>module.components/common.MenuButton</code></dt>
<dd><p>Renders the button that opens the FileDeletionDrawer</p>
</dd>
<dt><a href="#FileControlPanel">FileControlPanel</a></dt>
<dd><p>The file control panel provides all the actions for the session files</p>
</dd>
<dt><a href="#OrderButtonRight">OrderButtonRight</a></dt>
<dd><p>Renders the right order button</p>
</dd>
<dt><a href="#OrderButtonLeft">OrderButtonLeft</a></dt>
<dd><p>Renders the left order button</p>
</dd>
<dt><a href="#OrderButtonUp">OrderButtonUp</a></dt>
<dd><p>Renders the upward order button</p>
</dd>
<dt><a href="#OrderButtonDown">OrderButtonDown</a></dt>
<dd><p>Renders the downward order button</p>
</dd>
<dt><a href="#OrderPanel">OrderPanel</a> ⇒ <code><a href="#external_react/Component">react/Component</a></code></dt>
<dd><p>Renders the control panel for the quiddity orders</p>
</dd>
<dt><a href="#PropertyInspectorMenu">PropertyInspectorMenu</a> ⇒ <code>module.components/common.MenuButton</code></dt>
<dd><p>Renders the button that opens the PropertyInspectorDrawer</p>
</dd>
<dt><a href="#SipDrawersMenu">SipDrawersMenu</a> ⇒ <code>module.components/common.MenuButton</code></dt>
<dd><p>Renders the button that opens the Sip drawers</p>
</dd>
<dt><a href="#FilterPanel">FilterPanel</a> ⇒ <code><a href="#external_mobx-react/ObserverComponent">mobx-react/ObserverComponent</a></code></dt>
<dd><p>Displays a filter panel for quiddity categories</p>
</dd>
<dt><a href="#SceneNameInput">SceneNameInput</a></dt>
<dd><p>The Scene control panel gives all controls for Scene modification (name, status)</p>
</dd>
<dt><a href="#SceneActivationCheckbox">SceneActivationCheckbox</a> ⇒ <code>external:ui-components/Inputs.Checkbox</code></dt>
<dd><p>Renders scene activation checkbox</p>
</dd>
</dl>

## Functions

<dl>
<dt><a href="#dropDecimalUnit">dropDecimalUnit(number, exponent)</a> ⇒ <code>number</code></dt>
<dd><p>Convert a decimal value to a lower metric in the SI</p>
</dd>
<dt><a href="#raiseDecimalUnit">raiseDecimalUnit(number, exponent)</a> ⇒ <code>number</code></dt>
<dd><p>Convert a decimal value to a greater metric in the SI</p>
</dd>
<dt><a href="#clamp">clamp(number, threshold)</a> ⇒ <code>number</code></dt>
<dd><p>Constrains a number to a specific maximum value.</p>
</dd>
<dt><a href="#getLogStore">getLogStore(logBindings)</a> ⇒ <code>string</code></dt>
<dd><p>Gets the store name from a log event</p>
</dd>
<dt><a href="#useNotificationStore">useNotificationStore()</a> ⇒ <code><a href="#module_stores/common.NotificationStore">NotificationStore</a></code></dt>
<dd><p>Global getter for the notification store</p>
</dd>
<dt><a href="#applyNotificationLog">applyNotificationLog(logLevel, logEvent)</a></dt>
<dd><p>Sends a log message to the notification store</p>
</dd>
<dt><a href="#cleanLogEvent">cleanLogEvent()</a> ⇒ <code>object</code></dt>
<dd><p>Cleans the logEvent object to be sent via the active socket and adds the sessionId property
{external:pino/logEvent} logEvent - Transmitted log event</p>
</dd>
<dt><a href="#applySocketLog">applySocketLog(logEvent)</a></dt>
<dd><p>Sends all clean logs through the active socket</p>
</dd>
<dt><a href="#uploadFile">uploadFile(localFile, onUploadFinished)</a></dt>
<dd><p>Helper that uploads a selected local file</p>
</dd>
<dt><a href="#downloadFile">downloadFile(linkRef, onDownloadStarted, [dataType])</a></dt>
<dd><p>Helper that downloads some content from a link</p>
</dd>
<dt><a href="#hasDebugMode">hasDebugMode()</a> ⇒ <code>boolean</code></dt>
<dd><p>Checks if the debug mode is enabled for Scenic</p>
</dd>
<dt><a href="#debugStores">debugStores(stores)</a> ⇒ <code>Object</code></dt>
<dd><p>Gets all store statements in JSON object</p>
</dd>
<dt><a href="#isObject">isObject(value)</a> ⇒ <code>boolean</code></dt>
<dd><p>Test if a given value is a Javascript Object or not</p>
</dd>
<dt><a href="#arrayToObject">arrayToObject(array, keyField)</a> ⇒ <code>Object</code></dt>
<dd><p>Convert an array of object to an object</p>
</dd>
<dt><a href="#stripEmptyArrays">stripEmptyArrays(object)</a> ⇒ <code>Object</code></dt>
<dd><p>Remove keys from an object whose values are empty arrays</p>
</dd>
<dt><a href="#getFromSwitcherTreePath">getFromSwitcherTreePath(object, treePath)</a> ⇒ <code>Object</code></dt>
<dd><p>Gets the object situated at the specified switcher path. A switcher path
   is a string of &quot;words.with.periods.and.sometimes.1.numbers&quot;. The object is traversed with every
   word between the periods representing a key in an object.
   This returns a copy of the object, mutating it won&#39;t affect the original object</p>
</dd>
<dt><a href="#getMutatedFromSwitcherTreePath">getMutatedFromSwitcherTreePath(object, treePath)</a> ⇒ <code>Object</code></dt>
<dd><p>return a copy of the passed object with the specified assignation.
   the object situated at the specified switcher path to the specified value.
   this will create the missing obects/arrays along the specified path, creating
   object when it needs to set something at a non positive integer key and arrays
   when it needs to set at positive integer keys.</p>
<p>   Example for the creation of non existing objects/arrays : if the object is {}, path
   is some.stuff.1 and the value is &quot;test&quot;, this code will first set the some key to an empty object
   because the next path (stuff) is not a positive integer. it will then stuff key of the some object
   to an empty array because the next key is a positive integer and will set the value &quot;test&quot; to
   the index 1 of the array. resulting in the following object :
   {some : {
   stuff:[<empty>,&quot;test&quot;]
   }
   }</p>
</dd>
<dt><a href="#getPrunedFromSwitcherTreePath">getPrunedFromSwitcherTreePath(object, treePath)</a> ⇒ <code>Object</code></dt>
<dd><p>Takes an object and a switcher tree path, clones the object and deletes all the values situated at and after
   the specified path in the clone object. Returns the entire cloned object minus the pruned parts.</p>
</dd>
<dt><a href="#useHover">useHover()</a> ⇒ <code><a href="#external_react/customHook">react/customHook</a></code></dt>
<dd><p>Custom React hook that simulates the hover event</p>
</dd>
<dt><a href="#escapeAsArray">escapeAsArray(output)</a> ⇒ <code>Array.&lt;string&gt;</code></dt>
<dd><p>Escape all new lines into an array</p>
</dd>
<dt><a href="#escapeToSafeCss">escapeToSafeCss()</a></dt>
<dd><p>Gets a version of the string that is sanitized for CSS purposes.
We need this function because we use URIs as category id for sip quiddities and the grid css breaks
if invalid or unescaped characters are in css properties.
see <a href="https://www.w3.org/TR/CSS21/syndata.html#characters">https://www.w3.org/TR/CSS21/syndata.html#characters</a> for the why of the different regexes</p>
</dd>
<dt><a href="#toString">toString(input)</a> ⇒ <code>string</code></dt>
<dd><p>Transform any value to a string, <code>null</code> and <code>undefined</code> values are converted to an empty string.</p>
</dd>
<dt><a href="#isStringEmpty">isStringEmpty(value)</a> ⇒ <code>boolean</code></dt>
<dd><p>Checks if a string is empty</p>
</dd>
<dt><a href="#cleanParenthesis">cleanParenthesis(input)</a> ⇒ <code>string</code></dt>
<dd><p>Cleans the parenthesis in a string</p>
</dd>
<dt><a href="#replaceAll">replaceAll(input, [oldStr], [newStr])</a> ⇒ <code>string</code></dt>
<dd><p>Sanitizes a string by replacing all matches with another character</p>
</dd>
<dt><a href="#parseDate">parseDate(input)</a> ⇒ <code>Date</code></dt>
<dd><p>Parses a string date to a javascript Date object</p>
</dd>
<dt><a href="#capitalize">capitalize(input)</a> ⇒ <code>string</code></dt>
<dd><p>Capitalizes the input string. Only the very first letter is capitalized.</p>
</dd>
<dt><a href="#toOrderButtonId">toOrderButtonId(orderId)</a> ⇒ <code>string</code></dt>
<dd><p>Transform order directions</p>
</dd>
<dt><a href="#toNumber">toNumber(unit, value)</a> ⇒ <code>number</code></dt>
<dd><p>Converts an unit to a displayable number</p>
</dd>
<dt><a href="#fromNumber">fromNumber(unit, convertedValue)</a> ⇒ <code>number</code></dt>
<dd><p>Converts displayed number to its original unit</p>
</dd>
<dt><a href="#fromPath">fromPath(path)</a> ⇒ <code>string</code></dt>
<dd><p>Get a config name from its paths</p>
</dd>
<dt><a href="#toPageIcon">toPageIcon(pageId)</a> ⇒ <code>string</code></dt>
<dd><p>Gets the icon type of a page</p>
</dd>
<dt><a href="#toDestinationKind">toDestinationKind(matrixCategory)</a> ⇒ <code>string</code></dt>
<dd><p>Gets the corresponding quiddity kind from a matrix category</p>
</dd>
<dt><a href="#toMatrixCategory">toMatrixCategory(kindId)</a> ⇒ <code>string</code></dt>
<dd><p>Gets the corresponding category from quiddity kind</p>
</dd>
<dt><a href="#populateStoreCreationBindings">populateStoreCreationBindings()</a> ⇒ <code>Map.&lt;string, function()&gt;</code></dt>
<dd><p>Creates creation bindings for quiddity-related Stores and sets them inside the QuiddityStore</p>
</dd>
<dt><a href="#populateModalCreationBindings">populateModalCreationBindings()</a> ⇒ <code>Map.&lt;string, function()&gt;</code></dt>
<dd><p>Creates creation bindings for modal-related Stores and sets them inside the QuiddityStore</p>
</dd>
<dt><a href="#populateStores">populateStores()</a> ⇒ <code>object</code></dt>
<dd><p>Creates all the app&#39;s Stores</p>
</dd>
<dt><a href="#initializeStores">initializeStores(stores)</a> ⇒ <code>boolean</code></dt>
<dd><p>Initializes the main stores: ConfigStore and QuiddityStore.</p>
<p>ConnectionStore and QuiddityMenuStore will be initialized in reaction to
the SceneStore and QuiddityStore&#39;s initialization, respectively.</p>
<p>Most other Stores will be initialized when their associated quiddity is created.</p>
</dd>
<dt><a href="#deinitializeStores">deinitializeStores(stores)</a></dt>
<dd><p>Cleans up the application stores</p>
</dd>
<dt><a href="#initializeSocket">initializeSocket(stores)</a></dt>
<dd><p>Initializes the socket handlers</p>
</dd>
<dt><a href="#Lock">Lock()</a> ⇒ <code>external:ui-components/Icon</code></dt>
<dd><p>Renders a lock icon</p>
</dd>
<dt><a href="#ConnectionLock">ConnectionLock()</a> ⇒ <code>external:ui-components/Icon</code></dt>
<dd><p>Renders alock icon that fits with Connection boxes</p>
</dd>
<dt><a href="#GridLayout">GridLayout(children)</a> ⇒ <code><a href="#external_react/Component">react/Component</a></code></dt>
<dd><p>GridLayout adapted for the NumberField</p>
</dd>
<dt><a href="#AvailabilityStatus">AvailabilityStatus(buddyStatus)</a> ⇒ <code><a href="#external_react/Component">react/Component</a></code></dt>
<dd><p>Renders the availability status of a SIP contact</p>
</dd>
<dt><a href="#Information">Information(buddyStatus, sendStatus, recvStatus)</a> ⇒ <code><a href="#external_react/Component">react/Component</a></code></dt>
<dd><p>Renders the SIP contact information</p>
</dd>
<dt><a href="#LockedDestinationWarning">LockedDestinationWarning(categoryId)</a> ⇒ <code><a href="#external_react/Component">react/Component</a></code></dt>
<dd><p>Displays the content of the tooltip</p>
</dd>
<dt><a href="#LockedDestinationTooltip">LockedDestinationTooltip(children, categoryId)</a> ⇒ <code>external:ui-components/Feedback.Tooltip</code></dt>
<dd><p>Displays a tooltip with information about locked destinations</p>
</dd>
<dt><a href="#Capability">Capability(capsKey, value)</a> ⇒ <code><a href="#external_react/Component">react/Component</a></code></dt>
<dd><p>Renders a capability entry</p>
</dd>
<dt><a href="#CapabilityGroup">CapabilityGroup(groupTitle, groupValue)</a> ⇒ <code><a href="#external_react/Component">react/Component</a></code></dt>
<dd><p>Renders a capability group</p>
</dd>
<dt><a href="#BlankAddon">BlankAddon()</a></dt>
<dd><p>Renders a blank addon</p>
</dd>
<dt><a href="#PageIcon">PageIcon(pageId)</a> ⇒ <code><a href="#external_react/Component">react/Component</a></code></dt>
<dd><p>Renders the page&#39;s icon</p>
</dd>
<dt><a href="#PageTitle">PageTitle(pageId)</a> ⇒ <code><a href="#external_react/Component">react/Component</a></code></dt>
<dd><p>Renders the page&#39;s title</p>
</dd>
<dt><a href="#ScenicSignature">ScenicSignature()</a> ⇒ <code><a href="#external_react/Component">react/Component</a></code></dt>
<dd><p>The signature of Scenic</p>
</dd>
<dt><a href="#PageMenu">PageMenu(pageId, pageStore)</a> ⇒ <code><a href="#external_mobx-react/ObserverComponent">mobx-react/ObserverComponent</a></code></dt>
<dd><p>Menu that renders all main menus of the Scenic app</p>
</dd>
<dt><a href="#QuiddityMenuFactory">QuiddityMenuFactory(menuModel)</a> ⇒ <code><a href="#external_react/Component">react/Component</a></code></dt>
<dd><p>Abstract wrapper that builds a menu item or group</p>
</dd>
<dt><a href="#OpenSessionModal">OpenSessionModal()</a> ⇒ <code>React.PureComponent</code></dt>
<dd><p>Scenic modal used to open a new session</p>
</dd>
<dt><a href="#ResetSessionModal">ResetSessionModal()</a> ⇒ <code>React.PureComponent</code></dt>
<dd><p>Scenic modal used to reset the current session</p>
</dd>
<dt><a href="#TrashCurrentSessionModal">TrashCurrentSessionModal()</a> ⇒ <code>React.PureComponent</code></dt>
<dd><p>Renders the modal that alerts the deletion of current session&#39;s file</p>
</dd>
<dt><a href="#RowEntry">RowEntry(className, title, subtitle, status, selected, disabled, onClick, width)</a> ⇒ <code><a href="#external_react/Component">react/Component</a></code></dt>
<dd><p>A generic entry that should be display in a column or as a row header</p>
</dd>
<dt><a href="#FileEntry">FileEntry()</a> ⇒ <code>React.PureComponent</code></dt>
<dd><p>Renders an entry that represents a file</p>
</dd>
<dt><a href="#Link">Link(href, children)</a> ⇒ <code><a href="#external_react/Component">react/Component</a></code></dt>
<dd><p>A link component that opens page in a new tab</p>
</dd>
<dt><a href="#PageTitle">PageTitle()</a> ⇒ <code><a href="#external_react/Component">react/Component</a></code></dt>
<dd><p>The page title</p>
</dd>
<dt><a href="#TabBar">TabBar()</a> ⇒ <code><a href="#external_react/Component">react/Component</a></code></dt>
<dd><p>Renders the tab bar for the Matrix</p>
</dd>
<dt><a href="#SideBar">SideBar()</a> ⇒ <code><a href="#external_react/Component">react/Component</a></code></dt>
<dd><p>Renders the side bar</p>
</dd>
<dt><a href="#DrawerOverlay">DrawerOverlay()</a> ⇒ <code><a href="#external_react/Component">react/Component</a></code></dt>
<dd><p>Renders all drawers used in the Matrix</p>
</dd>
<dt><a href="#Scene">Scene(scene, onClick)</a> ⇒ <code>external:ui-components/Layout.Cell</code></dt>
<dd><p>Renders a scene as a Cell</p>
</dd>
<dt><a href="#SeparatorTitle">SeparatorTitle(title)</a> ⇒ <code><a href="#external_react/Component">react/Component</a></code></dt>
<dd><p>Renders the separator title</p>
</dd>
<dt><a href="#ColumnSeparator">ColumnSeparator(style, header, color)</a> ⇒ <code><a href="#external_react/Component">react/Component</a></code></dt>
<dd><p>Renders the UI separator between matrix columns</p>
</dd>
<dt><a href="#EmptySourceColumnArea">EmptySourceColumnArea()</a> ⇒ <code><a href="#external_react/Component">react/Component</a></code></dt>
<dd><p>Renders a column area without source</p>
</dd>
<dt><a href="#SourceSeparator">SourceSeparator(style, header)</a> ⇒ <code><a href="#external_react/Component">react/Component</a></code></dt>
<dd><p>Renders the UI separator between matrix columns</p>
</dd>
<dt><a href="#SourceShmdata">SourceShmdata(entry)</a> ⇒ <code>module:components/matrix.ShmdataBox</code></dt>
<dd><p>Renders a shmdata box related to a source</p>
</dd>
<dt><a href="#EmptyShmdata">EmptyShmdata()</a> ⇒ <code>external:ui-components/Cell</code></dt>
<dd><p>Renders an empty box that replaces a shmdata</p>
</dd>
<dt><a href="#SourceLock">SourceLock()</a> ⇒ <code><a href="#external_react/Component">react/Component</a></code></dt>
<dd><p>Renders a lock on the source</p>
</dd>
<dt><a href="#EmptyStarter">EmptyStarter()</a> ⇒ <code><a href="#external_react/Component">react/Component</a></code></dt>
<dd><p>Renders an empty starter that does nothing</p>
</dd>
<dt><a href="#SourceSubtitle">SourceSubtitle(quiddity, shmdata)</a> ⇒ <code>string</code></dt>
<dd><p>Renders the subtitle of a source</p>
</dd>
<dt><a href="#EmptySource">EmptySource()</a> ⇒ <code><a href="#external_react/Component">react/Component</a></code></dt>
<dd><p>Renders an empty source</p>
</dd>
<dt><a href="#NewSessionMenu">NewSessionMenu()</a> ⇒ <code>module.components/common.MenuButton</code></dt>
<dd><p>Renders the button that resets the session</p>
</dd>
<dt><a href="#saveCurrentSession">saveCurrentSession(sessionStore, drawerStore)</a></dt>
<dd><p>Save the current session if it was already saved or open the FileSavingDrawer</p>
</dd>
<dt><a href="#SaveSessionMenu">SaveSessionMenu()</a> ⇒ <code>module.components/common.MenuButton</code></dt>
<dd><p>Renders the button that saves the current session</p>
</dd>
<dt><a href="#MatrixMenuPanel">MatrixMenuPanel()</a></dt>
<dd><p>Renders all menus that add extra actions with the Scenic matrix</p>
</dd>
</dl>

## External

<dl>
<dt><a href="#external_pino/logger">pino/logger</a></dt>
<dd><p>Pino logger provides a minimal JSON logger</p>
</dd>
<dt><a href="#external_pino/level">pino/level</a></dt>
<dd><p>Define each level of the Pino logger</p>
</dd>
<dt><a href="#external_pino/mergingObject">pino/mergingObject</a></dt>
<dd><p>StatusEnum defines a shared status for all UI components</p>
</dd>
<dt><a href="#external_pino/logEvent">pino/logEvent</a></dt>
<dd><p>Structure provided by the <code>transmit</code> capability of the logger</p>
</dd>
<dt><a href="#external_pino/logBindings">pino/logBindings</a></dt>
<dd><p>Array of bindings that is created by each child logger</p>
</dd>
<dt><a href="#external_react/customHook">react/customHook</a></dt>
<dd><p>Tuple that contains a callback and a value. The callback is triggering the value update.</p>
</dd>
<dt><a href="#external_ui-components/Common/StatusEnum">ui-components/Common/StatusEnum</a></dt>
<dd><p>StatusEnum defines a shared status for all UI components</p>
</dd>
<dt><a href="#external_mobx-react/ObserverComponent">mobx-react/ObserverComponent</a></dt>
<dd><p>An Mobx observer component that reacts to each of its properties notifying an update</p>
</dd>
<dt><a href="#external_react/Component">react/Component</a></dt>
<dd><p>A React component written with JSX</p>
</dd>
<dt><a href="#external_react/Context">react/Context</a></dt>
<dd><p>A React context that dispatches properties into nested children</p>
</dd>
<dt><a href="#external_ui-components/Inputs/Field">ui-components/Inputs/Field</a></dt>
<dd><p>The UI-Components Field</p>
</dd>
<dt><a href="#external_ui-components/Menu">ui-components/Menu</a></dt>
<dd><p>The UI-Components Menu</p>
</dd>
<dt><a href="#external_react-color">react-color</a></dt>
<dd><p>React color components</p>
</dd>
</dl>

<a name="module_utils/logger"></a>

## utils/logger
Define all logging capabilities of the webapp

<a name="module_utils/logger.notificationStore"></a>

### utils/logger.notificationStore : [<code>NotificationStore</code>](#module_stores/common.NotificationStore)
Attached notification store to the logger

**Kind**: static property of [<code>utils/logger</code>](#module_utils/logger)  
<a name="module_utils/logger.socketStore"></a>

### utils/logger.socketStore : <code>module:stores/common.SocketStore</code>
Attached socket store to the logger

**Kind**: static property of [<code>utils/logger</code>](#module_utils/logger)  
<a name="module_utils/logger.logger"></a>

### utils/logger.logger : [<code>pino/logger</code>](#external_pino/logger)
Creates a singleton logger for the webapp

**Kind**: static property of [<code>utils/logger</code>](#module_utils/logger)  
<a name="module_utils/logger.getLoggerLevel"></a>

### utils/logger.getLoggerLevel() ⇒ [<code>pino/level</code>](#external_pino/level)
Gets the level of the logger according to the app context
The checked contexts are (in order):
- URL parameter (url?level=debug)
- `NODE_ENV` variable (default values are configured by jest or by webpack configuration)

**Kind**: static method of [<code>utils/logger</code>](#module_utils/logger)  
**Returns**: [<code>pino/level</code>](#external_pino/level) - The logger level  
**See**: [jest configuration of environment variables](https://jestjs.io/docs/environment-variables)  
<a name="module_utils/logger.bindStores"></a>

### utils/logger.bindStores([stores])
Binds the notification store and the socketStore to the logger

**Kind**: static method of [<code>utils/logger</code>](#module_utils/logger)  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [stores] | <code>Object</code> | <code></code> | The stores that should be bound to the logger |
| stores.notificationStore | <code>string</code> |  | The notification store |
| stores.socketStore | <code>string</code> |  | The socket store |

<a name="module_models/common"></a>

## models/common
Define all common structures

<a name="module_models/common.Notification"></a>

### models/common.Notification
Model for making new notifications

**Kind**: static class of [<code>models/common</code>](#module_models/common)  
<a name="new_module_models/common.Notification_new"></a>

#### new Notification(id, message, [description], [status], [duration], [notificationClass])
Instantiates a new Notification model

**Throws**:

- <code>TypeError</code> Throws an error if the required message is missing


| Param | Type | Default | Description |
| --- | --- | --- | --- |
| id | <code>string</code> |  | ID of the notification it must be strings separated by a dash |
| message | <code>string</code> |  | Main message of the notification |
| [description] | <code>string</code> | <code>null</code> | Description of the notification |
| [status] | <code>models.StatusEnum</code> | <code>StatusEnum.FOCUS</code> | Status of the notification |
| [duration] | <code>number</code> | <code>5000</code> | Duration of the notification in millisecond |
| [notificationClass] | <code>string</code> | <code>&quot;&#x27;&#x27;&quot;</code> | Class of the notification |

<a name="module_models/common.Notification+id"></a>

#### notification.id
**Kind**: instance property of [<code>Notification</code>](#module_models/common.Notification)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| id | <code>string</code> | ID of the notification, it must be strings separated by a dash |

<a name="module_models/common.Notification+message"></a>

#### notification.message
**Kind**: instance property of [<code>Notification</code>](#module_models/common.Notification)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| message | <code>string</code> | Message of the notification |

<a name="module_models/common.Notification+description"></a>

#### notification.description
**Kind**: instance property of [<code>Notification</code>](#module_models/common.Notification)  
**Properties**

| Name | Type | Default | Description |
| --- | --- | --- | --- |
| [description] | <code>string</code> | <code>null</code> | Description of the notification |

<a name="module_models/common.Notification+status"></a>

#### notification.status
**Kind**: instance property of [<code>Notification</code>](#module_models/common.Notification)  
**Properties**

| Name | Type | Default | Description |
| --- | --- | --- | --- |
| [status] | <code>models.StatusEnum</code> | <code>StatusEnum.FOCUS</code> | Status of the notification |

<a name="module_models/common.Notification+duration"></a>

#### notification.duration
**Kind**: instance property of [<code>Notification</code>](#module_models/common.Notification)  
**Properties**

| Name | Type | Default | Description |
| --- | --- | --- | --- |
| [duration] | <code>number</code> | <code>5000</code> | Duration of the notification in millisecond (NaN triggers a permanent notification) |

<a name="module_models/common.Notification+notificationClass"></a>

#### notification.notificationClass ⇒ <code>string</code>
Gets the class of the notification

**Kind**: instance property of [<code>Notification</code>](#module_models/common.Notification)  
**Returns**: <code>string</code> - Returns the class of the notification or an empty string  
<a name="module_models/common.Notification+isPermanent"></a>

#### notification.isPermanent ⇒ <code>boolean</code>
Flags if a notification is permanent

**Kind**: instance property of [<code>Notification</code>](#module_models/common.Notification)  
**Returns**: <code>boolean</code> - Flag a permanent notification  
<a name="module_models/common.Notification+toJSON"></a>

#### notification.toJSON() ⇒ <code>object</code>
Transforms the notification to a JSON object

**Kind**: instance method of [<code>Notification</code>](#module_models/common.Notification)  
**Returns**: <code>object</code> - A JSON notification  
<a name="module_models/common.Notification.fromString"></a>

#### Notification.fromString(message) ⇒ [<code>Notification</code>](#module_models/common.Notification)
Creates a sample notification

**Kind**: static method of [<code>Notification</code>](#module_models/common.Notification)  
**Returns**: [<code>Notification</code>](#module_models/common.Notification) - A new notification  

| Param | Type | Description |
| --- | --- | --- |
| message | <code>string</code> | Message of the notification |

<a name="module_models/common.Notification.getLogTitle"></a>

#### Notification.getLogTitle(logLevel) ⇒ <code>string</code>
Gets a title from a log level

**Kind**: static method of [<code>Notification</code>](#module_models/common.Notification)  
**Returns**: <code>string</code> - A generic notification title  

| Param | Type | Description |
| --- | --- | --- |
| logLevel | [<code>pino/level</code>](#external_pino/level) | Level of logging |

<a name="module_models/common.Notification.fromLog"></a>

#### Notification.fromLog(logLevel, logObject, logBindings) ⇒ [<code>Notification</code>](#module_models/common.Notification)
Creates a notification from a log object

**Kind**: static method of [<code>Notification</code>](#module_models/common.Notification)  
**Returns**: [<code>Notification</code>](#module_models/common.Notification) - A new notification  

| Param | Type | Description |
| --- | --- | --- |
| logLevel | [<code>pino/level</code>](#external_pino/level) | Level of the current logger |
| logObject | [<code>pino/mergingObject</code>](#external_pino/mergingObject) | JSON log from the webapp |
| logBindings | [<code>pino/logBindings</code>](#external_pino/logBindings) | All bindings transmitted by a child logger |

<a name="module_models/common.StatusEnum"></a>

### models/common.StatusEnum : <code>object</code>
Define all adapters for the StatusEnum

**Kind**: static namespace of [<code>models/common</code>](#module_models/common)  
<a name="module_models/common.StatusEnum.fromBuddyStatus"></a>

#### StatusEnum.fromBuddyStatus(buddyStatus) ⇒ [<code>ui-components/Common/StatusEnum</code>](#external_ui-components/Common/StatusEnum)
Makes the availability status of a contact

**Kind**: static method of [<code>StatusEnum</code>](#module_models/common.StatusEnum)  
**Returns**: [<code>ui-components/Common/StatusEnum</code>](#external_ui-components/Common/StatusEnum) - The UI status  

| Param | Type | Description |
| --- | --- | --- |
| buddyStatus | <code>models.BuddyStatusEnum</code> | Status of a buddy |

<a name="module_models/common.StatusEnum.fromRecvStatus"></a>

#### StatusEnum.fromRecvStatus(receivingStatus) ⇒ [<code>ui-components/Common/StatusEnum</code>](#external_ui-components/Common/StatusEnum)
Makes the receiving status of a contact

**Kind**: static method of [<code>StatusEnum</code>](#module_models/common.StatusEnum)  
**Returns**: [<code>ui-components/Common/StatusEnum</code>](#external_ui-components/Common/StatusEnum) - The UI status  

| Param | Type | Description |
| --- | --- | --- |
| receivingStatus | <code>models.RecvStatusEnum</code> | Receiving status of a contact |

<a name="module_models/common.StatusEnum.fromSendStatus"></a>

#### StatusEnum.fromSendStatus(sendingStatus) ⇒ [<code>ui-components/Common/StatusEnum</code>](#external_ui-components/Common/StatusEnum)
Makes the sending status of a contact

**Kind**: static method of [<code>StatusEnum</code>](#module_models/common.StatusEnum)  
**Returns**: [<code>ui-components/Common/StatusEnum</code>](#external_ui-components/Common/StatusEnum) - The UI status  

| Param | Type | Description |
| --- | --- | --- |
| sendingStatus | <code>models.SendStatusEnum</code> | Sending status of a contact |

<a name="module_models/common.StatusEnum.fromLogLevel"></a>

#### StatusEnum.fromLogLevel(logLevel) ⇒ [<code>ui-components/Common/StatusEnum</code>](#external_ui-components/Common/StatusEnum)
Makes a status from a logging level

**Kind**: static method of [<code>StatusEnum</code>](#module_models/common.StatusEnum)  
**Returns**: [<code>ui-components/Common/StatusEnum</code>](#external_ui-components/Common/StatusEnum) - The UI status  

| Param | Type | Description |
| --- | --- | --- |
| logLevel | [<code>pino/level</code>](#external_pino/level) | A logging level |

<a name="module_models/common.ConfigEnum"></a>

### models/common.ConfigEnum : <code>enum</code>
Enum for all configuration file names

**Kind**: static enum of [<code>models/common</code>](#module_models/common)  
**Read only**: true  
<a name="module_models/shmdata"></a>

## models/shmdata
Define all structures that modelizes shmdatas

<a name="module_models/shmdata.Spec"></a>

### models/shmdata.Spec
Modelizes a connection spec attached to a quiddity

**Kind**: static class of [<code>models/shmdata</code>](#module_models/shmdata)  
<a name="new_module_models/shmdata.Spec_new"></a>

#### new Spec(label, description, can_do, [sfId], [swId])
Instantiates a Spec model


| Param | Type | Description |
| --- | --- | --- |
| label | <code>string</code> | Unique label from the spec of a quiddity |
| description | <code>string</code> | Description of the spec |
| can_do | <code>Set.&lt;string&gt;</code> | List of all compatible caps for connection |
| [sfId] | <code>number</code> | ID of the shmdata follower |
| [swId] | <code>number</code> | ID of the shmdata writer |

<a name="module_models/shmdata.Spec+label"></a>

#### spec.label
**Kind**: instance property of [<code>Spec</code>](#module_models/shmdata.Spec)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| label | <code>string</code> | Unique label from the spec of a quiddity |

<a name="module_models/shmdata.Spec+description"></a>

#### spec.description
**Kind**: instance property of [<code>Spec</code>](#module_models/shmdata.Spec)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| description | <code>string</code> | Description of the spec |

<a name="module_models/shmdata.Spec+canDo"></a>

#### spec.canDo
**Kind**: instance property of [<code>Spec</code>](#module_models/shmdata.Spec)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| can_do | <code>Set.&lt;string&gt;</code> | List of all compatible caps for connection |

<a name="module_models/shmdata.Spec+toJSON"></a>

#### spec.toJSON() ⇒ <code>object</code>
Transforms spec to a JSON object

**Kind**: instance method of [<code>Spec</code>](#module_models/shmdata.Spec)  
**Returns**: <code>object</code> - An object representation of the spec  
<a name="module_models/shmdata.Spec.fromJSON"></a>

#### Spec.fromJSON(json) ⇒ [<code>Spec</code>](#module_models/shmdata.Spec)
Parses a connection spec

**Kind**: static method of [<code>Spec</code>](#module_models/shmdata.Spec)  
**Returns**: [<code>Spec</code>](#module_models/shmdata.Spec) - A modelized GstObject for Scenic, null if an error occurs  

| Param | Type | Description |
| --- | --- | --- |
| json | <code>object</code> | json representation of the connection spec |

<a name="module_models/shmdata.MediaTypeEnum"></a>

### models/shmdata.MediaTypeEnum : <code>enum</code>
Enum for media types

**Kind**: static enum of [<code>models/shmdata</code>](#module_models/shmdata)  
**Read only**: true  
**See**: [List of media types](https://gstreamer.freedesktop.org/documentation/plugin-development/advanced/media-types.html?gi-language=c#list-of-defined-types)  
<a name="module_models/quiddity"></a>

## models/quiddity
Define all structures that modelize quiddities

<a name="module_models/quiddity.NdiStream"></a>

### models/quiddity.NdiStream
Set of properties used by Scenic to manage NDI streams

**Kind**: static class of [<code>models/quiddity</code>](#module_models/quiddity)  
<a name="new_module_models/quiddity.NdiStream_new"></a>

#### new NdiStream(id, name, label, host, [swPort])
Instantiates a new NDI stream with parameters parsed from its name


| Param | Type | Default | Description |
| --- | --- | --- | --- |
| id | <code>string</code> |  | Unique ID of the NDI stream |
| name | <code>string</code> |  | Name of the NDI stream (used as the quiddity name) |
| label | <code>string</code> |  | Label of the quiddity name (used to display the available streams) |
| host | <code>string</code> |  | IP address of the host that provides the NDI stream |
| [swPort] | <code>string</code> | <code>&quot;&#x27;8000&#x27;&quot;</code> | Port used in order to communicate with Switcher |

<a name="module_models/quiddity.NdiStream+id"></a>

#### ndiStream.id
**Kind**: instance property of [<code>NdiStream</code>](#module_models/quiddity.NdiStream)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| id | <code>id</code> | Unique ID generated by Scenic |

<a name="module_models/quiddity.NdiStream+label"></a>

#### ndiStream.label
**Kind**: instance property of [<code>NdiStream</code>](#module_models/quiddity.NdiStream)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| label | <code>string</code> | Default label of NDI streams provided by NewTek NDI |

<a name="module_models/quiddity.NdiStream+name"></a>

#### ndiStream.name
**Kind**: instance property of [<code>NdiStream</code>](#module_models/quiddity.NdiStream)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | Parsed name from the default label |

<a name="module_models/quiddity.NdiStream+host"></a>

#### ndiStream.host
**Kind**: instance property of [<code>NdiStream</code>](#module_models/quiddity.NdiStream)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| host | <code>string</code> | Parsed host name from the default label |

<a name="module_models/quiddity.NdiStream+swPort"></a>

#### ndiStream.swPort
**Kind**: instance property of [<code>NdiStream</code>](#module_models/quiddity.NdiStream)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| swPort | <code>string</code> | Switcher port used to generate shmdata names |

<a name="module_models/quiddity.NdiStream+hash"></a>

#### ndiStream.hash
**Kind**: instance property of [<code>NdiStream</code>](#module_models/quiddity.NdiStream)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| hash | <code>number</code> | Unique hash of the NDI label |

<a name="module_models/quiddity.NdiStream+directory"></a>

#### ndiStream.directory
**Kind**: instance property of [<code>NdiStream</code>](#module_models/quiddity.NdiStream)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| directory | <code>string</code> | Name of the directory where the shmdata will be generated |

<a name="module_models/quiddity.NdiStream+commandLine"></a>

#### ndiStream.commandLine
**Kind**: instance property of [<code>NdiStream</code>](#module_models/quiddity.NdiStream)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| commandLine | <code>string</code> | Executed command line in order to generate shmdatas from the NDI stream |

<a name="module_models/quiddity.NdiStream+properties"></a>

#### ndiStream.properties
**Kind**: instance property of [<code>NdiStream</code>](#module_models/quiddity.NdiStream)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| properties | <code>Object</code> | Init properties of the NDIOutput quiddity |

<a name="module_models/quiddity.NdiStream+toOption"></a>

#### ndiStream.toOption() ⇒ <code>Object</code>
Casts the NDI stream to an option for Select component

**Kind**: instance method of [<code>NdiStream</code>](#module_models/quiddity.NdiStream)  
**Returns**: <code>Object</code> - A selectable option  
<a name="module_models/quiddity.NdiStream.NDI_REGEX"></a>

#### NdiStream.NDI\_REGEX
Regex which helps to craft an ID for NDI streams

**Kind**: static constant of [<code>NdiStream</code>](#module_models/quiddity.NdiStream)  
<a name="module_models/quiddity.NdiStream.fromOutput"></a>

#### NdiStream.fromOutput(stdout, [swPort]) ⇒ <code>Array.&lt;models.NdiStream&gt;</code>
Creates NdiStreams from the ndi2shmdata output

**Kind**: static method of [<code>NdiStream</code>](#module_models/quiddity.NdiStream)  
**Returns**: <code>Array.&lt;models.NdiStream&gt;</code> - All NDI stream parsed in the output  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| stdout | <code>string</code> |  | Output of ndi2shmdata |
| [swPort] | <code>string</code> | <code>&quot;&#x27;8000&#x27;&quot;</code> | Port used in order to communicate with Switcher |

<a name="module_models/quiddity.NdiStream.fromNdiEntry"></a>

#### NdiStream.fromNdiEntry(entry, [swPort]) ⇒ <code>models.NdiStream</code>
Creates a NdiStream from a ndi2shmdata entry

**Kind**: static method of [<code>NdiStream</code>](#module_models/quiddity.NdiStream)  
**Returns**: <code>models.NdiStream</code> - The NDI stream parsed from a ndi2shmdata entry  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| entry | <code>string</code> |  | Entry of a NDI stream provided by ndi2shmdata |
| [swPort] | <code>string</code> | <code>&quot;&#x27;8000&#x27;&quot;</code> | Port used in order to communicate with Switcher |

<a name="module_stores/common"></a>

## stores/common
Define all stores for common UI behaviors

<a name="module_stores/common.RequirementError"></a>

### stores/common.RequirementError
Error triggered when stores failed their initialization

**Kind**: static class of [<code>stores/common</code>](#module_stores/common)  
<a name="new_module_stores/common.RequirementError_new"></a>

#### new RequirementError(dependentStore, requiredStore)
Instantiates a requirement error


| Param | Type | Description |
| --- | --- | --- |
| dependentStore | <code>string</code> | Name of the store that needs the requirement |
| requiredStore | <code>string</code> | Name of the required store |

<a name="module_stores/common.InitializationError"></a>

### stores/common.InitializationError
Error triggered when stores failed their initialization

**Kind**: static class of [<code>stores/common</code>](#module_stores/common)  
<a name="new_module_stores/common.InitializationError_new"></a>

#### new InitializationError(storeName)
Instantiates an initialization error


| Param | Type | Description |
| --- | --- | --- |
| storeName | <code>store</code> | Name of the deinitialized store |

<a name="module_stores/common.NotificationStore"></a>

### stores/common.NotificationStore ⇐ [<code>Store</code>](#module_stores/common.Store)
Stores all notifications

**Kind**: static class of [<code>stores/common</code>](#module_stores/common)  
**Extends**: [<code>Store</code>](#module_stores/common.Store)  
<a name="module_stores/common.NotificationStore+userNotifications"></a>

#### notificationStore.userNotifications
**Kind**: instance property of [<code>NotificationStore</code>](#module_stores/common.NotificationStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| userNotifications | [<code>Set.&lt;Notification&gt;</code>](#module_models/common.Notification) | The displayed notification |

<a name="module_stores/common.NotificationStore+expiredNotifications"></a>

#### notificationStore.expiredNotifications
**Kind**: instance property of [<code>NotificationStore</code>](#module_stores/common.NotificationStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| expiredNotifications | <code>Set.&lt;string&gt;</code> | All expired notification IDs |

<a name="module_stores/common.NotificationStore+muteClasses"></a>

#### notificationStore.muteClasses
**Kind**: instance property of [<code>NotificationStore</code>](#module_stores/common.NotificationStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| muteClasses | <code>Set.&lt;string&gt;</code> | All classes that aren't published as notifications |

<a name="module_stores/common.Store+socketStore"></a>

#### notificationStore.socketStore
**Kind**: instance property of [<code>NotificationStore</code>](#module_stores/common.NotificationStore)  
**Overrides**: [<code>socketStore</code>](#module_stores/common.Store+socketStore)  
**Properties**

| Type | Description |
| --- | --- |
| [<code>SocketStore</code>](#stores.SocketStore) | Socket manager |

<a name="module_stores/common.Store+initState"></a>

#### notificationStore.initState
**Kind**: instance property of [<code>NotificationStore</code>](#module_stores/common.NotificationStore)  
**Overrides**: [<code>initState</code>](#module_stores/common.Store+initState)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| initState | <code>number</code> | Initialization state of the store |

<a name="module_stores/common.Store+hasActiveSocket"></a>

#### notificationStore.hasActiveSocket
**Kind**: instance property of [<code>NotificationStore</code>](#module_stores/common.NotificationStore)  
**Overrides**: [<code>hasActiveSocket</code>](#module_stores/common.Store+hasActiveSocket)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| hasActiveSocket | <code>boolean</code> | Checks if the active socket exists |

<a name="module_stores/common.NotificationStore+applyNotificationLog"></a>

#### notificationStore.applyNotificationLog(logLevel, logEvent)
Add a notification from a logging event

**Kind**: instance method of [<code>NotificationStore</code>](#module_stores/common.NotificationStore)  

| Param | Type | Description |
| --- | --- | --- |
| logLevel | [<code>pino/level</code>](#external_pino/level) | The level of the logging |
| logEvent | [<code>pino/logEvent</code>](#external_pino/logEvent) | An event transmitted by the logger |

<a name="module_stores/common.NotificationStore+applyNotificationPush"></a>

#### notificationStore.applyNotificationPush(notification)
Pushes a notification in the queue and sets a timeout on it

**Kind**: instance method of [<code>NotificationStore</code>](#module_stores/common.NotificationStore)  

| Param | Type | Description |
| --- | --- | --- |
| notification | [<code>Notification</code>](#module_models/common.Notification) | The notification to push |

<a name="module_stores/common.NotificationStore+applyDurationTimeout"></a>

#### notificationStore.applyDurationTimeout(notification)
Applies a timeout with the notification duration

**Kind**: instance method of [<code>NotificationStore</code>](#module_stores/common.NotificationStore)  

| Param | Type | Description |
| --- | --- | --- |
| notification | [<code>Notification</code>](#module_models/common.Notification) | The notification to timeout |

<a name="module_stores/common.NotificationStore+isExpired"></a>

#### notificationStore.isExpired(notification) ⇒ <code>boolean</code>
Checks if a notification is expired

**Kind**: instance method of [<code>NotificationStore</code>](#module_stores/common.NotificationStore)  
**Returns**: <code>boolean</code> - Flags an expired notifiaction  

| Param | Type | Description |
| --- | --- | --- |
| notification | [<code>Notification</code>](#module_models/common.Notification) | The notification to check |

<a name="module_stores/common.NotificationStore+addUserNotification"></a>

#### notificationStore.addUserNotification(notification)
Adds a new notification

**Kind**: instance method of [<code>NotificationStore</code>](#module_stores/common.NotificationStore)  

| Param | Type | Description |
| --- | --- | --- |
| notification | [<code>Notification</code>](#module_models/common.Notification) | A new notification |

<a name="module_stores/common.NotificationStore+addExpiredNotification"></a>

#### notificationStore.addExpiredNotification(notificationId)
Sets an expired notification

**Kind**: instance method of [<code>NotificationStore</code>](#module_stores/common.NotificationStore)  

| Param | Type | Description |
| --- | --- | --- |
| notificationId | <code>string</code> | ID of the notification |

<a name="module_stores/common.NotificationStore+removeNotification"></a>

#### notificationStore.removeNotification(notification)
Removes a notification from the queue and the expired notifications

**Kind**: instance method of [<code>NotificationStore</code>](#module_stores/common.NotificationStore)  

| Param | Type | Description |
| --- | --- | --- |
| notification | [<code>Notification</code>](#module_models/common.Notification) | The notification to remove |

<a name="module_stores/common.NotificationStore+addMuteClass"></a>

#### notificationStore.addMuteClass(notificationClass)
Mute a notification class

**Kind**: instance method of [<code>NotificationStore</code>](#module_stores/common.NotificationStore)  

| Param | Type | Description |
| --- | --- | --- |
| notificationClass | <code>string</code> | Class to mute, usually a store name |

<a name="module_stores/common.NotificationStore+removeMuteClass"></a>

#### notificationStore.removeMuteClass(notificationClass)
Unmute a notification class

**Kind**: instance method of [<code>NotificationStore</code>](#module_stores/common.NotificationStore)  

| Param | Type | Description |
| --- | --- | --- |
| notificationClass | <code>string</code> | Class to unmute, usually a store name |

<a name="module_stores/common.NotificationStore+clear"></a>

#### notificationStore.clear()
Clears all notification structures

**Kind**: instance method of [<code>NotificationStore</code>](#module_stores/common.NotificationStore)  
<a name="module_stores/common.Store+isInitialized"></a>

#### notificationStore.isInitialized() ⇒ <code>boolean</code>
Check if the store's current initialization state is INITIALIZED

**Kind**: instance method of [<code>NotificationStore</code>](#module_stores/common.NotificationStore)  
**Overrides**: [<code>isInitialized</code>](#module_stores/common.Store+isInitialized)  
**Returns**: <code>boolean</code> - Flags true if store is INITIALIZED  
<a name="module_stores/common.Store+isInitializing"></a>

#### notificationStore.isInitializing() ⇒ <code>boolean</code>
Check if the store's current initialization state is INITIALIZING

**Kind**: instance method of [<code>NotificationStore</code>](#module_stores/common.NotificationStore)  
**Overrides**: [<code>isInitializing</code>](#module_stores/common.Store+isInitializing)  
**Returns**: <code>boolean</code> - Flags true if store is INITIALIZING  
<a name="module_stores/common.Store+isNotInitialized"></a>

#### notificationStore.isNotInitialized() ⇒ <code>boolean</code>
Check if the store's current initialization state is NOT_INITIALIZED

**Kind**: instance method of [<code>NotificationStore</code>](#module_stores/common.NotificationStore)  
**Overrides**: [<code>isNotInitialized</code>](#module_stores/common.Store+isNotInitialized)  
**Returns**: <code>boolean</code> - Flags true if store is NOT_INITIALIZED  
<a name="module_stores/common.Store+applySuccessfulInitialization"></a>

#### notificationStore.applySuccessfulInitialization() ⇒ <code>boolean</code>
Applies the INITIALIZED statement to the store
Should be called when everything is allright!

**Kind**: instance method of [<code>NotificationStore</code>](#module_stores/common.NotificationStore)  
**Overrides**: [<code>applySuccessfulInitialization</code>](#module_stores/common.Store+applySuccessfulInitialization)  
**Returns**: <code>boolean</code> - Returns true if the store is well-initialized  
<a name="module_stores/common.Store+setInitState"></a>

#### notificationStore.setInitState(state) ⇒ <code>boolean</code>
Sets a new initialization state for the store

**Kind**: instance method of [<code>NotificationStore</code>](#module_stores/common.NotificationStore)  
**Overrides**: [<code>setInitState</code>](#module_stores/common.Store+setInitState)  
**Returns**: <code>boolean</code> - Flags true if the initialization state was updated  

| Param | Type | Description |
| --- | --- | --- |
| state | [<code>InitStateEnum</code>](#models.InitStateEnum) | New initialization state |

<a name="module_stores/common.Store+toJSON"></a>

#### notificationStore.toJSON() ⇒ <code>object</code>
Prints the store as a JSON object for debug purpose

**Kind**: instance method of [<code>NotificationStore</code>](#module_stores/common.NotificationStore)  
**Overrides**: [<code>toJSON</code>](#module_stores/common.Store+toJSON)  
**Returns**: <code>object</code> - The store state in a JSON object  
<a name="module_stores/common.SessionStore"></a>

### stores/common.SessionStore ⇐ [<code>Store</code>](#module_stores/common.Store)
Stores and manages sessions

**Kind**: static class of [<code>stores/common</code>](#module_stores/common)  
**Extends**: [<code>Store</code>](#module_stores/common.Store)  
<a name="new_module_stores/common.SessionStore_new"></a>

#### new SessionStore(socketStore)
Instantiates a new FileStore


| Param | Type | Description |
| --- | --- | --- |
| socketStore | [<code>SocketStore</code>](#stores.SocketStore) | Stores and manages the current event-driven socket |

<a name="module_stores/common.SessionStore+notificationStore"></a>

#### sessionStore.notificationStore
**Kind**: instance property of [<code>SessionStore</code>](#module_stores/common.SessionStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| notificationStore | <code>stores.NotificationStore</code> | Stores the notifications |

<a name="module_stores/common.SessionStore+currentSessionId"></a>

#### sessionStore.currentSessionId
**Kind**: instance property of [<code>SessionStore</code>](#module_stores/common.SessionStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| currentSessionId | <code>string</code> | ID of the currently selected session file |

<a name="module_stores/common.SessionStore+sessions"></a>

#### sessionStore.sessions
**Kind**: instance property of [<code>SessionStore</code>](#module_stores/common.SessionStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| sessions | <code>Map.&lt;string, models.FileBridge&gt;</code> | Map of existing sessions hashed by ID |

<a name="module_stores/common.SessionStore+sessionTrash"></a>

#### sessionStore.sessionTrash
**Kind**: instance property of [<code>SessionStore</code>](#module_stores/common.SessionStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| sessionTrash | <code>Set.&lt;string&gt;</code> | Set of all sessions to delete |

<a name="module_stores/common.SessionStore+currentSession"></a>

#### sessionStore.currentSession
**Kind**: instance property of [<code>SessionStore</code>](#module_stores/common.SessionStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| currentSession | [<code>FileBridge</code>](#models.FileBridge) | File model of the current session |

<a name="module_stores/common.Store+socketStore"></a>

#### sessionStore.socketStore
**Kind**: instance property of [<code>SessionStore</code>](#module_stores/common.SessionStore)  
**Overrides**: [<code>socketStore</code>](#module_stores/common.Store+socketStore)  
**Properties**

| Type | Description |
| --- | --- |
| [<code>SocketStore</code>](#stores.SocketStore) | Socket manager |

<a name="module_stores/common.Store+initState"></a>

#### sessionStore.initState
**Kind**: instance property of [<code>SessionStore</code>](#module_stores/common.SessionStore)  
**Overrides**: [<code>initState</code>](#module_stores/common.Store+initState)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| initState | <code>number</code> | Initialization state of the store |

<a name="module_stores/common.Store+hasActiveSocket"></a>

#### sessionStore.hasActiveSocket
**Kind**: instance property of [<code>SessionStore</code>](#module_stores/common.SessionStore)  
**Overrides**: [<code>hasActiveSocket</code>](#module_stores/common.Store+hasActiveSocket)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| hasActiveSocket | <code>boolean</code> | Checks if the active socket exists |

<a name="module_stores/common.SessionStore+handleSocketChange"></a>

#### sessionStore.handleSocketChange(socket)
Handles changes to the app's socket

**Kind**: instance method of [<code>SessionStore</code>](#module_stores/common.SessionStore)  
**Todo**

- [ ] resolve the session mechanism


| Param | Type | Description |
| --- | --- | --- |
| socket | <code>external:socketIO/Socket</code> | Event-driven socket |

<a name="module_stores/common.SessionStore+handleSessionImported"></a>

#### sessionStore.handleSessionImported(jsonFile)
Handles the import of a session file

**Kind**: instance method of [<code>SessionStore</code>](#module_stores/common.SessionStore)  

| Param | Type | Description |
| --- | --- | --- |
| jsonFile | <code>object</code> | The JSON model of the session file |

<a name="module_stores/common.SessionStore+handleSessionReset"></a>

#### sessionStore.handleSessionReset()
Handles a session file reset

**Kind**: instance method of [<code>SessionStore</code>](#module_stores/common.SessionStore)  
<a name="module_stores/common.SessionStore+handleSessionLoaded"></a>

#### sessionStore.handleSessionLoaded(jsonFile)
Handles the load of the session file

**Kind**: instance method of [<code>SessionStore</code>](#module_stores/common.SessionStore)  

| Param | Type | Description |
| --- | --- | --- |
| jsonFile | <code>object</code> | The JSON model of the session file |

<a name="module_stores/common.SessionStore+handleSessionSaved"></a>

#### sessionStore.handleSessionSaved(jsonFile)
Handles the save of a session file

**Kind**: instance method of [<code>SessionStore</code>](#module_stores/common.SessionStore)  

| Param | Type | Description |
| --- | --- | --- |
| jsonFile | <code>object</code> | The JSON model of the session file |

<a name="module_stores/common.SessionStore+handleSessionDeleted"></a>

#### sessionStore.handleSessionDeleted(fileName)
Handles the deletion of a session file

**Kind**: instance method of [<code>SessionStore</code>](#module_stores/common.SessionStore)  

| Param | Type | Description |
| --- | --- | --- |
| fileName | <code>string</code> | The name of the file |

<a name="module_stores/common.SessionStore+updateSessionList"></a>

#### sessionStore.updateSessionList()
Updates the session file list

**Kind**: instance method of [<code>SessionStore</code>](#module_stores/common.SessionStore)  
<a name="module_stores/common.SessionStore+fetchSessionList"></a>

#### sessionStore.fetchSessionList() ⇒ <code>Array.&lt;model.FileBridge&gt;</code>
Fetches session file list on the server

**Kind**: instance method of [<code>SessionStore</code>](#module_stores/common.SessionStore)  
**Returns**: <code>Array.&lt;model.FileBridge&gt;</code> - List of all sessions in the server  
<a name="module_stores/common.SessionStore+fetchSessionContent"></a>

#### sessionStore.fetchSessionContent(fileBridge) ⇒ <code>string</code>
Fetches a session file content

**Kind**: instance method of [<code>SessionStore</code>](#module_stores/common.SessionStore)  
**Returns**: <code>string</code> - The content of the file  

| Param | Type | Description |
| --- | --- | --- |
| fileBridge | <code>model.FileBridge</code> | A file model |

<a name="module_stores/common.SessionStore+applySessionReset"></a>

#### sessionStore.applySessionReset()
Requests the reset of the current Scenic session

**Kind**: instance method of [<code>SessionStore</code>](#module_stores/common.SessionStore)  
<a name="module_stores/common.SessionStore+applySessionLoading"></a>

#### sessionStore.applySessionLoading(fileBridge)
Requests the load of a session file

**Kind**: instance method of [<code>SessionStore</code>](#module_stores/common.SessionStore)  

| Param | Type | Description |
| --- | --- | --- |
| fileBridge | [<code>FileBridge</code>](#models.FileBridge) | File model to load |

<a name="module_stores/common.SessionStore+applySessionSaving"></a>

#### sessionStore.applySessionSaving()
Requests the save of the current session file

**Kind**: instance method of [<code>SessionStore</code>](#module_stores/common.SessionStore)  
<a name="module_stores/common.SessionStore+applyClearTrash"></a>

#### sessionStore.applyClearTrash()
Requests the deletion of all sessions

**Kind**: instance method of [<code>SessionStore</code>](#module_stores/common.SessionStore)  
<a name="module_stores/common.SessionStore+applySessionDownload"></a>

#### sessionStore.applySessionDownload(fileBridge, $linkRef)
Downloads a session file stored on the server

**Kind**: instance method of [<code>SessionStore</code>](#module_stores/common.SessionStore)  

| Param | Type | Description |
| --- | --- | --- |
| fileBridge | <code>FileBridge</code> | Model of the file to download |
| $linkRef | <code>HTMLElement</code> | Reference to the download link |

<a name="module_stores/common.SessionStore+applySessionDeletion"></a>

#### sessionStore.applySessionDeletion(sessionId)
Requests the deletion of a session file

**Kind**: instance method of [<code>SessionStore</code>](#module_stores/common.SessionStore)  

| Param | Type | Description |
| --- | --- | --- |
| sessionId | <code>string</code> | ID of the file |

<a name="module_stores/common.SessionStore+applySessionSavingAs"></a>

#### sessionStore.applySessionSavingAs(fileName)
Requests the save of a session file as

**Kind**: instance method of [<code>SessionStore</code>](#module_stores/common.SessionStore)  
**Todo**

- [ ] Send a file model and force the use of the id instead of the name (sessions can't have the same name)


| Param | Type | Description |
| --- | --- | --- |
| fileName | <code>string</code> | Name of the new file |

<a name="module_stores/common.SessionStore+applySessionUpload"></a>

#### sessionStore.applySessionUpload(fileBlob)
Uploads a session file and imports it on the server

**Kind**: instance method of [<code>SessionStore</code>](#module_stores/common.SessionStore)  

| Param | Type | Description |
| --- | --- | --- |
| fileBlob | <code>File</code> | A generated blob from the WebAPI |

<a name="module_stores/common.SessionStore+applySessionImport"></a>

#### sessionStore.applySessionImport(fileBridge, fileContent)
Imports a session file onto the server

**Kind**: instance method of [<code>SessionStore</code>](#module_stores/common.SessionStore)  

| Param | Type | Description |
| --- | --- | --- |
| fileBridge | [<code>FileBridge</code>](#models.FileBridge) | Model of the file |
| fileContent | <code>string</code> | Content of the file |

<a name="module_stores/common.SessionStore+addSession"></a>

#### sessionStore.addSession(session)
Adds a FileBridge model in the sessions Map

**Kind**: instance method of [<code>SessionStore</code>](#module_stores/common.SessionStore)  

| Param | Type | Description |
| --- | --- | --- |
| session | [<code>FileBridge</code>](#models.FileBridge) | Session file to add |

<a name="module_stores/common.SessionStore+removeSession"></a>

#### sessionStore.removeSession(sessionId)
Deletes a stored FileBridge

**Kind**: instance method of [<code>SessionStore</code>](#module_stores/common.SessionStore)  

| Param | Type | Description |
| --- | --- | --- |
| sessionId | <code>string</code> | ID of the session file bridge |

<a name="module_stores/common.SessionStore+setCurrentSession"></a>

#### sessionStore.setCurrentSession(sessionId)
Set the current file ID

**Kind**: instance method of [<code>SessionStore</code>](#module_stores/common.SessionStore)  

| Param | Type | Description |
| --- | --- | --- |
| sessionId | <code>string</code> | Session ID to set as current |

<a name="module_stores/common.SessionStore+clearCurrentSession"></a>

#### sessionStore.clearCurrentSession()
Clears the current session file ID

**Kind**: instance method of [<code>SessionStore</code>](#module_stores/common.SessionStore)  
<a name="module_stores/common.SessionStore+addSessionToTrash"></a>

#### sessionStore.addSessionToTrash(sessionId)
Adds a file to the trash

**Kind**: instance method of [<code>SessionStore</code>](#module_stores/common.SessionStore)  

| Param | Type | Description |
| --- | --- | --- |
| sessionId | <code>string</code> | ID of the session file |

<a name="module_stores/common.SessionStore+removeSessionFromTrash"></a>

#### sessionStore.removeSessionFromTrash(sessionId)
Deletes a file from the trash

**Kind**: instance method of [<code>SessionStore</code>](#module_stores/common.SessionStore)  

| Param | Type | Description |
| --- | --- | --- |
| sessionId | <code>string</code> | ID of the session file |

<a name="module_stores/common.SessionStore+clearSessionTrash"></a>

#### sessionStore.clearSessionTrash()
Clears the trash of sessions

**Kind**: instance method of [<code>SessionStore</code>](#module_stores/common.SessionStore)  
<a name="module_stores/common.Store+isInitialized"></a>

#### sessionStore.isInitialized() ⇒ <code>boolean</code>
Check if the store's current initialization state is INITIALIZED

**Kind**: instance method of [<code>SessionStore</code>](#module_stores/common.SessionStore)  
**Overrides**: [<code>isInitialized</code>](#module_stores/common.Store+isInitialized)  
**Returns**: <code>boolean</code> - Flags true if store is INITIALIZED  
<a name="module_stores/common.Store+isInitializing"></a>

#### sessionStore.isInitializing() ⇒ <code>boolean</code>
Check if the store's current initialization state is INITIALIZING

**Kind**: instance method of [<code>SessionStore</code>](#module_stores/common.SessionStore)  
**Overrides**: [<code>isInitializing</code>](#module_stores/common.Store+isInitializing)  
**Returns**: <code>boolean</code> - Flags true if store is INITIALIZING  
<a name="module_stores/common.Store+isNotInitialized"></a>

#### sessionStore.isNotInitialized() ⇒ <code>boolean</code>
Check if the store's current initialization state is NOT_INITIALIZED

**Kind**: instance method of [<code>SessionStore</code>](#module_stores/common.SessionStore)  
**Overrides**: [<code>isNotInitialized</code>](#module_stores/common.Store+isNotInitialized)  
**Returns**: <code>boolean</code> - Flags true if store is NOT_INITIALIZED  
<a name="module_stores/common.Store+applySuccessfulInitialization"></a>

#### sessionStore.applySuccessfulInitialization() ⇒ <code>boolean</code>
Applies the INITIALIZED statement to the store
Should be called when everything is allright!

**Kind**: instance method of [<code>SessionStore</code>](#module_stores/common.SessionStore)  
**Overrides**: [<code>applySuccessfulInitialization</code>](#module_stores/common.Store+applySuccessfulInitialization)  
**Returns**: <code>boolean</code> - Returns true if the store is well-initialized  
<a name="module_stores/common.Store+setInitState"></a>

#### sessionStore.setInitState(state) ⇒ <code>boolean</code>
Sets a new initialization state for the store

**Kind**: instance method of [<code>SessionStore</code>](#module_stores/common.SessionStore)  
**Overrides**: [<code>setInitState</code>](#module_stores/common.Store+setInitState)  
**Returns**: <code>boolean</code> - Flags true if the initialization state was updated  

| Param | Type | Description |
| --- | --- | --- |
| state | [<code>InitStateEnum</code>](#models.InitStateEnum) | New initialization state |

<a name="module_stores/common.Store+toJSON"></a>

#### sessionStore.toJSON() ⇒ <code>object</code>
Prints the store as a JSON object for debug purpose

**Kind**: instance method of [<code>SessionStore</code>](#module_stores/common.SessionStore)  
**Overrides**: [<code>toJSON</code>](#module_stores/common.Store+toJSON)  
**Returns**: <code>object</code> - The store state in a JSON object  
<a name="module_stores/common.Store"></a>

### stores/common.Store
Base store with shared utility functions

**Kind**: static class of [<code>stores/common</code>](#module_stores/common)  
<a name="new_module_stores/common.Store_new"></a>

#### new Store(socketStore)
Instantiates a new Store


| Param | Type | Description |
| --- | --- | --- |
| socketStore | [<code>SocketStore</code>](#stores.SocketStore) | Socket manager |

<a name="module_stores/common.Store+socketStore"></a>

#### store.socketStore
**Kind**: instance property of [<code>Store</code>](#module_stores/common.Store)  
**Properties**

| Type | Description |
| --- | --- |
| [<code>SocketStore</code>](#stores.SocketStore) | Socket manager |

<a name="module_stores/common.Store+initState"></a>

#### store.initState
**Kind**: instance property of [<code>Store</code>](#module_stores/common.Store)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| initState | <code>number</code> | Initialization state of the store |

<a name="module_stores/common.Store+hasActiveSocket"></a>

#### store.hasActiveSocket
**Kind**: instance property of [<code>Store</code>](#module_stores/common.Store)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| hasActiveSocket | <code>boolean</code> | Checks if the active socket exists |

<a name="module_stores/common.Store+isInitialized"></a>

#### store.isInitialized() ⇒ <code>boolean</code>
Check if the store's current initialization state is INITIALIZED

**Kind**: instance method of [<code>Store</code>](#module_stores/common.Store)  
**Returns**: <code>boolean</code> - Flags true if store is INITIALIZED  
<a name="module_stores/common.Store+isInitializing"></a>

#### store.isInitializing() ⇒ <code>boolean</code>
Check if the store's current initialization state is INITIALIZING

**Kind**: instance method of [<code>Store</code>](#module_stores/common.Store)  
**Returns**: <code>boolean</code> - Flags true if store is INITIALIZING  
<a name="module_stores/common.Store+isNotInitialized"></a>

#### store.isNotInitialized() ⇒ <code>boolean</code>
Check if the store's current initialization state is NOT_INITIALIZED

**Kind**: instance method of [<code>Store</code>](#module_stores/common.Store)  
**Returns**: <code>boolean</code> - Flags true if store is NOT_INITIALIZED  
<a name="module_stores/common.Store+applySuccessfulInitialization"></a>

#### store.applySuccessfulInitialization() ⇒ <code>boolean</code>
Applies the INITIALIZED statement to the store
Should be called when everything is allright!

**Kind**: instance method of [<code>Store</code>](#module_stores/common.Store)  
**Returns**: <code>boolean</code> - Returns true if the store is well-initialized  
<a name="module_stores/common.Store+setInitState"></a>

#### store.setInitState(state) ⇒ <code>boolean</code>
Sets a new initialization state for the store

**Kind**: instance method of [<code>Store</code>](#module_stores/common.Store)  
**Returns**: <code>boolean</code> - Flags true if the initialization state was updated  

| Param | Type | Description |
| --- | --- | --- |
| state | [<code>InitStateEnum</code>](#models.InitStateEnum) | New initialization state |

<a name="module_stores/common.Store+toJSON"></a>

#### store.toJSON() ⇒ <code>object</code>
Prints the store as a JSON object for debug purpose

**Kind**: instance method of [<code>Store</code>](#module_stores/common.Store)  
**Returns**: <code>object</code> - The store state in a JSON object  
<a name="module_stores/common.Store.LOG"></a>

#### Store.LOG : [<code>pino/logger</code>](#external_pino/logger)
Dedicated logger for the Store

**Kind**: static constant of [<code>Store</code>](#module_stores/common.Store)  
<a name="module_stores/quiddity"></a>

## stores/quiddity
Define all stores for UI behaviors about quiddities

<a name="module_stores/quiddity.EncoderStore"></a>

### stores/quiddity.EncoderStore ⇐ [<code>Store</code>](#module_stores/common.Store)
Stores all managed encoders

**Kind**: static class of [<code>stores/quiddity</code>](#module_stores/quiddity)  
**Extends**: [<code>Store</code>](#module_stores/common.Store)  
<a name="new_module_stores/quiddity.EncoderStore_new"></a>

#### new EncoderStore(socketStore, settingStore, CapsStore)
Instantiates a new EncoderStore


| Param | Type | Description |
| --- | --- | --- |
| socketStore | [<code>SocketStore</code>](#stores.SocketStore) | Stores and manages the current event-driven socket |
| settingStore | [<code>SettingStore</code>](#stores.SettingStore) | Stores app's settings |
| CapsStore | [<code>CapsStore</code>](#stores.CapsStore) | Stores quiddities' capabilities |

<a name="module_stores/common.Store+socketStore"></a>

#### encoderStore.socketStore
**Kind**: instance property of [<code>EncoderStore</code>](#module_stores/quiddity.EncoderStore)  
**Overrides**: [<code>socketStore</code>](#module_stores/common.Store+socketStore)  
**Properties**

| Type | Description |
| --- | --- |
| [<code>SocketStore</code>](#stores.SocketStore) | Socket manager |

<a name="module_stores/common.Store+initState"></a>

#### encoderStore.initState
**Kind**: instance property of [<code>EncoderStore</code>](#module_stores/quiddity.EncoderStore)  
**Overrides**: [<code>initState</code>](#module_stores/common.Store+initState)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| initState | <code>number</code> | Initialization state of the store |

<a name="module_stores/common.Store+hasActiveSocket"></a>

#### encoderStore.hasActiveSocket
**Kind**: instance property of [<code>EncoderStore</code>](#module_stores/quiddity.EncoderStore)  
**Overrides**: [<code>hasActiveSocket</code>](#module_stores/common.Store+hasActiveSocket)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| hasActiveSocket | <code>boolean</code> | Checks if the active socket exists |

<a name="module_stores/quiddity.EncoderStore+isEncoderDisplayed"></a>

#### encoderStore.isEncoderDisplayed(quiddity) ⇒ <code>boolean</code>
Checks if the encoder, if any, should be available.

**Kind**: instance method of [<code>EncoderStore</code>](#module_stores/quiddity.EncoderStore)  
**Returns**: <code>boolean</code> - - Flags true if the encoder should be displayed  

| Param | Type | Description |
| --- | --- | --- |
| quiddity | <code>module:models/quiddity.Quiddity</code> | The quiddity model |

<a name="module_stores/quiddity.EncoderStore+areEntriesConnectable"></a>

#### encoderStore.areEntriesConnectable(sourceEntry, destinationEntry) ⇒ <code>boolean</code>
Checks if a connection box should be disabled

**Kind**: instance method of [<code>EncoderStore</code>](#module_stores/quiddity.EncoderStore)  
**Returns**: <code>boolean</code> - Returns false if the caps are incompatible  

| Param | Type | Description |
| --- | --- | --- |
| sourceEntry | <code>module:models/matrix.MatrixEntry</code> | The row entry of the matrix |
| destinationEntry | <code>module:models/matrix.MatrixEntry</code> | The column entry of the matrix |

<a name="module_stores/common.Store+isInitialized"></a>

#### encoderStore.isInitialized() ⇒ <code>boolean</code>
Check if the store's current initialization state is INITIALIZED

**Kind**: instance method of [<code>EncoderStore</code>](#module_stores/quiddity.EncoderStore)  
**Overrides**: [<code>isInitialized</code>](#module_stores/common.Store+isInitialized)  
**Returns**: <code>boolean</code> - Flags true if store is INITIALIZED  
<a name="module_stores/common.Store+isInitializing"></a>

#### encoderStore.isInitializing() ⇒ <code>boolean</code>
Check if the store's current initialization state is INITIALIZING

**Kind**: instance method of [<code>EncoderStore</code>](#module_stores/quiddity.EncoderStore)  
**Overrides**: [<code>isInitializing</code>](#module_stores/common.Store+isInitializing)  
**Returns**: <code>boolean</code> - Flags true if store is INITIALIZING  
<a name="module_stores/common.Store+isNotInitialized"></a>

#### encoderStore.isNotInitialized() ⇒ <code>boolean</code>
Check if the store's current initialization state is NOT_INITIALIZED

**Kind**: instance method of [<code>EncoderStore</code>](#module_stores/quiddity.EncoderStore)  
**Overrides**: [<code>isNotInitialized</code>](#module_stores/common.Store+isNotInitialized)  
**Returns**: <code>boolean</code> - Flags true if store is NOT_INITIALIZED  
<a name="module_stores/common.Store+applySuccessfulInitialization"></a>

#### encoderStore.applySuccessfulInitialization() ⇒ <code>boolean</code>
Applies the INITIALIZED statement to the store
Should be called when everything is allright!

**Kind**: instance method of [<code>EncoderStore</code>](#module_stores/quiddity.EncoderStore)  
**Overrides**: [<code>applySuccessfulInitialization</code>](#module_stores/common.Store+applySuccessfulInitialization)  
**Returns**: <code>boolean</code> - Returns true if the store is well-initialized  
<a name="module_stores/common.Store+setInitState"></a>

#### encoderStore.setInitState(state) ⇒ <code>boolean</code>
Sets a new initialization state for the store

**Kind**: instance method of [<code>EncoderStore</code>](#module_stores/quiddity.EncoderStore)  
**Overrides**: [<code>setInitState</code>](#module_stores/common.Store+setInitState)  
**Returns**: <code>boolean</code> - Flags true if the initialization state was updated  

| Param | Type | Description |
| --- | --- | --- |
| state | [<code>InitStateEnum</code>](#models.InitStateEnum) | New initialization state |

<a name="module_stores/common.Store+toJSON"></a>

#### encoderStore.toJSON() ⇒ <code>object</code>
Prints the store as a JSON object for debug purpose

**Kind**: instance method of [<code>EncoderStore</code>](#module_stores/quiddity.EncoderStore)  
**Overrides**: [<code>toJSON</code>](#module_stores/common.Store+toJSON)  
**Returns**: <code>object</code> - The store state in a JSON object  
<a name="module_stores/quiddity.EncoderStore.LOG"></a>

#### EncoderStore.LOG : [<code>pino/logger</code>](#external_pino/logger)
Dedicated logger for the EncoderStore

**Kind**: static constant of [<code>EncoderStore</code>](#module_stores/quiddity.EncoderStore)  
<a name="module_stores/quiddity.NicknameStore"></a>

### stores/quiddity.NicknameStore ⇐ [<code>stores/common</code>](#module_stores/common)
Stores all nicknames from given quiddity IDs

**Kind**: static class of [<code>stores/quiddity</code>](#module_stores/quiddity)  
**Extends**: [<code>stores/common</code>](#module_stores/common)  
<a name="new_module_stores/quiddity.NicknameStore_new"></a>

#### new NicknameStore(socketStore, quiddityStore)
Instantiates a new NicknameStore


| Param | Type | Description |
| --- | --- | --- |
| socketStore | <code>module:stores/common.SocketStore</code> | Stores and manages the current event-driven socket |
| quiddityStore | [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore) | Quiddity manager |

<a name="module_stores/quiddity.NicknameStore+nicknames"></a>

#### nicknameStore.nicknames
**Kind**: instance property of [<code>NicknameStore</code>](#module_stores/quiddity.NicknameStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| nicknames | <code>Map.&lt;string, string&gt;</code> | All nicknames hashed by quiddity ID |

<a name="module_stores/quiddity.NicknameStore+fetchNickname"></a>

#### nicknameStore.fetchNickname(quiddityId) ⇒ <code>string</code>
Fetch the nickname for a given quiddity ID

**Kind**: instance method of [<code>NicknameStore</code>](#module_stores/quiddity.NicknameStore)  
**Returns**: <code>string</code> - The quiddity's nickname  

| Param | Type | Description |
| --- | --- | --- |
| quiddityId | <code>string</code> | ID of the quiddity |

<a name="module_stores/quiddity.NicknameStore+handleSocketChange"></a>

#### nicknameStore.handleSocketChange(socket)
Handles changes to the app's socket

**Kind**: instance method of [<code>NicknameStore</code>](#module_stores/quiddity.NicknameStore)  

| Param | Type | Description |
| --- | --- | --- |
| socket | <code>external:socketIO/Socket</code> | Event-driven socket |

<a name="module_stores/quiddity.NicknameStore+handleQuiddityChange"></a>

#### nicknameStore.handleQuiddityChange([quiddityIds])
Synchronizes all nicknames for all watched quiddities

**Kind**: instance method of [<code>NicknameStore</code>](#module_stores/quiddity.NicknameStore)  
**Todo**

- [ ] improve reaction because when created nicknames
      are updated but removed before on-created signal


| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [quiddityIds] | <code>Array.&lt;string&gt;</code> | <code>[]</code> | All IDs of the created quiddities |

<a name="module_stores/quiddity.NicknameStore+applyNicknameUpdate"></a>

#### nicknameStore.applyNicknameUpdate(quiddityId, nickname)
Applies a new nickname for a quiddity

**Kind**: instance method of [<code>NicknameStore</code>](#module_stores/quiddity.NicknameStore)  

| Param | Type | Description |
| --- | --- | --- |
| quiddityId | <code>string</code> | ID of the quiddity |
| nickname | <code>string</code> | New nickname for the quiddity |

<a name="module_stores/quiddity.NicknameStore+initializeNickname"></a>

#### nicknameStore.initializeNickname(quiddityId)
Initializes the nickname Map for a given quiddity ID

**Kind**: instance method of [<code>NicknameStore</code>](#module_stores/quiddity.NicknameStore)  

| Param | Type | Description |
| --- | --- | --- |
| quiddityId | <code>string</code> | ID of the quiddity we want to nickname |

<a name="module_stores/quiddity.NicknameStore+makeDestinationHeadTitle"></a>

#### nicknameStore.makeDestinationHeadTitle(entry, [defaultLabel]) ⇒ <code>string</code>
Makes the title of the destination head

**Kind**: instance method of [<code>NicknameStore</code>](#module_stores/quiddity.NicknameStore)  
**Returns**: <code>string</code> - Returns the contact title or the quiddity nickname  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| entry | [<code>MatrixEntry</code>](#models.MatrixEntry) |  | A matrix entry |
| [defaultLabel] | <code>string</code> | <code>&quot;&#x27;NO NICKNAME&quot;</code> | Fallback label if there is no label returned |

<a name="module_stores/quiddity.NicknameStore+addNickname"></a>

#### nicknameStore.addNickname(quiddityId, nickname)
Updates a nickname

**Kind**: instance method of [<code>NicknameStore</code>](#module_stores/quiddity.NicknameStore)  

| Param | Type | Description |
| --- | --- | --- |
| quiddityId | <code>string</code> | Quiddity ID |
| nickname | <code>string</code> | New nickname for the quiddity |

<a name="module_stores/quiddity.NicknameStore+removeNickname"></a>

#### nicknameStore.removeNickname(quiddityId)
Deletes a nickname for a quiddity

**Kind**: instance method of [<code>NicknameStore</code>](#module_stores/quiddity.NicknameStore)  

| Param | Type | Description |
| --- | --- | --- |
| quiddityId | <code>string</code> | Quiddity ID |

<a name="module_stores/quiddity.NicknameStore.DEFAULT_NICKNAME"></a>

#### NicknameStore.DEFAULT\_NICKNAME : <code>string</code>
Nickname used by default for new quiddities

**Kind**: static constant of [<code>NicknameStore</code>](#module_stores/quiddity.NicknameStore)  
<a name="module_stores/quiddity.QuiddityStore"></a>

### stores/quiddity.QuiddityStore ⇐ [<code>Store</code>](#module_stores/common.Store)
Stores all special quiddities

**Kind**: static class of [<code>stores/quiddity</code>](#module_stores/quiddity)  
**Extends**: [<code>Store</code>](#module_stores/common.Store)  
<a name="new_module_stores/quiddity.QuiddityStore_new"></a>

#### new QuiddityStore(socketStore, configStore, kindStore, SpecStore, SessionStore)
Instantiates a new QuiddityStore


| Param | Type | Description |
| --- | --- | --- |
| socketStore | <code>module:stores/common.SocketStore</code> | Manages the websockets |
| configStore | <code>module:stores/common.ConfigStore</code> | Configuration manager |
| kindStore | [<code>KindStore</code>](#module_stores/quiddity.KindStore) | Quiddity classes manager |
| SpecStore | [<code>SpecStore</code>](#module_stores/shmdata.SpecStore) | ConnectionSpec manager |
| SessionStore | [<code>SessionStore</code>](#module_stores/common.SessionStore) | Sessions manager |

<a name="module_stores/quiddity.QuiddityStore+creationBindings"></a>

#### quiddityStore.creationBindings
**Kind**: instance property of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| creationBindings | <code>Map.&lt;string, function()&gt;</code> | Functions used to create a specific quiddity, hashed by quiddity class |

<a name="module_stores/quiddity.QuiddityStore+quiddities"></a>

#### quiddityStore.quiddities
**Kind**: instance property of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| quiddities | <code>Map.&lt;string, module:models/quiddity.Quiddity&gt;</code> | All quiddities hashed by their IDs |

<a name="module_stores/quiddity.QuiddityStore+selectedQuiddityIds"></a>

#### quiddityStore.selectedQuiddityIds
**Kind**: instance property of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| selectedQuiddityIds | <code>Set.&lt;string&gt;</code> | All selected quiddity ids by the user |

<a name="module_stores/quiddity.QuiddityStore+quiddityIds"></a>

#### quiddityStore.quiddityIds
**Kind**: instance property of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| quiddityIds | <code>Array.&lt;string&gt;</code> | All quiddity IDs |

<a name="module_stores/quiddity.QuiddityStore+selectedQuiddity"></a>

#### quiddityStore.selectedQuiddity
**Kind**: instance property of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| selectedQuiddity | <code>string</code> | ID of the first selected quiddity by the user |

<a name="module_stores/quiddity.QuiddityStore+usedKinds"></a>

#### quiddityStore.usedKinds
**Kind**: instance property of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| usedKinds | <code>Map.&lt;string, module:models/quiddity.Kind&gt;</code> | All currently instantiated kind ordered by quiddity ID |

<a name="module_stores/quiddity.QuiddityStore+usedKindIds"></a>

#### quiddityStore.usedKindIds
**Kind**: instance property of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| usedKindIds | <code>Set.&lt;string&gt;</code> | All instantiated quiddity kind IDs |

<a name="module_stores/quiddity.QuiddityStore+userQuiddities"></a>

#### quiddityStore.userQuiddities
**Kind**: instance property of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| userQuiddities | <code>Array.&lt;module:models/quiddity.Quiddity&gt;</code> | All quiddities editable by the user |

<a name="module_stores/quiddity.QuiddityStore+quiddityByNames"></a>

#### quiddityStore.quiddityByNames
**Kind**: instance property of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| quiddityByNames | <code>Map.&lt;string, module:models/quiddity.Quiddity&gt;</code> | All quiddities hashed by their unique nickname |

<a name="module_stores/quiddity.QuiddityStore+usedIndexes"></a>

#### quiddityStore.usedIndexes
**Kind**: instance property of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| usedIndexes | <code>Map.&lt;string, number&gt;</code> | All latest quiddity indexes for each quiddity class |

<a name="module_stores/quiddity.QuiddityStore+userQuiddityIds"></a>

#### quiddityStore.userQuiddityIds
**Kind**: instance property of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| userQuiddityIds | <code>Array.&lt;string&gt;</code> | All IDs of the matrix's quiddities |

<a name="module_stores/quiddity.QuiddityStore+destinations"></a>

#### quiddityStore.destinations
**Kind**: instance property of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| destinations | <code>Array.&lt;module:models/quiddity.Quiddity&gt;</code> | All reader quiddities |

<a name="module_stores/quiddity.QuiddityStore+destinationIds"></a>

#### quiddityStore.destinationIds
**Kind**: instance property of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| destinationIds | <code>Array.&lt;string&gt;</code> | All destination IDs |

<a name="module_stores/quiddity.QuiddityStore+sources"></a>

#### quiddityStore.sources
**Kind**: instance property of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| sources | <code>Array.&lt;module:models/quiddity.Quiddity&gt;</code> | All writer quiddities |

<a name="module_stores/quiddity.QuiddityStore+sourceIds"></a>

#### quiddityStore.sourceIds
**Kind**: instance property of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| sourceIds | <code>Array.&lt;string&gt;</code> | All source IDs |

<a name="module_stores/quiddity.QuiddityStore+quiddityTags"></a>

#### quiddityStore.quiddityTags
**Kind**: instance property of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| quiddityTags | <code>Map.&lt;string, Array.&lt;string&gt;&gt;</code> | Hashes all quiddity ids with their corresponding tags |

<a name="module_stores/quiddity.QuiddityStore+quiddityLabels"></a>

#### quiddityStore.quiddityLabels
**Kind**: instance property of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| quiddityLabels | <code>Map.&lt;string, Array.&lt;string&gt;&gt;</code> | Hashes all quiddity ids with their corresponding labels |

<a name="module_stores/quiddity.QuiddityStore+categorizedQuiddityIds"></a>

#### quiddityStore.categorizedQuiddityIds
**Kind**: instance property of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| categorizedQuiddityIds | <code>Map.&lt;string, Array.&lt;string&gt;&gt;</code> | Hashes all quiddity categories with their corresponding quiddity Ids note : This is only ever used in the order store for destination quiddities. If this is ever populated with source quiddities, some things will break. This and other functions related to destination quiddities (like matrixCategory and the related function in the MatrixCategoryEnum file) should probably be renamed now that there is some categorisation for the sources and that the categorisation logic is different for both. * @todo Rename the `MatrixCategoryEnum` to `QuiddityCategoryEnum` |

<a name="module_stores/quiddity.QuiddityStore+configStore"></a>

#### quiddityStore.configStore
**Kind**: instance property of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| configStore | <code>module:stores/common.ConfigStore</code> | Manages the configuration |

<a name="module_stores/quiddity.QuiddityStore+kindStore"></a>

#### quiddityStore.kindStore
**Kind**: instance property of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| kindStore | [<code>KindStore</code>](#module_stores/quiddity.KindStore) | Stores kinds |

<a name="module_stores/quiddity.QuiddityStore+nicknameStore"></a>

#### quiddityStore.nicknameStore
**Kind**: instance property of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| nicknameStore | [<code>NicknameStore</code>](#module_stores/quiddity.NicknameStore) | Stores nicknames |

<a name="module_stores/quiddity.QuiddityStore+specStore"></a>

#### quiddityStore.specStore
**Kind**: instance property of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| specStore | [<code>SpecStore</code>](#module_stores/shmdata.SpecStore) | Stores connection specs |

<a name="module_stores/quiddity.QuiddityStore+handlePrunedInfoTree"></a>

#### quiddityStore.handlePrunedInfoTree ⇒
Updates a quiddity's infoTree to reflect the changes received from switcher.

**Kind**: instance property of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Returns**: the value of the pruned value  

| Param | Type | Description |
| --- | --- | --- |
| quidId | <code>number</code> | Id of the quiddity |
| path | <code>string</code> | The switcher path of the change in the format "keys.in.the.tree" with nested keys      separated by a . |
| value | <code>null</code> \| <code>string</code> \| <code>number</code> \| <code>Object</code> | The value of the removed part of the tree |

<a name="module_stores/quiddity.QuiddityStore+handleGraftedInfoTree"></a>

#### quiddityStore.handleGraftedInfoTree
Updates a quiddity's infoTree to reflect the changes received from switcher.

**Kind**: instance property of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  

| Param | Type | Description |
| --- | --- | --- |
| quidId | <code>number</code> | Id of the quiddity |
| path | <code>string</code> | The switcher path of the change in the format "keys.in.the.tree" with nested keys      separated by a . |
| value | <code>null</code> \| <code>string</code> \| <code>number</code> \| <code>Object</code> | The value of the part added to the tree |

<a name="module_stores/common.Store+socketStore"></a>

#### quiddityStore.socketStore
**Kind**: instance property of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Overrides**: [<code>socketStore</code>](#module_stores/common.Store+socketStore)  
**Properties**

| Type | Description |
| --- | --- |
| [<code>SocketStore</code>](#stores.SocketStore) | Socket manager |

<a name="module_stores/common.Store+initState"></a>

#### quiddityStore.initState
**Kind**: instance property of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Overrides**: [<code>initState</code>](#module_stores/common.Store+initState)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| initState | <code>number</code> | Initialization state of the store |

<a name="module_stores/common.Store+hasActiveSocket"></a>

#### quiddityStore.hasActiveSocket
**Kind**: instance property of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Overrides**: [<code>hasActiveSocket</code>](#module_stores/common.Store+hasActiveSocket)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| hasActiveSocket | <code>boolean</code> | Checks if the active socket exists |

<a name="module_stores/quiddity.QuiddityStore+initialize"></a>

#### quiddityStore.initialize() ⇒ <code>boolean</code>
Initializes all stored quiddities by fetching Switcher

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Returns**: <code>boolean</code> - Flags true if it is well initialized  
**Throws**:

- <code>Error</code> Throw the initialization failure

<a name="module_stores/quiddity.QuiddityStore+initializeCurrentQuiddities"></a>

#### quiddityStore.initializeCurrentQuiddities()
Initializes all pre-existing quiddities in the server

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
<a name="module_stores/quiddity.QuiddityStore+initializeInitQuiddities"></a>

#### quiddityStore.initializeInitQuiddities()
Initializes all initial quiddities specified in the user-defined configuration

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
<a name="module_stores/quiddity.QuiddityStore+applyCreationBindings"></a>

#### quiddityStore.applyCreationBindings(quiddity) ⇒ <code>boolean</code>
Applies the creation of a bound quiddity

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Returns**: <code>boolean</code> - Returns true if the binding is applied  

| Param | Type | Description |
| --- | --- | --- |
| quiddity | <code>module:models/quiddity.Quiddity</code> | A bound quiddity |

<a name="module_stores/quiddity.QuiddityStore+fetchQuiddities"></a>

#### quiddityStore.fetchQuiddities() ⇒ <code>Array.&lt;object&gt;</code>
Fetches all created quiddities

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Returns**: <code>Array.&lt;object&gt;</code> - Returns all created quiddities on the server  
<a name="module_stores/quiddity.QuiddityStore+fetchAllQuiddityIds"></a>

#### quiddityStore.fetchAllQuiddityIds() ⇒ <code>Array.&lt;string&gt;</code>
Fetches all created quiddities by their IDs

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Returns**: <code>Array.&lt;string&gt;</code> - Array of all created quiddity IDs  
<a name="module_stores/quiddity.QuiddityStore+fetchQuiddity"></a>

#### quiddityStore.fetchQuiddity(quiddityId) ⇒ <code>module:models/quiddity.Quiddity</code>
Fetches a specific quiddity

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Returns**: <code>module:models/quiddity.Quiddity</code> - Returns the fetched quiddity or null if the quiddity was not created  

| Param | Type | Description |
| --- | --- | --- |
| quiddityId | <code>string</code> | ID of the quiddity to fetch |

<a name="module_stores/quiddity.QuiddityStore+fetchQuiddityTrees"></a>

#### quiddityStore.fetchQuiddityTrees(quidId) ⇒ <code>object</code>
Fetches the InfoTree, UserTree and the ConnectionSpecs of the quiddity

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Returns**: <code>object</code> - InfoTree of the quiddity  

| Param | Type | Description |
| --- | --- | --- |
| quidId | <code>string</code> | ID of the quiddity |

<a name="module_stores/quiddity.QuiddityStore+handleSocketChange"></a>

#### quiddityStore.handleSocketChange(socket)
Handles changes to the app's socket

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Todo**

- [ ] resolve session api


| Param | Type | Description |
| --- | --- | --- |
| socket | <code>external:socketIO/Socket</code> | Event-driven socket |

<a name="module_stores/quiddity.QuiddityStore+handleSessionChange"></a>

#### quiddityStore.handleSessionChange([sessionName])
Handles new session file loading

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [sessionName] | <code>string</code> | <code>&quot;&#x27;&#x27;&quot;</code> | Loaded file's name |

<a name="module_stores/quiddity.QuiddityStore+handleQuiddityCreation"></a>

#### quiddityStore.handleQuiddityCreation(json, [shouldAddQuiddity]) ⇒ <code>module:models/quiddity.Quiddity</code>
Handles a `quiddity.created` Switcher signal

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Returns**: <code>module:models/quiddity.Quiddity</code> - The quiddity model created from the json  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| json | <code>Object</code> |  | Quiddity representation in JSON |
| [shouldAddQuiddity] | <code>boolean</code> | <code>true</code> | Quiddity representation in JSON |

<a name="module_stores/quiddity.QuiddityStore+fallbackQuiddityModel"></a>

#### quiddityStore.fallbackQuiddityModel(json) ⇒ <code>object</code>
Fallbacks an incomplete quiddity modelby fetching missing attributes

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Returns**: <code>object</code> - a completed JSON model  

| Param | Type | Description |
| --- | --- | --- |
| json | <code>object</code> | the incomplete JSON model |

<a name="module_stores/quiddity.QuiddityStore+handleQuiddityRemoval"></a>

#### quiddityStore.handleQuiddityRemoval(quiddityId)
Handles a `quiddity.removed` Switcher signal

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  

| Param | Type | Description |
| --- | --- | --- |
| quiddityId | <code>string</code> | ID of the removed quiddity |

<a name="module_stores/quiddity.QuiddityStore+makeQuiddityKindIndex"></a>

#### quiddityStore.makeQuiddityKindIndex(kindId) ⇒ <code>number</code>
Creates the index of a quiddity by checking all indexes of quiddities with the same kind

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Returns**: <code>number</code> - The quiddity's index  

| Param | Type | Description |
| --- | --- | --- |
| kindId | <code>string</code> | ID of the quiddity's kind to index |

<a name="module_stores/quiddity.QuiddityStore+applyQuiddityCreation"></a>

#### quiddityStore.applyQuiddityCreation(kindId, [quidName], [nickname], [properties], [userTree]) ⇒ <code>Object</code>
Requests the creation of a quiddity

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Returns**: <code>Object</code> - The model of the created quiddity  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| kindId | <code>string</code> |  | Kind ID of the quiddity |
| [quidName] | <code>string</code> | <code>null</code> | Name of the quiddity (default will be a generated ID) |
| [nickname] | <code>string</code> | <code>&quot;&#x27;&#x27;&quot;</code> | Nickname of the quiddity (default will be quiddityId, or a generated ID) |
| [properties] | <code>Object</code> | <code>{}</code> | Initial properties of the new quiddity |
| [userTree] | <code>Object</code> | <code>{}</code> | Initial user data of the new quiddity |

<a name="module_stores/quiddity.QuiddityStore+applyQuiddityIndex"></a>

#### quiddityStore.applyQuiddityIndex(quiddityId, index)
Requests a change to the quiddity's index

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  

| Param | Type | Description |
| --- | --- | --- |
| quiddityId | <code>string</code> | ID of the quiddity |
| index | <code>number</code> | Index of the quiddity |

<a name="module_stores/quiddity.QuiddityStore+applyQuiddityRemoval"></a>

#### quiddityStore.applyQuiddityRemoval(quiddityId)
Requests the removal of a quiddity

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  

| Param | Type | Description |
| --- | --- | --- |
| quiddityId | <code>string</code> | ID of the quiddity |

<a name="module_stores/quiddity.QuiddityStore+applyQuiddityConnection"></a>

#### quiddityStore.applyQuiddityConnection(srcId, destId) ⇒ <code>Promise.&lt;number&gt;</code>
Requests a connection between two quiddities

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Returns**: <code>Promise.&lt;number&gt;</code> - ID of the created connection if succeed  

| Param | Type | Description |
| --- | --- | --- |
| srcId | <code>number</code> | ID of the source quiddity |
| destId | <code>number</code> | ID of the destination quiddity |

<a name="module_stores/quiddity.QuiddityStore+applyQuiddityDisconnection"></a>

#### quiddityStore.applyQuiddityDisconnection(dstId, sfId) ⇒ <code>Promise.&lt;boolean&gt;</code>
Requests a disconnection between a destination and a specific connection

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Returns**: <code>Promise.&lt;boolean&gt;</code> - True if the disconnection succeeded  

| Param | Type | Description |
| --- | --- | --- |
| dstId | <code>number</code> | ID of the destination |
| sfId | <code>number</code> | ID of the connection |

<a name="module_stores/quiddity.QuiddityStore+getQuiddityCategory"></a>

#### quiddityStore.getQuiddityCategory(quiddityId) ⇒ <code>string</code>
Provides the category of a quiddity

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Returns**: <code>string</code> - The quiddity's category  

| Param | Type | Description |
| --- | --- | --- |
| quiddityId | <code>string</code> | The id of the quiddity |

<a name="module_stores/quiddity.QuiddityStore+setCreationBindings"></a>

#### quiddityStore.setCreationBindings(bindings)
Sets the quiddity creation bindings Map

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  

| Param | Type | Description |
| --- | --- | --- |
| bindings | <code>Map.&lt;string, function()&gt;</code> | Map containing quiddity classes as keys and functions as values |

<a name="module_stores/quiddity.QuiddityStore+addCreationBinding"></a>

#### quiddityStore.addCreationBinding(kindId, boundFunction)
Adds a new quiddity creation binding in the creationBindings Map

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  

| Param | Type | Description |
| --- | --- | --- |
| kindId | <code>string</code> | Quiddity kind ID to bind a function to |
| boundFunction | <code>function</code> | Function to call when creating associated quiddity class |

<a name="module_stores/quiddity.QuiddityStore+removeCreationBinding"></a>

#### quiddityStore.removeCreationBinding(kindId)
Removes a new quiddity creation binding from the creationBindings Map

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  

| Param | Type | Description |
| --- | --- | --- |
| kindId | <code>string</code> | The bound quiddity kind |

<a name="module_stores/quiddity.QuiddityStore+addQuiddity"></a>

#### quiddityStore.addQuiddity(quiddity)
Adds a new quiddity to the store from a JSON model

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  

| Param | Type | Description |
| --- | --- | --- |
| quiddity | <code>module:models/quiddity.Quiddity</code> | JSON-based model of a quiddity |

<a name="module_stores/quiddity.QuiddityStore+addQuiddities"></a>

#### quiddityStore.addQuiddities(quiddities)
Adds a lot of quiddities at once. Does this by first getting all the existing entries
     from the quiddities. map and then creating a new map with the existing entries and the
     new ones and assigning it to this.quiddities

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  

| Param | Type | Description |
| --- | --- | --- |
| quiddities | <code>Array.&lt;module:models/quiddity.Quiddity&gt;</code> | The quiddity models to add |

<a name="module_stores/quiddity.QuiddityStore+removeQuiddity"></a>

#### quiddityStore.removeQuiddity(quiddity)
Deletes a quiddity from the store

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  

| Param | Type | Description |
| --- | --- | --- |
| quiddity | <code>module:models/quiddity.Quiddity</code> | The quiddity model to remove |

<a name="module_stores/quiddity.QuiddityStore+addSelectedQuiddity"></a>

#### quiddityStore.addSelectedQuiddity(quiddityId)
Adds a new quiddity in the selection batch

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  

| Param | Type | Description |
| --- | --- | --- |
| quiddityId | <code>string</code> | ID of the selected quiddity |

<a name="module_stores/quiddity.QuiddityStore+cleanSelectedQuiddities"></a>

#### quiddityStore.cleanSelectedQuiddities()
Clears the selected quiddities batch

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
<a name="module_stores/quiddity.QuiddityStore+clear"></a>

#### quiddityStore.clear()
Cleans up quiddities

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
<a name="module_stores/common.Store+isInitialized"></a>

#### quiddityStore.isInitialized() ⇒ <code>boolean</code>
Check if the store's current initialization state is INITIALIZED

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Overrides**: [<code>isInitialized</code>](#module_stores/common.Store+isInitialized)  
**Returns**: <code>boolean</code> - Flags true if store is INITIALIZED  
<a name="module_stores/common.Store+isInitializing"></a>

#### quiddityStore.isInitializing() ⇒ <code>boolean</code>
Check if the store's current initialization state is INITIALIZING

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Overrides**: [<code>isInitializing</code>](#module_stores/common.Store+isInitializing)  
**Returns**: <code>boolean</code> - Flags true if store is INITIALIZING  
<a name="module_stores/common.Store+isNotInitialized"></a>

#### quiddityStore.isNotInitialized() ⇒ <code>boolean</code>
Check if the store's current initialization state is NOT_INITIALIZED

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Overrides**: [<code>isNotInitialized</code>](#module_stores/common.Store+isNotInitialized)  
**Returns**: <code>boolean</code> - Flags true if store is NOT_INITIALIZED  
<a name="module_stores/common.Store+applySuccessfulInitialization"></a>

#### quiddityStore.applySuccessfulInitialization() ⇒ <code>boolean</code>
Applies the INITIALIZED statement to the store
Should be called when everything is allright!

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Overrides**: [<code>applySuccessfulInitialization</code>](#module_stores/common.Store+applySuccessfulInitialization)  
**Returns**: <code>boolean</code> - Returns true if the store is well-initialized  
<a name="module_stores/common.Store+setInitState"></a>

#### quiddityStore.setInitState(state) ⇒ <code>boolean</code>
Sets a new initialization state for the store

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Overrides**: [<code>setInitState</code>](#module_stores/common.Store+setInitState)  
**Returns**: <code>boolean</code> - Flags true if the initialization state was updated  

| Param | Type | Description |
| --- | --- | --- |
| state | [<code>InitStateEnum</code>](#models.InitStateEnum) | New initialization state |

<a name="module_stores/common.Store+toJSON"></a>

#### quiddityStore.toJSON() ⇒ <code>object</code>
Prints the store as a JSON object for debug purpose

**Kind**: instance method of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
**Overrides**: [<code>toJSON</code>](#module_stores/common.Store+toJSON)  
**Returns**: <code>object</code> - The store state in a JSON object  
<a name="module_stores/quiddity.QuiddityStore.LOG"></a>

#### QuiddityStore.LOG : [<code>pino/logger</code>](#external_pino/logger)
Dedicated logger for the QuiddityStore

**Kind**: static constant of [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore)  
<a name="module_stores/quiddity.KindStore"></a>

### stores/quiddity.KindStore ⇐ [<code>KindStore</code>](#module_stores/quiddity.KindStore)
Stores all classes

**Kind**: static class of [<code>stores/quiddity</code>](#module_stores/quiddity)  
**Extends**: [<code>KindStore</code>](#module_stores/quiddity.KindStore)  
<a name="module_stores/quiddity.KindStore+kinds"></a>

#### kindStore.kinds
**Kind**: instance property of [<code>KindStore</code>](#module_stores/quiddity.KindStore)  
**Overrides**: [<code>kinds</code>](#module_stores/quiddity.KindStore+kinds)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| kinds | <code>Map.&lt;string, module:models/quiddity&gt;</code> | All kinds hashed by their names |

<a name="module_stores/quiddity.KindStore+kindIds"></a>

#### kindStore.kindIds
**Kind**: instance property of [<code>KindStore</code>](#module_stores/quiddity.KindStore)  
**Overrides**: [<code>kindIds</code>](#module_stores/quiddity.KindStore+kindIds)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| kindIds | <code>Array.&lt;string&gt;</code> | All kind IDs |

<a name="module_stores/quiddity.KindStore+initialize"></a>

#### kindStore.initialize([reloadKinds]) ⇒ <code>boolean</code>
Initializes all available kinds in Switcher

**Kind**: instance method of [<code>KindStore</code>](#module_stores/quiddity.KindStore)  
**Overrides**: [<code>initialize</code>](#module_stores/quiddity.KindStore+initialize)  
**Returns**: <code>boolean</code> - Return false if the initialization failed  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [reloadKinds] | <code>boolean</code> | <code>false</code> | Force the reloading of all kinds |

<a name="module_stores/quiddity.KindStore+initializeKinds"></a>

#### kindStore.initializeKinds()
Initializes all quiddity kinds

**Kind**: instance method of [<code>KindStore</code>](#module_stores/quiddity.KindStore)  
**Overrides**: [<code>initializeKinds</code>](#module_stores/quiddity.KindStore+initializeKinds)  
<a name="module_stores/quiddity.KindStore+fetchKinds"></a>

#### kindStore.fetchKinds() ⇒ <code>Promise.&lt;Array.&lt;Object&gt;&gt;</code>
Fetches all quiddity kinds

**Kind**: instance method of [<code>KindStore</code>](#module_stores/quiddity.KindStore)  
**Overrides**: [<code>fetchKinds</code>](#module_stores/quiddity.KindStore+fetchKinds)  
**Returns**: <code>Promise.&lt;Array.&lt;Object&gt;&gt;</code> - Array of all JSON kinds  
<a name="module_stores/quiddity.KindStore+handleSocketChange"></a>

#### kindStore.handleSocketChange(socket)
Handles changes to the app's socket

**Kind**: instance method of [<code>KindStore</code>](#module_stores/quiddity.KindStore)  
**Overrides**: [<code>handleSocketChange</code>](#module_stores/quiddity.KindStore+handleSocketChange)  

| Param | Type | Description |
| --- | --- | --- |
| socket | <code>external:socketIO/Socket</code> | Event-driven socket |

<a name="module_stores/quiddity.KindStore+makeKindModel"></a>

#### kindStore.makeKindModel(json) ⇒ <code>module:models/quiddity.QuiddityKind</code>
Safely makes a kind model from a JSON object

**Kind**: instance method of [<code>KindStore</code>](#module_stores/quiddity.KindStore)  
**Overrides**: [<code>makeKindModel</code>](#module_stores/quiddity.KindStore+makeKindModel)  
**Returns**: <code>module:models/quiddity.QuiddityKind</code> - A kind model  

| Param | Type | Description |
| --- | --- | --- |
| json | <code>Object</code> | JSON representation of the kind |

<a name="module_stores/quiddity.KindStore+isFollower"></a>

#### kindStore.isFollower(kindId) ⇒ <code>boolean</code>
Checks if a kind represents a following quiddity

**Kind**: instance method of [<code>KindStore</code>](#module_stores/quiddity.KindStore)  
**Overrides**: [<code>isFollower</code>](#module_stores/quiddity.KindStore+isFollower)  
**Returns**: <code>boolean</code> - Returns true if the kind represents a following quiddiy  

| Param | Type | Description |
| --- | --- | --- |
| kindId | <code>string</code> | Name of the kind |

<a name="module_stores/quiddity.KindStore+isWriter"></a>

#### kindStore.isWriter(kindId) ⇒ <code>boolean</code>
Checks if a kind represents a writing quiddity

**Kind**: instance method of [<code>KindStore</code>](#module_stores/quiddity.KindStore)  
**Overrides**: [<code>isWriter</code>](#module_stores/quiddity.KindStore+isWriter)  
**Returns**: <code>boolean</code> - Returns true if the kind represents a writing quiddity  

| Param | Type | Description |
| --- | --- | --- |
| kindId | <code>string</code> | Name of the kind |

<a name="module_stores/quiddity.KindStore+addKind"></a>

#### kindStore.addKind(kind)
Add a new quiddity kind in the kinds Map

**Kind**: instance method of [<code>KindStore</code>](#module_stores/quiddity.KindStore)  
**Overrides**: [<code>addKind</code>](#module_stores/quiddity.KindStore+addKind)  

| Param | Type | Description |
| --- | --- | --- |
| kind | <code>module:models/quiddity.Kind</code> | The kind to add |

<a name="module_stores/quiddity.KindStore+clear"></a>

#### kindStore.clear()
Cleans up quiddities

**Kind**: instance method of [<code>KindStore</code>](#module_stores/quiddity.KindStore)  
**Overrides**: [<code>clear</code>](#module_stores/quiddity.KindStore+clear)  
<a name="module_stores/quiddity.KindStore.LOG"></a>

#### KindStore.LOG : [<code>pino/logger</code>](#external_pino/logger)
Dedicated logger for the KindStore

**Kind**: static constant of [<code>KindStore</code>](#module_stores/quiddity.KindStore)  
<a name="module_stores/quiddity.NdiStore"></a>

### stores/quiddity.NdiStore ⇐ [<code>Store</code>](#module_stores/common.Store)
Stores all current NDI streams on the network

**Kind**: static class of [<code>stores/quiddity</code>](#module_stores/quiddity)  
**Extends**: [<code>Store</code>](#module_stores/common.Store)  
<a name="new_module_stores/quiddity.NdiStore_new"></a>

#### new NdiStore(socketStore, configStore, quiddityStore, propertyStore, quiddityMenuStore, maxLimitStore, lockStore, modalStore)
Instantiates a new NdiStore


| Param | Type | Description |
| --- | --- | --- |
| socketStore | <code>module:stores/common.SocketStore</code> | Stores and manages the current event-driven socket |
| configStore | <code>module:stores/common.ConfigStore</code> | Configuration manager |
| quiddityStore | [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore) | Quiddity manager |
| propertyStore | <code>module:stores/quiddity.PropertyStore</code> | Property manager |
| quiddityMenuStore | <code>module:stores/quiddity.QuiddityMenuStore</code> | Quiddity menus manager |
| maxLimitStore | [<code>MaxLimitStore</code>](#module_stores/shmdata.MaxLimitStore) | MaxLimit manager |
| lockStore | <code>module:stores/matrix.LockStore</code> | Lock manager |
| modalStore | <code>module:stores/common.ModalStore</code> | Modal manager |

<a name="module_stores/quiddity.NdiStore+configStore"></a>

#### ndiStore.configStore
**Kind**: instance property of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| configStore | <code>module:stores/common.ConfigStore</code> | Stores and manages the app's configuration |

<a name="module_stores/quiddity.NdiStore+quiddityStore"></a>

#### ndiStore.quiddityStore
**Kind**: instance property of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| quiddityStore | [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore) | Stores and manages all quiddities |

<a name="module_stores/quiddity.NdiStore+propertyStore"></a>

#### ndiStore.propertyStore
**Kind**: instance property of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| propertyStore | <code>module:stores/quiddity.PropertyStore</code> | Stores and manages all quiddities |

<a name="module_stores/quiddity.NdiStore+maxLimitStore"></a>

#### ndiStore.maxLimitStore
**Kind**: instance property of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| maxLimitStore | [<code>MaxLimitStore</code>](#module_stores/shmdata.MaxLimitStore) | Stores and manages all quiddities connection limits |

<a name="module_stores/quiddity.NdiStore+modalStore"></a>

#### ndiStore.modalStore
**Kind**: instance property of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| modalStore | <code>module:stores/common.ModalStore</code> | Stores and manages all the app's modals |

<a name="module_stores/quiddity.NdiStore+isNetworkScanned"></a>

#### ndiStore.isNetworkScanned
**Kind**: instance property of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| isNetworkScanned | <code>boolean</code> | Flag to notify that the local network has been scanned at least once for NDI streams |

<a name="module_stores/quiddity.NdiStore+isNdiInputRequested"></a>

#### ndiStore.isNdiInputRequested
**Kind**: instance property of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| isNdiInputRequested | <code>boolean</code> | Flag to notify a user request |

<a name="module_stores/quiddity.NdiStore+ndiStreams"></a>

#### ndiStore.ndiStreams
**Kind**: instance property of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| streams | <code>Array.&lt;models.NdiStream&gt;</code> | Observable NDI streams on the local network |

<a name="module_stores/quiddity.NdiStore+selectedNdiStream"></a>

#### ndiStore.selectedNdiStream
**Kind**: instance property of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| selectedNdiStream | <code>Object</code> | Selected NDI Stream option |

<a name="module_stores/quiddity.NdiStore+ndiStreamOptions"></a>

#### ndiStore.ndiStreamOptions
**Kind**: instance property of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| ndiStreamOptions | <code>Array.&lt;Object&gt;</code> | Available NDI streams as options for Select components |

<a name="module_stores/quiddity.NdiStore+ndiStreamLabels"></a>

#### ndiStore.ndiStreamLabels
**Kind**: instance property of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| ndiStreamLabels | <code>Array.&lt;string&gt;</code> | Available NDI streams' labels |

<a name="module_stores/quiddity.NdiStore+hasSelectedStreams"></a>

#### ndiStore.hasSelectedStreams
**Kind**: instance property of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| hasSelectedStreams | <code>boolean</code> | Flag to notify if a selected stream can be found in the selected ndi stream array |

<a name="module_stores/quiddity.NdiStore+hasSelectionError"></a>

#### ndiStore.hasSelectionError
**Kind**: instance property of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| hasSelectionError | <code>boolean</code> | Flag to notify if a selected stream's label cannot be found |

<a name="module_stores/quiddity.NdiStore+ndiSnifferId"></a>

#### ndiStore.ndiSnifferId
**Kind**: instance property of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| ndiSnifferQuiddityId | <code>string</code> | The ID of the ndi sniffer quiddity |

<a name="module_stores/common.Store+socketStore"></a>

#### ndiStore.socketStore
**Kind**: instance property of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  
**Overrides**: [<code>socketStore</code>](#module_stores/common.Store+socketStore)  
**Properties**

| Type | Description |
| --- | --- |
| [<code>SocketStore</code>](#stores.SocketStore) | Socket manager |

<a name="module_stores/common.Store+initState"></a>

#### ndiStore.initState
**Kind**: instance property of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  
**Overrides**: [<code>initState</code>](#module_stores/common.Store+initState)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| initState | <code>number</code> | Initialization state of the store |

<a name="module_stores/common.Store+hasActiveSocket"></a>

#### ndiStore.hasActiveSocket
**Kind**: instance property of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  
**Overrides**: [<code>hasActiveSocket</code>](#module_stores/common.Store+hasActiveSocket)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| hasActiveSocket | <code>boolean</code> | Checks if the active socket exists |

<a name="module_stores/quiddity.NdiStore+initialize"></a>

#### ndiStore.initialize() ⇒ <code>boolean</code>
Initializes the store by checking the QuiddityStore state

**Kind**: instance method of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  
**Returns**: <code>boolean</code> - Flags true if store is well initialized  
**Throws**:

- <code>Error</code> Will throw an error if the quiddity store is not initialized

<a name="module_stores/quiddity.NdiStore+findNdiStream"></a>

#### ndiStore.findNdiStream(label) ⇒ [<code>NdiStream</code>](#module_models/quiddity.NdiStream)
Matches a selected label with the available NDI streams

**Kind**: instance method of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  
**Returns**: [<code>NdiStream</code>](#module_models/quiddity.NdiStream) - The NdiStream with the parametrized label  

| Param | Type | Description |
| --- | --- | --- |
| label | <code>string</code> | Label of the selected NDI stream from UI |

<a name="module_stores/quiddity.NdiStore+handleSocketChange"></a>

#### ndiStore.handleSocketChange(socket)
Handles changes to the app's socket

**Kind**: instance method of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  

| Param | Type | Description |
| --- | --- | --- |
| socket | <code>external:socketIO/Socket</code> | Event-driven socket |

<a name="module_stores/quiddity.NdiStore+handleStartedPropertyChange"></a>

#### ndiStore.handleStartedPropertyChange()
Handle each changes to startable properties

**Kind**: instance method of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  
<a name="module_stores/quiddity.NdiStore+handleQuiddityStoreInitialization"></a>

#### ndiStore.handleQuiddityStoreInitialization(initState)
Handles quiddityStore's initialization state changes

**Kind**: instance method of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  

| Param | Type | Description |
| --- | --- | --- |
| initState | <code>module:models/common.InitStateEnum</code> | State of the quiddityStore's initialization |

<a name="module_stores/quiddity.NdiStore+handleNdiInputRequest"></a>

#### ndiStore.handleNdiInputRequest(state)
Handles changes to the isNdiInputRequested flag

**Kind**: instance method of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  

| Param | Type | Description |
| --- | --- | --- |
| state | <code>boolean</code> | State of the isNdiInputRequested flag |

<a name="module_stores/quiddity.NdiStore+populateNdiStreams"></a>

#### ndiStore.populateNdiStreams()
Populates all available NDI streams

**Kind**: instance method of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  
<a name="module_stores/quiddity.NdiStore+updateNdiStreamSelection"></a>

#### ndiStore.updateNdiStreamSelection(label)
Updates the selection when NDI streams are updated

**Kind**: instance method of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  

| Param | Type | Description |
| --- | --- | --- |
| label | <code>string</code> | Label of the selected NdiStream |

<a name="module_stores/quiddity.NdiStore+handleSnifferOutputUpdate"></a>

#### ndiStore.handleSnifferOutputUpdate(output)
Updates all available streams from NDI Sniffer's output

**Kind**: instance method of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  
**See**: [ndi2shmdata](https://gitlab.com/sat-mtl/tools/ndi2shmdata)  

| Param | Type | Description |
| --- | --- | --- |
| output | <code>Object</code> | NDI Sniffer's stdout which lists all available NDI streams |

<a name="module_stores/quiddity.NdiStore+applyNdiInputQuiddityCreation"></a>

#### ndiStore.applyNdiInputQuiddityCreation([ndiLabel])
Converts the selected NDI stream to an NDI input quiddity

**Kind**: instance method of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [ndiLabel] | <code>string</code> | <code>null</code> | Label of the NDI stream to use as source |

<a name="module_stores/quiddity.NdiStore+applyNdiOutputQuiddityCreation"></a>

#### ndiStore.applyNdiOutputQuiddityCreation()
Creates an NDI output quiddity

**Kind**: instance method of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  
<a name="module_stores/quiddity.NdiStore+setNdiStreamSelection"></a>

#### ndiStore.setNdiStreamSelection([ndiOption])
Sets (or resets) a new selected option

**Kind**: instance method of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [ndiOption] | <code>Object</code> | <code></code> | The NdiStream selection |

<a name="module_stores/quiddity.NdiStore+setNdiInputRequestFlag"></a>

#### ndiStore.setNdiInputRequestFlag(isNdiInputRequested)
Flags the stream request

**Kind**: instance method of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  

| Param | Type | Description |
| --- | --- | --- |
| isNdiInputRequested | <code>boolean</code> | Flag the stream request |

<a name="module_stores/quiddity.NdiStore+setIsNetworkScanned"></a>

#### ndiStore.setIsNetworkScanned(value)
Flags the network scan status

**Kind**: instance method of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  

| Param | Type | Description |
| --- | --- | --- |
| value | <code>boolean</code> | Flag the network scan |

<a name="module_stores/quiddity.NdiStore+setNdiStreams"></a>

#### ndiStore.setNdiStreams(ndiStreams)
Sets new NDI Streams

**Kind**: instance method of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  

| Param | Type |
| --- | --- |
| ndiStreams | <code>Array.&lt;module:stores/quiddity.NdiStream&gt;</code> | 

<a name="module_stores/quiddity.NdiStore+clear"></a>

#### ndiStore.clear()
Clears all NDI streams

**Kind**: instance method of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  
<a name="module_stores/common.Store+isInitialized"></a>

#### ndiStore.isInitialized() ⇒ <code>boolean</code>
Check if the store's current initialization state is INITIALIZED

**Kind**: instance method of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  
**Overrides**: [<code>isInitialized</code>](#module_stores/common.Store+isInitialized)  
**Returns**: <code>boolean</code> - Flags true if store is INITIALIZED  
<a name="module_stores/common.Store+isInitializing"></a>

#### ndiStore.isInitializing() ⇒ <code>boolean</code>
Check if the store's current initialization state is INITIALIZING

**Kind**: instance method of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  
**Overrides**: [<code>isInitializing</code>](#module_stores/common.Store+isInitializing)  
**Returns**: <code>boolean</code> - Flags true if store is INITIALIZING  
<a name="module_stores/common.Store+isNotInitialized"></a>

#### ndiStore.isNotInitialized() ⇒ <code>boolean</code>
Check if the store's current initialization state is NOT_INITIALIZED

**Kind**: instance method of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  
**Overrides**: [<code>isNotInitialized</code>](#module_stores/common.Store+isNotInitialized)  
**Returns**: <code>boolean</code> - Flags true if store is NOT_INITIALIZED  
<a name="module_stores/common.Store+applySuccessfulInitialization"></a>

#### ndiStore.applySuccessfulInitialization() ⇒ <code>boolean</code>
Applies the INITIALIZED statement to the store
Should be called when everything is allright!

**Kind**: instance method of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  
**Overrides**: [<code>applySuccessfulInitialization</code>](#module_stores/common.Store+applySuccessfulInitialization)  
**Returns**: <code>boolean</code> - Returns true if the store is well-initialized  
<a name="module_stores/common.Store+setInitState"></a>

#### ndiStore.setInitState(state) ⇒ <code>boolean</code>
Sets a new initialization state for the store

**Kind**: instance method of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  
**Overrides**: [<code>setInitState</code>](#module_stores/common.Store+setInitState)  
**Returns**: <code>boolean</code> - Flags true if the initialization state was updated  

| Param | Type | Description |
| --- | --- | --- |
| state | [<code>InitStateEnum</code>](#models.InitStateEnum) | New initialization state |

<a name="module_stores/common.Store+toJSON"></a>

#### ndiStore.toJSON() ⇒ <code>object</code>
Prints the store as a JSON object for debug purpose

**Kind**: instance method of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  
**Overrides**: [<code>toJSON</code>](#module_stores/common.Store+toJSON)  
**Returns**: <code>object</code> - The store state in a JSON object  
<a name="module_stores/quiddity.NdiStore.LOG"></a>

#### NdiStore.LOG : [<code>pino/logger</code>](#external_pino/logger)
Dedicated logger for the NdiStore

**Kind**: static constant of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  
<a name="module_stores/quiddity.NdiStore.NDI_SNIFFER_OUTPUT_REGEX"></a>

#### NdiStore.NDI\_SNIFFER\_OUTPUT\_REGEX : <code>RegExp</code>
Matches all changes to NDI Sniffer's stdout output

**Kind**: static constant of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  
<a name="module_stores/quiddity.NdiStore.NDI_OUTPUT_MAX_LIMIT"></a>

#### NdiStore.NDI\_OUTPUT\_MAX\_LIMIT : <code>number</code>
Fixed max reader limit of the NDI Output

**Kind**: static constant of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  
<a name="module_stores/quiddity.NdiStore.NDI_OUTPUT_COMMAND_LINE"></a>

#### NdiStore.NDI\_OUTPUT\_COMMAND\_LINE : <code>string</code>
Command line executed by NDI_OUTPUT_KIND_ID quiddities

**Kind**: static constant of [<code>NdiStore</code>](#module_stores/quiddity.NdiStore)  
<a name="module_stores/quiddity.LockStore"></a>

### stores/quiddity.LockStore
Stores all locked quiddities

**Kind**: static class of [<code>stores/quiddity</code>](#module_stores/quiddity)  
<a name="new_module_stores/quiddity.LockStore_new"></a>

#### new LockStore(quiddityStore)
Instantiates a new LockStore


| Param | Type | Description |
| --- | --- | --- |
| quiddityStore | [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore) | Quiddity manager |

<a name="module_stores/quiddity.LockStore+lockedEntries"></a>

#### lockStore.lockedEntries
**Kind**: instance property of [<code>LockStore</code>](#module_stores/quiddity.LockStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| lockedEntries | <code>Map.&lt;string, module:models/matrix.MatrixEntry&gt;</code> | All locked matrix entries by entry IDs |

<a name="module_stores/quiddity.LockStore+lockableKindIds"></a>

#### lockStore.lockableKindIds
**Kind**: instance property of [<code>LockStore</code>](#module_stores/quiddity.LockStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| lockableKindIds | <code>Set.&lt;string&gt;</code> | All lockable kind IDs |

<a name="module_stores/quiddity.LockStore+lockableQuiddities"></a>

#### lockStore.lockableQuiddities
**Kind**: instance property of [<code>LockStore</code>](#module_stores/quiddity.LockStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| lockableQuiddities | <code>Set.&lt;string&gt;</code> | Set of quiddity IDs that shouldn't be synchronized with scenes |

<a name="module_stores/quiddity.LockStore+lockedQuiddities"></a>

#### lockStore.lockedQuiddities
**Kind**: instance property of [<code>LockStore</code>](#module_stores/quiddity.LockStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| lockedQuiddities | <code>Set.&lt;string&gt;</code> | Set of quiddity IDs that are locked Warning: It won't work with the SIP quiddity |

<a name="module_stores/quiddity.LockStore+lockedContacts"></a>

#### lockStore.lockedContacts
**Kind**: instance property of [<code>LockStore</code>](#module_stores/quiddity.LockStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| lockedContacts | <code>Set.&lt;Contact&gt;</code> | Set of contact names that are locked |

<a name="module_stores/quiddity.LockStore+addLockableKind"></a>

#### lockStore.addLockableKind(kindId)
Registers a lockable quiddity kind ID

**Kind**: instance method of [<code>LockStore</code>](#module_stores/quiddity.LockStore)  
    **Mermaid**:
      <div class="mermaid">
        sequenceDiagram
   User->>QuiddityStore: start Scenic
   activate QuiddityStore
   QuiddityStore->>QuiddityStore: discover classes
   QuiddityStore->>RtmpStore: react to class discovery
   activate RtmpStore
   deactivate QuiddityStore
   RtmpStore-->>LockStore: signal the RTMP class as lockable
   activate LockStore
   deactivate RtmpStore
   LockStore->>LockStore: update lockable classes
   deactivate LockStore
   User->>ConnectionStore: add a scene
   activate ConnectionStore
   ConnectionStore->>LockStore: inspect lockable quiddities
   activate LockStore
   LockStore-->>ConnectionStore: return lockable classes
   deactivate LockStore
   ConnectionStore->>ConnectionStore: get all connections from lockable quiddities
   ConnectionStore-->>User: Fix the assignation of the connections
   deactivate ConnectionStore
      </div>

| Param | Type | Description |
| --- | --- | --- |
| kindId | <code>string</code> | A quiddity kind ID |

<a name="module_stores/quiddity.LockStore+deleteLockableKind"></a>

#### lockStore.deleteLockableKind(kindId)
Unregisters a lockable quiddity kind

**Kind**: instance method of [<code>LockStore</code>](#module_stores/quiddity.LockStore)  

| Param | Type | Description |
| --- | --- | --- |
| kindId | <code>string</code> | A quiddity kind |

<a name="module_stores/quiddity.LockStore+isLockableQuiddity"></a>

#### lockStore.isLockableQuiddity(quiddityId) ⇒ <code>boolean</code>
Checks if a quiddity can be locked

**Kind**: instance method of [<code>LockStore</code>](#module_stores/quiddity.LockStore)  
**Returns**: <code>boolean</code> - Returns true if a quiddity is lockable  

| Param | Type | Description |
| --- | --- | --- |
| quiddityId | <code>string</code> | ID of the quiddity |

<a name="module_stores/quiddity.LockStore+isLockableConnection"></a>

#### lockStore.isLockableConnection(connection) ⇒ <code>boolean</code>
Checks if a connection is lockable by one of its quiddity

**Kind**: instance method of [<code>LockStore</code>](#module_stores/quiddity.LockStore)  
**Returns**: <code>boolean</code> - Returns true if a connection is lockable  

| Param | Type | Description |
| --- | --- | --- |
| connection | <code>module:models/userTree.Connection</code> | A connection model |

<a name="module_stores/quiddity.LockStore+isLockedConnection"></a>

#### lockStore.isLockedConnection(connection) ⇒ <code>boolan</code>
Checks if a connection is locked

**Kind**: instance method of [<code>LockStore</code>](#module_stores/quiddity.LockStore)  
**Returns**: <code>boolan</code> - Returns true if the connection is locked  

| Param | Type | Description |
| --- | --- | --- |
| connection | <code>module:models/userTree.Connection</code> | A connection model |

<a name="module_stores/quiddity.LockStore+addLock"></a>

#### lockStore.addLock(matrixEntry)
Registers a locked quiddity ID

**Kind**: instance method of [<code>LockStore</code>](#module_stores/quiddity.LockStore)  

| Param | Type | Description |
| --- | --- | --- |
| matrixEntry | <code>module:models/matrix.MatrixEntry</code> | A matrix entry |

<a name="module_stores/quiddity.LockStore+deleteLock"></a>

#### lockStore.deleteLock(matrixEntry)
Unregisters a locked quiddity ID

**Kind**: instance method of [<code>LockStore</code>](#module_stores/quiddity.LockStore)  

| Param | Type | Description |
| --- | --- | --- |
| matrixEntry | <code>module:models/matrix.MatrixEntry</code> | A matrix entry |

<a name="module_stores/quiddity.LockStore+toggleLock"></a>

#### lockStore.toggleLock(matrixEntry, [lockStatus])
Toggles the lock for a matrix ID

**Kind**: instance method of [<code>LockStore</code>](#module_stores/quiddity.LockStore)  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| matrixEntry | <code>module:models/matrix.MatrixEntry</code> |  | A matrix entry |
| [lockStatus] | <code>boolean</code> | <code>false</code> | Flags a locked quiddity |

<a name="module_stores/quiddity.LockStore.LOG"></a>

#### LockStore.LOG : [<code>pino/logger</code>](#external_pino/logger)
Dedicated logger for the LockStore

**Kind**: static constant of [<code>LockStore</code>](#module_stores/quiddity.LockStore)  
<a name="module_stores/matrix"></a>

## stores/matrix
Define all stores for UI behaviors about the matrix

<a name="module_stores/matrix.MatrixStore"></a>

### stores/matrix.MatrixStore
Extra store used to build the Matrix component by adding abstractions on shmdatas, quiddities and contacts
This is the highest-level store for everything matrix-related. It should be the first store
you look at when trying to add another layer of complexity on top of the matrix.
This is one of the last store to be instantiated during the app's initialization.

**Kind**: static class of [<code>stores/matrix</code>](#module_stores/matrix)  
**Todo**

- [ ] Wraps the categories as bindings

<a name="new_module_stores/matrix.MatrixStore_new"></a>

#### new MatrixStore(quiddityStore, propertyStore, orderStore, shmdataStore, contactStore, sceneStore, lockStore, encoderStore)
Instantiates a new MatrixStore

**Throws**:

- <code>TypeError</code> Will throw an error if a dependency store is not set


| Param | Type | Description |
| --- | --- | --- |
| quiddityStore | [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore) | Quiddity manager |
| propertyStore | <code>module:stores/quiddity.PropertyStor</code> | Property manager |
| orderStore | <code>module:stores/quiddity.OrderStore</code> | Manages all order of entries |
| shmdataStore | [<code>ShmdataStore</code>](#module_stores/shmdata.ShmdataStore) | Shmdata manager |
| contactStore | <code>module:stores/sip.ContactStore</code> | SIP contact manager |
| sceneStore | <code>module:stores/userTree.SceneStore</code> | Scene manager |
| lockStore | <code>module:stores/matrix.LockStore</code> | Lock manager |
| encoderStore | [<code>EncoderStore</code>](#module_stores/quiddity.EncoderStore) | Encoders manager |

<a name="module_stores/matrix.MatrixStore+destinationCategories"></a>

#### matrixStore.destinationCategories
**Kind**: instance property of [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| destinationCategories | <code>Map.&lt;string, Array.&lt;module:models/matrix.MatrixEntry&gt;&gt;</code> | All destinations mapped by category IDs |

<a name="module_stores/matrix.MatrixStore+sourceCategories"></a>

#### matrixStore.sourceCategories
**Kind**: instance property of [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| sourceCategories | <code>Map.&lt;string, Array.&lt;module:models/matrix.MatrixEntry&gt;&gt;</code> | All sources mapped by category IDs |

<a name="module_stores/matrix.MatrixStore+sourceEntries"></a>

#### matrixStore.sourceEntries
**Kind**: instance property of [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| sourceEntries | <code>Array.&lt;module:models/matrix.MatrixEntry&gt;</code> | All the source entries |

<a name="module_stores/matrix.MatrixStore+destinationEntries"></a>

#### matrixStore.destinationEntries
**Kind**: instance property of [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| destinationEntries | <code>Array.&lt;module:models/matrix.MatrixEntry&gt;</code> | All the destination entries |

<a name="module_stores/matrix.MatrixStore+matrixEntries"></a>

#### matrixStore.matrixEntries
**Kind**: instance property of [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| matrixEntries | <code>module:models/matrix.MatrixEntry</code> | All the matrix entries hashed by entry ID |

<a name="module_stores/matrix.MatrixStore+startableSources"></a>

#### matrixStore.startableSources
**Kind**: instance property of [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| startableSources | <code>Set.&lt;string&gt;</code> | All entries that are startable |

<a name="module_stores/matrix.MatrixStore+sipQuiddities"></a>

#### matrixStore.sipQuiddities
**Kind**: instance property of [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| startableSources | <code>Set.&lt;string&gt;</code> | All entries that are startable |

<a name="module_stores/matrix.MatrixStore+matrixConnectionsFromEntries"></a>

#### matrixStore.matrixConnectionsFromEntries
**Kind**: instance property of [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore)  
**Todo**

- [ ] : this does way too much work. This should look into the connection store to get existing connection
instead of querying the whole stack to recreate Connections objects

**Properties**

| Name | Type | Description |
| --- | --- | --- |
| matrixConnections | <code>Map.&lt;string, Array.&lt;module:models/matrix.MatrixEntry&gt;&gt;</code> | All connected entries of the matrix |

<a name="module_stores/matrix.MatrixStore+lockableCategories"></a>

#### matrixStore.lockableCategories
**Kind**: instance property of [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| lockableCategories | <code>Set.&lt;string&gt;</code> | All categories that represents a lockable quiddity class |

<a name="module_stores/matrix.MatrixStore+lockedCategories"></a>

#### matrixStore.lockedCategories
**Kind**: instance property of [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| lockedCategories | <code>Set.&lt;string&gt;</code> | All categories that contains a locked entry |

<a name="module_stores/matrix.MatrixStore+lockedConnections"></a>

#### matrixStore.lockedConnections
**Kind**: instance property of [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| lockedConnections | <code>Set.&lt;Connection&gt;</code> | All connections that are locked in the matrix |

<a name="module_stores/matrix.MatrixStore+lockedEntries"></a>

#### matrixStore.lockedEntries
**Kind**: instance property of [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| lockedEntries | <code>Set.&lt;string&gt;</code> | All IDs of all destinations that are locked and all sources that are connected to locked destinations |

<a name="module_stores/matrix.MatrixStore+lockedQuiddities"></a>

#### matrixStore.lockedQuiddities
**Kind**: instance property of [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| lockedQuiddities | <code>Set.&lt;string&gt;</code> | All locked quiddity IDs |

<a name="module_stores/matrix.MatrixStore+destinationQuiddities"></a>

#### matrixStore.destinationQuiddities
**Kind**: instance property of [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| destinationQuiddities | <code>Map.&lt;string, Set.&lt;string&gt;&gt;</code> | All entries hashed by destination quiddity IDs |

<a name="module_stores/matrix.MatrixStore+sourceQuiddities"></a>

#### matrixStore.sourceQuiddities
**Kind**: instance property of [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| sourceQuiddities | <code>Map.&lt;string, Set.&lt;string&gt;&gt;</code> | All entries ids hashed by source quiddity IDs |

<a name="module_stores/matrix.MatrixStore+matrixQuiddities"></a>

#### matrixStore.matrixQuiddities
**Kind**: instance property of [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| matrixQuiddities | <code>Map.&lt;string, Set.&lt;string&gt;&gt;</code> | All entries hashed by quiddity IDs |

<a name="module_stores/matrix.MatrixStore+connectedQuiddities"></a>

#### matrixStore.connectedQuiddities
**Kind**: instance property of [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| connectedQuiddities | <code>Map.&lt;string, Set.&lt;string&gt;&gt;</code> | All connections hashed by quiddity IDs |

<a name="module_stores/matrix.MatrixStore+populateSourceCategories"></a>

#### matrixStore.populateSourceCategories()
Populates the source categories

**Kind**: instance method of [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore)  
<a name="module_stores/matrix.MatrixStore+populateDestinationCategories"></a>

#### matrixStore.populateDestinationCategories()
Populates the destination categories

**Kind**: instance method of [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore)  
<a name="module_stores/matrix.MatrixStore+setSourceCategories"></a>

#### matrixStore.setSourceCategories(categories)
Sets the source categories

**Kind**: instance method of [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore)  

| Param | Type | Description |
| --- | --- | --- |
| categories | <code>Map.&lt;string, Array.&lt;module:models/matrix.MatrixEntry&gt;&gt;</code> | A map for the source categories |

<a name="module_stores/matrix.MatrixStore+setDestinationCategories"></a>

#### matrixStore.setDestinationCategories(categories)
Sets the destination categories

**Kind**: instance method of [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore)  

| Param | Type | Description |
| --- | --- | --- |
| categories | <code>Map.&lt;string, Array.&lt;module:models/matrix.MatrixEntry&gt;&gt;</code> | A map for the destination categories |

<a name="module_stores/matrix.MatrixStore+findCompatibleMediaType"></a>

#### matrixStore.findCompatibleMediaType() ⇒ <code>string</code>
Gets the compatible mediaType of an entry

**Kind**: instance method of [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore)  
**Returns**: <code>string</code> - The compatible mediaType of the entry  
<a name="module_stores/matrix.MatrixStore+findCompatibleShmdata"></a>

#### matrixStore.findCompatibleShmdata() ⇒ <code>module:models/shmdata.Shmdata</code>
Gets the compatible shmdata of an entry

**Kind**: instance method of [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore)  
**Returns**: <code>module:models/shmdata.Shmdata</code> - The compatible shmdata model of the entry  
<a name="module_stores/matrix.MatrixStore+getAllDestinationQuiddityEntries"></a>

#### matrixStore.getAllDestinationQuiddityEntries() ⇒ <code>Array.&lt;module:models/matrix.MatrixEntry&gt;</code>
Gets all entries associated with a destination quiddity

**Kind**: instance method of [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore)  
**Returns**: <code>Array.&lt;module:models/matrix.MatrixEntry&gt;</code> - All associated entries  
<a name="module_stores/matrix.MatrixStore+getDestinationContactEntries"></a>

#### matrixStore.getDestinationContactEntries() ⇒ <code>Array.&lt;module:models/matrix.MatrixEntry&gt;</code>
Gets all entries associated with the SIP id
The name of this function can be misleading : source quiddities can potentially be associated to a contact but
this function wouldn't return them. Moreover, This function is used in context where it is assumed that all matrixEntries returned
by it are destinations so changing it to return all quiddities associated with a contact would break stuff.

**Kind**: instance method of [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore)  
**Returns**: <code>Array.&lt;module:models/matrix.MatrixEntry&gt;</code> - All associated entries  
<a name="module_stores/matrix.MatrixStore+makeDestinationCategories"></a>

#### matrixStore.makeDestinationCategories() ⇒ <code>Map.&lt;string, Array.&lt;module:models/matrix.MatrixEntry&gt;&gt;</code>
Makes all destination categories

**Kind**: instance method of [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore)  
**Returns**: <code>Map.&lt;string, Array.&lt;module:models/matrix.MatrixEntry&gt;&gt;</code> - A map of all destinations by categories  
<a name="module_stores/matrix.MatrixStore+makeSourceCategories"></a>

#### matrixStore.makeSourceCategories() ⇒ <code>Map.&lt;string, Array.&lt;module:models/matrix.MatrixEntry&gt;&gt;</code>
Makes all source categories. Contrary to what the name might imply, this is called everytime a quiddity changes

**Kind**: instance method of [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore)  
**Returns**: <code>Map.&lt;string, Array.&lt;module:models/matrix.MatrixEntry&gt;&gt;</code> - A map of all sources by categories
     This is not the best place for this but its the best one we have right now.
     We would need to be able to categorize sources in a store that is hierarchically much
     closer to the quiddity store but we cannot at this moment because we have inconsistent
     state everywhere. See https://gitlab.com/sat-mtl/tools/scenic/scenic/-/issues/340 and
     the https://gitlab.com/sat-mtl/tools/scenic/scenic/-/tree/feat/refactor-order-store-for-classified-sources branch
     for more details.  
**Todo**

- [ ] Refactor the matrix cycle in order to add a better control on the UI updates

<a name="module_stores/matrix.MatrixStore+isShmdataDisplayed"></a>

#### matrixStore.isShmdataDisplayed() ⇒ <code>Boolean</code>
Checks if a shmdata should be displayed

**Kind**: instance method of [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore)  
**Returns**: <code>Boolean</code> - - Returns true if the shmdata is that of an encoder with displayEncoders params set to true or if the shmdata is that of non encoder  
<a name="module_stores/matrix.MatrixStore+getSourceQuiddityEntries"></a>

#### matrixStore.getSourceQuiddityEntries(quiddity) ⇒ <code>Array.&lt;module:models/matrix.MatrixEntry&gt;</code>
Gets all entries associated with a source quiddity

**Kind**: instance method of [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore)  
**Returns**: <code>Array.&lt;module:models/matrix.MatrixEntry&gt;</code> - All associated entries  

| Param | Type | Description |
| --- | --- | --- |
| quiddity | <code>module:models/quiddity.Quiddity</code> | The source quiddity |

<a name="module_stores/matrix.MatrixStore+getAllSourceQuiddityEntries"></a>

#### matrixStore.getAllSourceQuiddityEntries() ⇒ <code>Array.&lt;module:models/models.MatrixEntry&gt;</code>
Gets all entries produced by the quiddity sources

**Kind**: instance method of [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore)  
**Returns**: <code>Array.&lt;module:models/models.MatrixEntry&gt;</code> - All source matrix entries  
<a name="module_stores/matrix.MatrixStore+applyEntrySorting"></a>

#### matrixStore.applyEntrySorting(entries) ⇒ <code>Array.&lt;module:models/matrix.MatrixEntry&gt;</code>
Sorts entries by setting the VIDEO_RAW media type in first position

**Kind**: instance method of [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore)  
**Returns**: <code>Array.&lt;module:models/matrix.MatrixEntry&gt;</code> - All sorted entries  

| Param | Type | Description |
| --- | --- | --- |
| entries | <code>Array.&lt;module:models/matrix.MatrixEntry&gt;</code> | All the entries to sort |

<a name="module_stores/matrix.MatrixStore.LOG"></a>

#### MatrixStore.LOG : [<code>pino/logger</code>](#external_pino/logger)
Dedicated logger for the MatrixStore

**Kind**: static constant of [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore)  
<a name="module_stores/matrix.FilterStore"></a>

### stores/matrix.FilterStore ⇐ [<code>Store</code>](#module_stores/common.Store)
Stores all filters

**Kind**: static class of [<code>stores/matrix</code>](#module_stores/matrix)  
**Extends**: [<code>Store</code>](#module_stores/common.Store)  
<a name="new_module_stores/matrix.FilterStore_new"></a>

#### new FilterStore(socketStore, quiddityStore, contactStore)
Instantiates a new FilterStore

**Throws**:

- <code>TypeError</code> Throws an error when a required store is missing


| Param | Type | Description |
| --- | --- | --- |
| socketStore | <code>module:stores/common.SocketStore</code> | Manage all sockets |
| quiddityStore | [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore) | Store all quiddities |
| contactStore | <code>module:stores/sip.ContactStore</code> | Store all contacts |

<a name="module_stores/matrix.FilterStore+quiddityStore"></a>

#### filterStore.quiddityStore
**Kind**: instance property of [<code>FilterStore</code>](#module_stores/matrix.FilterStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| quiddityStore | [<code>QuiddityStore</code>](#module_stores/quiddity.QuiddityStore) | Stores all quiddities |

<a name="module_stores/matrix.FilterStore+contactStore"></a>

#### filterStore.contactStore
**Kind**: instance property of [<code>FilterStore</code>](#module_stores/matrix.FilterStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| contactStore | <code>module:stores/sip.ContactStore</code> | Stores all SIP contacts |

<a name="module_stores/matrix.FilterStore+categoryFilters"></a>

#### filterStore.categoryFilters
**Kind**: instance property of [<code>FilterStore</code>](#module_stores/matrix.FilterStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| categoryFilters | <code>Set.&lt;string&gt;</code> | All the category filters selected by the user |

<a name="module_stores/matrix.FilterStore+orderedCategories"></a>

#### filterStore.orderedCategories
**Kind**: instance property of [<code>FilterStore</code>](#module_stores/matrix.FilterStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| orderedCategories | <code>Map.&lt;string, string&gt;</code> | All categories ordered by quiddityID |

<a name="module_stores/matrix.FilterStore+userCategories"></a>

#### filterStore.userCategories
**Kind**: instance property of [<code>FilterStore</code>](#module_stores/matrix.FilterStore)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| categories | <code>Set.&lt;string&gt;</code> | All categories available for the filters |

<a name="module_stores/common.Store+socketStore"></a>

#### filterStore.socketStore
**Kind**: instance property of [<code>FilterStore</code>](#module_stores/matrix.FilterStore)  
**Overrides**: [<code>socketStore</code>](#module_stores/common.Store+socketStore)  
**Properties**

| Type | Description |
| --- | --- |
| [<code>SocketStore</code>](#stores.SocketStore) | Socket manager |

<a name="module_stores/common.Store+initState"></a>

#### filterStore.initState
**Kind**: instance property of [<code>FilterStore</code>](#module_stores/matrix.FilterStore)  
**Overrides**: [<code>initState</code>](#module_stores/common.Store+initState)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| initState | <code>number</code> | Initialization state of the store |

<a name="module_stores/common.Store+hasActiveSocket"></a>

#### filterStore.hasActiveSocket
**Kind**: instance property of [<code>FilterStore</code>](#module_stores/matrix.FilterStore)  
**Overrides**: [<code>hasActiveSocket</code>](#module_stores/common.Store+hasActiveSocket)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| hasActiveSocket | <code>boolean</code> | Checks if the active socket exists |

<a name="module_stores/matrix.FilterStore+handleCategoryChange"></a>

#### filterStore.handleCategoryChange()
Handles all changes of available categories

**Kind**: instance method of [<code>FilterStore</code>](#module_stores/matrix.FilterStore)  
<a name="module_stores/matrix.FilterStore+applyQuiddityFilter"></a>

#### filterStore.applyQuiddityFilter(quiddityId) ⇒ <code>boolean</code>
Applies a filter on a quiddity

**Kind**: instance method of [<code>FilterStore</code>](#module_stores/matrix.FilterStore)  
**Returns**: <code>boolean</code> - Flags true if the quiddity passes the filters  

| Param | Type | Description |
| --- | --- | --- |
| quiddityId | <code>string</code> | The ID of the quiddity |

<a name="module_stores/matrix.FilterStore+applyEntryFilter"></a>

#### filterStore.applyEntryFilter(matrixEntry) ⇒ <code>boolean</code>
Applies a filter to an entry of the matrix

**Kind**: instance method of [<code>FilterStore</code>](#module_stores/matrix.FilterStore)  
**Returns**: <code>boolean</code> - Flags true if the entry passes the filters  

| Param | Type | Description |
| --- | --- | --- |
| matrixEntry | <code>module:models/matrix.MatrixEntry</code> | A matrix entry |

<a name="module_stores/matrix.FilterStore+applyCellFilter"></a>

#### filterStore.applyCellFilter(sourceEntry, destinationEntry) ⇒ <code>boolean</code>
Applies a filter for a source and a destination

**Kind**: instance method of [<code>FilterStore</code>](#module_stores/matrix.FilterStore)  
**Returns**: <code>boolean</code> - Flags true if the entries pass the filter  

| Param | Type | Description |
| --- | --- | --- |
| sourceEntry | <code>module:models/matrix.MatrixEntry</code> | Matrix entry for a source |
| destinationEntry | <code>module:models/matrix.MatrixEntry</code> | Matrix entry for a destination |

<a name="module_stores/matrix.FilterStore+setOrderedCategories"></a>

#### filterStore.setOrderedCategories()
Sets the entire map of the categories mapped by quiddity IDs

**Kind**: instance method of [<code>FilterStore</code>](#module_stores/matrix.FilterStore)  
<a name="module_stores/matrix.FilterStore+addCategoryFilter"></a>

#### filterStore.addCategoryFilter()
Adds a category to filter

**Kind**: instance method of [<code>FilterStore</code>](#module_stores/matrix.FilterStore)  
<a name="module_stores/matrix.FilterStore+removeCategoryFilter"></a>

#### filterStore.removeCategoryFilter()
Deletes a category from the filters

**Kind**: instance method of [<code>FilterStore</code>](#module_stores/matrix.FilterStore)  
<a name="module_stores/matrix.FilterStore+clearCategoryFilters"></a>

#### filterStore.clearCategoryFilters()
Clears all categories from the filters

**Kind**: instance method of [<code>FilterStore</code>](#module_stores/matrix.FilterStore)  
<a name="module_stores/common.Store+isInitialized"></a>

#### filterStore.isInitialized() ⇒ <code>boolean</code>
Check if the store's current initialization state is INITIALIZED

**Kind**: instance method of [<code>FilterStore</code>](#module_stores/matrix.FilterStore)  
**Overrides**: [<code>isInitialized</code>](#module_stores/common.Store+isInitialized)  
**Returns**: <code>boolean</code> - Flags true if store is INITIALIZED  
<a name="module_stores/common.Store+isInitializing"></a>

#### filterStore.isInitializing() ⇒ <code>boolean</code>
Check if the store's current initialization state is INITIALIZING

**Kind**: instance method of [<code>FilterStore</code>](#module_stores/matrix.FilterStore)  
**Overrides**: [<code>isInitializing</code>](#module_stores/common.Store+isInitializing)  
**Returns**: <code>boolean</code> - Flags true if store is INITIALIZING  
<a name="module_stores/common.Store+isNotInitialized"></a>

#### filterStore.isNotInitialized() ⇒ <code>boolean</code>
Check if the store's current initialization state is NOT_INITIALIZED

**Kind**: instance method of [<code>FilterStore</code>](#module_stores/matrix.FilterStore)  
**Overrides**: [<code>isNotInitialized</code>](#module_stores/common.Store+isNotInitialized)  
**Returns**: <code>boolean</code> - Flags true if store is NOT_INITIALIZED  
<a name="module_stores/common.Store+applySuccessfulInitialization"></a>

#### filterStore.applySuccessfulInitialization() ⇒ <code>boolean</code>
Applies the INITIALIZED statement to the store
Should be called when everything is allright!

**Kind**: instance method of [<code>FilterStore</code>](#module_stores/matrix.FilterStore)  
**Overrides**: [<code>applySuccessfulInitialization</code>](#module_stores/common.Store+applySuccessfulInitialization)  
**Returns**: <code>boolean</code> - Returns true if the store is well-initialized  
<a name="module_stores/common.Store+setInitState"></a>

#### filterStore.setInitState(state) ⇒ <code>boolean</code>
Sets a new initialization state for the store

**Kind**: instance method of [<code>FilterStore</code>](#module_stores/matrix.FilterStore)  
**Overrides**: [<code>setInitState</code>](#module_stores/common.Store+setInitState)  
**Returns**: <code>boolean</code> - Flags true if the initialization state was updated  

| Param | Type | Description |
| --- | --- | --- |
| state | [<code>InitStateEnum</code>](#models.InitStateEnum) | New initialization state |

<a name="module_stores/common.Store+toJSON"></a>

#### filterStore.toJSON() ⇒ <code>object</code>
Prints the store as a JSON object for debug purpose

**Kind**: instance method of [<code>FilterStore</code>](#module_stores/matrix.FilterStore)  
**Overrides**: [<code>toJSON</code>](#module_stores/common.Store+toJSON)  
**Returns**: <code>object</code> - The store state in a JSON object  
<a name="module_stores/matrix.FilterStore.LOG"></a>

#### FilterStore.LOG : [<code>pino/logger</code>](#external_pino/logger)
Dedicated logger for the FilterStore

**Kind**: static constant of [<code>FilterStore</code>](#module_stores/matrix.FilterStore)  
<a name="module_stores/matrix.LayoutHelper"></a>

### stores/matrix.LayoutHelper
Helper for all CSS grid properties of the matrix layout

**Kind**: static class of [<code>stores/matrix</code>](#module_stores/matrix)  
<a name="new_module_stores/matrix.LayoutHelper_new"></a>

#### new LayoutHelper(matrixStore, innerGap)
Instantiates a new LayoutHelper


| Param | Type | Default | Description |
| --- | --- | --- | --- |
| matrixStore | [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore) |  | Store for all matrix properties |
| innerGap | <code>number</code> | <code>0</code> | The inner gap between all matrix elements |

<a name="module_stores/matrix.LayoutHelper+innerGap"></a>

#### layoutHelper.innerGap
**Kind**: instance property of [<code>LayoutHelper</code>](#module_stores/matrix.LayoutHelper)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| innerGap | <code>number</code> | The inner gap between all matrix elements |

<a name="module_stores/matrix.LayoutHelper+matrixStore"></a>

#### layoutHelper.matrixStore
**Kind**: instance property of [<code>LayoutHelper</code>](#module_stores/matrix.LayoutHelper)  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| matrixStore | [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore) | Store for all matrix properties |

<a name="module_stores/matrix.LayoutHelper+columnHeadingsAreasTemplate"></a>

#### layoutHelper.columnHeadingsAreasTemplate ⇒ <code>Array.&lt;string&gt;</code>
Gets the column headings template of CSS grid areas

**Kind**: instance property of [<code>LayoutHelper</code>](#module_stores/matrix.LayoutHelper)  
**Returns**: <code>Array.&lt;string&gt;</code> - A list of `grid-area` properties  
**See**: [grid-areas documentation](https://developer.mozilla.org/en-US/docs/Web/CSS/grid-area)  
<a name="module_stores/matrix.LayoutHelper+gridTemplateAreasStyleProperty"></a>

#### layoutHelper.gridTemplateAreasStyleProperty ⇒ <code>string</code>
Gets the grid-template-areas style of the matrix
+ It represents a string matrix of area keys
+ All areas are named with the source and the destination categories

**Kind**: instance property of [<code>LayoutHelper</code>](#module_stores/matrix.LayoutHelper)  
**Returns**: <code>string</code> - The computed `grid-template-areas` style property  
**See**: [grid-template-areas documentation](https://developer.mozilla.org/en-US/docs/Web/CSS/grid-template-areas)  
<a name="module_stores/matrix.LayoutHelper+gridTemplateColumnsStyleProperty"></a>

#### layoutHelper.gridTemplateColumnsStyleProperty ⇒ <code>string</code>
Gets the `grid-template-columns` style of the matrix
+ It repeats the `max-reader` property for every destination categories
+ It counts each category twice because it also counts separators
+ It computes the `matrix-header` area as a separator for the COMMON category

**Kind**: instance property of [<code>LayoutHelper</code>](#module_stores/matrix.LayoutHelper)  
**Returns**: <code>string</code> - The computed `grid-template-columns` style property  
**See**

- [grid-template-columns documentation](https://developer.mozilla.org/en-US/docs/Web/CSS/grid-template-columns)
- [repeat CSS function](https://developer.mozilla.org/en-US/docs/Web/CSS/repeat)

<a name="module_stores/matrix.LayoutHelper+gridLayoutStyle"></a>

#### layoutHelper.gridLayoutStyle ⇒ <code>Object</code>
Gets the grid-layout style of the matrix

**Kind**: instance property of [<code>LayoutHelper</code>](#module_stores/matrix.LayoutHelper)  
**Returns**: <code>Object</code> - The computed grid-layout styles  
**See**

- [The CSS Grid Layout guide](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout)
- [A complete guide about the Grid Layout](https://css-tricks.com/snippets/css/complete-guide-grid/)

<a name="module_stores/matrix.LayoutHelper+getSourceAreaKey"></a>

#### layoutHelper.getSourceAreaKey(sourceCategoryId) ⇒ <code>string</code>
Gets the key for a source area

**Kind**: instance method of [<code>LayoutHelper</code>](#module_stores/matrix.LayoutHelper)  
**Returns**: <code>string</code> - The generated area key  

| Param | Type | Description |
| --- | --- | --- |
| sourceCategoryId | <code>string</code> | The source category |

<a name="module_stores/matrix.LayoutHelper+getMatrixHeaderAreaKey"></a>

#### layoutHelper.getMatrixHeaderAreaKey() ⇒ <code>string</code>
Gets the key for the matrix header area

**Kind**: instance method of [<code>LayoutHelper</code>](#module_stores/matrix.LayoutHelper)  
**Returns**: <code>string</code> - A string key  
<a name="module_stores/matrix.LayoutHelper+getDestinationAreaKey"></a>

#### layoutHelper.getDestinationAreaKey(categoryId) ⇒ <code>string</code>
Gets the key for a destination area

**Kind**: instance method of [<code>LayoutHelper</code>](#module_stores/matrix.LayoutHelper)  
**Returns**: <code>string</code> - The generated area key  

| Param | Type | Description |
| --- | --- | --- |
| categoryId | <code>string</code> | The destination category |

<a name="module_stores/matrix.LayoutHelper+getDestinationSeparatorAreaKey"></a>

#### layoutHelper.getDestinationSeparatorAreaKey(categoryId) ⇒ <code>string</code>
Gets the key for a destination separator area

**Kind**: instance method of [<code>LayoutHelper</code>](#module_stores/matrix.LayoutHelper)  
**Returns**: <code>string</code> - The generated area key  

| Param | Type | Description |
| --- | --- | --- |
| categoryId | <code>string</code> | The destination category |

<a name="module_stores/matrix.LayoutHelper+getConnectionAreaKey"></a>

#### layoutHelper.getConnectionAreaKey(sourceCategoryId, destinationCategoryId) ⇒ <code>string</code>
Gets the key for a connection area

**Kind**: instance method of [<code>LayoutHelper</code>](#module_stores/matrix.LayoutHelper)  
**Returns**: <code>string</code> - The generated area key  

| Param | Type | Description |
| --- | --- | --- |
| sourceCategoryId | <code>string</code> | The source category |
| destinationCategoryId | <code>string</code> | The destination category |

<a name="module_stores/matrix.LayoutHelper+getGridAreaStyle"></a>

#### layoutHelper.getGridAreaStyle(areaKey) ⇒ <code>Object</code>
Gets the style of each grid areas

**Kind**: instance method of [<code>LayoutHelper</code>](#module_stores/matrix.LayoutHelper)  
**Returns**: <code>Object</code> - A CSS style representation  

| Param | Type | Description |
| --- | --- | --- |
| areaKey | <code>string</code> | The key of the area |

<a name="module_stores/matrix.LayoutHelper+getRowAreasTemplate"></a>

#### layoutHelper.getRowAreasTemplate(sourceCategory) ⇒ <code>Array.&lt;string&gt;</code>
Gets a row template of CSS grid areas

**Kind**: instance method of [<code>LayoutHelper</code>](#module_stores/matrix.LayoutHelper)  
**Returns**: <code>Array.&lt;string&gt;</code> - A list of `grid-area` properties  
**See**: [grid-areas documentation](https://developer.mozilla.org/en-US/docs/Web/CSS/grid-area)  

| Param | Type | Description |
| --- | --- | --- |
| sourceCategory | <code>module:models/matrix.MatrixCategoryEnum</code> | A matrix category |

<a name="module_stores/matrix.LayoutHelper+getSourceSeparatorAreaKey"></a>

#### layoutHelper.getSourceSeparatorAreaKey(sourceCategoryId) ⇒ <code>string</code>
Gets the area key for a source separator

**Kind**: instance method of [<code>LayoutHelper</code>](#module_stores/matrix.LayoutHelper)  
**Returns**: <code>string</code> - A CSS safe string to be used as a grid-area css property  

| Param | Type | Description |
| --- | --- | --- |
| sourceCategoryId | <code>string</code> | the identifier of the source category |

<a name="module_stores/matrix.LayoutHelper+getEmptyRowTemplate"></a>

#### layoutHelper.getEmptyRowTemplate() ⇒ <code>string</code>
A '.' represents an empty cell in the grid, we need to push one for every destination category. We can't push this.getSourceSeparatorAreaKey(sourceCategory) because
our row separator cannot be contiguous because of the destinationSeparators that span entire columns

**Kind**: instance method of [<code>LayoutHelper</code>](#module_stores/matrix.LayoutHelper)  
**Returns**: <code>string</code> - a '.'  
<a name="module_stores/matrix.LayoutHelper+getSourceSeparatorTemplate"></a>

#### layoutHelper.getSourceSeparatorTemplate() ⇒ <code>Array.&lt;string&gt;</code>
Gets the grid-template-areas entry for a source separator

**Kind**: instance method of [<code>LayoutHelper</code>](#module_stores/matrix.LayoutHelper)  
**Returns**: <code>Array.&lt;string&gt;</code> - A list of `grid-area` properties  
**See**: [grid-areas documentation](https://developer.mozilla.org/en-US/docs/Web/CSS/grid-area)  
<a name="module_components/Common"></a>

## components/Common
Define all common components

<a name="module_components/fields"></a>

## components/fields
Define all field components

<a name="module_components/fields.LanguageSelectField"></a>

### components/fields.LanguageSelectField ⇒ [<code>SelectionField</code>](#module_components/fields.SelectionField)
The language select input

**Kind**: static constant of [<code>components/fields</code>](#module_components/fields)  
**Returns**: [<code>SelectionField</code>](#module_components/fields.SelectionField) - A selection field  
<a name="module_components/fields.DisplayThumbnailsField"></a>

### components/fields.DisplayThumbnailsField ⇒ [<code>BooleanField</code>](#module_components/fields.BooleanField)
The thumbnail display field

**Kind**: static constant of [<code>components/fields</code>](#module_components/fields)  
**Returns**: [<code>BooleanField</code>](#module_components/fields.BooleanField) - A boolean field  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| isDisplayThumbnails | <code>boolean</code> | Flags if thumbnail is displayed or not |
| onToggle | <code>function</code> | Function triggered when the user clicks the switch |

<a name="module_components/fields.DisplayEncodersField"></a>

### components/fields.DisplayEncodersField ⇒ [<code>BooleanField</code>](#module_components/fields.BooleanField)
The encoders display field

**Kind**: static constant of [<code>components/fields</code>](#module_components/fields)  
**Returns**: [<code>BooleanField</code>](#module_components/fields.BooleanField) - A boolean field  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| isDisplayEncoders | <code>boolean</code> | Flags if encoder is displayed or not |
| onToggle | <code>function</code> | Function triggered when the user clicks the switch |

<a name="module_components/fields.ColorHuePicker"></a>

### components/fields.ColorHuePicker(value, onChange) ⇒ <code>external:react-color.HuePicker</code>
Color picker for the hue value

**Kind**: static method of [<code>components/fields</code>](#module_components/fields)  
**Returns**: <code>external:react-color.HuePicker</code> - A picker for the hue value  

| Param | Type | Description |
| --- | --- | --- |
| value | <code>string</code> | Current color value in hexadecimal format |
| onChange | <code>function</code> | Function triggered when the user change the picker value |

<a name="module_components/fields.ColorAlphaPicker"></a>

### components/fields.ColorAlphaPicker(value, onChange) ⇒ <code>external:react-color.AlphaPicker</code>
Color picker for the alpha value

**Kind**: static method of [<code>components/fields</code>](#module_components/fields)  
**Returns**: <code>external:react-color.AlphaPicker</code> - A picker for the alpha value  

| Param | Type | Description |
| --- | --- | --- |
| value | <code>string</code> | Current color value in hexadecimal format |
| onChange | <code>function</code> | Function triggered when the user change the picker value |

<a name="module_components/fields.ColorTextInput"></a>

### components/fields.ColorTextInput(value, disabled, status, onChange) ⇒ <code>external:ui-component/Inputs.InputText</code>
Text input for the color hexadecimal value

**Kind**: static method of [<code>components/fields</code>](#module_components/fields)  
**Returns**: <code>external:ui-component/Inputs.InputText</code> - A text input  

| Param | Type | Description |
| --- | --- | --- |
| value | <code>string</code> | Current color value in hexadecimal format |
| disabled | <code>boolean</code> | Flags a disabled input |
| status | <code>string</code> | Status of the form |
| onChange | <code>function</code> | Function triggered when the user change the picker value |

<a name="module_components/fields.ColorField"></a>

### components/fields.ColorField(title, description, value, disabled, onChange, status) ⇒ <code>external:ui-component/Inputs.Field</code>
Field component that lets users pick a color

**Kind**: static method of [<code>components/fields</code>](#module_components/fields)  
**Returns**: <code>external:ui-component/Inputs.Field</code> - A field component  
**Todo**

- [ ] Update the status when the property update fails


| Param | Type | Description |
| --- | --- | --- |
| title | <code>string</code> | Title of the field |
| description | <code>string</code> | Description of the field |
| value | <code>string</code> | Value of the property |
| disabled | <code>boolean</code> | Flags a disabled value |
| onChange | <code>function</code> | Function triggered when the property is changed by the user |
| status | <code>string</code> | Status of the form |

<a name="module_components/fields.StringField"></a>

### components/fields.StringField(id, title, value, disabled, onChange, description, status, [isPassword]) ⇒ <code>external:ui-component/Inputs.Field</code>
Field component that lets users input a string

**Kind**: static method of [<code>components/fields</code>](#module_components/fields)  
**Returns**: <code>external:ui-component/Inputs.Field</code> - A field component  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| id | <code>string</code> |  | Unique ID of the field |
| title | <code>string</code> |  | Title of the field |
| value | <code>string</code> |  | Value of the field |
| disabled | <code>boolean</code> |  | Flag a disabled value |
| onChange | <code>function</code> |  | Function triggered when the property is changed by the user |
| description | <code>string</code> |  | Description of the field |
| status | <code>string</code> |  | Status of the form |
| [isPassword] | <code>boolean</code> | <code>false</code> | Flags a password credential |

<a name="module_components/fields.SipServerField"></a>

### components/fields.SipServerField() ⇒ [<code>CredentialField</code>](#module_components/fields.CredentialField)
Field used to input the SIP server address

**Kind**: static method of [<code>components/fields</code>](#module_components/fields)  
**Returns**: [<code>CredentialField</code>](#module_components/fields.CredentialField) - A configured credential field  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| credentialProps | <code>object</code> | All properties accepted by a CredentialField |

<a name="module_components/fields.SipUserField"></a>

### components/fields.SipUserField() ⇒ [<code>CredentialField</code>](#module_components/fields.CredentialField)
Field used to input the SIP user

**Kind**: static method of [<code>components/fields</code>](#module_components/fields)  
**Returns**: [<code>CredentialField</code>](#module_components/fields.CredentialField) - A configured credential field  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| credentialProps | <code>object</code> | All properties accepted by a CredentialField |

<a name="module_components/fields.SipPasswordField"></a>

### components/fields.SipPasswordField() ⇒ [<code>CredentialField</code>](#module_components/fields.CredentialField)
Field used to input the SIP password

**Kind**: static method of [<code>components/fields</code>](#module_components/fields)  
**Returns**: [<code>CredentialField</code>](#module_components/fields.CredentialField) - A configured credential field  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| credentialProps | <code>object</code> | All properties accepted by a CredentialField |

<a name="module_components/fields.SipPortField"></a>

### components/fields.SipPortField() ⇒ [<code>CredentialField</code>](#module_components/fields.CredentialField)
Field used to input the SIP port

**Kind**: static method of [<code>components/fields</code>](#module_components/fields)  
**Returns**: [<code>CredentialField</code>](#module_components/fields.CredentialField) - A configured credential field  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| credentialProps | <code>object</code> | All properties accepted by a CredentialField |

<a name="module_components/fields.TurnServerField"></a>

### components/fields.TurnServerField() ⇒ [<code>CredentialField</code>](#module_components/fields.CredentialField)
Field used to input the TURN server address

**Kind**: static method of [<code>components/fields</code>](#module_components/fields)  
**Returns**: [<code>CredentialField</code>](#module_components/fields.CredentialField) - A configured credential field  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| credentialProps | <code>object</code> | All properties accepted by a CredentialField |

<a name="module_components/fields.TurnUserField"></a>

### components/fields.TurnUserField() ⇒ [<code>CredentialField</code>](#module_components/fields.CredentialField)
Field used to input the TURN user

**Kind**: static method of [<code>components/fields</code>](#module_components/fields)  
**Returns**: [<code>CredentialField</code>](#module_components/fields.CredentialField) - A configured credential field  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| credentialProps | <code>object</code> | All properties accepted by a CredentialField |

<a name="module_components/fields.TurnPasswordField"></a>

### components/fields.TurnPasswordField() ⇒ [<code>CredentialField</code>](#module_components/fields.CredentialField)
Field used to input the TURN password

**Kind**: static method of [<code>components/fields</code>](#module_components/fields)  
**Returns**: [<code>CredentialField</code>](#module_components/fields.CredentialField) - A configured credential field  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| credentialProps | <code>object</code> | All properties accepted by a CredentialField |

<a name="module_components/fields.StunServerField"></a>

### components/fields.StunServerField() ⇒ [<code>CredentialField</code>](#module_components/fields.CredentialField)
Field used to input the STUN server address

**Kind**: static method of [<code>components/fields</code>](#module_components/fields)  
**Returns**: [<code>CredentialField</code>](#module_components/fields.CredentialField) - A configured credential field  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| credentialProps | <code>object</code> | All properties accepted by a CredentialField |

<a name="module_components/fields.SameStunTurnLoginField"></a>

### components/fields.SameStunTurnLoginField() ⇒ [<code>CredentialField</code>](#module_components/fields.CredentialField)
Renders the switch that enables the login forms for the STUN/TURN servers

**Kind**: static method of [<code>components/fields</code>](#module_components/fields)  
**Returns**: [<code>CredentialField</code>](#module_components/fields.CredentialField) - A configured credential field  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| isSameStunTurnLogin | <code>boolean</code> | Flag if the STUN/TURN credentials are the same as SIP |
| onToggle | <code>function</code> | Function triggered when the user clicks on the switch |

<a name="module_components/fields.BooleanField"></a>

### components/fields.BooleanField(title, description, value, disabled, onChange, status) ⇒ [<code>ui-components/Inputs/Field</code>](#external_ui-components/Inputs/Field)
Field component that lets users toggle a boolean value

**Kind**: static method of [<code>components/fields</code>](#module_components/fields)  
**Returns**: [<code>ui-components/Inputs/Field</code>](#external_ui-components/Inputs/Field) - A field component  

| Param | Type | Description |
| --- | --- | --- |
| title | <code>string</code> | Title of the field |
| description | <code>string</code> | Description of the field |
| value | <code>string</code> | Value of the property |
| disabled | <code>boolean</code> | Flags a disabled value |
| onChange | <code>function</code> | Function triggered when the property is changed by the user |
| status | <code>string</code> | Status of the form |

<a name="module_components/fields.CredentialField"></a>

### components/fields.CredentialField() ⇒ [<code>ui-components/Inputs/Field</code>](#external_ui-components/Inputs/Field)
Field component for all credential input types

**Kind**: static method of [<code>components/fields</code>](#module_components/fields)  
**Returns**: [<code>ui-components/Inputs/Field</code>](#external_ui-components/Inputs/Field) - A field component  
**Properties**

| Name | Type | Default | Description |
| --- | --- | --- | --- |
| id | <code>string</code> |  | Unique ID of the field |
| [value] | <code>string</code> \| <code>number</code> | <code>&quot;&#x27;&#x27;&quot;</code> | Value of the input |
| [label] | <code>string</code> | <code>&quot;&#x27;&#x27;&quot;</code> | Label of the field |
| [description] | <code>string</code> | <code>&quot;&#x27;&#x27;&quot;</code> | Description of the field |
| [error] | <code>string</code> | <code>null</code> | Error message of the field |
| [isPassword] | <code>boolean</code> | <code>false</code> | Flags a password credential |
| [disabled] | <code>boolean</code> | <code>false</code> | Flags a disabled field |
| [onChange] | <code>function</code> | <code>Function.prototype</code> | Function triggered when the field is changed |
| [onCheck] | <code>function</code> | <code>Function.prototype</code> | Function triggered when the field is checked |

<a name="module_components/fields.SelectionField"></a>

### components/fields.SelectionField(title, option, options, disabled, onChange, description, status) ⇒ [<code>ui-components/Inputs/Field</code>](#external_ui-components/Inputs/Field)
Field component that lets users select an option

**Kind**: static method of [<code>components/fields</code>](#module_components/fields)  
**Returns**: [<code>ui-components/Inputs/Field</code>](#external_ui-components/Inputs/Field) - A field component  

| Param | Type | Description |
| --- | --- | --- |
| title | <code>string</code> | Title of the field |
| option | <code>Object</code> | Value of the property |
| options | <code>Array.&lt;Object&gt;</code> | All selectable options of the field |
| disabled | <code>boolean</code> | Flags a disabled value |
| onChange | <code>function</code> | Function triggered when the property is changed by the user |
| description | <code>string</code> | Description of the field |
| status | <code>string</code> | Status of the form |

<a name="module_components/fields.NumberField"></a>

### components/fields.NumberField(title, description, value, min, max, step, unit, disabled, onChange, status) ⇒ [<code>ui-components/Inputs/Field</code>](#external_ui-components/Inputs/Field)
Field component that lets users input a numeric value
It validates its parameters by ensuring it won't break sub-components.
For example, it decreases the `min` property when it is equal to `max`.

**Kind**: static method of [<code>components/fields</code>](#module_components/fields)  
**Returns**: [<code>ui-components/Inputs/Field</code>](#external_ui-components/Inputs/Field) - - A field component  

| Param | Type | Description |
| --- | --- | --- |
| title | <code>string</code> | Title of the field |
| description | <code>string</code> | Description of the field |
| value | <code>string</code> | Value of the property |
| min | <code>number</code> | Minimum value of the field |
| max | <code>number</code> | Maximum value of the field |
| step | <code>number</code> | Specify the step used to increment or decrement the value |
| unit | [<code>UnitEnum</code>](#models.UnitEnum) | Convert a value to display |
| disabled | <code>boolean</code> | Flags a disabled value |
| onChange | <code>function</code> | Function triggered when the property is changed by the user |
| status | <code>string</code> | Status of the form |

<a name="module_components/pages"></a>

## components/pages
Define all page components

<a name="module_components/pages.AdvancedSipSettingsSection"></a>

### components/pages.AdvancedSipSettingsSection : [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Subsection that displays all advanced SIP settings

**Kind**: static constant of [<code>components/pages</code>](#module_components/pages)  
    **Selector**:
      <div class="mermaid">
        `#AdvancedSipSettingsSection`
      </div>
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| sipCredentialStore | <code>stores.SipCredentialStore</code> | Store all SIP credentials |

<a name="module_components/pages.RequiredSipSettingsSection"></a>

### components/pages.RequiredSipSettingsSection : [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Subsection that displays all required SIP settings

**Kind**: static constant of [<code>components/pages</code>](#module_components/pages)  
    **Selector**:
      <div class="mermaid">
        `#RequiredSipSettingsSection`
      </div>
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| sipCredentialStore | <code>stores.SipCredentialStore</code> | Store all SIP credentials |

<a name="module_components/pages.SipSettingsControlSection"></a>

### components/pages.SipSettingsControlSection : [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Subsection that provides all global SIP actions

**Kind**: static constant of [<code>components/pages</code>](#module_components/pages)  
    **Selector**:
      <div class="mermaid">
        `#SipSettingsControlSection`
      </div>
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| sipStore | [<code>SipStore</code>](#stores.SipStore) | Manage all SIP actions |
| sipCredentialStore | <code>stores.SipCredentialStore</code> | Store all SIP credentials |

<a name="module_components/pages.SettingsPage"></a>

### components/pages.SettingsPage : [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Page that displays all the settings of the app

**Kind**: static constant of [<code>components/pages</code>](#module_components/pages)  
    **Selector**:
      <div class="mermaid">
        `#SettingsPage`
      </div>
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| settingStore | <code>store.SettingStore</code> | Store all settings |
| sipStore | [<code>SipStore</code>](#stores.SipStore) | Manage all SIP actions |
| sipCredentialStore | <code>stores.SipCredentialStore</code> | Store all SIP credentials |

<a name="module_components/pages.MatrixPage"></a>

### components/pages.MatrixPage ⇒ [<code>react/Component</code>](#external_react/Component)
Page that displays all the source and destination matrix

**Kind**: static constant of [<code>components/pages</code>](#module_components/pages)  
**Returns**: [<code>react/Component</code>](#external_react/Component) - The matrix page  
    **Selector**:
      <div class="mermaid">
        `#MatrixPage`
      </div>
<a name="module_components/pages.AssistanceSection"></a>

### components/pages.AssistanceSection() ⇒ [<code>react/Component</code>](#external_react/Component)
The Assistance section

**Kind**: static method of [<code>components/pages</code>](#module_components/pages)  
**Returns**: [<code>react/Component</code>](#external_react/Component) - The Assistance section  
    **Selector**:
      <div class="mermaid">
        `#AssistanceSection`
      </div>
<a name="module_components/pages.InfoSection"></a>

### components/pages.InfoSection() ⇒ [<code>react/Component</code>](#external_react/Component)
The Info section

**Kind**: static method of [<code>components/pages</code>](#module_components/pages)  
**Returns**: [<code>react/Component</code>](#external_react/Component) - The Info section  
    **Selector**:
      <div class="mermaid">
        `#InfoSection`
      </div>
<a name="module_components/pages.ContributingSection"></a>

### components/pages.ContributingSection() ⇒ [<code>react/Component</code>](#external_react/Component)
The Contributing section

**Kind**: static method of [<code>components/pages</code>](#module_components/pages)  
**Returns**: [<code>react/Component</code>](#external_react/Component) - The Contributing section  
    **Selector**:
      <div class="mermaid">
        `#ContributingSection`
      </div>
<a name="module_components/pages.LicenseSubSection"></a>

### components/pages.LicenseSubSection() ⇒ [<code>react/Component</code>](#external_react/Component)
The Licence section

**Kind**: static method of [<code>components/pages</code>](#module_components/pages)  
**Returns**: [<code>react/Component</code>](#external_react/Component) - The Licence section  
    **Selector**:
      <div class="mermaid">
        `#LicenseSubSection`
      </div>
<a name="module_components/pages.VersionSubSection"></a>

### components/pages.VersionSubSection() ⇒ [<code>react/Component</code>](#external_react/Component)
The Version subsection

**Kind**: static method of [<code>components/pages</code>](#module_components/pages)  
**Returns**: [<code>react/Component</code>](#external_react/Component) - The Version section  
    **Selector**:
      <div class="mermaid">
        `#VersionSection`
      </div>
<a name="module_components/pages.ContributorsSection"></a>

### components/pages.ContributorsSection() ⇒ [<code>react/Component</code>](#external_react/Component)
The Contributors section

**Kind**: static method of [<code>components/pages</code>](#module_components/pages)  
**Returns**: [<code>react/Component</code>](#external_react/Component) - The Contributors section  
    **Selector**:
      <div class="mermaid">
        `#ContributorsSection`
      </div>
<a name="module_components/pages.AuthorsSubSection"></a>

### components/pages.AuthorsSubSection() ⇒ [<code>react/Component</code>](#external_react/Component)
The Authors subsection

**Kind**: static method of [<code>components/pages</code>](#module_components/pages)  
**Returns**: [<code>react/Component</code>](#external_react/Component) - The Contributors section  
    **Selector**:
      <div class="mermaid">
        `#AuthorsSubSection`
      </div>
<a name="module_components/pages.TestersSubSection"></a>

### components/pages.TestersSubSection() ⇒ [<code>react/Component</code>](#external_react/Component)
The Testers subsection

**Kind**: static method of [<code>components/pages</code>](#module_components/pages)  
**Returns**: [<code>react/Component</code>](#external_react/Component) - The Testers section  
    **Selector**:
      <div class="mermaid">
        `#TestersSection`
      </div>
<a name="module_components/pages.MetalabSection"></a>

### components/pages.MetalabSection() ⇒ [<code>react/Component</code>](#external_react/Component)
The Metalab section

**Kind**: static method of [<code>components/pages</code>](#module_components/pages)  
**Returns**: [<code>react/Component</code>](#external_react/Component) - The Metalab section  
    **Selector**:
      <div class="mermaid">
        `#MetalabSection`
      </div>
<a name="module_components/pages.AboutSection"></a>

### components/pages.AboutSection() ⇒ [<code>react/Component</code>](#external_react/Component)
The About section

**Kind**: static method of [<code>components/pages</code>](#module_components/pages)  
**Returns**: [<code>react/Component</code>](#external_react/Component) - The About section  
    **Selector**:
      <div class="mermaid">
        `#AboutSection`
      </div>
<a name="module_components/pages.SatLogo"></a>

### components/pages.SatLogo() ⇒ [<code>react/Component</code>](#external_react/Component)
Logo of the SAT

**Kind**: static method of [<code>components/pages</code>](#module_components/pages)  
**Returns**: [<code>react/Component</code>](#external_react/Component) - The SAT logo  
    **Selector**:
      <div class="mermaid">
        `#HelpPage .SatLogo`
      </div>
<a name="module_components/pages.HelpPage"></a>

### components/pages.HelpPage() ⇒ [<code>react/Component</code>](#external_react/Component)
Page that displays the help resources

**Kind**: static method of [<code>components/pages</code>](#module_components/pages)  
**Returns**: [<code>react/Component</code>](#external_react/Component) - The Help page  
    **Selector**:
      <div class="mermaid">
        `#HelpPage`
      </div>
<a name="module_components/pages.GeneralSettingsSection"></a>

### components/pages.GeneralSettingsSection() ⇒ [<code>react/Component</code>](#external_react/Component)
The section about the general settings

**Kind**: static method of [<code>components/pages</code>](#module_components/pages)  
**Returns**: [<code>react/Component</code>](#external_react/Component) - All general settings  
    **Selector**:
      <div class="mermaid">
        `#GeneralSettingsSection`
      </div>
<a name="module_components/pages.SipSettingsSection"></a>

### components/pages.SipSettingsSection() ⇒ [<code>react/Component</code>](#external_react/Component)
Section for all SIP settings

**Kind**: static method of [<code>components/pages</code>](#module_components/pages)  
**Returns**: [<code>react/Component</code>](#external_react/Component) - A section for all SIP settings  
    **Selector**:
      <div class="mermaid">
        `#SipSettingsSection`
      </div>
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| sipStore | [<code>SipStore</code>](#stores.SipStore) | Manage all SIP actions |
| sipCredentialStore | <code>stores.SipCredentialStore</code> | Store all SIP credentials |

<a name="module_components/pages.LivePage"></a>

### components/pages.LivePage(sceneStore) ⇒ [<code>react/Component</code>](#external_react/Component)
Page that displays all scenes

**Kind**: static method of [<code>components/pages</code>](#module_components/pages)  
**Returns**: [<code>react/Component</code>](#external_react/Component) - The live page  
    **Selector**:
      <div class="mermaid">
        `#LivePage`
      </div>

| Param | Type | Description |
| --- | --- | --- |
| sceneStore | <code>module:stores/userTree.SceneStore</code> | Store all scenes |

<a name="module_components/bars"></a>

## components/bars
Define all bar components

<a name="module_components/bars.MemoryUsageSection"></a>

### components/bars.MemoryUsageSection ⇒ [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Renders the status bar section for the memory usage

**Kind**: static constant of [<code>components/bars</code>](#module_components/bars)  
**Returns**: [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent) - The memory usage section  
<a name="module_components/bars.CpuUsageSection"></a>

### components/bars.CpuUsageSection ⇒ [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Renders the status bar section for the CPU usage

**Kind**: static constant of [<code>components/bars</code>](#module_components/bars)  
**Returns**: [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent) - The CPU usage section  
<a name="module_components/bars.NetworkInterfaceRx"></a>

### components/bars.NetworkInterfaceRx ⇒ [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Renders the RX rate of the selected network interface

**Kind**: static constant of [<code>components/bars</code>](#module_components/bars)  
**Returns**: [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent) - The RX rate  
<a name="module_components/bars.NetworkInterfaceTx"></a>

### components/bars.NetworkInterfaceTx ⇒ [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Renders the TX rate of the selected network interface

**Kind**: static constant of [<code>components/bars</code>](#module_components/bars)  
**Returns**: [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent) - The TX rate  
<a name="module_components/bars.NetworkInterfaceIp"></a>

### components/bars.NetworkInterfaceIp ⇒ [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Renders the IPv4 address of the selected network interface

**Kind**: static constant of [<code>components/bars</code>](#module_components/bars)  
**Returns**: [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent) - The IP address  
<a name="module_components/bars.NetworkInterfaceSelect"></a>

### components/bars.NetworkInterfaceSelect ⇒ [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Renders the select input for network interfaces

**Kind**: static constant of [<code>components/bars</code>](#module_components/bars)  
**Returns**: [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent) - The network interace select input  
<a name="module_components/bars.HelpSection"></a>

### components/bars.HelpSection ⇒ [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Renders the status bar section for the help message

**Kind**: static constant of [<code>components/bars</code>](#module_components/bars)  
**Returns**: [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent) - The help section  
<a name="module_components/bars.StatusBar"></a>

### components/bars.StatusBar ⇒ [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Renders the status bar of Scenic

**Kind**: static constant of [<code>components/bars</code>](#module_components/bars)  
**Returns**: [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent) - The status bar  
<a name="module_components/bars.CpuBar"></a>

### components/bars.CpuBar() ⇒ [<code>react/Component</code>](#external_react/Component)
Renders a bar that represents the status of a CPU

**Kind**: static method of [<code>components/bars</code>](#module_components/bars)  
**Returns**: [<code>react/Component</code>](#external_react/Component) - A CPU status bar  

| Param | Type | Description |
| --- | --- | --- |
| props.id | <code>string</code> | ID of the CPU core |
| props.core | <code>object</code> | Description of the CPU core |

<a name="module_components/bars.NetworkInterfaceSection"></a>

### components/bars.NetworkInterfaceSection() ⇒ [<code>react/Component</code>](#external_react/Component)
Renders the status bar section for the network interfaces

**Kind**: static method of [<code>components/bars</code>](#module_components/bars)  
**Returns**: [<code>react/Component</code>](#external_react/Component) - The network interface status  
<a name="module_components/wrappers"></a>

## components/wrappers
Define all wrapper components

<a name="module_components/wrappers.HelpWrapper"></a>

### components/wrappers.HelpWrapper(children, message) ⇒ [<code>react/Component</code>](#external_react/Component)
Wraps each component that displays a help message

**Kind**: static method of [<code>components/wrappers</code>](#module_components/wrappers)  
**Returns**: [<code>react/Component</code>](#external_react/Component) - The wrapped component  

| Param | Type | Description |
| --- | --- | --- |
| children | [<code>react/Component</code>](#external_react/Component) | Any kind of component |
| message | <code>string</code> | The help message |

<a name="module_components/modals"></a>

## components/modals
Define all modal components

<a name="module_components/modals.RtmpModal"></a>

### components/modals.RtmpModal ⇒ [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Scenic modal used to create an RTMP quiddity

**Kind**: static constant of [<code>components/modals</code>](#module_components/modals)  
**Returns**: [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent) - The RTMP modal  
    **Selector**:
      <div class="mermaid">
        `#RtmpModal`
      </div>
<a name="module_components/modals.RtmpModal.exports.UrlInput"></a>

#### RtmpModal.exports.UrlInput ⇒ [<code>react/Component</code>](#external_react/Component)
Renders the InputText for the RTMP URL

**Kind**: static constant of [<code>RtmpModal</code>](#module_components/modals.RtmpModal)  
**Returns**: [<code>react/Component</code>](#external_react/Component) - The RTMP URL text input  
    **Selector**:
      <div class="mermaid">
        `.UrlInput`
      </div>
<a name="module_components/modals.RtmpModal.exports.UrlField"></a>

#### RtmpModal.exports.UrlField ⇒ [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Renders the field for the RTMP URL

**Kind**: static constant of [<code>RtmpModal</code>](#module_components/modals.RtmpModal)  
**Returns**: [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent) - The RTMP URL text field  
<a name="module_components/modals.RtmpModal.exports.StreamKeyInput"></a>

#### RtmpModal.exports.StreamKeyInput ⇒ [<code>react/Component</code>](#external_react/Component)
Renders the InputPassword for the RTMP stream key

**Kind**: static constant of [<code>RtmpModal</code>](#module_components/modals.RtmpModal)  
**Returns**: [<code>react/Component</code>](#external_react/Component) - The RTMP stream key text input  
    **Selector**:
      <div class="mermaid">
        `.StreamKeyInput`
      </div>
<a name="module_components/modals.RtmpModal.StreamKeyField"></a>

#### RtmpModal.StreamKeyField ⇒ [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Renders the field for the RTMP stream key

**Kind**: static constant of [<code>RtmpModal</code>](#module_components/modals.RtmpModal)  
**Returns**: [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent) - The RTMP stream key input field  
<a name="module_components/modals.RtmpModal.exports.RtmpCancelButton"></a>

#### RtmpModal.exports.RtmpCancelButton ⇒ [<code>CancelButton</code>](#module_components/common.CancelButton)
Renders the Cancel Button for the Modal

**Kind**: static constant of [<code>RtmpModal</code>](#module_components/modals.RtmpModal)  
**Returns**: [<code>CancelButton</code>](#module_components/common.CancelButton) - The cancel button of the Modal  
<a name="module_components/modals.RtmpModal.exports.RtmpConfirmButton"></a>

#### RtmpModal.exports.RtmpConfirmButton ⇒ [<code>ConfirmButton</code>](#module_components/common.ConfirmButton)
Renders the Confirm Button for the Modal

**Kind**: static constant of [<code>RtmpModal</code>](#module_components/modals.RtmpModal)  
**Returns**: [<code>ConfirmButton</code>](#module_components/common.ConfirmButton) - The confirm button of the Modal  
<a name="module_components/modals.RtmpModal.exports.RtmpFooter"></a>

#### RtmpModal.exports.RtmpFooter ⇒ [<code>react/Component</code>](#external_react/Component)
Renders the Footer for the Modal

**Kind**: static constant of [<code>RtmpModal</code>](#module_components/modals.RtmpModal)  
**Returns**: [<code>react/Component</code>](#external_react/Component) - The footer of the Modal  
<a name="module_components/modals.RenameNicknameModal"></a>

### components/modals.RenameNicknameModal ⇒ [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Scenic modal used to confirm renaming of a nickname

**Kind**: static constant of [<code>components/modals</code>](#module_components/modals)  
**Returns**: [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent) - The RenameNickname modal  
    **Selector**:
      <div class="mermaid">
        `#RenameNicknameModal`
      </div>
<a name="module_components/modals.RenameNicknameModal.exports.ModalFooter"></a>

#### RenameNicknameModal.exports.ModalFooter ⇒ [<code>react/Component</code>](#external_react/Component)
Renders the footer of the RenameNicknameModal

**Kind**: static constant of [<code>RenameNicknameModal</code>](#module_components/modals.RenameNicknameModal)  
**Returns**: [<code>react/Component</code>](#external_react/Component) - The footer of the Modal  

| Param | Type | Description |
| --- | --- | --- |
| props.onCancel | <code>function</code> | Function triggered when the user cancels the rename nickname action |
| props.onRename | <code>function</code> | Function triggered when the user confirms the rename nickname action |

<a name="module_components/modals.exports.NewContactModal"></a>

### components/modals.exports.NewContactModal ⇒ [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Scenic modal used to create a new contact

**Kind**: static constant of [<code>components/modals</code>](#module_components/modals)  
**Returns**: [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent) - The modal  
    **Selector**:
      <div class="mermaid">
        `#NewContactModal`
      </div>
<a name="module_components/modals.SwitcherConnectionModal"></a>

### components/modals.SwitcherConnectionModal ⇒ [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Scenic modal used to connect to Switcher

**Kind**: static constant of [<code>components/modals</code>](#module_components/modals)  
**Returns**: [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent) - The connection modal  
    **Selector**:
      <div class="mermaid">
        `#SwitcherConnectionModal`
      </div>
<a name="module_components/modals.SwitcherConnectionModal.SWITCHER_CONNECTION_MODAL_ID"></a>

#### SwitcherConnectionModal.SWITCHER\_CONNECTION\_MODAL\_ID : <code>string</code>
ID of the configuration error modal

**Kind**: static constant of [<code>SwitcherConnectionModal</code>](#module_components/modals.SwitcherConnectionModal)  
<a name="module_components/modals.SwitcherConnectionModal.exports.UrlInput"></a>

#### SwitcherConnectionModal.exports.UrlInput ⇒ [<code>react/Component</code>](#external_react/Component)
Renders the InputText for the host URL

**Kind**: static constant of [<code>SwitcherConnectionModal</code>](#module_components/modals.SwitcherConnectionModal)  
**Returns**: [<code>react/Component</code>](#external_react/Component) - The host URL text input  
    **Selector**:
      <div class="mermaid">
        `.UrlInput`
      </div>
<a name="module_components/modals.SwitcherConnectionModal.UrlField"></a>

#### SwitcherConnectionModal.UrlField ⇒ [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Renders the input field for the host URL

**Kind**: static constant of [<code>SwitcherConnectionModal</code>](#module_components/modals.SwitcherConnectionModal)  
**Returns**: [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent) - The host URL field input  
<a name="module_components/modals.SwitcherConnectionModal.exports.PortInput"></a>

#### SwitcherConnectionModal.exports.PortInput ⇒ [<code>react/Component</code>](#external_react/Component)
Renders the InputText for the host port

**Kind**: static constant of [<code>SwitcherConnectionModal</code>](#module_components/modals.SwitcherConnectionModal)  
**Returns**: [<code>react/Component</code>](#external_react/Component) - The host port text input  
    **Selector**:
      <div class="mermaid">
        `.PortInput`
      </div>
<a name="module_components/modals.SwitcherConnectionModal.PortField"></a>

#### SwitcherConnectionModal.PortField ⇒ [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Renders the field for the host port

**Kind**: static constant of [<code>SwitcherConnectionModal</code>](#module_components/modals.SwitcherConnectionModal)  
**Returns**: [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent) - The host field input  
<a name="module_components/modals.SwitcherConnectionModal.exports.SwitcherConnectionButton"></a>

#### SwitcherConnectionModal.exports.SwitcherConnectionButton ⇒ <code>external:mobx-react/ObservedComponent</code>
Renders the Connect Button for the Modal

**Kind**: static constant of [<code>SwitcherConnectionModal</code>](#module_components/modals.SwitcherConnectionModal)  
**Returns**: <code>external:mobx-react/ObservedComponent</code> - The connect button of the Modal  
    **Selector**:
      <div class="mermaid">
        `.ConnectButton`
      </div>
<a name="module_components/modals.NewFileModal"></a>

### components/modals.NewFileModal() ⇒ [<code>react/Component</code>](#external_react/Component)
Scenic modal used to create a new file

**Kind**: static method of [<code>components/modals</code>](#module_components/modals)  
**Returns**: [<code>react/Component</code>](#external_react/Component) - The NewFile modal  
    **Selector**:
      <div class="mermaid">
        `#NewFileModal`
      </div>
<a name="loggerFunctions"></a>

## loggerFunctions : <code>Object.&lt;string, function()&gt;</code>
Allows mocking of a function called by another function in the same module

**Kind**: global constant  
**See**: [How to mock specific module function in jest](https://medium.com/@qjli/how-to-mock-specific-module-function-in-jest-715e39a391f4)  
<a name="SIP_DEFAULT_PORT"></a>

## SIP\_DEFAULT\_PORT
**Kind**: global constant  
<a name="PROPERTY_ID_REGEX"></a>

## PROPERTY\_ID\_REGEX : <code>RegExp</code>
Matches prefix and suffix pattern of property IDs

**Kind**: global constant  
<a name="List"></a>

## List
**Kind**: global constant  
**Todo**

- [ ] Find another way to handle these use cases instead of hardcoding this stuff

<a name="PageWrapper"></a>

## PageWrapper ⇒ [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Renders a wrapper that selects and renders the active page

**Kind**: global constant  
**Returns**: [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent) - - The page wrapper for an active page  
<a name="StartedCheckbox"></a>

## StartedCheckbox ⇒ [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Displays a checkbox in order to change the `started` property

**Kind**: global constant  
**Returns**: [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent) - - The started checkbox  
    **Selector**:
      <div class="mermaid">
        `.QuiddityCheckbox`
      </div>

| Param | Type | Description |
| --- | --- | --- |
| quiddity | [<code>Quiddity</code>](#models.Quiddity) | The quiddity model |

<a name="NicknameLabel"></a>

## NicknameLabel ⇒ [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Displays a nickname label for a source entry

**Kind**: global constant  
**Returns**: [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent) - - The nickname label of a quiddity  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| quiddityId | <code>string</code> |  | The ID of the quiddity to be labeled |
| [defaultLabel] | <code>string</code> | <code>&quot;&#x27;NO NICKNAME&quot;</code> | Fallback label if there is no label returned |

<a name="SendingStatus"></a>

## SendingStatus ⇒ [<code>react/Component</code>](#external_react/Component)
Renders the sending status of a SIP contact

**Kind**: global constant  
**Returns**: [<code>react/Component</code>](#external_react/Component) - A HTML description item  

| Param | Type | Description |
| --- | --- | --- |
| sendStatus | <code>object</code> | A SIP sending status |

<a name="ReceivingStatus"></a>

## ReceivingStatus ⇒ [<code>react/Component</code>](#external_react/Component)
Renders the receiving status of a SIP contact

**Kind**: global constant  
**Returns**: [<code>react/Component</code>](#external_react/Component) - A HTML description item  

| Param | Type | Description |
| --- | --- | --- |
| recvStatus | <code>object</code> | A SIP receiving status |

<a name="SipContactInformationTooltip"></a>

## SipContactInformationTooltip
Tooltip that displays all contact informations

**Kind**: global constant  
<a name="Capabilities"></a>

## Capabilities ⇒ [<code>react/Component</code>](#external_react/Component)
Renders all capabilities in a HTML definition list

**Kind**: global constant  
**Returns**: [<code>react/Component</code>](#external_react/Component) - A definition list of all capabilities  
    **Selector**:
      <div class="mermaid">
        `.Capabilities`
      </div>

| Param | Type | Description |
| --- | --- | --- |
| shmdata | <code>models/shmdata.Shmdata</code> | The shmdata that will render capabilities |

<a name="ShmdataInformationTooltip"></a>

## ShmdataInformationTooltip ⇒ [<code>react/Component</code>](#external_react/Component)
Renders the tooltip

**Kind**: global constant  
**Returns**: [<code>react/Component</code>](#external_react/Component) - A tooltip with the shmdata information  
    **Selector**:
      <div class="mermaid">
        `.ShmdataInformationTooltip[data-shmdata="shmdata id"][data-media="media type"]`
      </div>

| Param | Type | Description |
| --- | --- | --- |
| shmdata | <code>models/shmdata.Shmdata</code> | The shmdata that will render capabilities |
| children | [<code>react/Component</code>](#external_react/Component) | The children component of the tooltip |

<a name="SceneTabBar"></a>

## SceneTabBar ⇒ [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Renders the scene tab bar that contains all tabs of user created scenes and the default scene tab

**Kind**: global constant  
**Returns**: [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent) - The scene tab bar  
    **Selector**:
      <div class="mermaid">
        `#SceneTabBar`
      </div>
<a name="PageButton"></a>

## PageButton ⇒ [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Renders the menu button that triggers the page rendering

**Kind**: global constant  
**Returns**: [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent) - A button for each page  
    **Selector**:
      <div class="mermaid">
        `#PageMenu .PageButton`
      </div>

| Param | Type | Description |
| --- | --- | --- |
| pageId | <code>string</code> | ID of the page |
| pageStore | <code>module:stores/common.PageStore</code> | Store all pages |

<a name="QuiddityMenu"></a>

## QuiddityMenu ⇒ [<code>ui-components/Menu</code>](#external_ui-components/Menu)
A whole quiddity menu built from a menu collection

**Kind**: global constant  
**Returns**: [<code>ui-components/Menu</code>](#external_ui-components/Menu) - A menu collection of menus that creates quiddities  

| Param | Type | Description |
| --- | --- | --- |
| collection | <code>module:models/menus.MenuGroup</code> | A collection of menu models |
| addonBefore | [<code>react/Component</code>](#external_react/Component) | A component that prefixes the menu name |

<a name="PropertyField"></a>

## PropertyField ⇒ [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Renders the Field used to change a property according to its type

**Kind**: global constant  
**Returns**: [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent) - The property field
There is an edge case that enables the property for a locked quiddity when:
- It is a destination
- And it is a started property
Because, there is no other way to deactivate the RTMP and the NDI destinations  
    **Selector**:
      <div class="mermaid">
        `.PropertyField[data-quiddity="quiddity id"][data-nickname="quiddity nickname"][data-property="property id"]`
      </div>

| Param | Type | Description |
| --- | --- | --- |
| quiddityId | <code>string</code> | ID of the quiddity |
| propertyId | <code>string</code> | ID of the property |

<a name="PropertyInspectorDrawer"></a>

## PropertyInspectorDrawer ⇒ [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Drawer that displays all property of a selected quiddity

**Kind**: global constant  
**Returns**: [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent) - The property inspector drawer  
    **Selector**:
      <div class="mermaid">
        `#PropertyInspectorDrawer`
      </div>
<a name="ConfigurationModal"></a>

## ConfigurationModal
Scenic modal used to warn user of an invalid configuration

**Kind**: global constant  
<a name="ConfigurationModal.CONFIGURATION_MODAL_ID"></a>

### ConfigurationModal.CONFIGURATION\_MODAL\_ID : <code>string</code>
ID of the configuration error modal

**Kind**: static constant of [<code>ConfigurationModal</code>](#ConfigurationModal)  
<a name="PanelBar"></a>

## PanelBar ⇒ [<code>react/Component</code>](#external_react/Component)
Renders all matrix panels

**Kind**: global constant  
**Returns**: [<code>react/Component</code>](#external_react/Component) - The matrix page panels  
    **Selector**:
      <div class="mermaid">
        `.PanelBar`
      </div>
<a name="MenuBar"></a>

## MenuBar ⇒ [<code>react/Component</code>](#external_react/Component)
Renders the menu bar

**Kind**: global constant  
**Returns**: [<code>react/Component</code>](#external_react/Component) - The matrix page menu bar  
    **Selector**:
      <div class="mermaid">
        `.MenuBar`
      </div>
**Todo**

- [ ] Refactor FilterPanel and QuiddityMenuPanel as functionnal components

<a name="Scenes"></a>

## Scenes ⇒ [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Displays all scenes created by the user

**Kind**: global constant  
**Returns**: [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent) - All scenes rendered as Cells  

| Param | Type | Description |
| --- | --- | --- |
| sceneStore | <code>module:stores/userTree.SceneStore</code> | Store all scenes |

<a name="SeparatorLockIcon"></a>

## SeparatorLockIcon ⇒ [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Renders a lock icon at the top of the separator

**Kind**: global constant  
**Returns**: [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent) - The separator lock icon  

| Param | Type | Description |
| --- | --- | --- |
| matrixStore | [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore) | Store all matrix properties |
| categoryId | <code>string</code> | The destination category |

<a name="SeparatorHeader"></a>

## SeparatorHeader ⇒ [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Renders the header at the top of the separator

**Kind**: global constant  
**Returns**: [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent) - The separator header  

| Param | Type | Description |
| --- | --- | --- |
| matrixStore | [<code>MatrixStore</code>](#module_stores/matrix.MatrixStore) | Store all matrix properties |
| categoryId | <code>string</code> | The destination category |

<a name="PreviewWrapper"></a>

## PreviewWrapper
A preview windows for all video shmdatas

**Kind**: global constant  
    **Selector**:
      <div class="mermaid">
        `#PreviewWrapper[data-quiddity="quiddity id"][data-shmdata="shmdata path"]`
      </div>
<a name="DestinationHead"></a>

## DestinationHead ⇒ [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Displays a destination head for a destination quiddity

**Kind**: global constant  
**Returns**: [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent) - - The destination head of a quiddity  
    **Selector**:
      <div class="mermaid">
        `#DestinationHead`
      </div>

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| entry | [<code>MatrixEntry</code>](#models.MatrixEntry) |  | A matrix entry |
| [defaultLabel] | <code>string</code> | <code>&quot;&#x27;NO NICKNAME&quot;</code> | Fallback label if there is no label returned |

<a name="SourceStarter"></a>

## SourceStarter ⇒ [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Renders a checkbox that starts or stops a source

**Kind**: global constant  
**Returns**: [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent) - The starter checkbox  
    **Selector**:
      <div class="mermaid">
        `.SourceStarter[data-source="source id"][data-nickname="source nickname"]`
      </div>

| Param | Type | Description |
| --- | --- | --- |
| quiddity | <code>module:models/quiddity.Quiddity</code> | The source quiddity |

<a name="SourceHead"></a>

## SourceHead ⇒ [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Renders the head of the source

**Kind**: global constant  
**Returns**: [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent) - An entry component  
    **Selector**:
      <div class="mermaid">
        `.SourceHead[data-source="source id"][data-nickname="source nickname"]`
      </div>

| Param | Type | Description |
| --- | --- | --- |
| quiddity | <code>module:models/quiddity.Quiddity</code> | The source quiddity |
| shmdata | <code>module:models/shmdata.Shmdata</code> | The source shmdata |

<a name="Source"></a>

## Source ⇒ [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Renders a source row in the matrix

**Kind**: global constant  
**Returns**: [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent) - A source component for the matrix  

| Param | Type | Description |
| --- | --- | --- |
| entry | <code>modules:models/matrix.MatrixEntry</code> | An entry of the matrix |

<a name="QuiddityMenuPanel"></a>

## QuiddityMenuPanel ⇒ [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Displays a menu panel for quiddities (sources, destinations and compressions)

**Kind**: global constant  
**Returns**: [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent) - - The nickname label of a quiddity  
    **Selector**:
      <div class="mermaid">
        `#QuiddityMenuPanel`
      </div>
<a name="SaveSessionAsMenu"></a>

## SaveSessionAsMenu ⇒ <code>module.components/common.MenuButton</code>
Renders the button that opens the FileSavingDrawer

**Kind**: global constant  
**Returns**: <code>module.components/common.MenuButton</code> - A menu button  
    **Selector**:
      <div class="mermaid">
        `#SaveAsSessionMenu`
      </div>
<a name="OpenSessionMenu"></a>

## OpenSessionMenu ⇒ <code>module.components/common.MenuButton</code>
Renders the button that opens the FileLoadingDrawer

**Kind**: global constant  
**Returns**: <code>module.components/common.MenuButton</code> - A menu button  
    **Selector**:
      <div class="mermaid">
        `#OpenSessionMenu`
      </div>
<a name="DeleteSessionMenu"></a>

## DeleteSessionMenu ⇒ <code>module.components/common.MenuButton</code>
Renders the button that opens the FileDeletionDrawer

**Kind**: global constant  
**Returns**: <code>module.components/common.MenuButton</code> - A menu button  
    **Selector**:
      <div class="mermaid">
        `#DeleteMenuButton`
      </div>
<a name="FileControlPanel"></a>

## FileControlPanel
The file control panel provides all the actions for the session files

**Kind**: global constant  
<a name="OrderButtonRight"></a>

## OrderButtonRight
Renders the right order button

**Kind**: global constant  
<a name="OrderButtonLeft"></a>

## OrderButtonLeft
Renders the left order button

**Kind**: global constant  
<a name="OrderButtonUp"></a>

## OrderButtonUp
Renders the upward order button

**Kind**: global constant  
<a name="OrderButtonDown"></a>

## OrderButtonDown
Renders the downward order button

**Kind**: global constant  
<a name="OrderPanel"></a>

## OrderPanel ⇒ [<code>react/Component</code>](#external_react/Component)
Renders the control panel for the quiddity orders

**Kind**: global constant  
**Returns**: [<code>react/Component</code>](#external_react/Component) - The order panel only when the user has selected a quiddity  
<a name="PropertyInspectorMenu"></a>

## PropertyInspectorMenu ⇒ <code>module.components/common.MenuButton</code>
Renders the button that opens the PropertyInspectorDrawer

**Kind**: global constant  
**Returns**: <code>module.components/common.MenuButton</code> - A menu button  
    **Selector**:
      <div class="mermaid">
        `#PropertyInspectorMenu`
      </div>
<a name="SipDrawersMenu"></a>

## SipDrawersMenu ⇒ <code>module.components/common.MenuButton</code>
Renders the button that opens the Sip drawers

**Kind**: global constant  
**Returns**: <code>module.components/common.MenuButton</code> - A menu button  
    **Selector**:
      <div class="mermaid">
        `#SipDrawersMenu`
      </div>
<a name="FilterPanel"></a>

## FilterPanel ⇒ [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Displays a filter panel for quiddity categories

**Kind**: global constant  
**Returns**: [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent) - - The filter panel for sources and destinations  
    **Selector**:
      <div class="mermaid">
        `#FilterPanel`
      </div>
<a name="SceneNameInput"></a>

## SceneNameInput
The Scene control panel gives all controls for Scene modification (name, status)

**Kind**: global constant  
<a name="SceneActivationCheckbox"></a>

## SceneActivationCheckbox ⇒ <code>external:ui-components/Inputs.Checkbox</code>
Renders scene activation checkbox

**Kind**: global constant  
**Returns**: <code>external:ui-components/Inputs.Checkbox</code> - The scene activation checkbox  
    **Selector**:
      <div class="mermaid">
        `#SceneControlPanel .Checkbox`
      </div>
<a name="dropDecimalUnit"></a>

## dropDecimalUnit(number, exponent) ⇒ <code>number</code>
Convert a decimal value to a lower metric in the SI

**Kind**: global function  
**Returns**: <code>number</code> - The converted value  

| Param | Type | Description |
| --- | --- | --- |
| number | <code>number</code> | The number value to convert |
| exponent | <code>number</code> | The base 10 exponent of the conversion |

<a name="raiseDecimalUnit"></a>

## raiseDecimalUnit(number, exponent) ⇒ <code>number</code>
Convert a decimal value to a greater metric in the SI

**Kind**: global function  
**Returns**: <code>number</code> - The converted value  

| Param | Type | Description |
| --- | --- | --- |
| number | <code>number</code> | The number value to convert |
| exponent | <code>number</code> | The base 10 exponent of the conversion |

<a name="clamp"></a>

## clamp(number, threshold) ⇒ <code>number</code>
Constrains a number to a specific maximum value.

**Kind**: global function  
**Returns**: <code>number</code> - The new clamped number  

| Param | Type | Description |
| --- | --- | --- |
| number | <code>number</code> | The number to clamp |
| threshold | <code>number</code> | Maximum possible value for the number |

<a name="getLogStore"></a>

## getLogStore(logBindings) ⇒ <code>string</code>
Gets the store name from a log event

**Kind**: global function  
**Returns**: <code>string</code> - The store emitter  

| Param | Type | Description |
| --- | --- | --- |
| logBindings | [<code>pino/logBindings</code>](#external_pino/logBindings) | All bindings transmitted by a child logger |

<a name="useNotificationStore"></a>

## useNotificationStore() ⇒ [<code>NotificationStore</code>](#module_stores/common.NotificationStore)
Global getter for the notification store

**Kind**: global function  
**Returns**: [<code>NotificationStore</code>](#module_stores/common.NotificationStore) - The global notification store of Scenic  
<a name="applyNotificationLog"></a>

## applyNotificationLog(logLevel, logEvent)
Sends a log message to the notification store

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| logLevel | [<code>pino/level</code>](#external_pino/level) | Level of the logging |
| logEvent | [<code>pino/logEvent</code>](#external_pino/logEvent) | Transmitted log event |

<a name="cleanLogEvent"></a>

## cleanLogEvent() ⇒ <code>object</code>
Cleans the logEvent object to be sent via the active socket and adds the sessionId property
{external:pino/logEvent} logEvent - Transmitted log event

**Kind**: global function  
**Returns**: <code>object</code> - cleanedLogEvent - The cleaned log object  
<a name="applySocketLog"></a>

## applySocketLog(logEvent)
Sends all clean logs through the active socket

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| logEvent | [<code>pino/logEvent</code>](#external_pino/logEvent) | Transmitted log event |

<a name="uploadFile"></a>

## uploadFile(localFile, onUploadFinished)
Helper that uploads a selected local file

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| localFile | <code>Blob</code> | The local file's blob |
| onUploadFinished | <code>function</code> | Function triggered when the upload is finished |

<a name="downloadFile"></a>

## downloadFile(linkRef, onDownloadStarted, [dataType])
Helper that downloads some content from a link

**Kind**: global function  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| linkRef | <code>React.Ref</code> |  | A reference to the component that starts the download process |
| onDownloadStarted | <code>function</code> |  | Function triggered when the donwload is started. It must return the downloadable content |
| [dataType] | <code>string</code> | <code>&quot;&#x27;text/plain&#x27;&quot;</code> | Type of the downloaded data |

<a name="hasDebugMode"></a>

## hasDebugMode() ⇒ <code>boolean</code>
Checks if the debug mode is enabled for Scenic

**Kind**: global function  
**Returns**: <code>boolean</code> - Returns true if the debug mode is enabled  
<a name="debugStores"></a>

## debugStores(stores) ⇒ <code>Object</code>
Gets all store statements in JSON object

**Kind**: global function  
**Returns**: <code>Object</code> - A JSON object with all stores  

| Param | Type | Description |
| --- | --- | --- |
| stores | <code>Object</code> | A map that contains all stores |

<a name="isObject"></a>

## isObject(value) ⇒ <code>boolean</code>
Test if a given value is a Javascript Object or not

**Kind**: global function  
**Returns**: <code>boolean</code> - Flags true if value is an object  

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | Value to test |

<a name="arrayToObject"></a>

## arrayToObject(array, keyField) ⇒ <code>Object</code>
Convert an array of object to an object

**Kind**: global function  
**Returns**: <code>Object</code> - Produced object  

| Param | Type | Description |
| --- | --- | --- |
| array | <code>Array.&lt;Object&gt;</code> | Array to convert |
| keyField | <code>string</code> | Object property which will be the keys of the new object |

<a name="stripEmptyArrays"></a>

## stripEmptyArrays(object) ⇒ <code>Object</code>
Remove keys from an object whose values are empty arrays

**Kind**: global function  
**Returns**: <code>Object</code> - Object without key-value pairs containing empty arrays  

| Param | Type | Description |
| --- | --- | --- |
| object | <code>Object</code> | Object to process |

<a name="getFromSwitcherTreePath"></a>

## getFromSwitcherTreePath(object, treePath) ⇒ <code>Object</code>
Gets the object situated at the specified switcher path. A switcher path
   is a string of "words.with.periods.and.sometimes.1.numbers". The object is traversed with every
   word between the periods representing a key in an object.
   This returns a copy of the object, mutating it won't affect the original object

**Kind**: global function  
**Returns**: <code>Object</code> - The object situated at the end of the string path.  

| Param | Type | Description |
| --- | --- | --- |
| object | <code>Object</code> | Object to process |
| treePath | <code>Object</code> | string of the tree path to use. |

<a name="getMutatedFromSwitcherTreePath"></a>

## getMutatedFromSwitcherTreePath(object, treePath) ⇒ <code>Object</code>
return a copy of the passed object with the specified assignation.
   the object situated at the specified switcher path to the specified value.
   this will create the missing obects/arrays along the specified path, creating
   object when it needs to set something at a non positive integer key and arrays
   when it needs to set at positive integer keys.

   Example for the creation of non existing objects/arrays : if the object is {}, path
   is some.stuff.1 and the value is "test", this code will first set the some key to an empty object
   because the next path (stuff) is not a positive integer. it will then stuff key of the some object
   to an empty array because the next key is a positive integer and will set the value "test" to
   the index 1 of the array. resulting in the following object :
   {some : {
   stuff:[<empty>,"test"]
   }
   }

**Kind**: global function  
**Returns**: <code>Object</code> - The object situated at the end of the string path.  

| Param | Type | Description |
| --- | --- | --- |
| object | <code>Object</code> | Object to process |
| treePath | <code>Object</code> | string of the tree path to use. |

<a name="getPrunedFromSwitcherTreePath"></a>

## getPrunedFromSwitcherTreePath(object, treePath) ⇒ <code>Object</code>
Takes an object and a switcher tree path, clones the object and deletes all the values situated at and after
   the specified path in the clone object. Returns the entire cloned object minus the pruned parts.

**Kind**: global function  
**Returns**: <code>Object</code> - A copy of original object pruned at the specified path.  

| Param | Type | Description |
| --- | --- | --- |
| object | <code>Object</code> | Object to process |
| treePath | <code>Object</code> | string of the tree path to prune. |

<a name="useHover"></a>

## useHover() ⇒ [<code>react/customHook</code>](#external_react/customHook)
Custom React hook that simulates the hover event

**Kind**: global function  
**Returns**: [<code>react/customHook</code>](#external_react/customHook) - A react hook  
<a name="escapeAsArray"></a>

## escapeAsArray(output) ⇒ <code>Array.&lt;string&gt;</code>
Escape all new lines into an array

**Kind**: global function  
**Returns**: <code>Array.&lt;string&gt;</code> - Array of escaped strings  

| Param | Type | Description |
| --- | --- | --- |
| output | <code>string</code> | A multilines command line output |

<a name="escapeToSafeCss"></a>

## escapeToSafeCss()
Gets a version of the string that is sanitized for CSS purposes.
We need this function because we use URIs as category id for sip quiddities and the grid css breaks
if invalid or unescaped characters are in css properties.
see https://www.w3.org/TR/CSS21/syndata.html#characters for the why of the different regexes

**Kind**: global function  
    **Params**:
      <code>string</code>      <div class="mermaid">
         input
      </div>
<a name="toString"></a>

## toString(input) ⇒ <code>string</code>
Transform any value to a string, `null` and `undefined` values are converted to an empty string.

**Kind**: global function  
**Returns**: <code>string</code> - A string representation of the input  

| Param | Type | Description |
| --- | --- | --- |
| input | <code>boolean</code> \| <code>number</code> \| <code>string</code> | Value to transform into string |

<a name="isStringEmpty"></a>

## isStringEmpty(value) ⇒ <code>boolean</code>
Checks if a string is empty

**Kind**: global function  
**Returns**: <code>boolean</code> - True, if the value is empty  

| Param | Type | Description |
| --- | --- | --- |
| value | <code>string</code> | String to validate |

<a name="cleanParenthesis"></a>

## cleanParenthesis(input) ⇒ <code>string</code>
Cleans the parenthesis in a string

**Kind**: global function  
**Returns**: <code>string</code> - A string without parenthesis  

| Param | Type | Description |
| --- | --- | --- |
| input | <code>string</code> | The string to clean |

<a name="replaceAll"></a>

## replaceAll(input, [oldStr], [newStr]) ⇒ <code>string</code>
Sanitizes a string by replacing all matches with another character

**Kind**: global function  
**Returns**: <code>string</code> - A string with dots  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| input | <code>string</code> |  | The string to sanitize |
| [oldStr] | <code>string</code> | <code>&quot;&#x27;.&#x27;&quot;</code> | The character to replace |
| [newStr] | <code>string</code> | <code>&quot;&#x27;_&#x27;&quot;</code> | The character to replace with |

<a name="parseDate"></a>

## parseDate(input) ⇒ <code>Date</code>
Parses a string date to a javascript Date object

**Kind**: global function  
**Returns**: <code>Date</code> - A Date object  
**See**: [JavaScript Date documentation](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date)  

| Param | Type | Description |
| --- | --- | --- |
| input | <code>string</code> | A stringified date |

<a name="capitalize"></a>

## capitalize(input) ⇒ <code>string</code>
Capitalizes the input string. Only the very first letter is capitalized.

**Kind**: global function  
**Returns**: <code>string</code> - A capitalized string  

| Param | Type | Description |
| --- | --- | --- |
| input | <code>string</code> | The string to capitalize |

<a name="toOrderButtonId"></a>

## toOrderButtonId(orderId) ⇒ <code>string</code>
Transform order directions

**Kind**: global function  
**Returns**: <code>string</code> - ID for the order buttons  

| Param | Type | Description |
| --- | --- | --- |
| orderId | <code>string</code> | Unique order direction |

<a name="toNumber"></a>

## toNumber(unit, value) ⇒ <code>number</code>
Converts an unit to a displayable number

**Kind**: global function  
**Returns**: <code>number</code> - The converted value  

| Param | Type | Description |
| --- | --- | --- |
| unit | <code>model.UnitEnum</code> |  |
| value | <code>number</code> | The value in a specific unit |

<a name="fromNumber"></a>

## fromNumber(unit, convertedValue) ⇒ <code>number</code>
Converts displayed number to its original unit

**Kind**: global function  
**Returns**: <code>number</code> - The value in its unit  

| Param | Type | Description |
| --- | --- | --- |
| unit | <code>model.UnitEnum</code> |  |
| convertedValue | <code>number</code> | The value used in the UI |

<a name="fromPath"></a>

## fromPath(path) ⇒ <code>string</code>
Get a config name from its paths

**Kind**: global function  
**Returns**: <code>string</code> - The configuration name  

| Param | Type | Description |
| --- | --- | --- |
| path | <code>string</code> | Path of the configuration file |

<a name="toPageIcon"></a>

## toPageIcon(pageId) ⇒ <code>string</code>
Gets the icon type of a page

**Kind**: global function  
**Returns**: <code>string</code> - Type of the icon's page  

| Param | Type | Description |
| --- | --- | --- |
| pageId | <code>string</code> | ID of the page |

<a name="toDestinationKind"></a>

## toDestinationKind(matrixCategory) ⇒ <code>string</code>
Gets the corresponding quiddity kind from a matrix category

**Kind**: global function  
**Returns**: <code>string</code> - A corresponding quiddity kind  

| Param | Type | Description |
| --- | --- | --- |
| matrixCategory | <code>module:models/matrix.MatrixCategoryEnum</code> | A matrix category |

<a name="toMatrixCategory"></a>

## toMatrixCategory(kindId) ⇒ <code>string</code>
Gets the corresponding category from quiddity kind

**Kind**: global function  
**Returns**: <code>string</code> - A corresponding destination category  

| Param | Type | Description |
| --- | --- | --- |
| kindId | <code>string</code> | A quiddity kind ID |

<a name="populateStoreCreationBindings"></a>

## populateStoreCreationBindings() ⇒ <code>Map.&lt;string, function()&gt;</code>
Creates creation bindings for quiddity-related Stores and sets them inside the QuiddityStore

**Kind**: global function  
**Returns**: <code>Map.&lt;string, function()&gt;</code> - All bindings that create quiddities  
**Todo**

- [ ] [swIO] retry the special quiddities

<a name="populateModalCreationBindings"></a>

## populateModalCreationBindings() ⇒ <code>Map.&lt;string, function()&gt;</code>
Creates creation bindings for modal-related Stores and sets them inside the QuiddityStore

**Kind**: global function  
**Returns**: <code>Map.&lt;string, function()&gt;</code> - All bindings that display contextual modals  
**Todo**

- [ ] [swIO] retry the special quiddities

<a name="populateStores"></a>

## populateStores() ⇒ <code>object</code>
Creates all the app's Stores

**Kind**: global function  
**Returns**: <code>object</code> - Object with all stores used by Scenic  
<a name="initializeStores"></a>

## initializeStores(stores) ⇒ <code>boolean</code>
Initializes the main stores: ConfigStore and QuiddityStore.

ConnectionStore and QuiddityMenuStore will be initialized in reaction to
the SceneStore and QuiddityStore's initialization, respectively.

Most other Stores will be initialized when their associated quiddity is created.

**Kind**: global function  
**Returns**: <code>boolean</code> - Flags true if app is well initialized  

| Param | Type | Description |
| --- | --- | --- |
| stores | <code>object</code> | All populated stores |

<a name="deinitializeStores"></a>

## deinitializeStores(stores)
Cleans up the application stores

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| stores | <code>object</code> | All populated stores |

<a name="initializeSocket"></a>

## initializeSocket(stores)
Initializes the socket handlers

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| stores | <code>object</code> | All populated stores |

<a name="Lock"></a>

## Lock() ⇒ <code>external:ui-components/Icon</code>
Renders a lock icon

**Kind**: global function  
**Returns**: <code>external:ui-components/Icon</code> - A styled lock icon  
    **Selector**:
      <div class="mermaid">
        `.Lock`
      </div>
<a name="ConnectionLock"></a>

## ConnectionLock() ⇒ <code>external:ui-components/Icon</code>
Renders alock icon that fits with Connection boxes

**Kind**: global function  
**Returns**: <code>external:ui-components/Icon</code> - A styled lock icon  
    **Selector**:
      <div class="mermaid">
        `.ConnectionLock`
      </div>
<a name="GridLayout"></a>

## GridLayout(children) ⇒ [<code>react/Component</code>](#external_react/Component)
GridLayout adapted for the NumberField

**Kind**: global function  
**Returns**: [<code>react/Component</code>](#external_react/Component) - A grid layout  

| Param | Type | Description |
| --- | --- | --- |
| children | [<code>react/Component</code>](#external_react/Component) | All components in the grid |

<a name="AvailabilityStatus"></a>

## AvailabilityStatus(buddyStatus) ⇒ [<code>react/Component</code>](#external_react/Component)
Renders the availability status of a SIP contact

**Kind**: global function  
**Returns**: [<code>react/Component</code>](#external_react/Component) - A HTML description item  

| Param | Type | Description |
| --- | --- | --- |
| buddyStatus | <code>object</code> | A SIP buddy status |

<a name="Information"></a>

## Information(buddyStatus, sendStatus, recvStatus) ⇒ [<code>react/Component</code>](#external_react/Component)
Renders the SIP contact information

**Kind**: global function  
**Returns**: [<code>react/Component</code>](#external_react/Component) - A HTML description list  

| Param | Type | Description |
| --- | --- | --- |
| buddyStatus | <code>object</code> | A SIP buddy status |
| sendStatus | <code>object</code> | A SIP sending status |
| recvStatus | <code>object</code> | A SIP receiving status |

<a name="LockedDestinationWarning"></a>

## LockedDestinationWarning(categoryId) ⇒ [<code>react/Component</code>](#external_react/Component)
Displays the content of the tooltip

**Kind**: global function  
**Returns**: [<code>react/Component</code>](#external_react/Component) - The content of the tooltip  

| Param | Type | Description |
| --- | --- | --- |
| categoryId | <code>string</code> | Name of the category |

<a name="LockedDestinationTooltip"></a>

## LockedDestinationTooltip(children, categoryId) ⇒ <code>external:ui-components/Feedback.Tooltip</code>
Displays a tooltip with information about locked destinations

**Kind**: global function  
**Returns**: <code>external:ui-components/Feedback.Tooltip</code> - A tooltip  

| Param | Type | Description |
| --- | --- | --- |
| children | [<code>react/Component</code>](#external_react/Component) | Wrapped component by the tooltip |
| categoryId | <code>string</code> | Name of the category |

<a name="Capability"></a>

## Capability(capsKey, value) ⇒ [<code>react/Component</code>](#external_react/Component)
Renders a capability entry

**Kind**: global function  
**Returns**: [<code>react/Component</code>](#external_react/Component) - A capability rendered as a HTML definition entry  

| Param | Type | Description |
| --- | --- | --- |
| capsKey | <code>string</code> | The key of the capability |
| value | <code>string</code> \| <code>number</code> | The value of the capability |

<a name="CapabilityGroup"></a>

## CapabilityGroup(groupTitle, groupValue) ⇒ [<code>react/Component</code>](#external_react/Component)
Renders a capability group

**Kind**: global function  
**Returns**: [<code>react/Component</code>](#external_react/Component) - A capability rendered as a HTML definition entry  
    **Selector**:
      <div class="mermaid">
        `.CapabilityGroup`
      </div>

| Param | Type | Description |
| --- | --- | --- |
| groupTitle | <code>string</code> | The title of the capability group |
| groupValue | <code>Map.&lt;string, string&gt;</code> | The capabilities of the group |

<a name="BlankAddon"></a>

## BlankAddon()
Renders a blank addon

**Kind**: global function  
<a name="PageIcon"></a>

## PageIcon(pageId) ⇒ [<code>react/Component</code>](#external_react/Component)
Renders the page's icon

**Kind**: global function  
**Returns**: [<code>react/Component</code>](#external_react/Component) - Icon of the page  
    **Selector**:
      <div class="mermaid">
        `#PageMenu .PageIcon`
      </div>

| Param | Type | Description |
| --- | --- | --- |
| pageId | <code>string</code> | ID of the page |

<a name="PageTitle"></a>

## PageTitle(pageId) ⇒ [<code>react/Component</code>](#external_react/Component)
Renders the page's title

**Kind**: global function  
**Returns**: [<code>react/Component</code>](#external_react/Component) - Title of the page  
    **Selector**:
      <div class="mermaid">
        `#PageMenu .PageTitle`
      </div>

| Param | Type | Description |
| --- | --- | --- |
| pageId | <code>string</code> | ID of the page |

<a name="ScenicSignature"></a>

## ScenicSignature() ⇒ [<code>react/Component</code>](#external_react/Component)
The signature of Scenic

**Kind**: global function  
**Returns**: [<code>react/Component</code>](#external_react/Component) - The Scenic logo  
    **Selector**:
      <div class="mermaid">
        `#PageMenu .ScenicSignature`
      </div>
<a name="PageMenu"></a>

## PageMenu(pageId, pageStore) ⇒ [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent)
Menu that renders all main menus of the Scenic app

**Kind**: global function  
**Returns**: [<code>mobx-react/ObserverComponent</code>](#external_mobx-react/ObserverComponent) - The page menu  
    **Selector**:
      <div class="mermaid">
        `#PageMenu`
      </div>

| Param | Type | Description |
| --- | --- | --- |
| pageId | <code>string</code> | ID of the page |
| pageStore | <code>module:stores/common.PageStore</code> | Store all pages |

<a name="QuiddityMenuFactory"></a>

## QuiddityMenuFactory(menuModel) ⇒ [<code>react/Component</code>](#external_react/Component)
Abstract wrapper that builds a menu item or group

**Kind**: global function  
**Returns**: [<code>react/Component</code>](#external_react/Component) - A menu group or a menu item  
**Throws**:

- <code>TypeError</code> Argument `menuModel` must be a MenuItem or a MenuGroup model


| Param | Type | Description |
| --- | --- | --- |
| menuModel | <code>module:models/menus.MenuGroup</code> \| <code>module:models/menus.MenuItem</code> | Model for menu group or a menu item |

<a name="OpenSessionModal"></a>

## OpenSessionModal() ⇒ <code>React.PureComponent</code>
Scenic modal used to open a new session

**Kind**: global function  
**Returns**: <code>React.PureComponent</code> - A modal  

| Param | Type | Description |
| --- | --- | --- |
| props.visible | <code>boolean</code> | Flags if the modal is visible |
| props.onCancel | <code>function</code> | Function triggered when the user cancels the session action |
| props.onConfirm | <code>function</code> | Function triggered when the user confirms the session action |

<a name="OpenSessionModal.OPEN_SESSION_MODAL_ID"></a>

### OpenSessionModal.OPEN\_SESSION\_MODAL\_ID : <code>string</code>
ID of the open session modal

**Kind**: static constant of [<code>OpenSessionModal</code>](#OpenSessionModal)  
<a name="ResetSessionModal"></a>

## ResetSessionModal() ⇒ <code>React.PureComponent</code>
Scenic modal used to reset the current session

**Kind**: global function  
**Returns**: <code>React.PureComponent</code> - A modal  

| Param | Type | Description |
| --- | --- | --- |
| props.visible | <code>boolean</code> | Flags if the modal is visible |
| props.onCancel | <code>function</code> | Function triggered when the user cancels the session reset |
| props.onReset | <code>function</code> | Function triggered when the user resets the current session |

<a name="ResetSessionModal.RESET_SESSION_MODAL_ID"></a>

### ResetSessionModal.RESET\_SESSION\_MODAL\_ID : <code>string</code>
ID of the reset session modal

**Kind**: static constant of [<code>ResetSessionModal</code>](#ResetSessionModal)  
<a name="TrashCurrentSessionModal"></a>

## TrashCurrentSessionModal() ⇒ <code>React.PureComponent</code>
Renders the modal that alerts the deletion of current session's file

**Kind**: global function  
**Returns**: <code>React.PureComponent</code> - The alert modal  

| Param | Type | Description |
| --- | --- | --- |
| props.visible | <code>boolean</code> | Flags if the current session's file is trashed |
| props.onCancel | <code>function</code> | Function triggered when the action is canceled |
| props.onConfirm | <code>function</code> | Function triggered when the user tashes the current file |

<a name="RowEntry"></a>

## RowEntry(className, title, subtitle, status, selected, disabled, onClick, width) ⇒ [<code>react/Component</code>](#external_react/Component)
A generic entry that should be display in a column or as a row header

**Kind**: global function  
**Returns**: [<code>react/Component</code>](#external_react/Component) - A row entry  
    **Selector**:
      <div class="mermaid">
        `.RowEntry[data-entry="entry title"]`
      </div>

| Param | Type | Description |
| --- | --- | --- |
| className | <code>string</code> | Extra class name of the component |
| title | <code>string</code> | Title of the entry |
| subtitle | <code>string</code> | Subtitle of the entry |
| status | <code>string</code> | Status of the entry |
| selected | <code>boolean</code> | Flags a selected entry |
| disabled | <code>boolean</code> | Flags a disabled entry |
| onClick | <code>function</code> | Function triggered when the entry is clicked |
| width | <code>number</code> | Set a custom width of the entry |

<a name="FileEntry"></a>

## FileEntry() ⇒ <code>React.PureComponent</code>
Renders an entry that represents a file

**Kind**: global function  
**Returns**: <code>React.PureComponent</code> - An entry  

| Param | Type | Description |
| --- | --- | --- |
| props.file | [<code>FileBridge</code>](#models.FileBridge) | A file model |
| props.onClick | <code>function</code> | Function triggered when the entry is clicked |
| props.isCurrent | <code>boolean</code> | Flags if the file represents the current session |
| props.selected | <code>boolean</code> | Flags if the entry is selected |
| props.status | <code>models.StatusEnum</code> | Status of the entry |

<a name="Link"></a>

## Link(href, children) ⇒ [<code>react/Component</code>](#external_react/Component)
A link component that opens page in a new tab

**Kind**: global function  
**Returns**: [<code>react/Component</code>](#external_react/Component) - A tweaked HTML link  

| Param | Type | Description |
| --- | --- | --- |
| href | <code>string</code> | URL of the link |
| children | <code>node</code> | Content of the link |

<a name="PageTitle"></a>

## PageTitle() ⇒ [<code>react/Component</code>](#external_react/Component)
The page title

**Kind**: global function  
**Returns**: [<code>react/Component</code>](#external_react/Component) - The page title  
<a name="TabBar"></a>

## TabBar() ⇒ [<code>react/Component</code>](#external_react/Component)
Renders the tab bar for the Matrix

**Kind**: global function  
**Returns**: [<code>react/Component</code>](#external_react/Component) - The matrix page Tab bar  
    **Selector**:
      <div class="mermaid">
        `.TabBar`
      </div>
**Todo**

- [ ] Refactor SceneTabBar as functionnal components

<a name="SideBar"></a>

## SideBar() ⇒ [<code>react/Component</code>](#external_react/Component)
Renders the side bar

**Kind**: global function  
**Returns**: [<code>react/Component</code>](#external_react/Component) - The matrix side bar  
    **Selector**:
      <div class="mermaid">
        `.SideBar`
      </div>
<a name="DrawerOverlay"></a>

## DrawerOverlay() ⇒ [<code>react/Component</code>](#external_react/Component)
Renders all drawers used in the Matrix

**Kind**: global function  
**Returns**: [<code>react/Component</code>](#external_react/Component) - All wrapped drawers  
    **Selector**:
      <div class="mermaid">
        `.DrawerOverlay`
      </div>
<a name="Scene"></a>

## Scene(scene, onClick) ⇒ <code>external:ui-components/Layout.Cell</code>
Renders a scene as a Cell

**Kind**: global function  
**Returns**: <code>external:ui-components/Layout.Cell</code> - A Cell component  

| Param | Type | Description |
| --- | --- | --- |
| scene | <code>module:models/userTree.Scene</code> | A scene model |
| onClick | <code>function</code> | Function triggered when the user click on the Cell |

<a name="SeparatorTitle"></a>

## SeparatorTitle(title) ⇒ [<code>react/Component</code>](#external_react/Component)
Renders the separator title

**Kind**: global function  
**Returns**: [<code>react/Component</code>](#external_react/Component) - The rendered title  

| Param | Type | Description |
| --- | --- | --- |
| title | <code>string</code> | Title of the separator |

<a name="ColumnSeparator"></a>

## ColumnSeparator(style, header, color) ⇒ [<code>react/Component</code>](#external_react/Component)
Renders the UI separator between matrix columns

**Kind**: global function  
**Returns**: [<code>react/Component</code>](#external_react/Component) - The rendered separator  

| Param | Type | Description |
| --- | --- | --- |
| style | <code>object</code> | Custom style of the separator |
| header | <code>string</code> | Custom header at the top of the separator |
| color | <code>string</code> | Custom color of the separator |

<a name="EmptySourceColumnArea"></a>

## EmptySourceColumnArea() ⇒ [<code>react/Component</code>](#external_react/Component)
Renders a column area without source

**Kind**: global function  
**Returns**: [<code>react/Component</code>](#external_react/Component) - An empty column  
<a name="SourceSeparator"></a>

## SourceSeparator(style, header) ⇒ [<code>react/Component</code>](#external_react/Component)
Renders the UI separator between matrix columns

**Kind**: global function  
**Returns**: [<code>react/Component</code>](#external_react/Component) - The rendered separator  

| Param | Type | Description |
| --- | --- | --- |
| style | <code>object</code> | Custom style of the separator |
| header | <code>string</code> | Custom header at the top of the separator |

<a name="SourceShmdata"></a>

## SourceShmdata(entry) ⇒ <code>module:components/matrix.ShmdataBox</code>
Renders a shmdata box related to a source

**Kind**: global function  
**Returns**: <code>module:components/matrix.ShmdataBox</code> - The shmdata box  
    **Selector**:
      <div class="mermaid">
        `.SourceShmdata[data-shmdata="shmdata path"]`
      </div>

| Param | Type | Description |
| --- | --- | --- |
| entry | <code>module:models/matrix.MatrixEntry</code> | The rendered matrix entry |

<a name="EmptyShmdata"></a>

## EmptyShmdata() ⇒ <code>external:ui-components/Cell</code>
Renders an empty box that replaces a shmdata

**Kind**: global function  
**Returns**: <code>external:ui-components/Cell</code> - An empty cell  
    **Selector**:
      <div class="mermaid">
        `.EmptyShmdata`
      </div>
<a name="SourceLock"></a>

## SourceLock() ⇒ [<code>react/Component</code>](#external_react/Component)
Renders a lock on the source

**Kind**: global function  
**Returns**: [<code>react/Component</code>](#external_react/Component) - A row prefix with the lock icon  
    **Selector**:
      <div class="mermaid">
        `.SourceLock`
      </div>
<a name="EmptyStarter"></a>

## EmptyStarter() ⇒ [<code>react/Component</code>](#external_react/Component)
Renders an empty starter that does nothing

**Kind**: global function  
**Returns**: [<code>react/Component</code>](#external_react/Component) - An empty div  
    **Selector**:
      <div class="mermaid">
        `.EmptyStarter`
      </div>
<a name="SourceSubtitle"></a>

## SourceSubtitle(quiddity, shmdata) ⇒ <code>string</code>
Renders the subtitle of a source

**Kind**: global function  
**Returns**: <code>string</code> - The source subtitle  

| Param | Type | Description |
| --- | --- | --- |
| quiddity | <code>module:models/quiddity.Quiddity</code> | The source quiddity |
| shmdata | <code>module:models/shmdata.Shmdata</code> | The source shmdata |

<a name="EmptySource"></a>

## EmptySource() ⇒ [<code>react/Component</code>](#external_react/Component)
Renders an empty source

**Kind**: global function  
**Returns**: [<code>react/Component</code>](#external_react/Component) - An empty div with the size of a source  
<a name="NewSessionMenu"></a>

## NewSessionMenu() ⇒ <code>module.components/common.MenuButton</code>
Renders the button that resets the session

**Kind**: global function  
**Returns**: <code>module.components/common.MenuButton</code> - A menu button  
    **Selector**:
      <div class="mermaid">
        `#NewSessionMenu`
      </div>
<a name="saveCurrentSession"></a>

## saveCurrentSession(sessionStore, drawerStore)
Save the current session if it was already saved or open the FileSavingDrawer

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| sessionStore | [<code>SessionStore</code>](#module_stores/common.SessionStore) | Store that manages sessions |
| drawerStore | <code>module:stores/common.DrawerStore</code> | Store that manages drawers |

<a name="SaveSessionMenu"></a>

## SaveSessionMenu() ⇒ <code>module.components/common.MenuButton</code>
Renders the button that saves the current session

**Kind**: global function  
**Returns**: <code>module.components/common.MenuButton</code> - A menu button  
    **Selector**:
      <div class="mermaid">
        `#SaveSessionMenu`
      </div>
<a name="MatrixMenuPanel"></a>

## MatrixMenuPanel()
Renders all menus that add extra actions with the Scenic matrix

**Kind**: global function  
<a name="external_pino/logger"></a>

## pino/logger
Pino logger provides a minimal JSON logger

**Kind**: global external  
**See**: [Pino `logger` instance](https://getpino.io/#/docs/api?id=export)  
<a name="external_pino/level"></a>

## pino/level
Define each level of the Pino logger

**Kind**: global external  
**See**: [Pino `level` attribute](https://getpino.io/#/docs/api?id=logger-level)  
<a name="external_pino/mergingObject"></a>

## pino/mergingObject
StatusEnum defines a shared status for all UI components

**Kind**: global external  
**See**: [Pino `mergingObject` option](https://getpino.io/#/docs/api?id=mergingobject-object)  
<a name="external_pino/logEvent"></a>

## pino/logEvent
Structure provided by the `transmit` capability of the logger

**Kind**: global external  
**See**: [Pino `transmit` function](https://getpino.io/#/docs/browser?id=transmit-object)  
<a name="external_pino/logBindings"></a>

## pino/logBindings
Array of bindings that is created by each child logger

**Kind**: global external  
**See**: [Pino `bindings` API](https://getpino.io/#/docs/api?id=bindings)  
<a name="external_react/customHook"></a>

## react/customHook
Tuple that contains a callback and a value. The callback is triggering the value update.

**Kind**: global external  
**See**: [Documentation to build Custom hooks](https://reactjs.org/docs/hooks-custom.html)  
<a name="external_ui-components/Common/StatusEnum"></a>

## ui-components/Common/StatusEnum
StatusEnum defines a shared status for all UI components

**Kind**: global external  
**See**: [UIComponents documentation](https://sat-mtl.gitlab.io/telepresence/ui-components/)  
<a name="external_mobx-react/ObserverComponent"></a>

## mobx-react/ObserverComponent
An Mobx observer component that reacts to each of its properties notifying an update

**Kind**: global external  
**See**: [Mobx's React guide](https://mobx.js.org/react-integration.html)  
<a name="external_react/Component"></a>

## react/Component
A React component written with JSX

**Kind**: global external  
**See**: [React component documentation](https://reactjs.org/docs/components-and-props.html)  
<a name="external_react/Context"></a>

## react/Context
A React context that dispatches properties into nested children

**Kind**: global external  
**See**: [React context documentation](https://reactjs.org/docs/context.html)  
<a name="external_ui-components/Inputs/Field"></a>

## ui-components/Inputs/Field
The UI-Components Field

**Kind**: global external  
**See**: [Field specification](https://sat-mtl.gitlab.io/telepresence/ui-components/#/Components?id=field)  
<a name="external_ui-components/Menu"></a>

## ui-components/Menu
The UI-Components Menu

**Kind**: global external  
**See**: [MenuGroup specification](https://sat-mtl.gitlab.io/telepresence/ui-components/#/Components?id=menu)  
<a name="external_react-color"></a>

## react-color
React color components

**Kind**: global external  
**See**: [React Color homepage](https://casesandberg.github.io/react-color/)  
