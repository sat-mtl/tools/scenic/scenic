#!/bin/bash

if [[ ! -z `which node` ]]; then
  echo "nodejs is already installed !"
  exit 1
fi

NODE_VERSION="${NODE_VERSION:-10}"
NODE_URL="https://deb.nodesource.com/setup_${NODE_VERSION}.x"

echo "Install common dependencies"

export DEBIAN_FRONTEND=noninteractive
apt update -qq
apt install -y -qq curl gnupg build-essential

echo "Install NodeJS"

curl -sL "${NODE_URL}" | bash -
apt install -y -qq nodejs

echo "Install h2o"
apt install -y -qq h2o

