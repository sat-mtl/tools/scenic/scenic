#!/bin/bash

# Executes unit tests on files and re-executes the tests when the files are changed.
# NOTE : This script should be used from the npm scripts, it is provided with the `npm run test:watch` command.
#
# $1 - A pattern used to find the tested files.
#
# Examples
#
#  watch_test Modal
#  npm run test:watch -- Modal
#  watch_test PropertyStore
#  npm run test:watch -- PropertyStore
#
# The script uses `entr` and `fd` commands and generates a glob pattern from the argument.
# In the example, all files named with the string 'Modal' will be targeted.
#
# See [The `fd` command](https://github.com/sharkdp/fd)
# See [The `entr` command](https://github.com/eradman/entr)
PATTERN="${1}"

HAS_FD=`command -v fd`
HAS_ENTR=`command -v entr`

find () {
  fd "${PATTERN}" -e js
}

watch () {
  entr npm test -- \
       --coverage=false \
       "${PATTERN}"
}

if ! [ -x "${HAS_FD}" ]; then
  echo "fd could not be found" >&2
  exit 1
elif ! [ -x "${HAS_ENTR}" ]; then
  echo "entr could not be found" >&2
  exit 1
else
  find | watch
fi
