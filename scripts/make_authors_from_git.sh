#!/bin/bash

AUTHORS="../docs/AUTHORS.md"
if ! [ -f "$AUTHORS" ]
then
    AUTHORS="./AUTHORS.md"
    if ! [ -f "$AUTHORS" ]
    then
	echo "no authors file found, exiting"
	exit
    fi
fi

# order by number of commits
git log --format='%aN' | \
    sed 's/Francois/François/' | \
    sed 's/François/François/' | \
    sed 's/Fran\]ois/François/' | \
    sed 's/ubald/François Ubald Brien/' | \
    sed 's/^chesnel$/Pierre-Antoine Chesnel/' | \
    sed 's/^Chesnel$/Pierre-Antoine Chesnel/' | \
    sed 's/^Arnaud$/Arnaud Grosjean/' | \
    sed 's/nicolas/Nicolas Bouillot/' | \
    sed 's/Nina/Nicolas Bouillot/' | \
    sed 's/Jeremie Soria/Jérémie Soria/' | \
    sed 's/francoisph/François Philippe/' | \
    sed 's/fphilippe/François Philippe/' | \
    sed 's/vlaurent/Valentin Laurent/' | \
    sed 's/vlnk/Valentin Laurent/' | \
    sed 's/flecavalier/Francis Lecavalier/' | \
    sed 's/serigne saliou dia/Serigne Saliou Dia/' | \
    grep -v metalab | \
    grep -v 4d3d3d3 | \
    sort | \
    uniq -c | sort -bgr | \
    sed 's/\ *[0-9]*\ /\* /' > ${AUTHORS}

