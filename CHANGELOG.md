Release Notes
===================

scenic 4.2.0 (2024-07-23)
---------------------------------

A mostly invisible update that lays groundwork for future development. Most notorious changes are importing scenic-api and scenic-selenium repositories and adapting to Switcher.IO API changes. Important back-end work to finish wrapping Switcher.IO package, updating GStreamer from 1.22.6 to 1.24.2, updating ndi2shmdata v0.7.0 (updating NDI to v6.0.0) and update to Ubuntu 22.04 base.

 * Remove decoder from Webcam bundle  ([!917])(https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/917)
 * Use alpine 3.19 tag since h2o package was removed starting from 3.20 ([!916])(https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/916)
 * Use pixel_format 0 (YUYV 4:2:2) instead of 1 (H.264) for Webcam bundle ([!915])(https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/915)
 * ✅ Add scenic-selenium tests to Scenic repo ([!912])(https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/912)
 * Pre-fill sip parameters in url search ([!906])(https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/906)
 * Complete Switcher.IO integration ([!903])(https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/903)

Released with Switcher 4.2.0 back-end which bring following improvements.

Switcher improvements :

 * Update lint job to use clang-format 14 and prebuilt docker image ([!648](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/648))
 * Implements the connection spec inspection command in swctl ([!647](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/647))
 * Adds a global connection state getter and connection events for follower quiddities ([!646](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/646))
 * Add an ldconfig step and add a note if a user installation of pipx doesn't cut it ([!644](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/644))
 * Complete Switcher.IO integration ([!640](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/640))
 * Properly notifies clients on connection spec changes ([!635](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/635))

Scenic-core build system improvements :

 * Add test contact list ([!194](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/194))
 * Fix buildx on TLS dind ([!193](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/193))
 * Update ndi2shmdata to v0.7.0 ([!192](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/192))
 * Update GStreamer from 1.22.6 to 1.24.2 ([!191](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/191))
 * Complete Switcher.IO integration ([!188](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/188))

scenic 4.1.4 (2024-03-20)
---------------------------------
This new bug fix release of Scenic 4.1.x brings major changes. Most notorious change is the inclusion of a custom build of GStreamer library, major over-haul of the video codec handling, replacement of custom NVENC encoder by upstream GStreamer NVENC support, use of standard audio RTP payloading, NDI regressions fixes and many fixes for multi-threading related crashes. Includes experimental features for OPUS audio encoder, dynamic pipeline management using new external Switcher plugin pipesplint and bleeding-edge rswebrtc support.

 * Fix a major source of ghost SIP sources ([!886](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/886))
   * fixes 👻 The return of the son of the ghost SIP sources ([#541](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/issues/541))
 * ⬆️ Update menus and bundles to use videnc ([!886](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/886))
 * Remove selection property fallback and properly handles status of dynamically added and removed properties ([!882](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/882))
   * fixes 🐛 When scenic is refreshed, it sets all the properties of every quiddities to their current values. In the case of v4l2 this results in the FPS going back to its default value. ([#571](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/issues/571))
 * 🐛 Properly handles deleted properties
   * fixes 🐛 Scenic does not handle deleted properties ([#566](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/issues/566))
 * ✨ Save SIP credentials in a cookie and pre-fill the SIP fields with that ([!885](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/885))
 * Shift hidden properties of NDI Input from custom to default ([!893](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/893))
   * fixes ✨ Renaming NDI destination does not rename broadcasted NDI source name on network ([#408](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/issues/408))
 * Recreates the connection object on click ([!891](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/891))
   * fixes It is not possible to disconnect a quiddity from a pipesplint when something had already been disconnected from it before without refreshing the scenic page ([#591](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/issues/591))
 * 🐛 When scenic is refreshed, it sets all the properties of every quiddities to their current values. In the case of v4l2 this results in the FPS going back to its default value. ([!571](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/issues/571))
 * ✨ Save SIP credentials in a cookie and pre-fill the SIP fields with that ([!544](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/issues/544))
 * ✨ Hardcodes the new encoders in the list of allowable destination for a sip source ([!895](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/895))
 * 🐛 Missing close button in open session menu panel regression after 4.1.0 ([!538](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/issues/538))
 * 🐛 Fix Double free or corruption crash when receiving a SIP call ([!266](https://gitlab.com/sat-mtl/tools/switcher/-/issues/266))
 * 🐛 Changing the sliders in the videnc quiddity do not give any feedback and do not seem to affect the encoder ([!565](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/issues/565))
 * 🐛 Scenic does not handle deleted properties ([!566](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/issues/566))
 * 🐛 Unreleased lock in pjsip ([!261](https://gitlab.com/sat-mtl/tools/switcher/-/issues/261))
 * 🐛 Infrequent segfaults in PJICEStreamTrans when receiving a SIP call ([!271](https://gitlab.com/sat-mtl/tools/switcher/-/issues/271))

Released with Switcher 3.5.4 and Shmdata 1.3.74 back-end which bring following improvements.

Switcher improvements :

 * 🐛 Fix multi-channel audio payloading and revert using rtpgstpay ([!638](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/638))
    * fix for Multi-channel audio regression using GStreamer 1.16+ ([#603](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/issues/603))
* Makes the watcher less crashy ([!632](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/632))
   * partial fix for 🐛 Switcher crash when an NDI source that is encoded by an nvenc stops transmiting ([#234](https://gitlab.com/sat-mtl/tools/switcher/-/issues/234)) and Changing resolution of remote NDI source while an NDI Input (attached to that source) is active crashes switcher ([#587](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/issues/587))
 * Makes some connection specs less vague ([!633](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/633))
   * fixes sdiInput does not work with NdiOutput starting from 4.1.4-rc1 ([#592](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/issues/592))
 * Notifies the clients for sfid changes in infotree ([!628](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/628))
 * Make the Ids helper class thread safe ([!627](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/627))
    * fixes Double free or corruption crash when receiving a SIP call ([#266](https://gitlab.com/sat-mtl/tools/switcher/-/issues/266))
 * 🏗️ Payload audio as PCM L24 or OPUS ([!627](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/627))
 * 🩹 Use short VBV buffer for h.264 encoders ([!626](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/626))
 * 🩹 Use h.264 constrained-baseline profile ([!625](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/625))
 * Handle null gstreamer property description ([!624](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/624))
 * 🔇 Mute more Switcher.IO logging ([!623](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/623))
 * ⬆️ Add gstnvcodec support to videnc ([!622](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/622))
 * 🩹 Disable auto starting external Jack server ([!621](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/621))
 * ⚰️ Remove jackserver quiddity ([!620](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/620))
    * fixes Linking jack and jackserver libraries in same Switcher process causes undefined behaviors ([#276](https://gitlab.com/sat-mtl/tools/switcher/-/issues/276))
 * Fixes a segfault when ICE fails while receiving a SIP call ([!619](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/619))
    * fixes 🐛 Infrequent segfaults in PJICEStreamTrans when receiving a SIP call ([#271](https://gitlab.com/sat-mtl/tools/switcher/-/issues/271))
 * Manage subscriptions for dynamic properties ([!604](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/604))
    * mitigates 🐛Every call to pySwitch::get_quid creates a new PyQuiddity which is then never deallocated ([#212](https://gitlab.com/sat-mtl/tools/switcher/-/issues/212))
 * Adds bundle loading to swctl ([!616](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/616))
 * Disable Jack tests in CI ([!614](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/614))
 * [threads] Fix crash caused by concurrent access to a std::string ([!617](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/617))
    * fixes Occasional segfault in check_threaded_wrapper ([#268](https://gitlab.com/sat-mtl/tools/switcher/-/issues/268))
 * Omnibus clang & ASAN bugfix MR ([!606](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/606))

Shmdata improvements :

* Fixes a bug where we would await an empty future. Replaces the std::async with a thread and fixes the bug ([!92](https://gitlab.com/sat-mtl/tools/shmdata/-/merge_requests/92))
  * partial fix for 🐛 Switcher crash when an NDI source that is encoded by an nvenc stops transmiting ([#234](https://gitlab.com/sat-mtl/tools/switcher/-/issues/234)) and Changing resolution of remote NDI source while an NDI Input (attached to that source) is active crashes switcher ([#587](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/issues/587))
* 🐛 Properly manage resources when a dead shmdata is detected by a writer ([!89](https://gitlab.com/sat-mtl/tools/shmdata/-/merge_requests/89))
  * fixes If a "dead" shmdata is found by a writer, it resets its UnixSocketServer and then makes a new semaphore. This leaves a gap of time where a client can decide to connect to the wrong semaphore ID ([#25](https://gitlab.com/sat-mtl/tools/shmdata/-/issues/25))
* 🐛 Adds a timed reset on the reader semaphore when a writer tries to write so as... ([!90](https://gitlab.com/sat-mtl/tools/shmdata/-/merge_requests/90))
  * fixes If a connected reader crashes in the middle of its on_buffer callback it will bring down the writer with it. ([#24](https://gitlab.com/sat-mtl/tools/shmdata/-/issues/24))
  * fixes Switcher issue Unreleased lock in pjsip ([#261](https://gitlab.com/sat-mtl/tools/switcher/-/issues/261))
* 🐛 Fix some thread-safety issues ([!88](https://gitlab.com/sat-mtl/tools/shmdata/-/merge_requests/88))

Scenic-core build system improvements :

* ✨ Add custom rswebrtc plugin ([!186](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/186))
* ✨ Add Switcher Pipesplint plugin ([!184](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/184))
* 👷 Fix missing cerbero tag for build job and disable buildx cache ([!183](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/183))
* ⬆️ Add GStreamer 1.22.6 from custom Cerbero build ([!180](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/merge_requests/180))

scenic 4.1.3 (2023-12-05)
---------------------------------

New bug fix release of Scenic 4.1.x that primarily addresses video properties and tool-tip issues.
  
  * 🐛 Makes the selection typed properties update their display when the value changes without user intervention ([!880](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/880))
    * fixes 🐛 SDI Input framerate dropdown does not reflect changes ([!547](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/issues/547))
  * 🐛 Properly updates the shmdata store's shmdata caps when the caps of an already existing shmdata changes. ([!879](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/879))
    * fixes 🐛 Video encoder tooltip does not reflect resolution and frame rate changes ([!543](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/issues/543))

Released with Switcher 3.5.3 back-end which bring following improvements.

Bug fixes of `v4l2src` properties lifecycle during runtime to allow better resolution and frame rate. Tweak of `pjsip` options to allow building switcher with GStreamer Cerbero SDK.

 * [mbag] Add necessary checks before dereferencing iterators ([!615](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/615))
 * 🔊 Display Switcher and GStreamer versions in log ([!608](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/608))
 * Lower default resolution and encoder bit rate ([!605](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/605))
   * fixes 🐛 If a user instantiates a Physical Video Input or an Encoder, the default bitrate and resolutions are too high for non-advanced use cases. ([!572](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/issues/572))
 * ✨ Add `-rcN` to switcher version string
 * ➖ Disable pjsip opus build option ([!607](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/607))
 * ➖ Disable pjsip opencore-amr build option ([!603](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/603))
 * 🐛 Properly updates v4l2 properties instead of nuking and rebuilding them ([!602](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/602))
   * fixes ✨ Force the bundled video inputs to produce 30fps content ([!545](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/issues/545))


scenic 4.1.2 (2023-10-25)
---------------------------------

New feature release of Scenic 4.1.x that brings back Thumbnail and Previews!

- 🐛 Fix very sticky notifications ([!878](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/878))
- ✨ Bring back preview ([!869](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/869))
- ✨ Bring back thumbnails ([!868](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/868))

Released with Switcher 3.5.2 back-end which bring following improvements.

New improvements on Switcher.io and switcher that brings back the thumbnails feature in Scenic and improve the `glfwin` support with screen detection.

 * 🐛 Allow empty invoke args in swctl ([!601](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/601))
 * 👽️ Switcher.io swctl CLI client improvements and documentation ([!592](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/592))
 * 🐛 Fix timelapse quiddity last_image property not updated in infotree ([!594](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/594))
 * ✨ Adds glfwin monitor selection ([!597](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/597))
 * ✨ Bring back thumbnails ([!596](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/596))
 * ✨ Prevent user from setting osc port to well known port range ([!591](https://gitlab.com/sat-mtl/tools/switcher/-/merge_requests/591))


scenic 4.1.1 (2023-08-29)
---------------------------------

Introducing the latest bugfix release of Scenic!

### 🐛 Bug fixes
 * 🐛 Fix unrequested detach on SIP destination ([!875](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/875))
 * 🐛 ✨ Change default SIP destination port to 5060  ([!874](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/874))
 * 🐛 Fix info tree state inconsistency by replaying graft and prune event on quiddity creation ([!861](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/861))
 * 🐛 Fix the contact disconnection regression ([!864](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/864))
 * 🐛 Fix the applyDetachShmdata function ([!863](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/863))
 * 🔥 Remove mention of file path when saving a session ([!860](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/860))
 * 🐛 Fix tooltips randomly not rendered ([!866](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/866))

### ✨ New features
 * ✨ Block session interaction while using SIP ([!862](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/862))
 * ✨ Add destination port field ([!873](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/873))

### ✅ Varia
 * 🌐 Translation for monitor property ([!870](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/870))

### 💥 Breaking changes

We have temporarily disabled session actions (creating new, saving and loading session) while sending or receiving media using SIP. This is to prevent unwanted edge cases and critical issues during artistic performances. We are aware that this may affect certain experiences, and we are actively working on reintroducing these features in a more consistent and improved form.

 * ✨ Block session interaction while using SIP([!862](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/862))

scenic 4.1.0 (2023-06-15)
---------------------------------

Introducing the latest release of Scenic!

This version marks a significant milestone as it supports `Switcher 3` integration. Refined across two residencies, including alpha and beta versions, Scenic now offers enhanced performance and stability.

### 🐛 Bug fixes

 * 🐛 Fix white screen when locking a sip source ([!859](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/859))
 * 🐛 The StringField aren't updated when we trigger the property panel from similar quiddities ([!854](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/854))
 * 🐛 Properly display default values in property drop down values ([!853](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/853))
 * 🐛 Fix property values not updated in the property pannel when setting a property to a falsy value when it previously was set to a non falsy value ([!852](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/852))
 * 🐛 Fix some white screen issues related to quiddities suddenly disappearing from locked connections ([!848](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/848))
 * 🐛 Fix white screen bug when a locked NDI (and probably sip ?) source is present at session reset ([!846](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/846))
 * 🐛 Update the help page information ([!845](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/845))
 * 🔧 Fix Scenic Station config to apply JACK max_number_of_channels ([!855](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/855))
 * 🐛 Mark infoTree graft and prunes as mobX actions ([!843](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/843))
 * 🐛 Fix switcher tree path functions having side effects on deep objects ([!842](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/842))
 * 🐛 Fix SIP logout error message ([!836](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/836))
 * 🐛 Fix unscrollable contact list ([!825](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/825))
 * 🐛 Fix creation of the scene panel upon switcher session change  ([!797](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/797))
 * 🐛 Allow hyphens in contact names ([!816](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/816))
 * 🐛 Correctly send nickname when creating a quiddity from the quiddity creation drawer ([!819](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/819))
 * 🐛 Fix calls to tree instead of infoTree ([!815](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/815))
 * [🐛 Properly detaches shmdata from contact](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/commit/c6966c1fbb0334e93710101e46cb40b424b43d77)
 * [🐛 Manage min and max for nvenc property panel](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/commit/242c45b68cad465c8c8d5b2fa9b123ca37f30721)
 * [💄 Set !important flag for the ShmdataInfoTooltip z-index](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/commit/8d589478e96ad08c4ccc226b7c58520f7723c596)

### 💥 Breaking changes

We have temporarily hidden certain unstable features to prevent unwanted edge cases and critical issues during artistic performances. We are aware that this may affect certain experiences, and we are actively working on reintroducing these features in a more consistent and improved form.

 * 💥 Migrate Scenic to Switcher3 and SwitcherIO ([!710](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/710))
 * 💥 Hide filter panel ([!856](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/856))
 * 💥 Hides the order panel ([!857](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/857))
 * 🩹 Temporarily hides display thumbnails ([!850](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/850))
 * 💥 Hide scene functionnalities ([!847](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/847))

### ✨ Feature streamlining

 * ✨ Recursively locks sources connected to locked destinations when the aforementioned sources are also destinations ([!851](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/851))
 * ✨ Use nickname instead of uuid in quiddity creation log ([!849](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/849))
 * ✨ Adapt scenic for uuids in quiddity ids. ([!837](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/837))
 * ⚡ Add h264Encoder speed-preset property and set it to ultrafast by default ([!838](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/838))
 * ✨ Add pulsesrc and pulsesink to default menus ([!834](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/834))

### ✅ Varia

 * 🔨 Enable source map ([!858](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/858))
 * 📝 Fix CSS selectors for scenic-selenium ([!840](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/840))
 * ⏪ Reverts the shift from clear to reset in integration tests ([!841](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/841))
 * 💚 Fix unit test bundle fixture for speed-preset parameter ([!839](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/839))
* 🔥 Rip out unnecessary connections and disconnections ([!823](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/823))
 * 🚚 Move nickname generation to Switcher ([!833](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/833))
 * ✅ Fix PulseAudio unit test ([!835](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/835))
 * 💚 Force tests image to ignore the switcher cache ([!831](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/831))
 * ✅ Puts reset() instead of clear() in the integration tests to avoid the switcher deadlock bug ([!829](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/829))
 * 📝 Add Valentin Foch in TESTER.md ([!822](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/822))
 * ✅ Fix failing unit and integration tests after migration ([!820](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/820))
 * [📝 Additional doc on NumberField component](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/commit/22ce8c9f294f1ec965f0583151d7ad639e3a2a48)
 * [🚚 Move station config to Scenic project](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/commit/0f7aef87981b1d1d59ed1ffd7923792748de3ef0)
 * [👷 Bypass all previous jobs when building docker image](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/commit/65b1fd16e1f82a3140340c408879280520de05db)
 * [👷 Make arm64 build optional](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/commit/be1afda0dc3ac67e815ac81072038959f316acaa)
 * [👷 Disable CI cache](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/commit/e73c3c5e57c3b256d1f75d9afafdf76a87fc7a10)
 * [📦 Update scenic-api to 0.5.0](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/commit/c5deb29d78b38c05b9ace30909e2b5455d8015e9)

scenic 4.0.10 (2023-01-26)
----------------------------------

* 🚑 Add the temporary label to the new contact modal ([!811](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/811))
* 🚑 Fix validation of the input fields for newContactModal ([!804](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/804))
* 🚑 Fix the styles of the add contact button ([!801](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/801))
* 🚑 Fix status of add contact confirm button ([!803](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/803))
* 🚑 Fix flickering add contact button ([!802](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/802))
* ✨ Add a contact creation modal ([!793](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/793))
* ✨ Categorize sip sources by contact ([!799](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/799))

scenic 4.0.9 (2022-12-05)
----------------------------------

* 🚑 Revert h2o drop to nobody user ([!794](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/794))
* ✨ Sort sip partners by online status ([!782](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/782))
* 🐛 Fix assignation of multiple sources to h264 encoder ([!788](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/788))
* 🐛 Fix h2o user privilege drop ([!772](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/772))
* 🌱 Add Scenic CI contacts file, seed and generator ([!770](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/770))
* 🚚 Tag release Docker images with name instead of slug ([!761](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/761))

scenic 4.0.8 (2022-09-27)
------------------------------

### Features
* 🔨 Add Gitpod configuration for Scenic project ([!734](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/734))
* 📦 Update ui-components for 0.2.1 ([!745](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/745))
* ⬆ Migrate to Mobx6 ([!735](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/735))

### Fixes
* 🐛 Fix GitLab urls ([!755](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/755))
* 🐛 Fix Gitpod Scenic Core image URL ([!754](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/754))
* 🐛 Add Owncast RTMP service and fix X display permission race condition ([!750](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/750))
* 🐛 Fix the row gap for the connection table ([!738](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/738))
* 🐛 Make an active connection of an NDI video stream to an h264 encoder ([!733](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/733))

### Hotfixes
* 🚑 Fix issue with videoOutput2000 and SIP quiddities ([!753](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/753))
* 🚑 Hotfix connection of sip sources to specific destinations ([!748](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/748))
* 🚑 Fix display of the configuration modal ([!743](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/743))

### Refactorings
* ♻ Display the open session modal when user wants to download a file without saving a new one ([!747](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/747))
* ♻ Refactor App.js as a functional component ([!728](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/728))
* ♻ Refactor the DestinationHead as a functional component ([!720](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/720))
* ♻ Transform quiddity components to functional ([!716](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/716))
* ♻ Transform panels components to functional ([!719](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/719))
* ♻ Refactor the SipContactInformationTooltip as a functional component ([!721](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/721))
* ♻ Refactor the SceneTabBar as a functionnal component ([!584](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/584))
* ♻ Add an EncoderStore in order to gather all the extra logic implied by the hidden encoders ([!737](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/737))
* ♻ Refactor the wrappers components as functional components ([!722](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/722))

scenic 4.0.7 (2022-08-12)
----------------------------------

### Features
* ✨ Refactor the MatrixEntry component to group multiple shmdatas in one source ([!705](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/705))
* ✨ Add a setting to display/hide the encoder in the matrix ([!704](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/704))

### Fixes
* 🐛 Fix display of the notifications list ([!713](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/713))
* 🐛 Fix label and description of the shmdata information for sip sources ([!712](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/712))
* 🐛 Fix delay in disconnecting/hiding contacts drawer ([!709](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/709))
* 🐛 Fix the display of user notifications in the UI
* 🐛 Fix page resizing when user notifications are displayed ([!703](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/703))
* 🐛 Prevent reinitialization of SIP sources

### Hotfixes
* 🚑 Hotfix the display of the preview for thumbnails ([!732](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/732))
* 🚑 Hotfix connection box display between sip entries ([!731](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/731))
* 🚑 Fix the appearance of a top margin for matrix rows when filtering ([!726](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/726))
* 🚑 If a user triggers the appearance of certain modals, they will appear in English ([!723](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/723))
* 🚑 Hotfix the rename nickname behaviour for a quiddity ([!724](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/724))
* 🚑 Hotfix the sip filter for sip sources

### Refactorings
* ♻ Convert all modals into functional components ([!577](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/577))
* 📦 Update Scenic docker image build and integrate in CI pipeline ([!706](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/merge_requests/706))

scenic 4.0.6 (2022-06-09)
----------------------------------

* ✨ Fix sources and destinations when scrolling
* ✨ Add order buttons to order sources and destinations
* ✏ Update telepresence-scenic links
* 💄 Reduce the gap of the destination separator and fix the rtmp label cutoff
* 💄 Capitalize the first letter of the pattern label property
* 🐛 Hide connection boxes behind source column area
* 🐛 Close property drawer when user unselects an entry
* 🐛 Resolve the order of special destinations
* ⚗ Change checkbox shapes for sources and scenes
* 🙈 Remove the Drawer backdrop for PropertyInspector and SipContacts
* 🚧 Refactor Order with OrderEnum

scenic 4.0.5 (2022-04-12)
----------------------------------

* ✨ Filter SIP sources and destinations
* ✨ Improve the identification labels for sources generated by the SIP quiddity
* 🐛 Fix the input behaviour when a Scene is renamed
* 🐛 Fix the nickname field behaviour when user cancels action
* 💄 Extend the width of the interface panel
* 🐛 Fix index generation when a quiddity is created

scenic 4.0.4 (2022-02-09)
----------------------------------

* ✨ Add filter for SIP destinations
* ✏ Fix description field for the STUN/TURN switch toggle
* ✏ Change 'SIP port' field's name
* 🐛 Bad rendering in Help section for contributors
* 🐛 Show scene control panel when scrolling down
* 📝 Add Feature Request and Bug Resolution Request templates

scenic 4.0.3 (2022-01-14)
----------------------------------

* ✨ Add thumbnails display toggle button to hide/show thumbnails
* ✨ Emit scenic Logging with socketIO
* ✨ Group Shmdata capabilities
* ✨ Add keyPress to SIP drawer
* 🐛 Fix the functionality of the cancel button for the RenameSceneModal
* 🐛 Hide empty Common Configuration section
* 🐛 Hot fix width and height display on test Pattern videos
* 🐛 Display bitrate value for video quiddities in Mbps
* 🐛 Fix display of available ndi streams on app reload
* 🐛 Fix session reinitialization issues
* 🐛 Fix the click on modal buttons
* 🐛 Add class name for the OpenFileButton component
* 🐛 Fix switch cases for video width and height properties
* 🐛 Scroll automatically to partners section when adding a contact
* 🐛 Hide width and height properties if resolution label is not custom
* 🐛 Add data attributes to Preview component
* 🐛 Remove UNKNOWN as default when clearing nickname input
* 🐛 Change DownloadButton type from secondary to primary
* 🐛 Fix RTMP inconsistency in property panel
* 💄 Fix scrolling glitches in the property inspector
* ✏ Fix property description typo
* 🚑 Fix the initialization of all bound stores
* 🚑 Hotfix the webcam bitrate
* 🚑 Hotfix the bitrate sliders
* ♻ Centralize the API population with the SocketStore
* ♻ Change log level from debug to info for pino logger
* ♻ Refactor the class management with the ClassStore
* ✅ Update CapsStore test file to include tests for all functions
* 💬 Add wiki link to help page
* 📝 Fix markdown table syntax error in SELECTOR_GLOSSARY.md
* 🔧 Remove log messages from the unit tests
* 🔊 Add quiddity and shmdata information where relevant

scenic 4.0.2 (2021-09-14)
----------------------------------

* ✨ Show commit number (SHA) in the Help page
* 🔧 Configure dev build with NodeJS 16
* 🐛 Fix Caps displayed in the tooltips are not updated
* 📝 Change the installation doc
* ✨ Deprecate the NetworkAPI given by Scenic Core
* 💚 Fix the bundle of 4.0.0 from the tag and master branches
* 🐛 Fix the the Hang Up All button responsiveness
* ✨ Sort translation keys
* 🐛 Lock the sources that are connected to a locked destination
* 🐛 Fix ui-components build with Webpack 5
* 🐛 Add a fallback when a selection property is initialized
* 🐛 Clear the shmdatas when the session is reset
* 🔖 Merge back hotfix 4.0.1

Scenic 4.0.1 (2021-08-12)
----------------------------------

* 🚑 Fix the crash when the Settings page is loaded

Scenic 4.0.0 (2021-08-11)
----------------------------------

* ✨ Prevent the user from deleting a source that is connected to a locked destination
* 🐛 Fix "Selection" do not show the selected item if the selected item is the first one
* 🐛 Fix OUT_OF_MEMORY error when it builds
* 🐛 Fix ressource instantiation with Ctrl+A
* 🐛 The selectors strike back
* 🐛 Improve shmdata tooltip and property menu selectors for QA tests
* 🐛 Fix the CI deployment
* 🐛 Unescape the displayed file path in notifications when it is saved
* 🐛 Resolve NDI source labels and starters
* 📦️ Upgrade Scenic to webpack 5
* ♿ Bring back SIP port in the SIP logger
* 📝 Update Scenic documentation
* ♻️ Refactor NPM test scripts and improve CI cache
* ♻️ Convert all fields into functionnal components
* ♻️ Remove Trans components from NotificationOverlay and HelpWrapper

Scenic 4.0.0-beta (2021-04-08)
----------------------------------

* ✨ Implement the UI lock for group of quiddities
  * ✨ Lock RTMP destinations when they are started
  * ✨ Lock the NDI destinations when they are started
  * ✨ Lock the SIP destinations when they are started
  * ✨ Implement the locked `ConnectionBox`
* ✨ Add custom max limits for quiddity classes
* ✨ Mute some invasive notifications
* ✨ The user should be able to download a session file the same way he can when they are attempting to create a new saved file
* ✨ Move advanced options of the SIP panel into the Parameter page
* ✨ Add 'Add Resource' (Ctrl+A) option
* 🐛 Add an empty source when there is no source
* 🐛 Remove the horizontal scrollbar at the bottom of the "Live" page
* 🐛 Let enough horizontal space for SIP contact status tags in French
* 🐛 Fix CSS of the FileDrawer entries
* 🐛 The cursor of property inputs is always moving at the end of the input
* 🐛 Page names are not translated on startup
* 🐛 Clean all contact conections when quiddities are removed
* 🐛 Replace ladpsa with shmdelay in audio bundles
* 🐛 Fix the connection inversions
* 🐛 Fix thumbnail not appearing for some bundles
* 🐛 Fix the duplicated entries with encoded source
* 🐛 When a user attempts to open a saved session, the overwrite warning message is confusing
* 🐛 Fix improper configuration files merging
* 🐛 Fix erratic behavior of the userTree stores when a file is loading
* 🐛 Fix duplicate creation of quiddities when it loads a session
* 🐛 Fix failing unit tests
* 🐛 When a user tries to save a session with the same name as a previously saved session, they are not warned of the consequences
* 🐛 Fix SIP login after clicking the 'New' button
* 🐛 When a session is opened with a lot of sources and destinations, the Notification panel crashes
* 💄 Remove capitalized field labels in property inspector
* 🌐 Update translation strings and i18next
* 🔧 Update default bundles configuration
* 🔥 Remove Decoder menu entry
* ♿ Improve help messages for each connection box
* 📝 Update the documentations of page components
* 📝 Update the documentation for the stores
* 📝 Rename and link external dependencies
* 📝 Link the API files to the api module
* 📝 Add documentation for the SIP login and logout buttons
* ♻ Refactor all store populations from unit tests
* ♻ Refactor the menu rendering
* ♻ Refactor the user flow with session file drawers
* ♻ Rename functions from 'delete' to 'remove'
* ♻ Clear quiddity classes when the QuiddityStore is reset
* ♻ Refactor Scenic configuration reading and handling
* ♻ Merge logging in the `NotificationStore`
* ⬆ Update Socket.IO to 3.1.0

Scenic 4.0.0-alpha (2020-12-07)
----------------------------------

### 💥 Breaking Changes

* 🎨 The UI has been entirely rewritten using the [React.js](https://reactjs.org/) library
* 💥 The matrix's "categories" have been replaced by "filters".
  * ✨ When no filter is applied, all sources and destinations are displayed in the matrix
  * ✨ When one or more filters are applied, only the sources/destinations associated with the selected filters are displayed
* 💥 Creating an RTMP destination will now spawn a modal requiring the streaming URL and connection key to be entered
* 💥 The `Shmdata Information` panel has been replaced by tooltips. These tooltips appear when hovering over a source in the matrix
* 💥 The `Parameters` page has been replaced by the `Settings` page, which now includes tabs allowing better categorization of the app's settings
* 💥 The language selector switch has been moved to the `Settings` Page
* 💥 SIP connections are not affected by scenes anymore
* 💥 The SIP panels have been entirely redesigned
  * ✨ A SIP contact's connection status can be consulted using new tooltips. These tooltips appear when hovering over a contact in the SIP Contacts panel.
  * 🔥 The advanced SIP login parameters have been removed, but will be added back to the `Settings` page in a future release
  * 🔥 The `Do Not Bother` switch has been removed
* ✨ SIP calls now abide to the new "session partners" logic
  * 🔥 The "hamburger" button below the contact name in the matrix has been removed. The call/hangup button is now located in the SIP Contacts panel, next to each session partner.
  * 💥 SIP contacts must be added in the current SIP session as "session partners" using the `+` button next to each of them in the SIP Contacts panel. Adding a contact to the session also adds it to the matrix.
  * 💥 Calls received from SIP contacts not in the current SIP session are *always* refused. To receive streams from a contact, it is imperative to first add it as a session partner using the `+` button next to the contact in the SIP Contacts panel.
    * 💥 SIP contacts in the current SIP session (the session partners) are part of a SIP allowlist: their calls will always be accepted and their streams will automatically appear in the matrix.
    * 💥 SIP contacts not in the current SIP session (those who are NOT session partners) are part of a SIP blocklist. Their calls will systematically be refused.
* ✨ A connection modal will open on Scenic launch if no Switcher instance is running on the localhost.
* ✨ The `Help` page has been entirely redesigned and now included lots of helpful links
* 🔥 The `Advanced` page and the `Add a ressource` panel have been temporarily removed

### General Changes

* 🔥 Remove SubscriptionStore
* 🐛 Detect the frequency properties
* 🐛 Add doublePrecision method on Property
* 🐛 Add an ID for each property field
* 🐛 Fix the disabled status of the properties
* 🐛 Rename menu entry for video test input
* 🐛 Activate the default scene by default
* 🐛 Allow empty initQuiddities user config
* 🐛 Fix ThumbnailStore
* 🐛 Add a Log Out button in the SIP panel
* 🐛 Fix Caps parsing from string
* 🐛 Disable the reset button when the quiddity is started
* 🐛 The connections between sources and SIP contacts are not resolved in the Matrix
* 🐛 Fix failing Docker CI job
* 🐛 Add the partner statuses in a tooltip
* 🐛 RTMP Destinations do not retain source assignations as expected
* 🐛 Fix the false active state of any instantiated source
* 🐛 Fix the favicon error
* 🐛 Fix the Desired Bitrate slider rendering
* 🐛 Fix the session saving and loading for 4.0.0 bêta
* 👷 Publish Scenic to S3 from CI
* 👷 Prevent CI builds from expiring
* ✨ Integrate the Tooltip with the shmdata informations
* ✨ Implement the notification zone
* ✨ Render the stat in the Shmdata tooltip
* ✨ Implement the Filter selector
* ✨ Add the Help Page
* ✨ Add the Settings store
* ✨ Add the Settings page
* ✨ Add the SIP availability status
* ✨ Add quiddity status in the UI
* ✨ Allow renaming of NDI output streams
* ✨ Improve the matrix layout for the sip contacts
* ✨ Modelize the GstCaps with the Capabilities model
* ✨ Create SocketStore
* ✨ Add RtmpStore
* ✨ Add the PreviewWrapper component
* ✨ Add new Property Inspector Drawer
* ✨ Add SIP contact drawer
* ✨ Add SoapStore
* ✨ Add SIP contacts validation and fetching
* 💄 Improve the style of the disabled properties
* ♻ Fix broken PreviewStore
* ♻ Rename UserDataAPI functions
* ♻ Refactor NDIStreamStore
* 🏗 Add an ID and a ClassName for all modals and their inputs
* 📝 Add a naming convention for class names
* 📝 Add documentation for the SIP login and logout buttons

scenic 3.3.5 (2020-07-14)
----------------------------------

* 🐛 Fix version badge and coverage artifact creation
* 🐛 Fix bitrate initial value by improving bitrate conversion for Sliders
* 🐛 Reference bundle ci job in the bundle URL

scenic 3.3.4-rc1 (2020-03-27)
----------------------------------

* 👷 Add release bundle when a tag is created

scenic 3.3.4 (2020-03-26)
----------------------------------

* ✨ Add support links in the help page
* 🐛 Fix slider rendering for MIDI input
* 🐛 Fix broken sliders on Chromium
* 🐛 Set quiddity tree debug view at the bottom of the property panel
* 🌐 Add missing translations
* ✨ Send default bundles to server on connection
* ✅ Add integration tests for quiddity creation
* ✨ Integrate new ui-component's Input Number and Slider
* ✨ Add default menu configuration

scenic 3.3.3 (2020-02-06)
----------------------------------

* 🐛 Change state only for internal update
* 🐛 Fix disabled state
* ✅ Resolve unit tests for Number.js
* 💄 Cut styling for NumberSelector
* 🚧 Integrate new NumberSelector
* ⬆️ Update ui-component version
* 👷 Add Gitlab coverage from unit tests
* ⚗ Integrate React thumbnail
* 🌱 Add new models for Matrix and Shmdata
* 🚚  Move Preview and Thumbnail stores into the timelapse folder
* 🌐 Update translations
* ✨ Add a React component for the Matrix
* ✨ Add React components for Source and Destination
* ✨ Add React component for Shmdata
* 🎨 Add store for shmdatas
* ✨ Add a store for Thumbnails
* ✨ Add a store for byte rate status of shmdatas
* ✨ Add store for managing subscriptions
* ✨ Add store for quiddity in the matrix
* ♻️ Change instantiation of stores with socket
* ♻️ Change Quiddity model and add models for Stat and Property
* ✅ Add API calls for quiddity property, quiddity tree, shmdata and thumbnails
* 🔥 Delete Control Page

scenic 3.3.2 (2020-01-07)
---------------------------------

* 📝 Update documentation for 3.3.2
* 🐛 Fix exclusive physical video output matching in Source menu
* 🐛 Fix broken Scenic remote access
* 👷 Publish mkdocs from CI
* 📝 Update developer documentation
* Remove jq dependency
* 🌐 Add translations for new quiddity  descriptions
* Adapt ui-component changes to scenic
* Fix support for Modal with react-bootstrap
* Integrate next UI design with the package.json scripts
* Add new Destination managers for a Scenic matrix with React
* Move schemas into models root folder
* Replace occurences of 'quiddity' with 'resource'
* Add SortableQuiddities managers for quiddity order synchronization
* Add new React components used to manage Nicknames

scenic 3.3.1 (2019-11-25)
---------------------------------

* 📝 Update documentation for 3.3.1
* 📄 Upgrade version and reference ui-components
* Add nix-shell configuration for dev
* ➖ Remove all merged ui-components
* Refactor QuiddityAPI method names and move it to a `quiddity` folder
* Add new library for logging
* 🌐 Add translations related to MIDI and OSC
* 🔀 Fix broken started property for OSC destinations
* Add new scene and connection models
* Add new DestinationHead react component
* Move all input components into their own folder
* Add a class name glossary for new UI components
* Implement ConnexionBox with React
* Add comparator for quiddity order
* Add optional chaining proposal to Babel
* Load LanguageSwitcher from Marionnette jQuery element
* Use jQuery element for QuiddityMenu rendering
* Remove `scenic` from common classes
* Change Tab `active` prop by `selected`
* Update non MarionnetteJS libraries
* Add new React NavBar component for Scenes

scenic 3.3.0 (2019-10-11)
-------------------------

This new release brings **NewTek NDI®** support and implements new React components into the UI.
Over time, the Backbone.js/Marionnette framework will be completely replaced by React.

* Added auto-generated Scenic documentation
* Added CI task to automatically upload a build of Scenic on Gitlab when merging on master
* Added CI task to deploy Scenic's Docker container
* Added default categories when no menu configuration is present
* Added full Docker support
* Added LanguageSwitcher React component
* Added Make recipe to install Scenic with Apache2 instead of h2o
* Added Modal React component
* Added "NDI® Input" source and "NDI® output" destination
* Added Preview React component
* Added Select React component
* Added Spinner React component
* Blocked interactions with the Preview window
* Brought back detailed Switcher error messages
* Cleaned Timelapse blobs that accumulated in the browser's memory
* Converted sources byte rate from bits/second to megabits/second
* Created new "assets" folder
* Fixed a bug where the Settings panel would not open automatically after creating a source
* Fixed a bug where Scenic would stop responding after switching Scenes
* Fixed a bug where a SDI source could be inactive after loading a session
* Fixed a bug where the shmdata path was not updated in the UI after loading a session
* Fixed a bug where the scroll bar would not appear in the Settings panel
* Fixed a bug where the active scene would be set to Off automatically after loading a session
* Fixed a bug where refreshing the browser would make inactive session contacts disappear
* Fixed a bug when incorrect source assignation when switching scenes
* Fixed a bug where the contact list was unavailable after the SIP logging
* Fixed inconsistent warning messsages behavior when deactivating sources
* Fixed some more French translations
* Refactored "config" folder and configuration files
* Removed all mentions of "shmdata", "quiddity" and "jack" in the UI
* Removed hover effects on matrix junctions of incompatible sources/destinations
* Renamed test signal quiddities to "Audio test signal" and "Video test signal"
* Removed the alphabetical ordering of sources in the matrix (sources now appear at the end of the matrix)
* Removed types labels in Information panel
* Updated documentation
* Updated libraries

scenic 3.2.0 (2019-05-15)
-------------------------

* Added Jest unit tests
* Added some translations
* Added documentation about the development process
* Added buttons in the "Help" tab to display the user support page and the user manual
* Updated install instructions
* Refactored the CI pipeline
* Create categories based on the labels present in the sources and destinations menu
* Changed some sources and destinations labels
* Removed the chat completely
* Fixed a bug where switching scenes would sometimes make the application crash
* Fixed a bug where the RTMP destination would sometimes display erroneous connections in the matrix
* Fixed a bug where the vertical scroll bar would disappear
* Fixed a bug where the 'i' and 's' keyboard shortcuts would not display the right data
* Fixed a bug where the 'edit' state on the quiddity name input field was still active when the focus was lost
* Fixed a bug where a saved Scenic session would hang forever on loading

scenic 3.1.4 (2019-01-30)
-------------------------

Set the tooltip based on the type of quiddity view (source or destination)

scenic 3.1.3 (2019-01-30)
-------------------------

Initialize the hostname and port to set the server URL

scenic 3.1.2 (2019-01-28)
-------------------------

Added unit tests checking in CI

scenic 3.1.1 (2019-01-24)
-------------------------

Make French the default language

scenic 3.1.0 (2018-12-13)
-------------------------

* Updated and refactored documentation and README
* Added CONTRIBUTING, CODEOWNERS, AUTHORS and CODE_OF_CONDUCT files
* Added missing translations to the UI and fixed strings that were mistranslated
* Added translations for Switcher error messages
* Filtered Switcher error messages to replace some of them with a generic error message
* Removed all IDE-related configs from the repository, moved them to new dev-tools repository
* Stopped displaying i18next logs in the client console, which would always pollute the console with warnings about non-translated strings
* Prevented users from entering quiddity nicknames with accents and diacritics: these special characters often caused problems with SIP and JACK
* Prevented confusing properties and data from being displayed in the UI through the use of a new configuration JSON. These filtered properties are flagged in the log
* Fixed a bug where Chromium would always pop a window asking whether or not to translate the page
* Fixed broken authentication function
* Fixed a bug where SIP contacts would disappear when activation the "Do Not Disturb" option
* Fixed a bug where boolean properties of quiddities would not be correctly initialized in the UI
* Fixed a bug where the user could not scroll down a property panel when it was too long for the screen
* Fixed a bug where the "Same login for TURN Server" toggle was displayed at the wrong place
* Fixed a bug where the UI would not set the "SIP Advanced tab" fields with the proper default values
* Fixed a bug with keyboard shortcuts not doing their intended actions
* Fixed a bug caused by the chat that would cause a grey screen when reopening Scenic after closing it while connected to SIP
* Fixed a bug where loading a saved session with two scenes or more would only show one scene in the "Connections" screen
* Fixed a bug where the favicon would not load

scenic 3.0.0 (2018-07-23)
-------------------------

* New development cycle started!
* Separated the Scenic server-side from the client-side: they are now 2 different projects with a brand new repo each
* Refactored and reformatted some of the code
* Cleaned and updated dependencies
* Completely removed i18next from the server; internationalization will now be handled by the client exclusively

Scenic 2.10.2 (2017-11-27)
-------------------------

Fixed scenic quit when changing language or pressing f5

Scenic 2.10.1 (2017-10-27)
-------------------------

* Refactored switcher log access
* Fixed css bug on contact actions transition when chat is disabled
* Hide vertical scrollbar in scene menu
* Updated version of socketio, changed ping timeout to 60 sec
* Added a scrollbar when there are more than 10 scenes

Scenic 2.10.0 (2017-09-01)
-------------------------

* Add a Chat !!
* User can talk directly to each other or create group of discussions

Scenic 2.9.21 (2017-08-14)
-------------------------

* Fixed bug when changing the name of a quiddity from a remote control
* Fixed bug when adding a contact from a remote control
* Add the possibility to pin a scene
* Fixed bug when renaming a file

scenic 2.9.20 (2017-07-19)
-------------------------

* Optimization of the scene system
* Fixed some bugs synchronization with the remote control
* Fixed various bugs around scenes
* Possibility to put a scene off in the connections screen
* Add some function to rename a quiddity

scenic 2.9.19 (2017-07-03)
-------------------------

* Fixed the header of sources and destinations when scrolling the matrix
* Add the possibility to block all contact during a call
* STUN, TURN server can be saved in a config file
* Fixed various bugs around scenes and matrix

scenic 2.9.18 (2017-06-07)
-------------------------

* Remove the system of groups for the Matrix
* Add the possibility to create Scenes that saved the connections for each matrix
* Add a connections page to switch between Scenes
* Add the possibility to duplicate a scene

scenic 2.9.17 (2017-04-12)
-------------------------

* Fixed bug on shmdata connections with rtmp quiddity.
* Fixed bug on the display of an erased quiddity of a group.
* Fixed refresh of a scenic remote when adding a SIP contact.
* Fixed the drag of a shmdata after a hang up of a SIP call.
* Fixed bugs on drag&drop.
* Fixed bug on duplicated SIP contact in the matrix.
* Clean some code in the matrix menus.

scenic 2.9.16 (2017-03-31)
-------------------------

* Fixed bug on exclusive bundle in the matrix menu
* Fixed visual bug on the group buttons
* Fixed bug when renaming a group
* Add the possibility to rename the default Group
* Fixed bug on empty shmdata source after opening a saved session
* Fixed bug on group button after hang up and recall a SIP call
* Fixed bug on mail address when adding a contact
* Remove empty default group when hang up a SIP call
* Fixed bug when moving a contact to a new group.

scenic 2.9.15 (2017-03-17)
-------------------------

Fixed bug when receiving several SIP quiddities in the matrix

scenic 2.9.14 (2017-03-10)
-------------------------

* Optimization of the matrix
* Fixed synchronization of the matrix with a remote
* Fixed some graphic bugs on Ipad
* Fixed bugs around the SIP quiddity

scenic 2.9.13 (2017-02-17)
-------------------------

* Fixed crash when loading or reset big file
* Optimize property inspector view
* Fixed the display of the Button group and preview video

scenic 2.9.12 (2017-02-06)
-------------------------

* Possibility to get properties of a given quiddity in a new tab.
* Add a fullscreen mode for the properties Page
* Disable the drag&Drop of quiddities when a group is connected.
* Fixed the display of the button group for iPad.
* Fixed bug on select menu for quiddity properties.

Scenic 2.9.11 (2017-01-20)
-------------------------

Fixed various bugs in matrix connections, properties quiddity, SIP quiddity, shmdata connections.

Scenic 2.9.10 (2017-01-03)
-------------------------

Added the connections between groups

Scenic 2.9.9 (2016-11-28)
-------------------------

* Fixed various bugs in touchscreen mode
* Fixed various bugs in the edit inspector fo the property of a quiddity
* Added shortcuts to manage the dialogs box
* When a quiddity is created, it appears directly in the matrix without being prompted for a name
* Fixed various bugs in SIP calls
* Fixed a bug on the exclusive quiddities when put back in the menu

Scenic 2.9.8 (2016-11-11)
-------------------------

* Added the possibility to group in the matrix the sources, destinations, contact or RTP destinations
* Groups can be move and rename
* Added sub menus to the custom Menus
* Added exclusive quiddity to the menus
* The name of a quiddity created from a custom menu matchs with the custom menu entry
* Fixed various bugs for the SIP connections
* Fixed various bugs for the inspector

Scenic 2.9.7 (2016-09-30)
-------------------------

* Possibility to import and export scenic save file
* Add color picker on the property color of a quiddity
* Possibility to grab multiple sources and destinations with the key ctrl and shift
* Display the properties in the inspector of a gtkwin window when this one is focused
* Various Bugfixes

Scenic 2.9.6 (2016-09-13)
-------------------------

* Add the possibility to block a contact form the contact list in the inspector view
* Display the FPS
* Rework the sources/destinations matrix
* Various Bugfixes
* Various UI enhancements

Scenic 2.9.5 (2016-08-31)
-------------------------

* Added the possibility to rename a quiddity
* Sources/Destinations matrix : new icons, improve the visibility of the buttons in touch mode, add custom menus actions ( edit, remove, call, ...) for each quiddities
* Fixed bugs in properties inspector

Scenic 2.9.4 (2016-08-17)
-------------------------

* Added the possibility to authenticate to scenic when trying to connect from outside
* Added enable/disable, create/reset password functionalities for the authentication in Settings
* Added the possibility to create custom menus from a config file
* Added enable/disable functionalities for sources and destination menus
* Added the possibility to rename a saved scenic file
* Fixed bugs in the SIP form
* Fixed bugs in the dropdown menus in the properties of a quiddity
* Fixed a bug that when loading a save file triggers double subscription on the managed quiddities and those created in the node addon

Scenic 2.9.3 (2016-08-05)
-------------------------

* Fixed several bugs for the SIP login connection
* Add checkbox for the STUN/TURN login
* Fixed a bug zith the contact file
* Touch events by default
* SIP quiditty port is displayed in the advanced SIP login
* Fixed a bug that disconnect the thumbnails when creating a new document

Scenic 2.9.2 (2016-07-11)
-------------------------

Fix for possible deadlocks in prop and log callbacks of the node addon

Scenic 2.9.1 (2016-07-11)
-------------------------

* Regression - Removed privateQuiddities from the source + destinations menu
* Fixed a bug that required multiple clicks to change tabs (page was alternating between normal and empty matrix before switching)
* Fixed a bug that prevented the source dummy item from appearing when creating a destination first
* Fixed a bug that created duplicate notifications when a client reconnected multiple times to the server
* Fixed a bad reference to "self" in server messages code
* Restored the full assets copy to the build dir as webpack was not emitting all the used assets.

scenic 2.9.2 (2016-07-11)
-------------------------

Fix for possible deadlocks in prop and log callbacks of the node addon

scenic 2.9.0 (2016-07-06)
-------------------------

* Split SIP login between simple and advanced view
* Combined destinations menu
* New help page
* Better board filtering
* Error messages from switcher are now handled in the UI
* Various Bugfixes
* Various UI enhancements

scenic 2.8.1 (2016-06-22)
-------------------------

Fixed issue with ShmdataView

scenic 2.8.0 (2016-06-21)
-------------------------

* Merged the 3 matrix/table tabs into one!
* Software now properly usable on touch devices
* Inspector is now collapsible
* Revamped the tooltips system to support future contextual help
* In the advanced tab the tree in displayed in the inspector
* Server connection timeout reduced to be more reactive to stalls
* SIP calls are no properly hung up when creating a new document
* Various fixes in the UI

scenic 2.7.3 (2016-06-08)
-------------------------

Hotfix for jQuery ui widgets not being loaded.

scenic 2.7.2 (2016-06-08)
-------------------------

Critical bugfix, 2.7.1 was broken.

scenic 2.7.1 (2016-06-07)
-------------------------

New build process using webpack (see readme for build instructions)

scenic 2.7.0 (2016-06-01)
-------------------------

* New Logo & Icon!!!
* Work on mobile too!
* Automatically reconnects when connection is dropped or server restarted
* Added keyboard shortcuts to the interface (see README)
* Added a new "Advanced" tab to edit all hidden quiddities
* Inspector is now a fixed panel on the right side of the interface
* File loading and SIP login is now done in the new inspector panel
* Added row/column highlight when moving mouse on matrix connections
* Added possibility to block certain tabs/quiddities from the ui with a config file
* Added command line support for providing a defaults file to switcher for automatic quiddity configuration
* Added the possibility to create a new document
* Added save+save as functionality and file overwrite protection
* Properties are now handled through the quiddity information tree
* Matrix is now tighter and support more sources destinations without having to scroll
* Matrix now groups sources by category
* Added userData information tree in quiddities to support scenic-specific configuration (like source/destination order)
* Sources and destinations are sortable (though positions are not saved for the moment)
* Reduced client/server traffic by caching results of can-sink-caps on the client
* SIP is now a "system quiddity" is is no longer created on demand, it is handled by scenic at initialization
* Fixed bug that stopped thumbnails updates when closing large video preview
* Fixed bug that would leave the property sheet visible after loading a file
* Fixed a bug where certain conditions would crash the front end when loading a save file
* Various other fixes...

scenic 2.6.0 (2016-05-03)
-------------------------

* Migrated node addon into this repository
* Converted properties to use the tree system
* Switched to node 5.x
* Added quiddity description in the inspector
* SOAP (switcher-ctrl) is now disabled by default, see --help to enable it
* Fixed bug where loaded document's quiddities didn't get their properties subscribed to
* Fixed SIP connection status in the UI when loading a file
* Fixed video preview resize in the UI

scenic 2.5.1 (2016-04-25)
-------------------------

New node addon, directly integrated as an NPM module

scenic 2.5.0 (2016-??-??)
-------------------------

Added video shmdata previews

scenic 2.4.1 (2016-03-14)
-------------------------

Minor release fixing various UI issues.

scenic 2.4.0 (2015-10-23)
-------------------------

This release is a minor release in the stable 2.0 series.

New Features:

* Fraction editor
* Property hierarchy
* Support of switcher new property system

scenic 2.3.0 (2015-07-23)
-------------------------

This release is a minor release in the stable 2.0 series.

New Features:

* Entire rewrite of the MIDI/Control functionality (property mapping)

Fixes & enhancements:

* Improved session management in the client
* Rate-limited property updates from server
* Property tree improvements

scenic 2.2.4 (2015-06-22)
-------------------------

This release is a minor release in the stable 2.0 series.

Fixes & enhancements:

* Finished converting network protocol between client and server

scenic 2.2.3 (2015-06-18)
-------------------------

This release is a hotfix release in the stable 2.0 series.

Fixes & enhancements:

* Chrome process is now detached from the node process
* Pruning all shmdata in a reader or writer tree now correctly notifies the ui

scenic 2.2.0 (2015-06-15)
-------------------------

This release is a major release in the stable 2.0 series.

New Features

* Shmdata1 (gstreamer1.0) support
* Rewritten core node add-on
* Rewritten network protocol
* Improved localization system
* Visual feedback and notifications (on every connected client) when loading a save file

Fixes & enhancements:

* SIP destinations stay in the destination table across sessions
* Fixed CPU hogging from the CPU meter redraw routine (!)
* Better unit testing
* New make tasks for future packaging

Scenic 2.1.0 (2015-05-12)
------------------------

This release is a major release in the stable 2.0 series.

New Features

* SIP Integration
* Improved UI

Fixed bugs:

* Many, taking over from a previous programmer so I can't list precisely

Scenic 2.0.24 (2014-09-12)
-------------------------

This release is a bugfix release in the stable 2.0 series.

Fixed bugs:

* Fix express path

scenic 2.0.22 (2014-09-11)
-------------------------

This release is a bugfix release in the stable 2.0 series.

Fixed bugs:

* path issues introduced in previous release

scenic 2.0.18 (2014-09-10)
-------------------------

This release is a developer snapshot in the stable 2.0 series.

New features:

* Filter display of shmdatas
* Display CPU and network usage in UI's header
* Show only pertinent information when creating jack sinks/sources

Fixed bugs:

* Source name with fakesink not correct under certain conditions
* Scenic would not start without network
* Various fixes to mouse click handling
* Auto selecting value when editing properties
* Creation of 'undefined' when clicking on source/network
* Hiding (dis)connect when configuring audio destination
* Preview failing when httpsdpdec would take a long time
* Dangling jack inputs after removal
* some sources would not become available under sink tab

Known bugs:

* CPU meter displays +1 CPU which is a total average (first meter)
* There is no indication of the actual network bandwidth consumption only a relative amount capped at 15MBps

scenic 2.0.16 (2014-06-10)
-------------------------

This a bug-fix release developer snapshot in the stable 2.0 series.

Fixed bugs:

* Fix installation paths
* Handle version lookup with -v or --version even if optimist not present
* Prepare for proper handling by debian packaging system

Known bugs:

* Automatic launch of Scenic following the modules installation may fail. Quitting the chrome application and relaunching scenic works

scenic 2.0.12 (2014-04-10)
-------------------------

This a bug-fix release developer snapshot in the stable 2.0 series.

Fixe bugs:

* Fix installation paths
* Handle version lookup with -v or --version even if optimist not present

Known bugs:

* If install script was triggered due to missing node modules, the automatic launch of Scenic following the modules installation fails. Quitting the chro
me application and relaunching scenic works

Scenic 2.0.8 (2014-04-01)
-------------------------

This release is a developer snapshot in the stable 2.0 series.

New features:

* Save and load scenes
* Connect audio sources and sinks to jack server
* Check and install missing dependencies automatically
* Install nodejs modules locally for each user

Known bugs:

* Sometimes the engine crashes when rapidly creating and distroying quiddities, including when loading from saved file

Scenic 0.2.0 (2013-10-15)
-------------------------

This release is a developer snapshot in the stable 0.4 series.

New features:

* Automatically detect audio/video sources
* Control quiddities properties dynamically
* Support MIDI
* MIDI learn
* Audio video previews (local and remote)

Known bugs:

* Sometimes engine hangs after multiple opening/closing of preview window
* No obvious error messages when communication with remote host is not possible

Scenic 0.2.0 (2013-07-05)
-------------------------

This release is a developer snapshot in the stable 0.2 series.

New features:

* Use The functionnality of switcher 2.0 with an interface web

Known bugs:

* scenic sometimes leaves completely at server start
