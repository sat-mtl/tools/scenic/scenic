with import <nixpkgs> {};

let
  mach-nix = import (builtins.fetchGit {
    url = "https://github.com/DavHau/mach-nix";
    ref = "refs/tags/3.3.0";
  }) {};

  pyEnv = mach-nix.mkPython rec {
    requirements = builtins.readFile ./docs/requirements.txt;
  };

  jsPkgs = with nodePackages; [
    npm-check-updates
  ];

in mkShell {
  name = "scenic-dev";

  nativeBuildInput = [
    binutils gcc gnumake openssl pkgconfig # common deps
  ];

  # The packages in the `buildInputs` list will be added to the PATH in our shell
  buildInputs = [
    pyEnv
    git less vim # common tools
    nodejs-16_x # js tools
    jsPkgs
    entr
  ];

  shellHook = ''
    echo "Using ${python.name}"
    echo "Using NodeJS $(node --version)"
  '';
}
